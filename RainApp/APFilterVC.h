//
//  APFilterVC.h
//  Rain
//
//  Created by EgorMac on 18/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APFilterVC : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITextField *countryTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UITextField *birthdayStartTextField;
    IBOutlet UITextField *birthdayEndTextField;
    IBOutlet UILabel *ageLabel;
    IBOutlet UIButton *maleButton;
    IBOutlet UIButton *femaleButton;
    IBOutlet UIButton *searchButton;
    IBOutlet UIImageView *cityUnderline;
    
    UIToolbar *countryToolbar;
    UIToolbar *cityToolbar;
    
    APServerAPI *API;
    
    int gender;
    CGFloat keyboardAnimatedDistance;
    
    BOOL countriesDownloaded;
    BOOL citiesDownloaded;
    
    int chosenCountryID;
    int chosenCityID;
    
    UIActivityIndicatorView *activityIndicator;
    
    NSString *countryID;
    NSString *cityID;
    
    NSArray *ageFromArray;
    NSArray *ageToArray;
}

- (IBAction)filter:(id)sender;
- (IBAction)maleButtonPress:(id)sender;
- (IBAction)femaleButtonPress:(id)sender;

@end
