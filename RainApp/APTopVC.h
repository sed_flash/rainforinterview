//
//  APTopVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "APProfileVC.h"

@interface APTopVC : UNViewController <UITableViewDelegate, UITableViewDataSource>
{
    APServerAPI *API;
    
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITableView *topTableView;
    UIRefreshControl *refreshControl;
    
    NSMutableArray *users;
    
    NSString *selectedUserMail;
}

@property (nonatomic, assign) BOOL isNeedDisplayFilterResults;
@property (nonatomic, assign) BOOL isNeedDisplaySearchResults;
@property (nonatomic, strong) APServerAPI *API;

@end
