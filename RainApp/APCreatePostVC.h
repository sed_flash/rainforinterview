//
//  APCreatePostVC.h
//  Rain
//
//  Created by EgorMac on 15/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "HPGrowingTextView.h"
#import "APTagPeopleVC.h"

@interface APCreatePostVC : UIViewController <HPGrowingTextViewDelegate, UIAlertViewDelegate, APProgressBarDelegate, TagPeopleDelegate>
{
    IBOutlet UIImageView *photoImageView;
    IBOutlet UIImageView *videoImageView;
    IBOutlet HPGrowingTextView *commentTextView;
    IBOutlet UIBarButtonItem *doneButton;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *tagPeopleLabel;
    IBOutlet UITextView *taggedPeopleTextView;
    IBOutlet UIProgressView *progressView;
    UIBarButtonItem *activityButton;
    UIActivityIndicatorView *activityIndicator;
    
    CGFloat keyboardAnimatedDistance;
    
    BOOL isVideo;
    NSURL *videoURL;
    UIImage *photoImage;
    
    APServerAPI *API;
    
    NSMutableArray *taggedPeople;
}

@property (nonatomic, strong) NSMutableArray *taggedPeople;
@property (nonatomic, strong) UIImage *photoImage;
@property (nonatomic) BOOL isVideo;
@property (nonatomic, strong) NSURL *videoURL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintToAdjust;

- (IBAction)sendPost:(id)sender;

@end
