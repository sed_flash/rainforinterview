//
//  APCommentsNotificationCell.h
//  Rain
//
//  Created by EgorMac on 14/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "NSDate+TimeAgo.h"


@interface APCommentsNotificationCell : UITableViewCell
{
    IBOutlet UIImageView *photoImageView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *timeAgoLabel;
    IBOutlet UIButton *userButton;
    
    IBOutlet UIImageView *commentedPhotoView;
    
    NSDictionary *notification;
}

@property (nonatomic, strong) NSDictionary *notification;
@property (nonatomic, strong) IBOutlet UIButton *userButton;


@end
