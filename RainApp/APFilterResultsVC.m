//
//  APFilterResultsVC.m
//  Rain
//
//  Created by EgorMac on 19/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFilterResultsVC.h"

@interface APFilterResultsVC ()

@end

@implementation APFilterResultsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDataSource
{
    foundUsers = [[NSDictionary alloc] init];
    foundUsersIDs = [[NSArray alloc] init];
    
    NSArray *users = [[NSUserDefaults standardUserDefaults] objectForKey:@"filteredUsers"];
    NSMutableArray *usersIDs = [[NSMutableArray alloc] init];
    NSMutableDictionary *usersDictionary = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *user in users) {
        NSString *userID = [[user objectForKey:@"id"] stringValue];
        [usersIDs addObject:userID];
        [usersDictionary setObject:user forKey:userID];
    }
    
    foundUsersIDs = [usersIDs copy];
    foundUsers = [usersDictionary copy];
    
    users = nil;
    usersIDs = nil;
    usersDictionary = nil;
    
    NSLog(@"foundUsersIDs - \n%@", foundUsersIDs);
    NSLog(@"foundUsers - \n%@", foundUsers);
    
    if ([foundUsersIDs count] == 0)
    {
        [friendsCollectionView setHidden:YES];
        [noResultsLabel setHidden:NO];
        [noResultsLabel setText:NSLocalizedString(@"noResults", nil)];
        [filterButton setEnabled:NO];
    } else {
        [friendsCollectionView setHidden:NO];
        [noResultsLabel setHidden:YES];
        [filterButton setEnabled:YES];
    }
    
    [friendsCollectionView reloadData];
}

@end
