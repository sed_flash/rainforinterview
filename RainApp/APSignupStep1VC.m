//
//  APSignupStep1VC.m
//  Rain
//
//  Created by EgorMac on 17/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APSignupStep1VC.h"
#import "APServerAPI.h"

@interface APSignupStep1VC ()

@end

@implementation APSignupStep1VC {
    BOOL passIsNotCorrect;
}

@synthesize API;

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    if ([[UIScreen mainScreen] bounds].size.height <= 480.0f) {
        [acceptButtonConstraint setConstant:8.0f];
        [continueButtonConstraint setConstant:14.0f];
        [rainLogoHeight setConstant:105.7f];
        [rainLogoWidth setConstant:121.8f];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    passIsNotCorrect = NO;
    // Do any additional setup after loading the view
    
    [emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [nicknameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [confirmPasswordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [self setupUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signupSuccess)
                                                 name:@"SignupSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signupFailed)
                                                 name:@"SignupFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper Methods

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
}

- (void)configureTextFields
{
    [emailTextField setPlaceholder:NSLocalizedString(@"email", nil)];
    [nicknameTextField setPlaceholder:NSLocalizedString(@"nickname", nil)];
    [passwordTextField setPlaceholder:NSLocalizedString(@"password", nil)];
    [confirmPasswordTextField setPlaceholder:NSLocalizedString(@"confirmPassword", nil)];
    
    for (UIView *subview in self.view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]]) {
            UIView *textFieldLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
            [(UITextField *)subview setLeftView:textFieldLeftView];
            [(UITextField *)subview setLeftViewMode:UITextFieldViewModeAlways];
        }
    }
}

- (void)setupUIElements
{
    [self configureNavigationBar];
    [self configureTextFields];
    
    [registrationLabel setText:NSLocalizedString(@"registration", nil)];
    [acceptLabel setText:NSLocalizedString(@"IAcceptUserAgreement", nil)];
    [continueButton setTitle:NSLocalizedString(@"continue", nil) forState:UIControlStateNormal];
    [continueButton setTitle:NSLocalizedString(@"continue", nil) forState:UIControlStateHighlighted];
    [[continueButton layer] setCornerRadius:3.0f];
    [continueButton setEnabled:NO];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat viewHeight, bottomOfTextField, topOfKeyboard;
    
    viewHeight = viewRect.size.height;
    bottomOfTextField = textFieldRect.origin.y + textFieldRect.size.height;
    topOfKeyboard = viewHeight - PORTRAIT_KEYBOARD_HEIGHT - 10; // 10 point padding
    
    if (bottomOfTextField >= topOfKeyboard) keyboardAnimatedDistance = bottomOfTextField - topOfKeyboard;
    else keyboardAnimatedDistance = 0.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{   
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldIsNotCorrect
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    if ([passwordTextField.text length] == [confirmPasswordTextField.text length] &&
        [passwordTextField.text length] != 0 && [passwordTextField.text length] < 6) {
        
        passwordTextField.text = @"";
        confirmPasswordTextField.text = @"";
    
        passIsNotCorrect = YES;
        return YES;
    }
    
    return result;
}

#pragma mark - IBAction methods

- (IBAction)acceptAgreement:(id)sender
{
    [acceptButton setSelected:!acceptButton.selected];
    [continueButton setEnabled:acceptButton.selected];
}

- (IBAction)signup:(id)sender
{
    if ([self textFieldIsNotCorrect] ||
        ![[passwordTextField text] isEqualToString:[confirmPasswordTextField text]]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        NSString *email = [emailTextField text];
        NSString *nickname = [nicknameTextField text];
        NSString *password = [passwordTextField text];
        
        NSMutableDictionary *user = [[NSMutableDictionary alloc] init];
        [user setObject:email forKey:@"email"];
        [user setObject:nickname forKey:@"nickname"];
        [user setObject:password forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"userData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [API signup];
    }
}

#pragma mark - Helper methods

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        if (passIsNotCorrect) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                                message:NSLocalizedString(@"notEnoughtSymbols", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            passIsNotCorrect = NO;
        } else  {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                            message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = @"";
        
        for (NSString *currentMessage in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"])
        {
            errorMessage = [errorMessage stringByAppendingFormat:@"\n%@", currentMessage];
        }
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)signupSuccess
{
    /*
    
    NSDictionary *dict =[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    
    
    
    NSString *firstName =[dict valueForKey:@"first_name"];
    NSString *lastName = [dict valueForKey:@"last_name"];
    
    NSString *birthday = [dict valueForKey:@"birthday"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MM yyyy"];
    NSDate *birthDate = [dateFormatter dateFromString:birthday];
    NSLog(@"birthDate is %@", birthDate);
    
    NSDateFormatter *enDateFormatter = [[NSDateFormatter alloc] init];
    [enDateFormatter setDateFormat:@"dd MMMM yyyy"];
    [enDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
    birthday = [enDateFormatter stringFromDate:birthDate];
    
    NSMutableDictionary *filledProfile = [[NSMutableDictionary alloc] init];
    [filledProfile setObject:firstName forKey:@"firstName"];
    [filledProfile setObject:lastName forKey:@"lastName"];
    [filledProfile setObject:birthday forKey:@"birthday"];
    [filledProfile setObject:[NSNumber numberWithInt:dict] forKey:@"gender"];
    
    
//    [filledProfile setObject:[NSNumber numberWithInt:chosenCountryID] forKey:@"countryID"];
//    [filledProfile setObject:[NSNumber numberWithInt:chosenCityID] forKey:@"cityID"];
    
    [[NSUserDefaults standardUserDefaults] setObject:filledProfile forKey:@"filledProfileData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    

    [API fillProfile]; */
    
    [self performSegueWithIdentifier:@"goToSignUpStep2" sender:self];
    
}

- (void)signupFailed
{
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
