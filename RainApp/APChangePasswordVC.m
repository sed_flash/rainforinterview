//
//  APChangePasswordVC.m
//  Rain
//
//  Created by EgorMac on 29/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APChangePasswordVC.h"

@interface APChangePasswordVC ()

@end

@implementation APChangePasswordVC

static NSString *successAlert = @"success";
static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    API = [[APServerAPI alloc] init];
    newPassword = nil;
    
    [self setupUIElements];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(passwordChangedSuccess)
                                                 name:@"ChangePasswordSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(passwordChangedFailed)
                                                 name:@"ChangePasswordFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)passwordChangedSuccess
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:successAlert];
}

- (void)passwordChangedFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:successAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"changePassword(title)", nil)
                                                        message:NSLocalizedString(@"passwordSuccessfullyChanged", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert setTag:3];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 3) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [[self navigationController] popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - Helper Methods

- (void)configureTextFields
{
    [passwordTextField setPlaceholder:NSLocalizedString(@"newPassword", nil)];
    [confirmPasswordTextField setPlaceholder:NSLocalizedString(@"confirmNewPassword", nil)];
    UIView *passwordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    UIView *confirmPasswordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    [passwordTextField setLeftView:passwordPaddingView];
    [passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [confirmPasswordTextField setLeftView:confirmPasswordPaddingView];
    [confirmPasswordTextField setLeftViewMode:UITextFieldViewModeAlways];
}

- (void)setupUIElements
{
    [self configureTextFields];
    [savePasswordButton setTitle:NSLocalizedString(@"savePassword", nil) forState:UIControlStateNormal];
    [savePasswordButton setTitle:NSLocalizedString(@"savePassword", nil) forState:UIControlStateHighlighted];
    [[savePasswordButton layer] setCornerRadius:3.0f];
    [[self navigationItem] setTitle:NSLocalizedString(@"changePassword(title)", nil)];
}

#pragma mark - IBAction Methods

- (IBAction)savePassword:(id)sender
{
    if ([[passwordTextField text] isEqualToString:@""] ||
        [[confirmPasswordTextField text] isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"fieldsAreNotValid", nil)
                                                            message:NSLocalizedString(@"fieldsAreEmpty", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        if ([[passwordTextField text] isEqualToString:[confirmPasswordTextField text]]) {
            NSLog(@"save password");
            [API changePasswordTo:passwordTextField.text andConfirmation:confirmPasswordTextField.text];
            [self showActivityIndicator];
            newPassword = passwordTextField.text;
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"fieldsAreNotValid", nil)
                                                                message:NSLocalizedString(@"passwordsAreNotEqual", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor colorWithRed:93.0/255.0
                                       green:124.0/255.0
                                        blue:173.0/255.0
                                       alpha:1.0]];
    [activity setTag:5];
    [activity setCenter:self.view.center];
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
