//
//  APPushNotificationCell.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APPushNotificationCell.h"

@implementation APPushNotificationCell

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    API = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(turnPushNotificationsSuccess)
                                                 name:@"TurnPushNotificationsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(turnPushNotificationsFailed)
                                                 name:@"TurnPushNotificationsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setPushOn)
                                                 name:@"SetPushOn"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setPushOff)
                                                 name:@"SetPushOff"
                                               object:nil];
    [pushLabel setText:NSLocalizedString(@"pushNotifications", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchPushNotifications:(id)sender
{
    NSDictionary *settings = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSettings"];
    
    int hideProfile = (int)[[settings objectForKey:@"hide_profile"] integerValue];
    
    if ([pushSwitch isOn]) {
        
        // Включить Push-уведомления
        NSLog(@"Включить Push-уведомления");
        [API sendProfileAccess:!hideProfile andPushNotifications:YES];
    } else {
        
        // Выключить Push-уведомления
        NSLog(@"Выключить Push-уведомления");
        [API sendProfileAccess:!hideProfile andPushNotifications:NO];
    }
}

- (void)setPushOn
{
    [pushSwitch setOn:YES animated:YES];
}

- (void)setPushOff
{
    [pushSwitch setOn:NO animated:YES];
}

- (void)turnPushNotificationsSuccess
{
    NSLog(@"turn push notifications success");
}

- (void)turnPushNotificationsFailed
{
    NSLog(@"turn push notifications failed");
    [pushSwitch setOn:!pushSwitch.isOn animated:YES];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
    [pushSwitch setOn:!pushSwitch.isOn animated:YES];
}

- (void)serverError
{
    NSLog(@"server error");
    [self showAlertWithIdentifier:serverError];
    [pushSwitch setOn:!pushSwitch.isOn animated:YES];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}


@end
