//
//  APTagPeopleVC.m
//  Rain
//
//  Created by EgorMac on 16/06/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import "APTagPeopleVC.h"
#import "APCreatePostVC.h"

@interface APTagPeopleVC ()

@end

@implementation APTagPeopleVC
@synthesize API;
@synthesize tagPeopleDelegate;
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersSuccess)
                                                 name:@"FollowersSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersFailed)
                                                 name:@"FollowersFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    
    [self showActivityIndicator];
    
    NSString *userMail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
    [API getFollowersForUserWithEmail:userMail];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setDataSource
{
    people = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followers"] copy];
    NSMutableArray *peopleMutable = [[NSMutableArray alloc] init];
    for (NSDictionary *friend in people)
    {
        NSString *fullName = [NSString stringWithFormat:@"%@ %@",
                              [friend objectForKey:@"first_name"],
                              [friend objectForKey:@"last_name"]];
        NSMutableDictionary *mutableFriend = [friend mutableCopy];
        [mutableFriend setObject:fullName forKey:@"full_name"];
        [peopleMutable addObject:mutableFriend];
    }
    
    people = [peopleMutable copy];
    peopleMutable = nil;
    [peopleTableView reloadData];
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
        
    } else {
        return [people count];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"PeopleCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    NSDictionary *friend;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        friend = [searchResults objectAtIndex:indexPath.row];
    } else {
        friend = [people objectAtIndex:indexPath.row];
    }
    
    NSString *followerName = [friend objectForKey:@"full_name"];
    [[cell textLabel] setText:followerName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *userID;
    NSString *name;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        userID = [[[searchResults objectAtIndex:indexPath.row] objectForKey:@"id"] stringValue];
        name = [[searchResults objectAtIndex:indexPath.row] objectForKey:@"full_name"];
    }
    else
    {
        userID = [[[people objectAtIndex:indexPath.row] objectForKey:@"id"] stringValue];
        name = [[people objectAtIndex:indexPath.row] objectForKey:@"full_name"];
    }
    
    if([self.tagPeopleDelegate respondsToSelector:@selector(tagPeopleVCDissmissed:)])
    {
        NSDictionary *friend = @{@"id":userID, @"name":name};
        
        [self.tagPeopleDelegate tagPeopleVCDissmissed:friend];
    }
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - Search

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.full_name contains[cd] %@",
                                    searchText];
    
    searchResults = [people filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - Activity

- (void)showActivityIndicator
{
    [peopleTableView setHidden:YES];
    activityIndicator = [[UIActivityIndicatorView alloc] init];
    [activityIndicator setColor:[UIColor colorWithRed:93.0/255.0
                                                green:124.0/255.0
                                                 blue:173.0/255.0
                                                alpha:1.0]];
    CGPoint center = self.view.center;
    center.y -= (self.tabBarController.tabBar.frame.size.height + self.navigationController.navigationBar.frame.size.height);
    [activityIndicator setCenter:center];
    [[self view] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)removeActivityIndicator
{
    [peopleTableView setHidden:NO];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

#pragma mark - Success and Failed methods

- (void)followersSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)followersFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)configureUI
{
    [cancelButton setTitle:NSLocalizedString(@"cancel", nil)];
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"
                                                                                               size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"tagPeople", nil)];
}

#pragma mark - IBAction Methods

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
