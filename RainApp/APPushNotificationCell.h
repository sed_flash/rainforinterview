//
//  APPushNotificationCell.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APPushNotificationCell : UITableViewCell
{
    IBOutlet UILabel *pushLabel;
    IBOutlet UISwitch *pushSwitch;
    APServerAPI *API;
}

- (IBAction)switchPushNotifications:(id)sender;

@end
