//
//  APPostVC.m
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APPostVC.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImage+APColor.h"
#import "UIColor+CustomColor.h"
#import "AFNetworking/AFHTTPRequestOperation.h"
#import "APVideoModel.h"

#define kCell @"CommentCell"

@interface APPostVC ()

@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@end

@implementation APPostVC

@synthesize postID, currentPost;
@synthesize moviePlayer;
@synthesize author;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tb=self.commentsTableView;
    
    [self.commentsTableView setDelegate:self];
    [self.commentsTableView setDataSource:self];
    // Do any additional setup after loading the view.
    
    user = [[NSUserDefaults standardUserDefaults] objectForKey:@"foundedUser"];
    NSLog(@"user is %@", user);
    
    NSDictionary *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *tokenEmail = [token objectForKey:@"email"];
    
    if ([[user objectForKey:@"email"] isEqualToString:tokenEmail])
    {
        [repostButton setEnabled:NO];
        [upperRepostButton setEnabled:NO];
    }
    else
    {
        [repostButton setEnabled:YES];
        [upperRepostButton setEnabled:YES];
    }
    
    userPosts = [user objectForKey:@"posts"];
    commentsIDs = [[NSArray alloc] init];
    comments = [[NSDictionary alloc] init];
    startingLikesCount = 0;
    startingRepostsCount = 0;
    
    API = [[APServerAPI alloc] init];
    deletePostLink = [[NSString alloc] init];
    deleteCommentLink = [[NSString alloc] init];
    selectedAuthor = [[NSString alloc] init];
    
    [_commentsTableView registerNib:[UINib nibWithNibName:@"APCustumCommentCellXib" bundle:nil] forCellReuseIdentifier:kCell];
    
    commentOwner = NO;
//    [self registerObservers];
    [self setUpUIElements];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
}

- (void)registerObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(commentsSuccess)
                                                 name:@"CommentsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(commentsFailed)
                                                 name:@"CommentsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(likeSuccess)
                                                 name:@"LikeSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(likeFailed)
                                                 name:@"LikeFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(repostSuccess)
                                                 name:@"RepostSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(repostFailed)
                                                 name:@"RepostFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deletePostSuccess)
                                                 name:@"DeletePostSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deletePostFailed)
                                                 name:@"DeletePostFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteCommentSuccess)
                                                 name:@"DeleteCommentSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteCommentFailed)
                                                 name:@"DeleteCommentFailed"
                                               object:API];
    [_commentsTableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if(object == self.commentsTableView && [keyPath isEqualToString:@"contentSize"]) {
        [self.view setNeedsUpdateConstraints];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
    [self configureNavigationBar];
    [self registerObservers];
    
//    if (commentsIDs.count > 0)
//        [_commentsTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    NSLog(@"scroll view frame size: %f", scrollView.frame.size.height);
//    NSLog(@"scroll view content size: %f", scrollView.contentSize.height);

    CGSize newScrollViewSize = scrollView.contentSize;
    newScrollViewSize.height = 50 + photoImageView.frame.size.height + 40 + descriptionView.frame.size.height+20;
    
    [scrollView setContentSize:newScrollViewSize];
    
//    [_commentsTableView reloadData];
//    NSLog(@"new scroll view content size: %f", scrollView.contentSize.height);
//    NSLog(@"new scroll view frame size: %f", scrollView.frame.size.height);

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.commentsTableView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    NSLog(@"updateViewConstraints method");
    
    CGFloat descHeight;
    if ([descriptionView.text isEqualToString:@""]) {
        descHeight = 0.0f;
    } else
    {
        descHeight = descriptionView.contentSize.height;
    }
    
    [descriptionViewConstraint setConstant:descHeight];
    [tableViewConstraint setConstant:self.commentsTableView.contentSize.height];
    CGSize newScrollViewSize = scrollView.contentSize;
    newScrollViewSize.height = 50 + photoImageView.frame.size.height + 40 + descriptionView.frame.size.height+20 + self.commentsTableView.frame.size.height + 20 + 40;
    [scrollView setContentSize:newScrollViewSize];
    
    [likeButtonLeadingConstraint setConstant:self.view.frame.size.width/16];
    [commentButtonLeadingConstraint setConstant:self.view.frame.size.width/7.2];
    [repostButtonLeadingConstraint setConstant:self.view.frame.size.width/6.2];
    [moreButtonTrailingConstraint setConstant:self.view.frame.size.width/16];
    
    [panelLikeLeading setConstant:self.view.frame.size.width/16];
    [panelCommentsLeading setConstant:self.view.frame.size.width/5];
    [panelViewsLeading setConstant:self.view.frame.size.width/5];
    [panelRepostsLeading setConstant:self.view.frame.size.width/5];
    
    [self performSelector:@selector(showScrollView) withObject:self afterDelay:1.0];
}

- (void)showScrollView
{
//    NSLog(@"scroll view frame height: %f", scrollView.frame.size.height);
//    NSLog(@"scroll view content size height: %f", scrollView.contentSize.height);
    if (scrollView.contentSize.height == 0.0f ||
        scrollView.contentSize.height < 600.0f)
    {
        CGSize newScrollViewSize = scrollView.contentSize;
        newScrollViewSize.height = 50 + photoImageView.frame.size.height + 40 + descriptionView.frame.size.height+20 + self.commentsTableView.frame.size.height + 20 + 40;
        [scrollView setContentSize:newScrollViewSize];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Video Methods

- (void)setVideo
{
    APVideoModel *videoModel = [[APVideoModel alloc] init];
    
    NSURL *videoURL = [[NSURL alloc] initWithString:[currentPost objectForKey:@"path"]];

    NSString *path = [videoModel getPathToFileWithURL:videoURL];
    if ([videoModel checkIfVideoIsHasOnDevice:path]) {
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
    } else {
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
        [videoModel saveVideoToDeviceWithURL:videoURL andPathToSave:path];
    }
    
    CGRect videoFrame = photoImageView.frame;
    videoFrame.size.width = self.view.frame.size.width;
    videoFrame.size.height = self.view.frame.size.width;
    [moviePlayer.view setFrame:videoFrame];
    [scrollView addSubview:moviePlayer.view];
    
    [moviePlayer setFullscreen:YES animated:YES];
    [moviePlayer setAllowsAirPlay:YES];
    [moviePlayer setShouldAutoplay:NO];
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    [moviePlayer setMovieSourceType:MPMovieSourceTypeFile];
    
    [moviePlayer prepareToPlay];
}

#pragma mark - Success and Fail methods

- (void)commentsSuccess
{
    NSLog(@"comments success");
    
    
    [self removeCommentsActivity];
    
    [self setDataSource];
}

- (void)commentsFailed
{
    [self showErrorLabel];
    NSLog(@"comments failed");
}

- (void)likeSuccess
{
    NSLog(@"like success");
    
   // [self removeLikesActivity];
    NSInteger currentLikesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"likes"] integerValue];
    
//    if ([likesImageView isHighlighted]) {
//        
//        NSLog(@"likes highlighted!");
//        [likesImageView setHighlighted:NO];
//    } else {
//        NSLog(@"likes not highlighted!");
//        [likesImageView setHighlighted:YES];
//    }
    
    startingLikesCount = currentLikesCount;

    [likesCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentLikesCount]];
    [likeButton setEnabled:YES];
}

- (void)likeFailed
{
    [self removeLikesActivity];
    [self.commentsTableView setHidden:YES];
    [self showErrorLabel];
    [likeButton setEnabled:YES];
}

- (void)repostSuccess
{
    NSLog(@"repost success");
    
    [self removeRepostsActivity];
    [repostsCountLabel setHidden:NO];
    [repostsImageView setHidden:NO];
    
    NSInteger currentRepostsCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"reposts"] integerValue];
    
    if ([repostsImageView isHighlighted]) {
        
        NSLog(@"reposts highlighted!");
        [repostsImageView setHighlighted:NO];
    } else {
        NSLog(@"reposts not highlighted!");
        [repostsImageView setHighlighted:YES];
    }
    
    startingRepostsCount = currentRepostsCount;
    
    [repostsCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentRepostsCount]];
    [repostButton setEnabled:YES];
}

- (void)repostFailed
{
    [self removeRepostsActivity];
    [self.commentsTableView setHidden:YES];
    [self showErrorLabel];
    [repostButton setEnabled:YES];
}

- (void)deletePostSuccess
{
    postAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:NSLocalizedString(@"postSuccessfullyDeleted", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
    [postAlertView show];
    [self removeDeletingActivity];
}

- (void)deletePostFailed
{
    [self showErrorLabel];
    [self removeDeletingActivity];
}

- (void)deleteCommentSuccess
{
    commentAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                               message:NSLocalizedString(@"commentSuccessfullyDeleted", nil)
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                     otherButtonTitles:nil];
    [commentAlertView show];
    [self removeDeletingActivity];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentsCountDecreased"
                                                        object:self];
}

- (void)deleteCommentFailed
{
    [self showErrorLabel];
    [self removeDeletingActivity];
}

- (void)alertViewCancel:(UIAlertView *)alertView
{
    NSLog(@"cancel button");
    
    if (alertView == postAlertView)
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else if (alertView == commentAlertView)
    {
        [self getComments];
    }
}

- (void)networkError
{
    if ([statsView viewWithTag:11]) {
        [self removeLikesActivity];
    }
    
    if ([statsView viewWithTag:12]) {
        [self removeRepostsActivity];
    }
    
    [self showErrorLabel];
    [likeButton setEnabled:YES];
    NSLog(@"network error!");
}

- (void)serverError
{
    if ([statsView viewWithTag:11]) {
        [self removeLikesActivity];
    }
    
    if ([statsView viewWithTag:12]) {
        [self removeRepostsActivity];
    }
    
    [self showErrorLabel];
    [likeButton setEnabled:YES];
    NSLog(@"server error!");
}

- (void)showCommentsActivity
{
    if ([scrollView viewWithTag:10]) {
        UILabel *errorLabel = (UILabel *)[scrollView viewWithTag:10];
        [errorLabel removeFromSuperview];
    }
    
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setFrame:CGRectMake(self.view.center.x,
                                  self.commentsTableView.frame.origin.y+10,
                                  activity.frame.size.width,
                                  activity.frame.size.height)];
    [activity setColor:[UIColor lightGrayColor]];
    [activity setTag:7];
    [scrollView addSubview:activity];
    [activity startAnimating];
    
    [self.commentsTableView setHidden:YES];
}

- (void)removeCommentsActivity
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[scrollView viewWithTag:7];
    [activity stopAnimating];
    [activity removeFromSuperview];
    [self.commentsTableView setHidden:NO];
}

- (void)showLikesActivity
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setFrame:CGRectMake(0,
                                  0,
                                  50,
                                  50)];
    
    [activity setCenter:CGPointMake(likesImageView.center.x + 10, likesImageView.center.y)];
    
    [activity setColor:[UIColor lightGrayColor]];
    [activity setTag:11];
    [statsView addSubview:activity];
    [activity startAnimating];
    [activity setHidden:NO];
}

- (void)removeLikesActivity
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[statsView viewWithTag:11];
    [activity stopAnimating];
    [activity removeFromSuperview];
    [likesCountLabel setHidden:NO];
    [likesImageView setHidden:NO];
}

- (void)showRepostsActivity
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setFrame:CGRectMake(0,
                                  0,
                                  50,
                                  50)];
    [activity setCenter:CGPointMake(repostsImageView.center.x + 10, repostsImageView.center.y)];
    [activity setColor:[UIColor lightGrayColor]];
    [activity setTag:12];
    [statsView addSubview:activity];
    [activity startAnimating];
    [activity setHidden:NO];
}

- (void)removeRepostsActivity
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[statsView viewWithTag:12];
    [activity stopAnimating];
    [activity removeFromSuperview];
}

- (void)showErrorLabel
{
    NSLog(@"show error label");
    
    // create and show label with ui text instead activity indicator
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[scrollView viewWithTag:7];
    CGRect labelRect = activity.frame;
    labelRect.origin.x = 0;
    labelRect.origin.y = self.commentsTableView.frame.origin.y;
    labelRect.size.width = scrollView.frame.size.width;
    labelRect.size.height = 21.0f;
    [activity stopAnimating];
    [activity removeFromSuperview];
    
    UILabel *errorLabel = [[UILabel alloc] initWithFrame:labelRect];
    [errorLabel setTextAlignment:NSTextAlignmentCenter];
    [errorLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    [errorLabel setTextColor:[UIColor lightGrayColor]];
    [errorLabel setTag:10];
    [scrollView addSubview:errorLabel];
    [errorLabel setText:NSLocalizedString(@"errorLoadingData", nil)];
    
    NSLog(@"error label is %@", errorLabel);
}

- (void)setCurrentPost:(NSDictionary *)newCurrentPost
{
    if (currentPost != newCurrentPost) {
        NSLog(@"set currentPost method");
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
        [activity setCenter:[photoImageView center]];
        [activity setColor:[UIColor clearColor]];
        [self.view addSubview:activity];
        [activity startAnimating];
        [activity stopAnimating];
        [activity removeFromSuperview];
        
        currentPost = newCurrentPost;
        
        NSLog(@"current post is %@", currentPost);
        
        NSString *photoString;
        if ([[currentPost objectForKey:@"path"] hasPrefix:@"http://rain-app.com/media/video"])
        {
            [[self navigationItem] setTitle:NSLocalizedString(@"video", nil)];
            photoString = [currentPost objectForKey:@"preview_image"];
            [self setVideo];
        } else {
            
            NSLog(@"set photo...");
            
            [[self navigationItem] setTitle:NSLocalizedString(@"photo", nil)];
            photoString = [currentPost objectForKey:@"full_image"];
        }
        
        __weak typeof(self) weakSelf = self;
        __weak NSString *weakPhotoString = photoString;
        __weak UITableView *weakCommentsTableView = self.commentsTableView;
        
        progressView.hidden = NO;
        
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:photoString]
                          placeholderImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                   options:0
                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                      NSLog(@"%ld bytes from %ld received", (long)receivedSize, (long)expectedSize);
                                      [progressView setProgress:(float)receivedSize/expectedSize];
                                  }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     [photoImageView setImage:image];
                                     [progressView setHidden:YES];
                                     
                                     // get comments
                                     [weakSelf getComments];
                                     [weakCommentsTableView reloadData];
                                     [weakCommentsTableView setFrame:CGRectMake(weakCommentsTableView.frame.origin.x, weakCommentsTableView.frame.origin.y,
                                                                                weakCommentsTableView.frame.size.width, weakCommentsTableView.contentSize.height)];
                                     
                                     if (error) {
                                         NSLog(@"fail for %@\n with error:\n%@", weakPhotoString, error.localizedDescription);
                                         [weakSelf showErrorLabel];
                                     }

                                 }];
        NSDate *createdAt;
        
        if ([[currentPost objectForKey:@"created_at"] isKindOfClass:[NSString class]]) {
            NSString *createdAtString = [currentPost objectForKey:@"created_at"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
            createdAt = [dateFormatter dateFromString:createdAtString];
        } else {
            createdAt = [currentPost objectForKey:@"created_at"];
        }
        NSString *timeAgo = [createdAt timeAgo];
        [timeLabel setText:timeAgo];
        
        NSData *descriptionData = [[currentPost objectForKey:@"description"] dataUsingEncoding:NSUTF8StringEncoding];
        NSString *descriptionString = [[NSString alloc] initWithData:descriptionData
                                                            encoding:NSNonLossyASCIIStringEncoding];
        [descriptionView setText:descriptionString];
        
        [repostsCountLabel setText:[[currentPost objectForKey:@"reposts"] stringValue]];
        
        // post placeholder stats
//        [likesCountLabel setText:@"0"];
//        [commentsCountLabel setText:@"0"];
//        [viewsCountLabel setText:@"1"];
    }
}

- (void)setAuthor:(NSDictionary *)newAuthor
{
    NSLog(@"set author");
    
    author = newAuthor;
    
    NSLog(@"author - %@", author);
    
    if ([self authorIsEmpty]) {
//        [avatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [UIImageView apa_useMaskToImageView:avatarImageView
                                   forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [avatarImageView setHidden:YES];
        [dropImageView setHidden:YES];
        [nameLabel setText:@""];
        [timeLabel setText:@""];
    } else {
        NSString *avatarPath = [author objectForKey:@"avatar"];
        NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarPath]];
        
        __weak NSString *weakAvatarPath = avatarPath;
        __weak UIImageView *weakAvatarImageView = avatarImageView;
        
        [avatarImageView setImageWithURLRequest:photoRequest
                               placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]
                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
//             [weakAvatarImageView setImage:image];
             [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                        forImage:image];
         }
         
                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                            NSLog(@"fail for %@", weakAvatarPath);
//                                            [weakAvatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                            [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                                                       forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                        }];
        
        
        [nameLabel setText:[author objectForKey:@"nick"]];
        
        if ([[author objectForKey:@"email"] isEqualToString:[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"]]) {
            [repostButton setEnabled:NO];
            [upperRepostButton setEnabled:NO];
        } else {
            [repostButton setEnabled:YES];
            [upperRepostButton setEnabled:YES];
        }
    }
}

- (BOOL)authorIsEmpty
{
    if ([[author objectForKey:@"avatar"] isEqualToString:@""] ||
        [[author objectForKey:@"avatar_small"] isEqualToString:@""] ||
        [[author objectForKey:@"email"] isEqualToString:@""] ||
        [[[author objectForKey:@"id"] stringValue] isEqualToString:@""] ||
        [[author objectForKey:@"nick"] isEqualToString:@""])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)getComments
{
    NSLog(@"get comments");
    [self showCommentsActivity];
    
    [API getCommentsForPostID:[[currentPost objectForKey:@"id"] stringValue]];
}

- (void)setDataSource
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"comments"]) {
        NSArray *tempCommentsArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"comments"];
        NSMutableArray *mutCommentsIDs = [[NSMutableArray alloc] init];
        NSMutableDictionary *mutComments = [[NSMutableDictionary alloc] init];
        
        for (NSDictionary *currentComment in tempCommentsArray) {
            
            int commentIDInteger = arc4random() % 10000;
            NSString *commentID = [NSString stringWithFormat:@"%d", commentIDInteger];
            
            NSString *currentAuthor = [[currentComment objectForKey:@"user"] objectForKey:@"nick"];
            NSString *email = [[currentComment objectForKey:@"user"] objectForKey:@"email"];
            NSString *avatarString = [[currentComment objectForKey:@"user"] objectForKey:@"avatar"];
            NSLog(@"current comment is \n%@", currentComment);
            NSLog(@"avatar string is %@", avatarString);
            NSString *text = [NSString stringWithFormat:@"%@: %@", currentAuthor, [currentComment objectForKey:@"text"]];
            NSString *createdAt = [currentComment objectForKey:@"created_at"];
            
            NSMutableDictionary *tempCommentDictionary = [[NSMutableDictionary alloc] init];
            [tempCommentDictionary setObject:commentID forKey:@"commentID"];
            [tempCommentDictionary setObject:createdAt forKey:@"createdAt"];
            [tempCommentDictionary setObject:text forKey:@"text"];
            [tempCommentDictionary setObject:currentAuthor forKey:@"author"];
            [tempCommentDictionary setObject:email forKey:@"email"];
            [tempCommentDictionary setObject:avatarString forKey:@"avatar"];
            if ([currentComment objectForKey:@"link_delete"] != nil) {
                NSString *deleteLink = [currentComment objectForKey:@"link_delete"];
                [tempCommentDictionary setObject:deleteLink forKey:@"link_delete"];
            }
            
            [mutCommentsIDs addObject:commentID];
            [mutComments setObject:tempCommentDictionary forKey:commentID];
        }
        
        commentsIDs = [mutCommentsIDs copy];
        comments = [mutComments copy];
        [self.commentsTableView setHidden:NO];
//        [self.commentsTableView reloadData];
        
        CGSize newScrollViewSize = scrollView.contentSize;
        newScrollViewSize.height = 50 + photoImageView.frame.size.height + 40 + descriptionView.frame.size.height+20 + self.commentsTableView.frame.size.height + 20;
        
        [scrollView setContentSize:newScrollViewSize];
        //[self updateViewConstraints];
        
        [self updatePostStats];
        
    } else {
        NSLog(@"something wrong with comments in user defaults");
    }
}

- (void)updatePostStats
{
    currentContent = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentContent"];
    NSLog(@"current content is %@", currentContent);
    
    if ([[currentContent objectForKey:@"like"] isEqualToNumber:[NSNumber numberWithInt:1]])
    {
        [likesImageView setHighlighted:YES];
    }
    else
    {
        [likesImageView setHighlighted:NO];
    }
    
    NSUInteger repost = [[currentContent objectForKey:@"repost"] integerValue];
    NSLog(@"repost = %lu", (unsigned long)repost);
    
    if (repost == 1)
    {
        [repostsImageView setHighlighted:YES];
    }
    else
    {
        [repostsImageView setHighlighted:NO];
    }
    
    startingLikesCount = [[currentContent objectForKey:@"likes"] integerValue];
    [likesCountLabel setText:[[currentContent objectForKey:@"likes"] stringValue]];
    [repostsCountLabel setText:[[currentContent objectForKey:@"reposts"] stringValue]];
    [commentsCountLabel setText:[[currentContent objectForKey:@"comments"] stringValue]];
    [viewsCountLabel setText:[[currentContent objectForKey:@"views"] stringValue]];
    
    if ([currentContent objectForKey:@"link_delete"] != nil) {
        [deleteButton setHidden:NO];
        deletePostLink = [currentContent objectForKey:@"link_delete"];
    } else {
        [deleteButton setHidden:YES];
    }
    
    [self configureTaggedPeopleTextView];
}

- (void)configureTaggedPeopleTextView
{
    if ([[currentContent objectForKey:@"marks"] isKindOfClass:[NSArray class]]) {
        NSArray *taggedFriends = [currentContent objectForKey:@"marks"];
        NSString *fullText = @"";
        NSString *localizedTaggedString = NSLocalizedString(@"tagged", nil);
        for (NSDictionary *friend in taggedFriends) {
            NSString *name = [NSString stringWithFormat:@"%@ %@",
                              [friend objectForKey:@"first_name"],
                              [friend objectForKey:@"last_name"]];
            fullText = [fullText stringByAppendingString:[NSString stringWithFormat:@", %@", name]];
        }
        fullText = [fullText substringFromIndex:2];
        
        [taggetPeopleTextView setText:[NSString stringWithFormat:@"%@: %@", localizedTaggedString, fullText]];
        [taggetPeopleTextView setTextColor:[UIColor colorWithRed:93.0/255.0
                                                           green:124.0/255.0
                                                            blue:173.0/255.0
                                                           alpha:1.0]];
    }
    else
    {
        [taggetPeopleTextView setText:@""];
        [taggetPeopleTextView setHidden:NO];
        NSLog(@"class is %@", [[currentContent objectForKey:@"marks"] class]);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.commentsTableView reloadData];
    });
    NSLog(@"tagged friends: %@", taggetPeopleTextView.text);
}

- (void)setUpUIElements
{
    if(![author objectForKey:@"nick"]) {     
        NSString *avatarPath = [user objectForKey:@"avatar_small"];
        NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarPath]];
        
        __weak NSString *weakAvatarPath = avatarPath;
        __weak UIImageView *weakAvatarImageView = avatarImageView;
        
        [avatarImageView setImageWithURLRequest:photoRequest
                               placeholderImage:nil
                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
//             [weakAvatarImageView setImage:image];
             [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                        forImage:image];
         }
         
                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                            NSLog(@"fail for %@", weakAvatarPath);
                                        }];
        [nameLabel setText:[user objectForKey:@"first_name"]];
    }
    
    // Tap Gesture
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
//    self.navigationController.view.backgroundColor = [UIColor rainPurple];
//    self.navigationController.navigationBar.backgroundColor = [UIColor rainPurple];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    
}

- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:masked];
    
    CGImageRelease(mask);
    CGImageRelease(masked);
    
    return maskedImage;
}

- (UIImage *)placeHolderImage
{
    CGSize imageSize = CGSizeMake(320/3, 320/3);
    UIColor *fillColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [fillColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    if (actionSheet == shareActionSheet) {
        switch (buttonIndex) {
            case 0:
                [self reportInappropriate];
                break;
                
            case 1:
                [self copyLink];
                break;
                
            case 2:
                [self saveToPhotoLibrary];
                break;
                
            default:
                break;
        }
    }
    else if (actionSheet == postActionSheet)
    {
        switch (buttonIndex) {
            case 0:
                [self deletePost];
                break;
                
            default:
                break;
        }
    }
    else if (actionSheet == commentActionSheet)
    {
        if (commentOwner == YES) {
            switch (buttonIndex) {
                case 0:
                    [self deleteComment];
                    break;
                case 1:
                    [self performSegueWithIdentifier:@"showUserFromPostVC"
                                              sender:self];
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (buttonIndex) {
                case 0:
                    [self performSegueWithIdentifier:@"showUserFromPostVC"
                                              sender:self];
                    break;
                default:
                    break;
            }
        }
    }
    else
    {
        NSLog(@"something wrong with action sheet");
    }
}

- (IBAction)updateComments:(id)sender
{
    [self getComments];
}

- (void) animateLike
{
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        showLikeInImage.transform = CGAffineTransformMakeScale(1.3, 1.3);
        showLikeInImage.hidden=NO;
        showLikeInImage.alpha = 1.0;
    }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                             showLikeInImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                                                  showLikeInImage.transform = CGAffineTransformMakeScale(1.3, 1.3);
                                                  showLikeInImage.alpha = 0.0;
                                                  showLikeInImage.hidden=YES;
                                              }
                                                               completion:^(BOOL finished) {
                                                                   showLikeInImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                                               }];
                                          }];
                     }];
}






- (IBAction)like:(id)sender
{
    [self animateLike];
    NSLog(@"like!");
    [likesCountLabel setHidden:YES];
    [likesImageView setHidden:YES];
    [likeButton setEnabled:NO];
    
    [self showLikesActivity];

    NSLog(@"like success");
    NSString *liked = @"0";
    [self removeLikesActivity];
    NSInteger currentLikesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"likes"] integerValue];
    
    
    if ([likesImageView isHighlighted])
    {
        
        NSLog(@"likes highlighted!");
        [likesImageView setHighlighted:NO];
        liked = @"0";
    } else {
        NSLog(@"likes not highlighted!");
        [likesImageView setHighlighted:YES];
        liked = @"1";
    }
    
    
    
    [likesCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentLikesCount]];
    [likeButton setEnabled:YES];
    
    NSMutableDictionary *postMutDict = [currentPost mutableCopy];
    [postMutDict setObject:[NSNumber numberWithInt:[liked integerValue]] forKey:@"like"];
    
    NSMutableDictionary *mutStats = [[postMutDict objectForKey:@"stats"] mutableCopy];
    
    [mutStats setObject:[NSNumber numberWithInt:[likesCountLabel.text integerValue]] forKey:@"likes"];
    if(mutStats)
    {
        [postMutDict setObject:mutStats forKey:@"stats"];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLiked"
                                                        object:nil
                                                      userInfo:[postMutDict copy]];
    NSLog(@"postmutdict:\n%@", postMutDict);
    }
    
    //[self likeSuccess];
    
    [API likePostWithID:[[currentPost objectForKey:@"id"] stringValue]];
}

- (IBAction)repost:(id)sender
{
    [repostsCountLabel setHidden:YES];
    [repostsImageView setHidden:YES];
    [repostButton setEnabled:NO];
    
    [self showRepostsActivity];
    
    [API repostPostWithID:[[currentPost objectForKey:@"id"] stringValue]];
}

- (IBAction)share:(id)sender
{
    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"share", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"reportInappropriate", nil)
                                                    otherButtonTitles:NSLocalizedString(@"copyLink", nil),
                                  NSLocalizedString(@"saveToPhotoLibrary", nil), nil];
    [shareActionSheet showFromTabBar:[[self tabBarController] tabBar]];
}

- (IBAction)postActionSheet:(id)sender
{
    postActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"actions", nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                    destructiveButtonTitle:NSLocalizedString(@"delete", nil)
                                         otherButtonTitles:nil];
    [postActionSheet showFromTabBar:[[self tabBarController] tabBar]];
}

- (void)deletePost
{
    NSLog(@"delete post!");
    [API deletePost:deletePostLink];
    [self showDeletingActivity];
}

- (void)showDeletingActivity
{
    deletingActivityView = [[UIActivityIndicatorView alloc] init];
    [deletingActivityView setColor:[UIColor whiteColor]];
    [deletingActivityView setCenter:photoImageView.center];
    [scrollView addSubview:deletingActivityView];
    [deletingActivityView startAnimating];
}

- (void)removeDeletingActivity
{
    [deletingActivityView stopAnimating];
    [deletingActivityView removeFromSuperview];
}

- (IBAction)commentActionSheet:(id)sender
{
    if (commentOwner == YES)
    {
        commentActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"actions", nil)
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:NSLocalizedString(@"delete", nil)
                                                otherButtonTitles:selectedAuthor, nil];
    }
    else
    {
        commentActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"actions", nil)
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:selectedAuthor, nil];
    }
    
    [commentActionSheet showFromTabBar:[[self tabBarController] tabBar]];
}

- (void)deleteComment
{
    NSLog(@"delete comment!");
    [API deleteComment:deleteCommentLink];
    [self showDeletingActivity];
}

- (void)copyLink
{
    if ([currentPost objectForKey:@"path"]) {
        NSString *photoString = [currentPost objectForKey:@"path"];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.URL = [NSURL URLWithString:photoString];
    }
}

- (void)saveToPhotoLibrary
{
    if ([[currentPost objectForKey:@"path"] hasPrefix:@"http://rain-app.com/media/video"])
    {
        NSURL *sourceURL = [NSURL URLWithString:[[currentPost objectForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:sourceURL];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
            NSURL *tempURL = [documentsURL URLByAppendingPathComponent:[sourceURL lastPathComponent]];
            [data writeToURL:tempURL atomically:YES];
            UISaveVideoAtPathToSavedPhotosAlbum(tempURL.path, nil, NULL, NULL);
        }];
    }
    else
    {
        UIImage *image = [photoImageView image];
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    }
}

- (void)reportInappropriate
{
    [self performSegueWithIdentifier:@"reportPost" sender:self];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"commentsIDs count - %lu", (unsigned long)commentsIDs.count);
    return [commentsIDs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"comment cell test0");
    APCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:kCell];
    
    // Configure the cell for this indexPath
    NSString *commentID = [commentsIDs objectAtIndex:[indexPath row]];
    NSLog(@"comment cell test1");
    NSDictionary *comment = [comments objectForKey:commentID];
    NSLog(@"comment cell test2");
    
    NSLog(@"comment is %@", comment);
    
    NSData *commentData = [[comment objectForKey:@"text"] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *commentString = [[NSString alloc] initWithData:commentData
                                                    encoding:NSNonLossyASCIIStringEncoding];
    
    
    [[cell bodyLabel] setText:commentString];
    
    [[cell bodyLabel] setContentMode:UIViewContentModeBottomLeft];

    NSString *avatarPath = [comment objectForKey:@"avatar"];
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarPath]];
    
    __weak NSString *weakAvatarPath = avatarPath;
    __weak UIImageView *weakAvatarImageView = cell.photoView;
    
    [cell.photoView setImageWithURLRequest:photoRequest
                           placeholderImage:nil
                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         NSLog(@"success for comment - %@", weakAvatarPath);
//         [weakAvatarImageView setImage:image];
         [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                    forImage:image];
     }
     
                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                        NSLog(@"fail for %@, error is \n%@", weakAvatarPath, error.localizedDescription);
                                    }];
    // created at date string temporary
    [cell setCreatedAtDate:[comment objectForKey:@"createdAt"]];
    
    tableView.estimatedRowHeight = 75;
    tableView.rowHeight = UITableViewAutomaticDimension;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *commentID = [commentsIDs objectAtIndex:[indexPath row]];
    NSDictionary *comment = [comments objectForKey:commentID];

    selectedUserMail = [comment objectForKey:@"email"];
    selectedAuthor = [comment objectForKey:@"author"];
    
    if ([comment objectForKey:@"link_delete"] != nil) {
        commentOwner = YES;
        deleteCommentLink = [comment objectForKey:@"link_delete"];
    } else {
        commentOwner = NO;
    }
    
    [self commentActionSheet:[tableView cellForRowAtIndexPath:indexPath]];
}

#pragma mark - UITableView Delegate

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *commentID = [commentsIDs objectAtIndex:[indexPath row]];
//    NSDictionary *comment = [comments objectForKey:commentID];
//    NSData *commentData = [[comment objectForKey:@"text"] dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *text = [[NSString alloc] initWithData:commentData
//                                                    encoding:NSNonLossyASCIIStringEncoding];
//    
//    
//    NSDictionary *attributes;
//
//    @try {
//       attributes = @{ NSFontAttributeName:
//                 [UIFont fontWithName:@"Arial-Thin"size:18.0],
//             NSForegroundColorAttributeName:
//                 [UIColor colorWithRed:(35.0/255.0)
//                                 green:(70.0/255.0)
//                                  blue:(98.0/255.0)
//                                 alpha:1.0]};
//    }
//    @catch (NSException *exception)
//    {
//        
//    }
//   
//    
//    CGSize constraint = CGSizeMake(228, MAXFLOAT);
//    CGRect textsize = [text boundingRectWithSize:constraint
//                                         options:NSStringDrawingUsesLineFragmentOrigin
//                                      attributes:attributes
//                                         context:nil];
//    CGFloat timeAgoLabelHeight = 21.0f;
//    float textHeight = textsize.size.height + 24;
//    textHeight = (textHeight < 80.0) ? 80.0 : textHeight;
//    return textHeight + timeAgoLabelHeight;
//}
//
#pragma mark - UIAlertView Delegate

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"cancel button");
    
    if (alertView == postAlertView)
    {
        NSLog(@"alertview = postalertview");
        [[self navigationController] popViewControllerAnimated:YES];
        NSLog(@"VC poped");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedChanged" object:self];
        NSLog(@"notification posted");
    }
    else if (alertView == commentAlertView)
    {
        [self getComments];
    }
}

#pragma mark - Double Tap Gesture

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        // handling code
        
        [self like:self];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"segue...");
    
    if ([[segue identifier] isEqualToString:@"addCommentVC"]) {
        [[segue destinationViewController] setPostID:[[currentPost objectForKey:@"id"] stringValue]];
        [[segue destinationViewController] setParent:self];
    }
    
    else if ([[segue identifier] isEqualToString:@"addCommentVCFromBottomBar"])
    {
        [[segue destinationViewController] setPostID:[[currentPost objectForKey:@"id"] stringValue]];
        [[segue destinationViewController] setParent:self];
    }
    
    else if ([[segue identifier] isEqualToString:@"showUserFromPostVC"]) {
        
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] isEqualToString:selectedUserMail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserMail];
    }
    
    else if ([[segue identifier] isEqualToString:@"reportPost"])
    {
        [[segue destinationViewController] setPostID:[[currentPost objectForKey:@"id"] stringValue]];
        NSLog(@"report inappropriate post");
    }
}

@end
