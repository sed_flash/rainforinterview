//
//  APSettingsVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APSettingsVC : UIViewController <UIAlertViewDelegate>
{
    IBOutlet UIImageView *backgroundView;
    IBOutlet UIBarButtonItem *deleteAccountButton;
    
    APServerAPI *API;
}

- (IBAction)deleteAccount:(id)sender;

@end
