//
//  APCamController.m
//  Rain
//
//  Created by EgorMac on 14/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APCamController.h"
#import "APAppDelegate.h"
#import <sys/sysctl.h>

@interface APCamController ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *photoCenterConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *videoCenterConstraint;
@property (weak, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureLeft;
@property (weak, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureRight;

@end

@implementation APCamController {
    float beginPhotoConstr;
    float beginVidoeConstr;
}

static NSString *kMemoryAlert = @"memory";
static NSString *kStandartAlert = @"standart";

static NSInteger kActivityTag = 8972;
static CGFloat kPickerActivityBottomMargin = 100.0f;

- (void)updateViewConstraints
{
    [flashConstraint setConstant:self.view.frame.size.width/7];
    [changeCameraConstraint setConstant:self.view.frame.size.width/7];
    [super updateViewConstraints];
}

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    freeMemorySpaceMegabytes = (NSUInteger)[self freeDiskspace];
    
    // if we need blue background (not black)
    //  UIImageView *backgroundImageViewBlue = [[UIImageView alloc] initWithFrame:self.view.bounds];
    //    [backgroundImageViewBlue setBackgroundColor:[UIColor colorWithRed:93.0/255.0
    //                                                                green:124.0/255.0
    //                                                                 blue:173.0/255.0
    //                                                                alpha:1.0]];
    //[self.view addSubview:backgroundImageViewBlue];
    //[self.view sendSubviewToBack:backgroundImageViewBlue];
    
    [self configureUIElements];
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    [self setSession:session];
    
    // Setup the preview view
    [[self previewView] setSession:session];
    
    [[self previewView] setClipsToBounds:YES];
    
    // Check for device authorization
    [self checkDeviceAuthorizationStatus];
    
    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
    
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];
    
    dispatch_async(sessionQueue, ^{
        [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [APCamController deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error)
        {
            //            NSString *errorText = [NSString stringWithFormat:@"%@. %@",
            //                                   [error localizedDescription],
            //                                   [error localizedRecoverySuggestion]];
            //            [self showAlertWithIdentifier:kStandartAlert
            //                                  andText:errorText];
        }
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                
                [[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)[self interfaceOrientation]];
            });
        }
        
        AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
        AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
        
        if (error)
        {
            //            NSString *errorText = [NSString stringWithFormat:@"%@. %@",
            //                                   [error localizedDescription],
            //                                   [error localizedRecoverySuggestion]];
            //            [self showAlertWithIdentifier:kStandartAlert
            //                                  andText:errorText];
        }
        
        if ([session canAddInput:audioDeviceInput])
        {
            [session addInput:audioDeviceInput];
        }
        
        AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
        if ([session canAddOutput:movieFileOutput])
        {
            [session addOutput:movieFileOutput];
            AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
            {
                if ([connection isVideoStabilizationSupported])
                {
                    [connection setPreferredVideoStabilizationMode:AVCaptureVideoStabilizationModeAuto];
                }
            }
            [self setMovieFileOutput:movieFileOutput];
        }
        
        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([session canAddOutput:stillImageOutput])
        {
            [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
            [session addOutput:stillImageOutput];
            [self setStillImageOutput:stillImageOutput];
        }
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [photoLabel setAlpha:1.0f];
    [videoLabel setAlpha:0.5f];
    beginPhotoConstr = _photoCenterConstraint.constant;
    beginVidoeConstr = _videoCenterConstraint.constant;
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    if (isVideo == YES)
    {
        UIColor *yellowColor = [videoLabel textColor];
        [videoLabel setTextColor:[UIColor whiteColor]];
        CGRect photoLabelFrame = [photoLabel frame];
        [photoLabel setCenter:CGPointMake(self.view.center.x, photoLabel.center.y)];
        CGFloat deltaX = photoLabelFrame.origin.x - photoLabel.frame.origin.x;
        CGFloat newVideoLabelX = videoLabel.frame.origin.x - deltaX;
        [videoLabel setFrame:CGRectMake(newVideoLabelX,
                                        videoLabel.frame.origin.y,
                                        videoLabel.frame.size.width,
                                        videoLabel.frame.size.height)];
        [photoLabel setTextColor:yellowColor];
    }
    
    isVideo = NO;
    labelsAnimate = NO;
    videoRecording = NO;
    [timeLabel setHidden:YES];
    
    if (recordTimer) {
        [recordTimer invalidate];
        recordTimer = nil;
    }
    
    [photoLabel setText:NSLocalizedString(@"photo", nil)];
    [videoLabel setText:NSLocalizedString(@"video", nil)];
    [timeLabel setText:@"00:00/02:00"];
    
    [photoLabel setCenter:CGPointMake(self.view.center.x, photoLabel.center.y)];
    
    dispatch_async([self sessionQueue], ^{
        
        recordSeconds = 0;
        
        [self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
        [self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
        [self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        
        __weak APCamController *weakSelf = self;
        [self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
            APCamController *strongSelf = weakSelf;
            dispatch_async([strongSelf sessionQueue], ^{
                // Manually restarting the session since it must have been stopped due to an error.
                [[strongSelf session] startRunning];
            });
        }]];
        [[self session] startRunning];
    });
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _photoCenterConstraint.constant = 0;
    _videoCenterConstraint.constant = 60;
    
    dispatch_async([self sessionQueue], ^{
        [[self session] stopRunning];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
        
        [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
        [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
        [self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
        
        if (recordTimer) {
            [recordTimer invalidate];
            recordTimer = nil;
        }
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == CapturingStillImageContext)
    {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
        
        if (isCapturingStillImage)
        {
            [self runStillImageCaptureAnimation];
        }
    }
    else if (context == RecordingContext)
    {
        BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRecording)
            {
                [changeCamButton setEnabled:NO];
                [shotButton setSelected:YES];
                [shotButton setEnabled:YES];
                [lightButton setEnabled:NO];
            }
            else
            {
                [changeCamButton setEnabled:YES];
                [shotButton setSelected:NO];
                [shotButton setEnabled:YES];
                [lightButton setEnabled:YES];
            }
        });
    }
    else if (context == SessionRunningAndDeviceAuthorizedContext)
    {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRunning)
            {
                [changeCamButton setEnabled:YES];
                [shotButton setEnabled:YES];
            }
            else
            {
                [changeCamButton setEnabled:NO];
                [shotButton setEnabled:NO];
            }
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)[[self previewView] layer] captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:[gestureRecognizer view]]];
    [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

#pragma mark File Output Delegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    //
    if (error)
    {
        //        NSString *errorText = [NSString stringWithFormat:@"%@. %@",
        //                               [error localizedDescription],
        //                               [error localizedRecoverySuggestion]];
        //        [self showAlertWithIdentifier:kStandartAlert
        //                              andText:errorText];
    }
    
    videoRecording = NO;
    //    recordSeconds = 0;
    
    [shotButton setHidden:YES];
    [photoLabel setHidden:YES];
    [videoLabel setHidden:YES];
    [timeLabel setHidden:YES];
    
    NSLog(@"stopping");
    
    UIActivityIndicatorView *buttonActivityView = [[UIActivityIndicatorView alloc] init];
    [buttonActivityView setColor:[UIColor whiteColor]];
    [buttonActivityView setCenter:shotButton.center];
    [[self view] addSubview:buttonActivityView];
    [buttonActivityView startAnimating];
    
    [self setLockInterfaceRotation:NO];
    
    // Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
    UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
    [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:outputFileURL options:nil];
    
    //create an avassetrack with our asset
    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    //here we are setting its render size to its height x height (Square)
    videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height);
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(120, 30));
    
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    
    //Here we shift the viewing square up to the TOP of the video so we only see the top
    //    CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, 0 );
    
    //Use this code if you want the viewing square to be in the middle of the video
    CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height) /2 );
    
    //Make sure the square is portrait
    CGAffineTransform t2 = CGAffineTransformRotate(t1, M_PI_2);
    
    CGAffineTransform finalTransform = t2;
    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    //Create an Export Path to store the cropped video
    NSString * documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *exportPath = [documentsPath stringByAppendingFormat:@"/CroppedVideo.mp4"];
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL:exportUrl error:nil];
    
    //Export
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    exporter.videoComposition = videoComposition;
    exporter.outputURL = exportUrl;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
             generator.appliesPreferredTrackTransform = YES;
             NSError *err = NULL;
             CMTime time = CMTimeMake(1, 2);
             CGImageRef oneRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
             UIImage *pickerImage = [[UIImage alloc] initWithCGImage:oneRef];
             
             [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
             
             if (backgroundRecordingID != UIBackgroundTaskInvalid)
                 [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
             
             [buttonActivityView stopAnimating];
             [buttonActivityView removeFromSuperview];
             [shotButton setHidden:NO];
             [photoLabel setHidden:NO];
             [videoLabel setHidden:NO];
             
             //                 [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:exporter.outputURL completionBlock:^(NSURL *assetURL, NSError *error) {
             //                     if (error)
             //                         NSLog(@"%@", error);
             //
             ////                     [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
             //
             //                     if (_backgroundRecordingID != UIBackgroundTaskInvalid)
             //                         [[UIApplication sharedApplication] endBackgroundTask:_backgroundRecordingID];
             //                     NSLog(@"done!");
             //                 }];
             
             //             UISaveVideoAtPathToSavedPhotosAlbum([exporter.outputURL relativePath], self, nil, nil);
             
             
             APCreatePostVC *createPostVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"CreatePostVC"];
             [createPostVC setIsVideo:YES];
             [createPostVC setPhotoImage:pickerImage];
             [createPostVC setVideoURL: exporter.outputURL];
             [self.navigationController pushViewController:createPostVC animated:YES];
         });
     }];
}

#pragma mark Device Configuration

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [[self videoDeviceInput] device];
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            //            NSString *errorText = [NSString stringWithFormat:@"%@. %@",
            //                                   [error localizedDescription],
            //                                   [error localizedRecoverySuggestion]];
            //            [self showAlertWithIdentifier:kStandartAlert
            //                                  andText:errorText];
        }
    });
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"localized description: \n%@", [error localizedDescription]);
            NSLog(@"localized recovery options: \n%@", [error localizedRecoveryOptions]);
            NSLog(@"localized recovery suggestion: \n%@", [error localizedRecoverySuggestion]);
            NSLog(@"localized failure reason: \n%@", [error localizedFailureReason]);
        }
    }
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted)
        {
            //Granted access to mediaType
            [self setDeviceAuthorized:YES];
        }
        else
        {
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"AVCam!"
                                            message:@"AVCam doesn't have permission to use Camera, please change privacy settings"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}

#pragma mark - Helper Methods

- (void)updateTimeLabel
{
    recordSeconds++;
    int minutes = (recordSeconds % 3600) / 60;
    int seconds = (recordSeconds % 3600) % 60;
    NSLog(@"time: %@", [NSString stringWithFormat:@"%02d:%02d/02:00", minutes, seconds]);
    [timeLabel setText:[NSString stringWithFormat:@"%02d:%02d/02:00", minutes, seconds]];
    if ([[timeLabel text] isEqualToString:@"01:59/02:00"])
    {
        NSLog(@"stop video");
        recordSeconds = 0;
        [self makeShot:self];
        
    }
    
    if (videoRecording == YES) {
        recordTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self
                                                     selector:@selector(updateTimeLabel)
                                                     userInfo:nil
                                                      repeats:NO];
    }
}

- (UIImage *)fixOrientationOfImage:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (void)configureUIElements
{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self configureLibraryButton];
    [self setHidesBottomBarWhenPushed:YES];
}

- (void)configureLibraryButton
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Enumerate just the photos and videos group by using ALAssetsGroupSavedPhotos.
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        // Within the group enumeration block, filter to enumerate just photos.
        [group setAssetsFilter:[ALAssetsFilter allPhotos]];
        
        // Chooses the photo at the last index
        [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop) {
            
            // The end of the enumeration is signaled by asset == nil.
            if (alAsset) {
                ALAssetRepresentation *representation = [alAsset defaultRepresentation];
                UIImage *latestPhoto = [UIImage imageWithCGImage:[representation fullScreenImage]];
                
                // Stop the enumerations
                *stop = YES; *innerStop = YES;
                
                // Do something interesting with the AV asset.
                CGImageRef imageRef = CGImageCreateWithImageInRect([latestPhoto CGImage], CGRectMake(0, 200, 520, 520));
                UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
                CGImageRelease(imageRef);
                
                
                [libraryButton setBackgroundImage:croppedImage forState:UIControlStateNormal];
            }
        }];
    } failureBlock: ^(NSError *error) {
        // Typically you should handle an error more gracefully than this.
        NSLog(@"No groups");
    }];
}

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[self previewView] layer] setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [[[self previewView] layer] setOpacity:1.0];
        }];
    });
}

#pragma mark - UIImagePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info dictionary - \n%@", info);
    
    UIImage *pickerImage;
    
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        
        [self showPickerActivityView];
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
        
        //create an avassetrack with our asset
        AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        //create a video composition and preset some settings
        AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
        videoComposition.frameDuration = CMTimeMake(1, 30);
        
        //here we are setting its render size to its height x height (Square)
        //        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height);
        
        if (clipVideoTrack.naturalSize.height >= 640.0f)
        {
            videoComposition.renderSize = CGSizeMake(640.0f, 640.0f);
        } else {
            videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height);
        }
        
        NSLog(@"clipVideoTrack.naturalSize.width = %f", clipVideoTrack.naturalSize.width);
        NSLog(@"clipVideoTrack.naturalSize.height = %f", clipVideoTrack.naturalSize.height);
        
        //create a video instruction
        AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(120, 30));
        
        AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
        
        //Here we shift the viewing square up to the TOP of the video so we only see the top
        CGAffineTransform videoPreferredTransform = clipVideoTrack.preferredTransform;
        NSLog(@"\n videoPreferredTransform.a = %f\n videoPreferredTransform.b = %f\n videoPreferredTransform.c = %f\n videoPreferredTransform.d = %f\n videoPreferredTransform.tx = %f\n videoPreferredTransform.ty = %f\n", videoPreferredTransform.a, videoPreferredTransform.b, videoPreferredTransform.c, videoPreferredTransform.d, videoPreferredTransform.tx, videoPreferredTransform.ty);
        
        CGFloat rotationAngle;
        CGAffineTransform newTransform;
        
        if (videoPreferredTransform.a == -1 &&
            videoPreferredTransform.b == 0 &&
            videoPreferredTransform.c == 0 &&
            videoPreferredTransform.d == -1)
        {
            CGFloat newTX = clipVideoTrack.naturalSize.width - (clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height)/2;
            
            newTransform = CGAffineTransformMake(-1, 0, 0, -1, newTX, clipVideoTrack.naturalSize.height);
        }
        
        else if (videoPreferredTransform.a == 1 &&
                 videoPreferredTransform.b == 0 &&
                 videoPreferredTransform.c == 0 &&
                 videoPreferredTransform.d == 1)
        {
            CGFloat newTX = - (clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height)/2;
            newTransform = CGAffineTransformMake(1, 0, 0, 1, newTX, 0);
        }
        
        else
        {
            CGFloat newTX = - (clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height)/2;
            newTransform = CGAffineTransformTranslate(videoPreferredTransform, newTX, 0);
        }
        
        //Make sure the square is portrait
        CGAffineTransform finalTransform = newTransform;
        [transformer setTransform:finalTransform atTime:kCMTimeZero];
        
        //add the transformer layer instructions, then add to video composition
        instruction.layerInstructions = [NSArray arrayWithObject:transformer];
        videoComposition.instructions = [NSArray arrayWithObject: instruction];
        
        //Create an Export Path to store the cropped video
        NSString * documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *exportPath = [documentsPath stringByAppendingFormat:@"/CroppedVideo.mp4"];
        NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
        
        //Remove any prevouis videos at that path
        [[NSFileManager defaultManager]  removeItemAtURL:exportUrl error:nil];
        
        //Export
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        exporter.videoComposition = videoComposition;
        exporter.outputURL = exportUrl;
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        
        [exporter exportAsynchronouslyWithCompletionHandler:^
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Call when finished
                 //                 [self exportDidFinish:exporter];
                 
                 // thumbnail
                 
                 AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                 generator.appliesPreferredTrackTransform = YES;
                 NSError *err = NULL;
                 CMTime time = CMTimeMake(1, 2);
                 CGImageRef oneRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
                 UIImage *pickerImage = [[UIImage alloc] initWithCGImage:oneRef];
                 
                 APCreatePostVC *createPostVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"CreatePostVC"];
                 [createPostVC setIsVideo:YES];
                 [createPostVC setPhotoImage:pickerImage];
                 [createPostVC setVideoURL: exporter.outputURL];
                 [self.navigationController pushViewController:createPostVC animated:NO];
                 [self removePickerActivityView];
                 NSLog(@"dismiss");
                 [self dismissViewControllerAnimated:YES completion:nil];
             });
         }];
        
    } else
    {
        
        pickerImage = [info objectForKey:UIImagePickerControllerOriginalImage];

//        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];

//        photoPicker.cropBlock = ^(UIImage *image)
//        {
//            
//            
//            APFiltersVC *filtersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"FiltersVC"];
//            [filtersVC setPhotoImage:image];
//            [self.navigationController pushViewController:filtersVC animated:YES];
//            
//            
//            NSLog(@"choose from library button tapped");
//            
//        };
//        
//        [self presentViewController:photoPicker animated:YES completion:nil];
        
        
        
        APFiltersVC *filtersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"FiltersVC"];
        [filtersVC setPhotoImage:pickerImage];
        [self.navigationController pushViewController:filtersVC animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    NSLog(@"picker image is %@", pickerImage);
}

- (void)showPickerActivityView
{
    UIApplication *currentApplication = [UIApplication sharedApplication];
    APAppDelegate *currentAppDelegate = [currentApplication delegate];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor whiteColor]];
    [activity setCenter:currentAppDelegate.window.center];
    CGRect activityFrame = activity.frame;
    activityFrame.origin.y = currentAppDelegate.window.bounds.size.height - kPickerActivityBottomMargin;
    activity.frame = activityFrame;
    activity.tag = kActivityTag;
    [currentAppDelegate.window addSubview:activity];
    [activity startAnimating];
}

- (void)removePickerActivityView
{
    UIApplication *currentApplication = [UIApplication sharedApplication];
    APAppDelegate *currentAppDelegate = [currentApplication delegate];
    
    UIActivityIndicatorView *activity = [currentAppDelegate.window viewWithTag:kActivityTag];
    [activity stopAnimating];
    [activity removeFromSuperview];
}

#pragma mark - IBAction Methods

- (IBAction)showNet:(id)sender
{
    [netButton setSelected:!netButton.selected];
    [netImageView setHidden:!netImageView.hidden];
}

- (IBAction)showLight:(id)sender
{
    [lightButton setSelected:!lightButton.selected];
}

- (IBAction)changeCamera:(id)sender
{
    [changeCamButton setEnabled:NO];
    [shotButton setEnabled:NO];
    
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *currentVideoDevice = [[self videoDeviceInput] device];
        AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
        AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
        
        switch (currentPosition)
        {
            case AVCaptureDevicePositionUnspecified:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
            case AVCaptureDevicePositionBack:
                preferredPosition = AVCaptureDevicePositionFront;
                break;
            case AVCaptureDevicePositionFront:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
        }
        
        AVCaptureDevice *videoDevice = [APCamController deviceWithMediaType:AVMediaTypeVideo preferringPosition:preferredPosition];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
        
        [[self session] beginConfiguration];
        
        [[self session] removeInput:[self videoDeviceInput]];
        if ([[self session] canAddInput:videoDeviceInput])
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:currentVideoDevice];
            
            [APCamController setFlashMode:AVCaptureFlashModeAuto forDevice:videoDevice];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:videoDevice];
            
            [[self session] addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
        }
        else
        {
            [[self session] addInput:[self videoDeviceInput]];
        }
        
        [[self session] commitConfiguration];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [changeCamButton setEnabled:YES];
            [shotButton setEnabled:YES];
        });
    });
}

- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)makeShot:(id)sender
{
    if (isVideo == NO) {
        
        if (freeMemorySpaceMegabytes < 2) {
            [self showAlertWithIdentifier:kMemoryAlert andText:memoryAlertText];
        } else {
            dispatch_async([self sessionQueue], ^{
                // Update the orientation on the still image output video connection before capturing.
                [[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
                
                // Flash set to Auto for Still Capture
                
                if ([lightButton isSelected])
                {
                    [APCamController setFlashMode:AVCaptureFlashModeOn forDevice:[[self videoDeviceInput] device]];
                } else {
                    [APCamController setFlashMode:AVCaptureFlashModeOff forDevice:[[self videoDeviceInput] device]];
                }
                
                // Capture a still image.
                [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                    
                    if (imageDataSampleBuffer)
                    {
                        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                        UIImage *image = [[UIImage alloc] initWithData:imageData];
                        image = [self fixOrientationOfImage:image];
                        //				[[[ALAssetsLibrary alloc] init] writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:nil];
                        
                        APFiltersVC *filtersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"FiltersVC"];
                        [filtersVC setPhotoImage:image];
                        [self.navigationController pushViewController:filtersVC animated:YES];
                    }
                }];
            });
        }
        
    }
    else
    {
        if (freeMemorySpaceMegabytes < 100) {
            [self showAlertWithIdentifier:kMemoryAlert andText:memoryAlertText];
        } else {
            [photoLabel setHidden:YES];
            [videoLabel setHidden:YES];
            
            [timeLabel setHidden:NO];
            
            recordTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(updateTimeLabel)
                                                         userInfo:nil
                                                          repeats:NO];
            videoRecording = YES;
            
            dispatch_async([self sessionQueue], ^{
                if (![[self movieFileOutput] isRecording])
                {
                    
                    
                    [self setLockInterfaceRotation:YES];
                    
                    if ([[UIDevice currentDevice] isMultitaskingSupported])
                    {
                        // Setup background task. This is needed because the captureOutput:didFinishRecordingToOutputFileAtURL: callback is not received until AVCam returns to the foreground unless you request background execution time. This also ensures that there will be time to write the file to the assets library when AVCam is backgrounded. To conclude this background execution, -endBackgroundTask is called in -recorder:recordingDidFinishToOutputFileURL:error: after the recorded file has been saved.
                        [self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
                    }
                    
                    // Update the orientation on the movie file output video connection before starting recording.
                    [[[self movieFileOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[[self previewView] layer] connection] videoOrientation]];
                    
                    // Configure flash for video recording
                    [APCamController setFlashMode:AVCaptureFlashModeOff
                                        forDevice:[[self videoDeviceInput] device]];
                    
                    // Start recording to a temporary file.
                    NSString *outputFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[@"movie" stringByAppendingPathExtension:@"mov"]];
                    [[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:self];
                }
                else
                {
                    videoRecording = NO;
                    [[self movieFileOutput] stopRecording];
                }
            });
        }
    }
}

- (IBAction)chooseFromLibrary:(id)sender
{
    dispatch_async([self sessionQueue], ^{
        
        NSLog(@"choose from library button tapped");
        
        //        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        //
        //        if ([UIImagePickerController
        //             isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        //        {
        //            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        //            [imagePicker setMediaTypes:[UIImagePickerController availableMediaTypesForSourceType:
        //                                        UIImagePickerControllerSourceTypePhotoLibrary]];
        //        }
        //
        //        [imagePicker setDelegate:self];
        //        [imagePicker setAllowsEditing:YES];
        //        [imagePicker setVideoMaximumDuration:119.0];
        //        [self presentViewController:imagePicker
        //                           animated:YES
        //                         completion:nil];
    });
    
    chooseMediaActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"chooseFromLibrary", nil)
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:NSLocalizedString(@"photo", nil),
                              NSLocalizedString(@"video", nil), nil];
    [chooseMediaActionSheet showInView:self.view];
}

- (uint64_t)freeDiskspace
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return ((totalFreeSpace/1024ll)/1024ll);
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"available types savedPhotosAlbum: \n%@", [UIImagePickerController availableMediaTypesForSourceType:
                                                      UIImagePickerControllerSourceTypeSavedPhotosAlbum]);
    NSLog(@"available types savedPhotosAlbum: \n%@", [UIImagePickerController availableMediaTypesForSourceType:
                                                      UIImagePickerControllerSourceTypeSavedPhotosAlbum]);
    
    switch (buttonIndex) {
        case 0:
        {
            NSLog(@"Photo button tapped");
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            
            if ([UIImagePickerController
                 isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                NSArray *mediaTypeArray = [[NSArray alloc] initWithObjects:@"public.image", nil];
                [imagePicker setMediaTypes:mediaTypeArray];
            }
            
            [imagePicker setDelegate:self];
            [imagePicker setAllowsEditing:NO];
            [self presentViewController:imagePicker
                               animated:YES
                             completion:nil];
            break;
        }
        case 1:
        {
            NSLog(@"Video button tapped");
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            
            if ([UIImagePickerController
                 isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                NSArray *mediaTypeArray = [[NSArray alloc] initWithObjects:@"public.movie", nil];
                [imagePicker setMediaTypes:mediaTypeArray];
            }
            
            [imagePicker setDelegate:self];
            [imagePicker setAllowsEditing:YES];
            [imagePicker setVideoMaximumDuration:119.0];
            [self presentViewController:imagePicker
                               animated:YES
                             completion:nil];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Alert

- (void)showAlertWithIdentifier:(NSString *)identifier andText:(NSString *)errorText
{
    if ([identifier isEqualToString:kMemoryAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:errorText
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:kStandartAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:errorText
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSLog(@"Something wrong with alert");
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Cancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Swipe photo/video

- (IBAction)swipeToLeft:(UISwipeGestureRecognizer *)sender {
    NSLog(@"Was left");
    
    if (beginPhotoConstr == _photoCenterConstraint.constant) {
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.7f
                         animations:^{
                             [photoLabel setAlpha:0.5f];
                             [videoLabel setAlpha:1.0f];
                             _photoCenterConstraint.constant -= 60;
                             _videoCenterConstraint.constant -= 60;
                             [self.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             isVideo = YES;
                         }];
    }
}

- (IBAction)swipeToRight:(UISwipeGestureRecognizer *)sender {
    NSLog(@"Was right");
    
    if (beginPhotoConstr - 60 == _photoCenterConstraint.constant) {
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.7f
                         animations:^{
                             [photoLabel setAlpha:1.0f];
                             [videoLabel setAlpha:0.5f];
                             _photoCenterConstraint.constant += 60;
                             _videoCenterConstraint.constant += 60;
                             [self.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             isVideo = NO;
                         }];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
