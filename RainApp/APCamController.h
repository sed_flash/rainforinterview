//
//  APCamController.h
//  Rain
//
//  Created by EgorMac on 14/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "APFiltersVC.h"
#import "AVCamPreviewView.h"
#import "APCreatePostVC.h"
#import "TWPhotoPickerController.h"

static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface APCamController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, AVCaptureFileOutputRecordingDelegate, UIAlertViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UIImageView *netImageView;
    
    IBOutlet UIButton *netButton;
    IBOutlet UIButton *lightButton;
    IBOutlet UIButton *changeCamButton;
    IBOutlet UIButton *shotButton;
    
    IBOutlet UIButton *libraryButton;
    
    IBOutlet UILabel *photoLabel;
    IBOutlet UILabel *videoLabel;
    IBOutlet UILabel *timeLabel;
    
    BOOL isVideo;
    BOOL labelsAnimate;
    
    NSTimer *recordTimer;
    int recordSeconds;
    
    BOOL videoRecording;
    
    NSUInteger freeMemorySpaceMegabytes;
    NSString *memoryAlertText;
    
    UIActionSheet *chooseMediaActionSheet;
    
    IBOutlet NSLayoutConstraint *flashConstraint;
    IBOutlet NSLayoutConstraint *changeCameraConstraint;
}

@property (nonatomic, weak) IBOutlet AVCamPreviewView *previewView;
// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;


@property (nonatomic,strong)TWPhotoPickerController *photoPicker;


// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;

- (IBAction)showNet:(id)sender;
- (IBAction)showLight:(id)sender;
- (IBAction)changeCamera:(id)sender;
- (IBAction)goBack:(id)sender;
- (IBAction)makeShot:(id)sender;
- (IBAction)chooseFromLibrary:(id)sender;

@end
