//
//  APLoginVC.m
//  Rain
//
//  Created by EgorMac on 16/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APLoginVC.h"

@interface APLoginVC ()

@end

@implementation APLoginVC

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [self setupUIElements];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:animated];
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signinSuccess)
                                                 name:@"SigninSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signinFailed)
                                                 name:@"SigninFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];

    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    API = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)setupUIElements
{
    
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    
    [toLogInLabel setText:NSLocalizedString(@"toLoginInAccount", nil)];
    
    [emailTextField setPlaceholder:NSLocalizedString(@"email", nil)];
    [passwordTextField setPlaceholder:NSLocalizedString(@"password", nil)];
    UIView *emailPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    UIView *passwordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    [emailTextField setLeftView:emailPaddingView];
    [emailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [passwordTextField setLeftView:passwordPaddingView];
    [passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    [loginButton setTitle:NSLocalizedString(@"toLogIn", nil) forState:UIControlStateNormal];
    [loginButton setTitle:NSLocalizedString(@"toLogIn", nil) forState:UIControlStateHighlighted];
    [[loginButton layer] setCornerRadius:3.0f];
    
    [forgotPasswordLabel setText:NSLocalizedString(@"forgotPassword?", nil)];
}

#pragma mark - IBAction Methods

- (IBAction)login:(id)sender
{
    if ([self textFieldIsEmpty]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        [self showActivityIndicator];
        
        NSString *email = [emailTextField text];
        NSString *password = [passwordTextField text];
//        email=@"Nozdrinov@i.ua";
//        password=@"56368d6513ec5"
        NSMutableDictionary *user = [[NSMutableDictionary alloc] init];
        [user setObject:email forKey:@"email"];
        [user setObject:password forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"userData"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [API signinWithEmail:email andPassword:password];
    }
}

- (void)signinSuccess
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:@"userData"]!=nil)
    {
        NSLog(@"userData in Login VC - \n%@", [defaults objectForKey:@"currentUser"]);
        
        BOOL currentUserHasNilFirstname = ([[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] == nil);
        BOOL currentUserHasEmptyFirstname = ([[[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] isEqualToString:@""]);
        
        if (currentUserHasEmptyFirstname)
        {
            [self performSegueWithIdentifier:@"logToStep2" sender:self];
        }
        else
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"])
            {
                NSLog(@"User already logged in. Redirecting to news feed");
                [self performSegueWithIdentifier:@"loginSuccess" sender:self];
                [[[self navigationController] navigationBar] setHidden:YES];
            }
        }
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor colorWithRed:(251.0/255.0)
                                                                              green:(195.0/255.0)
                                                                               blue:(10.0/255.0)
                                                                              alpha:1.0]];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    
    [self removeActivityIndicator];
}

- (void)signinFailed
{
    [self showAlertWithIdentifier:serverAlert];
    [self removeActivityIndicator];
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
    [self removeActivityIndicator];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        NSLog(@"error message is: \n%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]);
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            NSLog(@"error message is string:\n%@", errorMessage);
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            NSLog(@"error message is array:\n%@", errorMessage);
            NSLog(@"error message class is %@", [errorMessage class]);
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];

    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] init];
    UIColor *customBlueColor = [UIColor colorWithRed:(93.0/255.0) green:(124.0/255.0) blue:(173.0/255.0) alpha:1.0];
    [activityIndicator setColor:customBlueColor];
    [activityIndicator setTag:5];
    [activityIndicator setCenter:loginButton.center];
    loginButton.hidden = YES;
    [[self view] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activityIndicator stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    loginButton.hidden = NO;
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat viewHeight, bottomOfTextField, topOfKeyboard;

    viewHeight = viewRect.size.height;
    bottomOfTextField = textFieldRect.origin.y + textFieldRect.size.height;
    topOfKeyboard = viewHeight - PORTRAIT_KEYBOARD_HEIGHT - 10; // 10 point padding
    
    if (bottomOfTextField >= topOfKeyboard) keyboardAnimatedDistance = bottomOfTextField - topOfKeyboard;
    else keyboardAnimatedDistance = 0.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldIsEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

@end
