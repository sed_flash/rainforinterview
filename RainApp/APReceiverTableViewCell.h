//
//  APReceiverTableViewCell.h
//  Rain
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APReceiverTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *message;
@property (nonatomic, strong) NSData *avatarData;

@end
