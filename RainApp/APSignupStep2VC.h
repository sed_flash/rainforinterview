//
//  APSignupStep2VC.h
//  Rain
//
//  Created by EgorMac on 18/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APSignupStep2VC : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *dayTextField;
    IBOutlet UITextField *monthTextField;
    IBOutlet UITextField *yearTextField;
    IBOutlet UITextField *countryTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UIButton *maleButton;
    IBOutlet UIButton *femaleButton;
    IBOutlet UIButton *signupButton;
    
    UIToolbar *dateToolbar;
    UIToolbar *countryToolbar;
    UIToolbar *cityToolbar;
    
    IBOutlet NSLayoutConstraint *firstNameYConstraint;
    IBOutlet NSLayoutConstraint *firstNameHeightConstraint;
    IBOutlet NSLayoutConstraint *lastNameTopSpaceContraint;
    IBOutlet NSLayoutConstraint *dateTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *countryTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *cityTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *sexButtonsTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *signupButtonTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *rainLogoWidth;
    IBOutlet NSLayoutConstraint *rainLogoHeight;
    
    APServerAPI *API;
    
    int gender;
    CGFloat keyboardAnimatedDistance;
    
    BOOL countriesDownloaded;
    BOOL citiesDownloaded;
    
    int chosenCountryID;
    int chosenCityID;
    
    UIActivityIndicatorView *activityIndicator;
    
    NSString *countryID;
    NSString *cityID;
}

@property (nonatomic, strong) APServerAPI *API;

- (IBAction)maleButtonPress:(id)sender;
- (IBAction)femaleButtonPress:(id)sender;
- (IBAction)signup:(id)sender;

@end
