//
//  APFoundFriendsVC.m
//  Rain
//
//  Created by EgorMac on 13/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFoundFriendsVC.h"

@interface APFoundFriendsVC ()

@end

@implementation APFoundFriendsVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureUIElements];
    [self setDataSource];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureUIElements
{
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
    [[self navigationItem] setTitle:NSLocalizedString(@"searchFriends", nil)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
}

#pragma mark - IBAction methods

- (IBAction)filtering:(id)sender
{
    NSLog(@"filtering...");
}

#pragma mark - Helper Methods

- (void)setDataSource
{
    foundUsers = [[NSDictionary alloc] init];
    foundUsersIDs = [[NSArray alloc] init];
    
    NSArray *users = [[NSUserDefaults standardUserDefaults] objectForKey:@"foundFriends"];
    NSMutableArray *usersIDs = [[NSMutableArray alloc] init];
    NSMutableDictionary *usersDictionary = [[NSMutableDictionary alloc] init];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userData= [defaults objectForKey:@"userData"];
    NSString* Email=[userData  objectForKey:@"email"];
    
    for (NSDictionary *user in users)
    
    {
        NSString *email=[user objectForKey:@"email"];
        if(![Email isEqualToString:email])
        {
            NSString *userID = [[user objectForKey:@"id"] stringValue];
            [usersIDs addObject:userID];
            [usersDictionary setObject:user forKey:userID];
        }
      
    }
    
    foundUsersIDs = [usersIDs copy];
    foundUsers = [usersDictionary copy];
    
    users = nil;
    usersIDs = nil;
    usersDictionary = nil;
    
    NSLog(@"foundUsersIDs - \n%@", foundUsersIDs);
    NSLog(@"foundUsers - \n%@", foundUsers);
    
    if ([foundUsersIDs count] == 0)
    {
        [friendsCollectionView setHidden:YES];
        [noResultsLabel setHidden:NO];
        [noResultsLabel setText:NSLocalizedString(@"noResults", nil)];
        [filterButton setEnabled:NO];
    } else
    {
        [friendsCollectionView setHidden:NO];
        [noResultsLabel setHidden:YES];
        [filterButton setEnabled:YES];
    }
}

#pragma mark - Collection View Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 35;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    
    if ([indexPath row] < 6)
    {
        size = CGSizeMake(106, 140);
    }
    else if ([indexPath row] > 13)
    {
        size = CGSizeMake(63.6, 102);
    }
    else
    {
        size = CGSizeMake(80, 120);
    }
    
    return size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [foundUsersIDs count];
}

#pragma mark - Collection View DataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    APFoundFriendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FoundFriend" forIndexPath:indexPath];
    NSString *userID = [foundUsersIDs objectAtIndex:indexPath.row];
    NSDictionary *currentUser = [foundUsers objectForKey:userID];
    NSString *userName = [currentUser objectForKey:@"firstname"];
    NSString *rating = [[currentUser objectForKey:@"rating"] stringValue];
    
    NSLog(@"current user is %@", currentUser);
    
    [[cell nameLabel] setText:userName];
    [[cell rateLabel] setText:rating];
    
    if ([[currentUser objectForKey:@"avatar_small"] isKindOfClass:[NSString class]]) {
        
        if ([[currentUser objectForKey:@"avatar_small"] isEqualToString:@""])
        {
           if(![[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"])
           {
               [cell.photoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
           }
        }
        else
        {
            NSString *photoString = [currentUser objectForKey:@"avatar_small"];
            if(photoString.length==0)
            {
                photoString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
            }
            
            NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
            
            __weak UIImageView *weakImageView = [cell photoImageView];
            
            [[cell photoImageView] setImageWithURLRequest:photoRequest
                                         placeholderImage:[UIImage new]
                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
//                 [weakImageView setImage:image];
                 [UIImageView apa_useMaskToImageView:weakImageView
                                            forImage:image];
             }
             
                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                      NSLog(@"fail for cell photo");
                                                      [weakImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                                  }];
        }
    }
    else
    {
        NSLog(@"hasn't avatar");
        [[cell photoImageView] setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
    }
    
    return cell;
}

#pragma mark - Collection View Select Row method

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select");
    NSString *userID = [foundUsersIDs objectAtIndex:[indexPath row]];
    NSDictionary *user = [foundUsers objectForKey:userID];
    selectedUserEmail = [user objectForKey:@"email"];
    
    [self performSegueWithIdentifier:@"showUserFromFilterResults" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showUserFromFilterResults"]) {
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] isEqualToString:selectedUserEmail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserEmail];
    }
}

@end
