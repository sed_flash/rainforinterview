//
//  APFriendCell.m
//  Rain
//
//  Created by EgorMac on 14/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFriendCell.h"

@implementation APFriendCell
@synthesize friend, followed;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    API = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followSuccess)
                                                 name:@"FollowSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followFailed)
                                                 name:@"FollowFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFriend:(NSDictionary *)newFriend
{
    if (friend != newFriend) {
        friend = newFriend;
        [nameLabel setText:[NSString stringWithFormat:@"%@ %@", [friend objectForKey:@"first_name"], [friend objectForKey:@"last_name"]]];
        
        if ([[friend objectForKey:@"follow"] integerValue] == 1) {
            [followButton setSelected:YES];
        } else {
            [followButton setSelected:NO];
        }
        
        NSString *currentUser = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        
        if ([[friend objectForKey:@"email"] isEqualToString:currentUser])
        {
            [followButton setEnabled:NO];
        }
        
        if (![friend objectForKey:@"avatar"]) {
            [avatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            NSLog(@"don't set avatar, set placeholder");
        }
        else
        {
            if ([[friend objectForKey:@"avatar"] isEqualToString:@""] ||
                [[friend objectForKey:@"avatar"] isEqualToString:@"http://rain-app.com/upload/avatars/IOS_"]) {
                [avatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                NSLog(@"don't set avatar");
            }
            else {
                // set avatar
                NSString *avatarString = [friend objectForKey:@"avatar"];
                
                NSLog(@"set avatar from %@\n for %@", avatarString, [friend objectForKey:@"first_name"]);
                
                NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
                
                __weak UIImageView *weakAvatarImageView = avatarImageView;
                __weak NSDictionary *weakFriend = friend;
                
                [avatarImageView setImageWithURLRequest:photoRequest
                                             placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]
                                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                 {
                     [weakAvatarImageView setImage:image];
                     NSLog(@"success for %@", [weakFriend objectForKey:@"first_name"]);
                 }
                 
                                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                          [weakAvatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                                      }];
                
            }
        }
    }
}

- (void)followSuccess
{
    [self removeActivityIndicator];
    [followButton setSelected:!followButton.selected];
}

- (void)followFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    NSLog(@"server error");
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)showActivityIndicator
{
    [followButton setHidden:YES];
    activityView = [[UIActivityIndicatorView alloc] init];
    [activityView setColor:[UIColor colorWithRed:(35.0/255.0)
                                           green:(70.0/255.0)
                                            blue:(98.0/255.0)
                                           alpha:1.0]];
    [activityView setFrame:followButton.frame];
    [[self contentView] addSubview:activityView];
    [activityView startAnimating];
}

- (void)removeActivityIndicator
{
    [followButton setHidden:NO];
    [activityView stopAnimating];
    [activityView removeFromSuperview];
}

- (IBAction)follow:(id)sender
{
    [self showActivityIndicator];
    
    if ([[friend objectForKey:@"follow"] integerValue] == 1) {
        [API unfollowUserWithID:[friend objectForKey:@"id"]];
    } else {
        [API followUserWithID:[friend objectForKey:@"id"]];
    }
}

@end
