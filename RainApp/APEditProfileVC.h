//
//  APEditProfileVC.h
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "UIImageView+AFNetworking.h"

@interface APEditProfileVC : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIImageView *editPhotoImageView;
    IBOutlet UIImageView *hiddenImageView;
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *nicknameTextField;
    IBOutlet UITextField *dayTextField;
    IBOutlet UITextField *monthTextField;
    IBOutlet UITextField *yearTextField;
    IBOutlet UITextField *countryTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UIButton *maleButton;
    IBOutlet UIButton *femaleButton;
    IBOutlet UIButton *editPasswordButton;
    IBOutlet UIBarButtonItem *saveButton;
    UIBarButtonItem *activityButton;
    
    UIToolbar *dateToolbar;
    UIToolbar *countryToolbar;
    UIToolbar *cityToolbar;
    
    IBOutlet NSLayoutConstraint *firstNameYConstraint;
    IBOutlet NSLayoutConstraint *firstNameHeightConstraint;
    IBOutlet NSLayoutConstraint *lastNameTopSpaceContraint;
    IBOutlet NSLayoutConstraint *nickNameConstraint;
    IBOutlet NSLayoutConstraint *dateTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *countryTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *cityTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *sexButtonsTopSpaceConstraint;
    IBOutlet NSLayoutConstraint *editPasswordButtonTopSpaceConstraint;
    
    UIImage *originalAvatarImage;
    
    NSInteger gender;
    CGFloat keyboardAnimatedDistance;
    
    BOOL countriesDownloaded;
    BOOL citiesDownloaded;
    BOOL avatarChanged;
    
    NSInteger chosenCountryID;
    NSInteger chosenCityID;
    
    UIActivityIndicatorView *activityIndicator;
    
    APServerAPI *API;
    NSMutableDictionary *user;
    
    NSString *countryID;
    NSString *cityID;
}

@property (nonatomic, strong) NSMutableDictionary *user;

- (IBAction)editPhoto:(id)sender;
- (IBAction)maleButtonPress:(id)sender;
- (IBAction)femaleButtonPress:(id)sender;
- (IBAction)saveChanges:(id)sender;

@end
