//
//  APSearchFriendsVC.m
//  Rain
//
//  Created by EgorMac on 12/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APSearchFriendsVC.h"
#import "APTopVC.h"

@interface APSearchFriendsVC ()

@end

@implementation APSearchFriendsVC

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
    
    API = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchFriendsSuccess)
                                                 name:@"SearchFriendsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchFriendsFailed)
                                                 name:@"SearchFriendsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    API = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Success And Failed methods

- (void)searchFriendsSuccess
{
    NSLog(@"search friends success");
    [self removeActivityIndicator];
    [self performSegueWithIdentifier:@"backToTopVC" sender:self];
}

- (void)searchFriendsFailed
{
    NSLog(@"search friends failed");
    [self showAlertWithIdentifier:serverAlert];
    [self removeActivityIndicator];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
    [self removeActivityIndicator];
}

- (void)serverError
{
    NSLog(@"server error");
    [self showAlertWithIdentifier:serverError];
    [self removeActivityIndicator];
}

#pragma mark - Activity Indicator And Alerts

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)showActivityIndicator
{
    UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0)
                                               green:(93.0/255.0)
                                                blue:(120.0/255.0)
                                               alpha:1.0];
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:customBlueColor];
    [activity setTag:5];
    [activity setCenter:searchButton.center];
    searchButton.hidden = YES;
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    searchButton.hidden = NO;
}

#pragma mark - Helper Methods

- (void)configureUIElements
{
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateHighlighted];
    [[searchButton layer] setCornerRadius:3.0f];
    
    [searchTextField setPlaceholder:NSLocalizedString(@"nameOrNickname", nil)];
    UIView *textFieldPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    [searchTextField setLeftView:textFieldPaddingView];
    [searchTextField setLeftViewMode:UITextFieldViewModeAlways];
}

- (void)configureNavigationBar
{
    [[[self navigationController] navigationBar] setHidden:NO];

    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:18.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"searchFriends", nil)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];

    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textFieldIsEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

#pragma mark - IBAction Methods

- (IBAction)search:(id)sender
{
    NSLog(@"Search...");
    
    if ([self textFieldIsEmpty]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        [self showActivityIndicator];
        [API searchFriendWithName:searchTextField.text];
    }
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"backToTopVC"]) {
        APTopVC *topVC = [segue destinationViewController];
        topVC.isNeedDisplaySearchResults = YES;
    }
}


@end
