//
//  APAttachmentVC.h
//  Rain
//
//  Created by EgorMac on 05/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface APAttachmentVC : UIViewController
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIImageView *photoImageView;
    IBOutlet UIButton *doneButton;
    
    UIImage *photo;
    NSURL *videoURL;
    NSURL *imageURL;
}

@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

- (IBAction)saveToLibarary:(id)sender;
- (IBAction)done:(id)sender;

@end
