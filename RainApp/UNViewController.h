//
//  UNViewController.h
//  RetouchMe
//
//  Created by Nozdrinov Yegor 
//  Copyright (c) 2014 ISD. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UNViewController : UIViewController<UINavigationControllerDelegate>

@property (nonatomic,strong)UITableView *tb;
@property(nonatomic,assign)int selectedcount;

- (void)animateVisibleCells;
- (void)prepareVisibleCellsForAnimation;
@end
