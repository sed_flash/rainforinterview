//
//  APSignupStep1VC.h
//  Rain
//
//  Created by EgorMac on 17/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APSignupStep1VC : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *nicknameTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *confirmPasswordTextField;
    IBOutlet UILabel *registrationLabel;
    IBOutlet UILabel *acceptLabel;
    IBOutlet UIButton *acceptButton;
    IBOutlet UIButton *continueButton;
    
    IBOutlet NSLayoutConstraint *acceptButtonConstraint;
    IBOutlet NSLayoutConstraint *continueButtonConstraint;
    IBOutlet NSLayoutConstraint *rainLogoWidth;
    IBOutlet NSLayoutConstraint *rainLogoHeight;
    
    APServerAPI *API;
    
    CGFloat keyboardAnimatedDistance;
}

@property (nonatomic, strong) APServerAPI *API;

- (IBAction)acceptAgreement:(id)sender;

@end
