//
//  APForgotPasswordVC.m
//  Rain
//
//  Created by EgorMac on 18/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APForgotPasswordVC.h"
#import "APSendPasswordVC.h"

@interface APForgotPasswordVC ()

@end

@implementation APForgotPasswordVC

@synthesize API;

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchSuccess)
                                                 name:@"SearchSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchFailed)
                                                 name:@"SearchFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [self setUpUIElements];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper Methods

- (IBAction)search:(id)sender
{
    if ([self textFieldIsEmpty]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        NSString *userEmail = [emailTextField text];
        [API searchUserForRecovery:userEmail];
        [self showActivityIndicator];
    }
}

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textFieldIsEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    return result;
}


#pragma mark - Helper Methods

- (void)setUpUIElements
{
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    
    [cancelButton setTitleTextAttributes:@{
                                           NSFontAttributeName: [UIFont fontWithName:@"Arial" size:16.0],
                                           NSForegroundColorAttributeName: [UIColor whiteColor]
                                           } forState:UIControlStateNormal];
    
    [titleLabel setText:NSLocalizedString(@"enterEmail", nil)];
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateHighlighted];
    [[searchButton layer] setCornerRadius:3.0f];
    
    [emailTextField setPlaceholder:NSLocalizedString(@"email", nil)];
    UIView *textFieldLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    [emailTextField setLeftView:textFieldLeftView];
    [emailTextField setLeftViewMode:UITextFieldViewModeAlways];
}

- (void)searchSuccess
{
    [self performSegueWithIdentifier:@"showSendPasswordVC" sender:self];
    [self removeActivityIndicator];
}

- (void)searchFailed
{
    [self showAlertWithIdentifier:serverAlert];
    [self removeActivityIndicator];
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self showAlertWithIdentifier:serverError];
    [self removeActivityIndicator];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor colorWithRed:(93.0/255.0) green:(124.0/255.0) blue:(173.0/255.0) alpha:1.0]];
    [activity setTag:5];
    [activity setCenter:searchButton.center];
    [searchButton setHidden:YES];
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    [searchButton setHidden:NO];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showSendPasswordVC"]) {
        NSLog(@"set user data: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userForRecovery"]);
        [[segue destinationViewController] setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userForRecovery"]];
    }
}


@end
