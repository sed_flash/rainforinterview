//
//  APProfileVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "UIImageView+AFNetworking.h"
#import "APFriendsVC.h"
#import "APUserInfoVC.h"

@interface APProfileVC : UIViewController <UIScrollViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,DBCameraViewControllerDelegate>
{
    
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *containerView;
    IBOutlet UICollectionView *photosCollectionView;
    
    IBOutlet UIImageView *avatarImageView;
    
    IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
    
    IBOutlet UIImageView *accessImageView;
    
    IBOutlet UILabel *averageRateLabel;
    IBOutlet UILabel *followersCountLabel;
    IBOutlet UILabel *postsCountLabel;
    IBOutlet UILabel *followsCountLabel;
    
    IBOutlet UIImageView *badgeLabelBackground;
    IBOutlet UILabel *badgeLabel;
    
    IBOutlet UILabel *followersLabel;
    IBOutlet UILabel *followsLabel;
    IBOutlet UILabel *postsLabel;
    
    __weak IBOutlet UILabel *lblEdit;
    IBOutlet UIButton *editButton;
   IBOutlet UIButton *followButton;
    
    IBOutlet UIButton *dialogsButton;
    IBOutlet UIButton *sendMessageButton;
    
    IBOutlet UILabel *userBannedLabel;
    
    
    __weak IBOutlet UIImageView *imgBacground;
    
    
    NSString *userMail;
    
    NSMutableArray *userPosts;
    NSDictionary *user;
    
    BOOL userOwns;
    BOOL userFollowed;
    BOOL isNewPostsDownloaded;
    BOOL postsDownloading;
    
    UIImage *placeHolderImage;
    
    APServerAPI *API;
    
    UIActivityIndicatorView *followActivityView;
    UIRefreshControl *refreshControl;
    
    NSArray *followers;
    NSArray *followings;
    
    NSString *selectedUserEmail;
}


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *backgroundImageHeightConstraint;
@property (nonatomic, strong)DemoNavigationController * nav;

@property (nonatomic,strong)  UIActionSheet *bacgroundActionSheet;

@property (weak, nonatomic) IBOutlet UITableView *friendsTableView;

@property (nonatomic) BOOL userOwns;
@property (nonatomic, strong) NSString *userMail;
- (IBAction)btnPublickOnClick:(id)sender;
- (IBAction)btnFollowOnClick:(id)sender;

- (IBAction)btnFollowersOnClick:(id)sender;

- (IBAction)follow:(id)sender;
- (IBAction)sendMessage:(id)sender;

- (IBAction)btnBacgroundOnClick:(id)sender;


@end
