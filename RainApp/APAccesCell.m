//
//  APAccesCell.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APAccesCell.h"

@implementation APAccesCell

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    API = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileAccessSuccess)
                                                 name:@"ProfileAccessSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileAccessFailed)
                                                 name:@"ProfileAccessFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setAccessOn)
                                                 name:@"SetAccessOn"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setAccessOff)
                                                 name:@"SetAccessOff"
                                               object:nil];
    
    [accessLabel setText:NSLocalizedString(@"access", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchAccess:(id)sender
{
    NSDictionary *settings = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSettings"];
    
    int notifications = (int)[[settings objectForKey:@"notifications"] integerValue];
    
    if ([accessSwitch isOn]) {
        
        // Включить доступ
        NSLog(@"Включить доступ");
        [API sendProfileAccess:YES andPushNotifications:notifications];
    } else
    {
        // Выключить доступ
        NSLog(@"Выключить доступ");
        [API sendProfileAccess:NO andPushNotifications:notifications];
    }
}

- (void)setAccessOn
{
    NSLog(@"set access on (in cell)");
    [accessSwitch setOn:YES animated:YES];
}

- (void)setAccessOff
{
    NSLog(@"set access off (in cell)");
    [accessSwitch setOn:NO animated:YES];
}


- (void)profileAccessSuccess
{
    NSLog(@"profile access success");
}

- (void)profileAccessFailed
{
    NSLog(@"profile access failed");
    [accessSwitch setOn:!accessSwitch.isOn animated:YES];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
    [accessSwitch setOn:!accessSwitch.isOn animated:YES];
}

- (void)serverError
{
    NSLog(@"server error");
    [self showAlertWithIdentifier:serverError];
    [accessSwitch setOn:!accessSwitch.isOn animated:YES];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}


@end
