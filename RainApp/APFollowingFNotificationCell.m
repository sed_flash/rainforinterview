//
//  APFollowingFNotificationCell.m
//  Rain
//
//  Created by EgorMac on 28/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFollowingFNotificationCell.h"

@implementation APFollowingFNotificationCell
@synthesize secondUserButton;

- (void)setNotification:(NSDictionary *)newNotification
{
    [super setNotification:newNotification];
    
    NSDictionary *secondUser = [newNotification objectForKey:@"to"];
    if([secondUser isKindOfClass:[NSDictionary class]])
    {
    [descriptionLabel setText:NSLocalizedString(@"startedFollowing", nil)];
    [secondUserNameLabel setText:[secondUser objectForKey:@"nick"]];
    
//    [secondUserPhoto setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [UIImageView apa_useMaskToImageView:secondUserPhoto
                                   forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
    
    NSString *secondPhotoString;
    if ([[secondUser objectForKey:@"avatar_small"] isEqualToString:@""])
    {
        secondPhotoString = [secondUser objectForKey:@"avatar"];
    } else
    {
        secondPhotoString = [secondUser objectForKey:@"avatar_small"];
        if(!secondPhotoString)
        {
            secondPhotoString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
        }
    }
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:secondPhotoString]];
    
    __weak NSString *weakSecondPhotoString = secondPhotoString;
    __weak UIImageView *weakSecondPhotoImageView = secondUserPhoto;
    
    UIImage *placeHolderImage = [UIImage imageNamed:@"ProfileEditPhotoBlue"];
    
    [secondUserPhoto setImageWithURLRequest:photoRequest
                          placeholderImage:placeHolderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         NSLog(@"success for %@", weakSecondPhotoString);
//         [weakSecondPhotoImageView setImage:image];
         [UIImageView apa_useMaskToImageView:weakSecondPhotoImageView
                                    forImage:image];
     }
     
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                       NSLog(@"fail for %@, error: %@", weakSecondPhotoString, error);
//                                       [weakSecondPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                       [UIImageView apa_useMaskToImageView:weakSecondPhotoImageView
                                                                  forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                   }];
    }
}

@end
