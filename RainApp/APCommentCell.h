//
//  APCommentCell.h
//  Rain
//
//  Created by EgorMac on 03/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALView+PureLayout.h"
#import "PureLayoutDefines.h"

@interface APCommentCell : UITableViewCell
{
    IBOutlet UILabel *bodyLabel;
    IBOutlet UIImageView *photoView;
}

- (void)setCreatedAtDate:(NSString *)createdAt;

@property (strong, nonatomic) IBOutlet UILabel *bodyLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeAgoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *photoView;

@end
