//
//  APNoFriendsVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APNoFriendsVC : UIViewController
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UILabel *noFriendsLabel;
    IBOutlet UIButton *searchButton;
    
    APServerAPI *API;
    
}

@end
