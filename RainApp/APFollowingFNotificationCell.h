//
//  APFollowingFNotificationCell.h
//  Rain
//
//  Created by EgorMac on 28/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFollowCell.h"

@interface APFollowingFNotificationCell : APFollowCell
{
    IBOutlet UIImageView *secondUserPhoto;
    IBOutlet UIButton *secondUserButton;
    IBOutlet UILabel *secondUserNameLabel;
}

@property (nonatomic, strong) IBOutlet UIButton *secondUserButton;

@end
