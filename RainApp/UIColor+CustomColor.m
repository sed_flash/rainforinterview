//
//  UIColor+CustomColor.m
//  Rain
//
//  Created by EgorMac on 25/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+(UIColor *)rainBlueColor
{
    UIColor *color = [UIColor colorWithRed:(93.0/255.0)
                                     green:(124.0/255.0)
                                      blue:(173.0/255.0)
                                     alpha:1.0];
    return color;
}

+(UIColor *)rainPurple
{
    UIColor *color = [UIColor colorWithRed:(60.0/255.0)
                                     green:(80.0/255.0)
                                      blue:(144.0/255.0)
                                     alpha:1.0];
    return color;
}

+(UIColor *)rainGoldColor
{
    UIColor *color = [UIColor colorWithRed:(251.0/255.0)
                                     green:(195.0/255.0)
                                      blue:(10.0/255.0)
                                     alpha:1.0];
    return color;
}

+(UIColor *)rainLightGrayColor
{
    UIColor *color = [UIColor colorWithRed:255.0/255.0
                                     green:255.0/255.0
                                      blue:255.0/255.0
                                     alpha:0.3];
    return color;
}

@end
