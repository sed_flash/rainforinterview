//
//  APSignupStep2VC.m
//  Rain
//
//  Created by EgorMac on 18/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APSignupStep2VC.h"

@interface APSignupStep2VC ()

@end

@implementation APSignupStep2VC {
    BOOL isBirthdayPicker;
    BOOL isCountryPicker;
    BOOL isCityPeaker;
}

@synthesize API;

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static NSString *countriesAlert = @"countries";
static NSString *citiesAlert = @"cities";
static const NSString *birthdayPicker = @"birthdayPicker";
static const NSString *countryPicker = @"countryPicker";
static const NSString *cityPicker = @"cityPicker";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const int COUNTRIES_PICKER_TAG = 1;
static const int CITIES_PICKER_TAG = 2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    if ([[UIScreen mainScreen] bounds].size.height <= 480.0f) {
        [firstNameYConstraint setConstant:0.0f];
        [firstNameHeightConstraint setConstant:30.0f];
        [lastNameTopSpaceContraint setConstant:7.0f];
        [dateTopSpaceConstraint setConstant:7.0f];
        [countryTopSpaceConstraint setConstant:7.0f];
        [cityTopSpaceConstraint setConstant:7.0f];
        [sexButtonsTopSpaceConstraint setConstant:18.0f];
        [signupButtonTopSpaceConstraint setConstant:18.0f];
        [rainLogoHeight setConstant:105.7f];
        [rainLogoWidth setConstant:121.8f];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    
    // Do any additional setup after loading the view.
    API = [[APServerAPI alloc] init];
    chosenCountryID = 0;
    chosenCityID = 0;
    gender = 1;
    
    [firstNameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [lastNameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    UIImageView *imgView = maleButton.imageView;
    [imgView setImage:[UIImage imageNamed:@"maleButtonOff"]];
    imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imgView setTintColor:[UIColor colorWithRed:93.0f/255.0f green:124.0f/255.0f blue:173.0f/255.0f alpha:1.0f]];
    [maleButton setImage:imgView.image forState:UIControlStateSelected];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledSuccess)
                                                 name:@"ProfileFilledSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledFailed)
                                                 name:@"ProfileFilledFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesSuccess)
                                                 name:@"CountriesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesFailed)
                                                 name:@"CountriesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesSuccess)
                                                 name:@"CitiesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesFailed)
                                                 name:@"CitiesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count] > 0) {
        countriesDownloaded = YES;
        [self configureCountryPicker];
    }
    else
    {
        countriesDownloaded = NO;
        [API getCountries];
    }
    [self setupUIElements];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard notification

- (void)willShowKeyboard:(NSNotification *)notification {
    if (!dateToolbar.isHidden && ([firstNameTextField isFirstResponder] || [lastNameTextField isFirstResponder])) {
        if (isBirthdayPicker) {
            [self hideDatePickerAndToolbar:nil];
        } else if (isCountryPicker) {
            [self hideCountryPickerAndToolbar:nil];
        } else if (isCityPeaker) {
            [self hideCityPickerAndToolbar:nil];
        }
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setKeyboardType:UIKeyboardTypeDefault];
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat viewHeight, bottomOfTextField, topOfKeyboard;
    
    viewHeight = viewRect.size.height;
    bottomOfTextField = textFieldRect.origin.y + textFieldRect.size.height;
    topOfKeyboard = viewHeight - PORTRAIT_KEYBOARD_HEIGHT - 10; // 10 point padding
    
    if (bottomOfTextField >= topOfKeyboard) keyboardAnimatedDistance = bottomOfTextField - topOfKeyboard;
    else keyboardAnimatedDistance = 0.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == dayTextField || textField == monthTextField || textField == yearTextField)
    {
        //add toolbar
        [self addBirthdayToolbar];
    }
    
    if (textField == countryTextField) {
        
        [self setActivePicker:countryPicker];
        [dateToolbar removeFromSuperview];
        [cityToolbar removeFromSuperview];
        if (!countriesDownloaded) {
            activityIndicator = [[UIActivityIndicatorView alloc] init];
            CGRect frame = CGRectMake(0, 0, 50, 50);
            [activityIndicator setFrame:frame];
            [activityIndicator setCenter:CGPointMake(self.view.center.x, self.view.center.y)];
            UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
            [activityIndicator setColor:customBlueColor];
            [[self view] addSubview:activityIndicator];
            [activityIndicator startAnimating];
        } else {
            countryToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                          self.view.frame.size.height,
                                                                          self.view.frame.size.width,
                                                                          40)];
            [countryToolbar setBarStyle:UIBarStyleDefault];
            [countryToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
            [countryToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCountryPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            countryToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:countryToolbar];
                 CGRect frame = countryToolbar.frame;
                 frame.origin.y = countryToolbar.frame.origin.y - 216.0f - frame.size.height + keyboardAnimatedDistance;
                 countryToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
    if (textField == cityTextField) {
        [self setActivePicker:cityPicker];
        
        [dateToolbar removeFromSuperview];
        [countryToolbar removeFromSuperview];
        if (!citiesDownloaded) {
            
            if ([[countryTextField text] isEqualToString:@""]) {
                [cityTextField resignFirstResponder];
                [self showAlertWithIdentifier:@"noCountryAlert"];
            } else {
                [textField resignFirstResponder];
                activityIndicator = [[UIActivityIndicatorView alloc] init];
                [activityIndicator setFrame:textField.frame];
                [activityIndicator setCenter:textField.center];
                UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
                [activityIndicator setColor:customBlueColor];
                [[self view] addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
            
        } else {
            
            cityToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                       self.view.frame.size.height,
                                                                       self.view.frame.size.width,
                                                                       40)];
            [cityToolbar setBarStyle:UIBarStyleDefault];
            [cityToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
            [cityToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCityPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            cityToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:cityToolbar];
                 CGRect frame = cityToolbar.frame;
                 frame.origin.y = self.view.frame.size.height - 216.0f - frame.size.height + keyboardAnimatedDistance;
                 cityToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
}

- (void)addBirthdayToolbar {
    [self setActivePicker:birthdayPicker];
    [cityToolbar removeFromSuperview];
    [countryToolbar removeFromSuperview];
    
    dateToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                               self.view.frame.size.height,
                                                               self.view.frame.size.width,
                                                               40)];
    [dateToolbar setBarStyle:UIBarStyleDefault];
    [dateToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
    [dateToolbar setTranslucent:NO];
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                       style:UIBarButtonItemStyleDone target:nil
                                                                      action:@selector(hideDatePickerAndToolbar:)];
    [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                              NSForegroundColorAttributeName: [UIColor blackColor]}
                                  forState:UIControlStateNormal];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    dateToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options:UIViewAnimationOptionTransitionNone
                     animations:^
     {
         [self.view addSubview:dateToolbar];
         CGRect frame = dateToolbar.frame;
         frame.origin.y = dateToolbar.frame.origin.y - 216.0f - frame.size.height + keyboardAnimatedDistance;
         dateToolbar.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         
     }];
}

- (void)setActivePicker:(NSString *)pickerName {
    if ([birthdayPicker isEqualToString:pickerName]) {
        isBirthdayPicker = YES;
        isCountryPicker = NO;
        isCityPeaker = NO;
    } else if ([countryPicker isEqualToString:pickerName]) {
        isBirthdayPicker = NO;
        isCountryPicker = YES;
        isCityPeaker = NO;
    } else if ([cityPicker isEqualToString:pickerName]){
        isBirthdayPicker = NO;
        isCountryPicker = NO;
        isCityPeaker = YES;
    } else {
        isBirthdayPicker = NO;
        isCountryPicker = NO;
        isCityPeaker = NO;
    }
}

- (void)hideCountryPickerAndToolbar:(id)sender
{
    UIPickerView *picker = (UIPickerView *)countryTextField.inputView;
    if ([[countryTextField text] isEqualToString:@""]) {
        
        [countryTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:0] objectForKey:@"title"]];
        countryID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:0] objectForKey:@"id"] stringValue];
    }
    
    [countryTextField resignFirstResponder];
    [countryToolbar removeFromSuperview];
    countryToolbar = nil;
    
    [self updateCountryTextField:picker];
    [self setActivePicker:nil];
}

- (void)updateCountryTextField:(id)sender
{
    [API getCitiesForCountryID:countryID];
    chosenCountryID = [countryID integerValue];
    chosenCityID = 0;
    [cityTextField setText:@""];
    [cityTextField setInputView:nil];
    citiesDownloaded = NO;
}

- (void)hideCityPickerAndToolbar:(id)sender
{
    if ([[cityTextField text] isEqualToString:@""]) {
        [cityTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:0] objectForKey:@"title"]];
        cityID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:0] objectForKey:@"id"] stringValue];
    }
    
    [cityTextField resignFirstResponder];
    [cityToolbar removeFromSuperview];
    cityToolbar = nil;
    
    UIDatePicker *picker = (UIDatePicker *)cityTextField.inputView;
    [self updateCityTextField:picker];
    [self setActivePicker:nil];
}

- (void)updateCityTextField:(id)sender
{
    [cityTextField resignFirstResponder];
    chosenCityID = [cityID integerValue];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldIsEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

#pragma mark - UIPickerView Datasource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG) {
        int countriesCount;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count] > 0) {
            countriesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count];
        } else {
            countriesCount = 0;
        }
        
        return countriesCount;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        int citiesCount;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] count] > 0) {
            citiesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] count];
        } else {
            citiesCount = 0;
        }
        
        return citiesCount;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in numberOfRows method");
        return 0;
    }
}

#pragma mark - UIPickerView Delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSString *countryName = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"title"];
        return countryName;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        NSString *cityName = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"title"];
        return cityName;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
        return @"";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"did select");
    
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSLog(@"did select country");
        [countryTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"title"]];
        countryID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"id"] stringValue];
//        [API getCitiesForCountryID:countryID];
//        chosenCountryID = [countryID integerValue];
//        chosenCityID = 0;
//        [cityTextField setText:@""];
//        [cityTextField setInputView:nil];
//        citiesDownloaded = NO;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        [cityTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"title"]];
        cityID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"id"] stringValue];
//        [cityTextField resignFirstResponder];
//        chosenCityID = [cityID integerValue];
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
    }
}

#pragma mark - Helper Methods

- (void)setupUIElements
{
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[self navigationController] setNavigationBarHidden:NO];
    
    [self configureDatePicker];
    
    [firstNameTextField setPlaceholder:NSLocalizedString(@"firstname", nil)];
    [lastNameTextField setPlaceholder:NSLocalizedString(@"lastname", nil)];
    [dayTextField setPlaceholder:NSLocalizedString(@"day", nil)];
    [monthTextField setPlaceholder:NSLocalizedString(@"month", nil)];
    [yearTextField setPlaceholder:NSLocalizedString(@"year", nil)];
    [countryTextField setPlaceholder:NSLocalizedString(@"country", nil)];
    [cityTextField setPlaceholder:NSLocalizedString(@"city", nil)];
    
    for (UIView *subview in self.view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]] &&
            subview != monthTextField &&
            subview != yearTextField)
        {
            UIView *textFieldLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
            [(UITextField *)subview setLeftView:textFieldLeftView];
            [(UITextField *)subview setLeftViewMode:UITextFieldViewModeAlways];
        }
    }
    
    [[signupButton layer] setCornerRadius:3.0f];
    [signupButton setTitle:NSLocalizedString(@"signup", nil) forState:UIControlStateNormal];
    [signupButton setTitle:NSLocalizedString(@"signup", nil) forState:UIControlStateHighlighted];
    
//    [maleButton setSelected:YES];
    
}

- (void)configureDatePicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self
                   action:@selector(updateDateTextFields:)
         forControlEvents:UIControlEventValueChanged];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    NSLocale *ukLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    [currentCalendar setLocale:ukLocale];
    [datePicker setCalendar:currentCalendar];
    [datePicker setMaximumDate:[NSDate date]];
    [dayTextField setInputView:datePicker];
    [monthTextField setInputView:datePicker];
    [yearTextField setInputView:datePicker];
}

- (void)configureCountryPicker
{
    UIPickerView *countryPicker = [[UIPickerView alloc] init];
    [countryPicker setTag:COUNTRIES_PICKER_TAG];
    [countryPicker setDelegate:self];
    [countryTextField setInputView:countryPicker];
    NSLog(@"configure countries picker");
    NSLog(@"country text field input view is %@", countryTextField.inputView);
}

- (void)configureCitiesPicker
{
    NSLog(@"configureCitiesPicker");
    UIPickerView *citiesPicker = [[UIPickerView alloc] init];
    [citiesPicker setTag:CITIES_PICKER_TAG];
    [citiesPicker setDelegate:self];
    [cityTextField setInputView:citiesPicker];
}

- (void)updateDateTextFields:(id)sender
{
    UIDatePicker *picker = (UIDatePicker *)dayTextField.inputView;
    
    NSDateFormatter *dayDateFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *yearDateFormatter = [[NSDateFormatter alloc] init];
    [dayDateFormatter setDateFormat:@"dd"];
    [monthDateFormatter setDateFormat:@"MMMM"];
    [yearDateFormatter setDateFormat:@"yyyy"];
    NSDate *date = [picker date];
    [dayTextField setText:[NSString stringWithFormat:@"%@", [dayDateFormatter stringFromDate:date]]];
    [monthTextField setText:[NSString stringWithFormat:@"%@", [monthDateFormatter stringFromDate:date]]];
    [yearTextField setText:[NSString stringWithFormat:@"%@", [yearDateFormatter stringFromDate:date]]];
}

- (void)hideDatePickerAndToolbar:(id)sender
{
    [dayTextField resignFirstResponder];
    [monthTextField resignFirstResponder];
    [yearTextField resignFirstResponder];
    [dateToolbar removeFromSuperview];
    dateToolbar = nil;
    
    UIDatePicker *picker = (UIDatePicker *)dayTextField.inputView;
    [self updateDateTextFields:picker];
    [self setActivePicker:nil];
}

- (void)filledSuccess
{
    [self removeActivity];
    [self performSegueWithIdentifier:@"showTabBarVC" sender:self];
}

- (void)filledFailed
{
    [self removeActivity];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)countriesSuccess
{
    countriesDownloaded = YES;
    if ([[countryTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[countryTextField inputView] stopAnimating];
    }
    [self configureCountryPicker];
}

- (void)citiesSuccess
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    
    NSLog(@"citiesSuccess method");
    citiesDownloaded = YES;
    if ([[cityTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[cityTextField inputView] stopAnimating];
    }
    [self configureCitiesPicker];
}

- (void)countriesFailed
{
    [countryTextField resignFirstResponder];
    [self showAlertWithIdentifier:countriesAlert];
}

- (void)citiesFailed
{
    [self showAlertWithIdentifier:citiesAlert];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

- (void)networkError
{
    [self removeActivity];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivity];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError] ||
             [identifier isEqualToString:countriesAlert] ||
             [identifier isEqualToString:citiesAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if ([identifier isEqualToString:@"noCountryAlert"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"chooseTheCountry", nil)
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)showActivity
{
    UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0)
                                               green:(93.0/255.0)
                                                blue:(120.0/255.0)
                                               alpha:1.0];
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:customBlueColor];
    [activity setTag:5];
    [activity setCenter:signupButton.center];
    signupButton.hidden = YES;
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivity
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [activity removeFromSuperview];
    signupButton.hidden = NO;
}

#pragma mark - IBAction Methods

- (IBAction)signup:(id)sender
{
    if ([self textFieldIsEmpty]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        NSString *firstName = [firstNameTextField text];
        NSString *lastName = [lastNameTextField text];
        NSString *birthday = [NSString stringWithFormat:@"%@ %@ %@", [dayTextField text], [monthTextField text], [yearTextField text]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MM yyyy"];
        NSDate *birthDate = [dateFormatter dateFromString:birthday];
        NSLog(@"birthDate is %@", birthDate);
        
        NSDateFormatter *enDateFormatter = [[NSDateFormatter alloc] init];
        [enDateFormatter setDateFormat:@"dd MMMM yyyy"];
        [enDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
        birthday = [enDateFormatter stringFromDate:birthDate];
        
        NSMutableDictionary *filledProfile = [[NSMutableDictionary alloc] init];
        [filledProfile setObject:firstName forKey:@"firstName"];
        [filledProfile setObject:lastName forKey:@"lastName"];
        [filledProfile setObject:birthday forKey:@"birthday"];
        [filledProfile setObject:[NSNumber numberWithInt:gender] forKey:@"gender"];
        [filledProfile setObject:[NSNumber numberWithInt:chosenCountryID] forKey:@"countryID"];
        [filledProfile setObject:[NSNumber numberWithInt:chosenCityID] forKey:@"cityID"];
        
        [[NSUserDefaults standardUserDefaults] setObject:filledProfile forKey:@"filledProfileData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showActivity];
        
        [API fillProfile];
    }
}

- (IBAction)maleButtonPress:(id)sender
{
    if (!maleButton.selected) {
        [maleButton setSelected:YES];
        [femaleButton setSelected:NO];
        gender = 1;
    }
}

- (IBAction)femaleButtonPress:(id)sender
{
    if (!femaleButton.selected) {
        [femaleButton setSelected:YES];
        [maleButton setSelected:NO];
        gender = 2;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
