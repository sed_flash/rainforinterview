//
//  APProfilePhotoCell.m
//  Rain
//
//  Created by EgorMac on 27/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APProfilePhotoCell.h"

@implementation APProfilePhotoCell

@synthesize photoImageView;

- (instancetype)initWithFrame:(CGRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
