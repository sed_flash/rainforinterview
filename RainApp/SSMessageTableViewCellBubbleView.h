//
//  SSMessageTableViewCellBubbleView.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessageTableViewCell.h"

@interface SSMessageTableViewCellBubbleView : UIView {

@private
	
	NSString *_messageText;
    NSData *_avatarData;
    NSString *_attachmentURLString;
    UIImage *_attachmentImage;
	UIImage *_leftBackgroundImage;
	UIImage *_rightBackgroundImage;
	SSMessageStyle _messageStyle;
    
    UIImageView *avatarImageView;
    UIImageView *dropImageView;
    
    UIImageView *attachmentImageView;
}

@property (nonatomic, copy) NSString *messageText;
@property (nonatomic, retain) UIImage *leftBackgroundImage;
@property (nonatomic, retain) UIImage *rightBackgroundImage;
@property (nonatomic, copy) NSData *avatarData;
@property (nonatomic, copy) NSString *attachmentURLString;
@property (nonatomic, copy) UIImage *attachmentImage;
@property (nonatomic, assign) SSMessageStyle messageStyle;

+ (CGSize)textSizeForText:(NSString *)text;
+ (CGSize)bubbleSizeForText:(NSString *)text;
+ (CGFloat)cellHeightForText:(NSString *)text;

@end
