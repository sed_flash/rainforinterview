//
//  APProfileVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APProfileVC.h"
#import "APEditProfileVC.h"
#import "APProfilePhotoCell.h"
#import "APPostVC.h"
#import "APConversationVC.h"
#import "UserBGImage.h"

@interface APProfileVC ()
@property (strong, nonatomic) UIImageView *tempImageView;
@end

@implementation APProfileVC {
    int  Fool;
}

@synthesize userOwns;
@synthesize userMail;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    user = [[NSDictionary alloc] init];
    [[self view] setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self configureNavigationBar];
    [self setUIElementsHidden];
    placeHolderImage = [self placeHolderImage];
    
    [editButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:NO];
    
    API = [[APServerAPI alloc] init];
    isNewPostsDownloaded = NO;
    userFollowed = NO;
    
    Fool = -1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersSuccess)
                                                 name:@"FollowersSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersFailed)
                                                 name:@"FollowersFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followingsSuccess)
                                                 name:@"FollowingsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followingsFailed)
                                                 name:@"FollowingsFailed"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileSuccess)
                                                 name:@"SearchSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileFailed)
                                                 name:@"SearchFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followSuccess)
                                                 name:@"FollowSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followFailed)
                                                 name:@"FollowFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    
    if (userOwns == YES ||
        (!userMail))
    {
        userMail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        userOwns = YES;
    }
    
    [self setupONLYUIElements];
    
    NSLog(@"userMail is %@", userMail);
    
    if (refreshControl)
    {
        [refreshControl removeFromSuperview];
    }
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(checkForNewPosts)
             forControlEvents:UIControlEventValueChanged];
    [scrollView addSubview:refreshControl];
    
    if ([photosCollectionView numberOfItemsInSection:0] < 10) {
        if ([photosCollectionView numberOfItemsInSection:0] == 0)
        {
            [self showActivityIndicator];
        }
        [API showUser:userMail withOffset:0 andLimit:18];
    }
    [self configureNavigationBar];
    [self checkForNewPosts];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = self.friendsTableView;
    controller.refreshControl = refreshControl;
    [self showActivityIndicator];
    [API getFollowersForUserWithEmail:userMail];

    self.friendsTableView.hidden=YES;
    Fool=2;
}




- (void)followersSuccess
{
    
//    [self removeActivityIndicator];
    if(Fool == 0)
    {
        followers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followers"] copy];
    }
    else
    {
        followings = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followings"] copy];
    }
    
    [refreshControl endRefreshing];
    [self setNewTableViewHeight];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    if (Fool==0)
    {
        count = [followers count];
    }
    else
    {
        count = [followings count];
    }
    
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APFriendCell *cell = (APFriendCell *)[tableView dequeueReusableCellWithIdentifier:@"FreindsCell"];
    
    NSDictionary *friend;
    
    if (Fool==0)
    {
        if ([indexPath row] > followers.count) {
            friend = [followers objectAtIndex:0];
        }
        else
        {
            friend = [followers objectAtIndex:[indexPath row]];
        }
    } else
    {
        if ([indexPath row] > followings.count)
        {
            friend = [followings objectAtIndex:0];
        } else {
            friend = [followings objectAtIndex:[indexPath row]];
        }
    }
    
    [cell setFriend:friend];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *friend;
    
    if (Fool== 0)
    {
        friend = [followers objectAtIndex:[indexPath row]];
    }
    else
    {
        friend = [followings objectAtIndex:[indexPath row]];
    }
    
    selectedUserEmail = [friend objectForKey:@"email"];
    NSString *ownerEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
    APProfileVC *pvc = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"profileController"];
    [pvc setUserMail:selectedUserEmail];
    if ([ownerEmail isEqualToString:selectedUserEmail])
        [pvc setUserOwns:YES];
    else
        [pvc setUserOwns:NO];
    [self.navigationController pushViewController:pvc animated:YES];
}


- (void)followingsSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
    if(Fool == 0)
    {
        followers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followers"] copy];
    }
    else
    {
        followings = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followings"] copy];
    }
    
    [refreshControl endRefreshing];
    [self setNewTableViewHeight];
}

- (void)followingsFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)followersFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}


- (void)setNewTableViewHeight {
    [UIView animateWithDuration:0 animations:^{
        [self.friendsTableView reloadData];
    } completion:^(BOOL finished) {
        
        // If clicked button is Following or Followers and
        // page is not loaded right now, so friendsTableView shown
        if ((Fool == 0 || Fool == 1) && Fool != -1)
            _friendsTableView.hidden = NO;
        float tableContentHeight = _friendsTableView.contentSize.height;
        CGRect rect = _friendsTableView.frame;
        rect.size.height = tableContentHeight;
        _friendsTableView.frame = rect;
        
        if (Fool != 2)
        [scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame),
                                              CGRectGetHeight(imgBacground.frame) + tableContentHeight)];
    }];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)checkForNewPosts
{
    [API showUser:userMail withOffset:0 andLimit:18];
}

#pragma mark - alerts

- (void)profileSuccess
{
    NSLog(@"profile success");
    user = [[NSUserDefaults standardUserDefaults] objectForKey:@"foundedUser"];
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *publicDatabase = [container publicCloudDatabase];

    int userID = [[user objectForKey:@"id"] intValue];
    UserBGImage *userBG = [UserBGImage MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userID]
                                                        sortedBy:@"imageDate"
                                                       ascending:NO
                                                       inContext:[NSManagedObjectContext MR_rootSavingContext]];
//    NSArray *arr = [UserBGImage MR_findAllInContext:[NSManagedObjectContext MR_rootSavingContext]];
//    
//    int i = 1;
//    for (UserBGImage *c in arr) {
//        NSLog(@"date %d) %f", i, c.imageDate.timeIntervalSince1970);
//        i++;
//    }
//    
//    NSLog(@"LAST DATE IS %f", userBG.imageDate.timeIntervalSince1970);
   
    if (userBG) {
        dispatch_async(dispatch_get_main_queue(), ^{
            imgBacground.image = [UIImage imageWithData:userBG.userImage];
        });
    }
    
    NSString *idUser=[[user objectForKey:@"id"] stringValue];
        if(idUser)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID=%@",idUser];
        
            CKQuery *query = [[CKQuery alloc] initWithRecordType:@"Config" predicate:predicate];
            [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error)
             {
                 if (!error)
                 {
                     if(results.count>0)
                     {
                         CKRecord *artworkRecord=[results lastObject];
                         
                         CKAsset *asset=[artworkRecord objectForKey:@"BacgroundImage"];
                         if(asset)
                         {
                             NSData *imageData=[NSData dataWithContentsOfURL:asset.fileURL];
                             if(imageData)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     // This condition is true if user != own  and if Core Data entity is empty (if delete this
                                     // condition at first entrance image won't be installed)
                                     if (!userOwns || !userBG) {
                                         imgBacground.image = [UIImage imageWithData:imageData];
                                         
                                         // If current image data not equal last image in Core Data
                                         // so add one
                                         if (![imageData isEqualToData:userBG.userImage]) {
                                            [self saveBackgroundImage:imgBacground.image
                                                toCoreDataWithUserId:userID];
                                        }
                                     }
                                 });
//                                 [self.view layoutIfNeeded];
//                                 imgBacground.image = [UIImage imageWithData:imageData];
//                                 [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imgBacground.image) forKey:@"imageData"];
                             }
                         }
                     }
                 }
             }];
        }
    
    
             

    [self removeActivityIndicator];
    [self removeFollowActivityIndicator];
    postsDownloading = NO;
    
    if ([self isBanned])
    {
        NSLog(@"user is banned");
        [photosCollectionView setHidden:YES];
        userPosts = nil;
        [refreshControl endRefreshing];
        [photosCollectionView reloadData];
        [userBannedLabel setText:NSLocalizedString(@"userIsBanned", nil)];
        [userBannedLabel setHidden:NO];
        [accessImageView setHidden:YES];
        
    }
    else
    {
        [userBannedLabel setHidden:YES];
        if ([self isPrivate])
        {
            NSLog(@"private access");
            [photosCollectionView setHidden:YES];
            userPosts = nil;
            [refreshControl endRefreshing];
            [photosCollectionView reloadData];
            [accessImageView setHidden:NO];
            [sendMessageButton setEnabled:NO];
        }
        else
        {
            NSLog(@"open profile");
            if (Fool == 2)
                [photosCollectionView setHidden:NO];
            [accessImageView setHidden:YES];
            [sendMessageButton setEnabled:YES];
            [self setDataSource];
        }
    }


    
    [self setupUIElements];
    [self removePaginationIndicator];
}

- (void)profileFailed
{
    NSLog(@"profile failed");
    isNewPostsDownloaded = NO;
    postsDownloading = NO;
    [self showAlertWithIdentifier:serverAlert];
    [self removeActivityIndicator];
    [self removeFollowActivityIndicator];
    [self removePaginationIndicator];
}

- (void)followSuccess
{
    NSLog(@"follow success");
    [self checkForNewPosts];
//    [self removeFollowActivityIndicator];
}

- (void)followFailed
{
    [self removeFollowActivityIndicator];
    NSLog(@"follow failed");
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
    [self removeActivityIndicator];
    [self removeFollowActivityIndicator];
    [self removePaginationIndicator];
}

- (void)serverError
{
    NSLog(@"unknown server error");
    [self showAlertWithIdentifier:serverError];
    [self removeActivityIndicator];
    [self removeFollowActivityIndicator];
    [self removePaginationIndicator];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        } else {
            NSArray *errorMessageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
            errorMessage = @"";
            for (NSString *currentError in errorMessageArray) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"\n%@", currentError]];
            }
        }
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Activity Indicator

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] init];
    [activityIndicator setColor:[UIColor whiteColor]];
    [activityIndicator setTag:5];
    
    CGPoint center = CGPointMake(self.view.center.x, [[UIScreen mainScreen] bounds].size.height/2-activityIndicator.frame.size.height/2);
    
    [activityIndicator setCenter:center];
    [[self view] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)showPaginationIndicator
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] init];
    [activityIndicator setColor:[UIColor whiteColor]];
    [activityIndicator setTag:7];
    [activityIndicator setFrame:CGRectMake(self.view.center.x - 50, photosCollectionView.frame.origin.y + photosCollectionView.frame.size.height, 100, 100)];
    [scrollView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)showFollowActivityIndicator
{
    [followButton setHidden:YES];
    followActivityView = [[UIActivityIndicatorView alloc] init];
    [followActivityView setColor:[UIColor colorWithRed:(35.0/255.0)
                                                 green:(70.0/255.0)
                                                  blue:(98.0/255.0)
                                                 alpha:1.0]];
    [followActivityView setFrame:followButton.frame];
    [scrollView addSubview:followActivityView];
    [followActivityView startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activityIndicator stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    [[[self view] viewWithTag:3] removeFromSuperview];
}

- (void)removePaginationIndicator
{
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[[self view] viewWithTag:7];
    [activityIndicator stopAnimating];
    [[[self view] viewWithTag:7] removeFromSuperview];
}

- (void)removeFollowActivityIndicator
{
    [followButton setHidden:NO];
    [followActivityView stopAnimating];
    [followActivityView removeFromSuperview];
    [photosCollectionView reloadData];
}

- (UIImage *)placeHolderImage
{
    CGSize imageSize = CGSizeMake(self.view.frame.size.width/3, self.view.frame.size.width/3);
    UIColor *fillColor = [UIColor colorWithWhite:1.0 alpha:0.4];
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [fillColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


#pragma mark - Collection View Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
//    NSLog(@"minimum interitem spacing method");
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
//    NSLog(@"minimum line spacing spacing method");
    return 0.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"minimum size for item method");
    CGSize size = CGSizeMake(self.view.frame.size.width/3, self.view.frame.size.width/3);
    return size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // posts count
    NSLog(@"posts count - %lu", (unsigned long)[userPosts count]);
    NSLog(@"user posts - %@", userPosts);
    return [userPosts count];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *currentPostID = [userPostsIDs objectAtIndex:[indexPath item]];
}

#pragma mark - Collection View DataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cells count - %lu", (long)[photosCollectionView numberOfItemsInSection:0]);
    NSLog(@"cell is %lu", (long)indexPath.row);
    
//    NSLog(@"cell for row method");
    
    APProfilePhotoCell *photoCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"profilePhotoCell" forIndexPath:indexPath];
    
    NSDictionary *currentPost = [userPosts objectAtIndex:[indexPath row]];
    
    NSString *photoString = [currentPost objectForKey:@"small_image"];
    if ([photoString isEqualToString:@"http://rain-app.com"]) {
        photoString = [currentPost objectForKey:@"path"];
    }
    
    // if content is video
    
    if ([[currentPost objectForKey:@"path"] hasPrefix:@"http://rain-app.com/media/video"]) {
        [[photoCell videoImageView] setHidden:NO];
        [[photoCell videoImageView] setImage:[UIImage imageNamed:@"videoIcon.png"]];
    } else {
        [[photoCell videoImageView] setHidden:YES];
    }
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
    
    __weak UIImageView *weakImageView = [photoCell photoImageView];
    __weak NSString *weakPhotoString = photoString;
    
    [[photoCell photoImageView] setImageWithURLRequest:photoRequest
                                      placeholderImage:[UIImage new]
                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         [weakImageView setAlpha:1.0f];
         [weakImageView setImage:image];
         
         [UIView animateWithDuration:0.5
                               delay:0.1
                             options:UIViewAnimationOptionTransitionNone
                          animations:^
          {
              [weakImageView setAlpha:1.0f];
          }
                          completion:^(BOOL finished)
          {
              //              NSLog(@"animation finished");
          }];
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                   NSLog(@"fail for %@", weakPhotoString);
                                               }];
    return photoCell;
}

#pragma mark - Helper Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    float bottomEdge = aScrollView.contentOffset.y + aScrollView.frame.size.height;
    if (bottomEdge >= aScrollView.contentSize.height &&
        postsDownloading == NO) {
        NSLog(@"we are at the bottom");
        [self getNewPosts];
    }
}


- (void)getNewPosts
{
    NSLog(@"userposts count = %lu", (long)[photosCollectionView numberOfItemsInSection:0]);
    NSLog(@"user stats posts = %lu", (long)[[[user objectForKey:@"stats"] objectForKey:@"posts"] integerValue]);
    if ([photosCollectionView numberOfItemsInSection:0] == [[[user objectForKey:@"stats"] objectForKey:@"posts"] integerValue]) {
        NSLog(@"All posts downloaded. Nothing to do.");
    } else {
        isNewPostsDownloaded = YES;
        postsDownloading = YES;
        [self showPaginationIndicator];
        [API showUser:userMail withOffset:userPosts.count andLimit:18];
    }
}

- (void)setUIElementsHidden
{
    [[self navigationItem] setTitle:@""];
    [avatarImageView setHidden:YES];
    [photosCollectionView setHidden:YES];
    [averageRateLabel setHidden:YES];
    [followersCountLabel setHidden:YES];
    [postsCountLabel setHidden:YES];
    [followsCountLabel setHidden:YES];
    [badgeLabel setHidden:YES];
    [badgeLabelBackground setHidden:YES];
    [followersLabel setHidden:YES];
    [followsLabel setHidden:YES];
    [postsLabel setHidden:YES];
    [editButton setHidden:YES];
    [followButton setHidden:YES];
}

- (void)setDataSource
{
    NSLog(@"set data source");
    
    NSMutableArray *tempPostsArray = [[NSMutableArray alloc] init];
    tempPostsArray = [userPosts mutableCopy];
    
    
    if (isNewPostsDownloaded) {
        for (NSDictionary *post in [user objectForKey:@"posts"])
        {
            [tempPostsArray addObject:post];
        }
    } else {
        tempPostsArray = [[user objectForKey:@"posts"] mutableCopy];
    }
    
    tempPostsArray = [self removeDuplicatesFromPostsArray:tempPostsArray];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *postsArray = [NSArray array];
    postsArray = [tempPostsArray sortedArrayUsingDescriptors:sortDescriptors];
    
    userPosts = [postsArray copy];
    tempPostsArray = nil;
    postsArray = nil;
    
    [refreshControl endRefreshing];
    [photosCollectionView reloadData];
    [photosCollectionView reloadItemsAtIndexPaths:[photosCollectionView indexPathsForVisibleItems]];
    
    
    if (photosCollectionView.contentSize.height > 0) {
//        [collectionViewHeightConstraint setConstant:photosCollectionView.contentSize.height];
        float f = photosCollectionView.contentSize.height;
        collectionViewHeightConstraint.constant = photosCollectionView.contentSize.height;
        [scrollView setContentSize:CGSizeMake(CGRectGetWidth(scrollView.frame), f + 160)];
    } else {
        [collectionViewHeightConstraint setConstant:(userPosts.count/3+1)*320/3];
        
//        CGSize newScrollViewSize = containerView.frame.size;
//        newScrollViewSize.height = containerView.frame.size.height - photosCollectionView.contentSize.height + ((userPosts.count/3+1)*320/3);
//        [scrollView setContentSize:newScrollViewSize];
        float f = photosCollectionView.contentSize.height;
        [scrollView setContentSize:CGSizeMake(CGRectGetWidth(scrollView.frame), f + 160)];
    }
    NSLog(@"collectionview content size height = %f", photosCollectionView.contentSize.height);
    NSLog(@"scroll view content size height = %f", scrollView.contentSize.height);
}

- (NSMutableArray *)removeDuplicatesFromPostsArray:(NSMutableArray *)tempPostsArray
{
    NSMutableArray *uniqueTempPostsArray = [NSMutableArray array];
    NSMutableSet *postIDs = [NSMutableSet set];
    for (NSDictionary *post in tempPostsArray) {
        NSString *postID = [[post objectForKey:@"id"] stringValue];
        if (![postIDs containsObject:postID]) {
            [uniqueTempPostsArray addObject:post];
            [postIDs addObject:postID];
        }
    }
    
    return uniqueTempPostsArray;
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
}

- (void)configureAvatarImageView
{
    NSString *avatarString;
    if (![user objectForKey:@"avatar"])
    {
       if(![[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"])
       {

           NSLog(@"has not avatar, set placeholder image");
//           [avatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
           [UIImageView apa_useMaskToImageView:avatarImageView
                                      forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
//           [avatarImageView.layer setMasksToBounds:YES];
       }
        else
        {
            avatarString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
            
            [avatarImageView setImageWithURL:[NSURL URLWithString:avatarString]
                               placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            [UIImageView apa_useMaskToImageView:avatarImageView
                                       forImage:avatarImageView.image];
//            [avatarImageView.layer setMasksToBounds:YES];
        }
    }
    else
    {
        NSLog(@"has [user avatar]");
        
        if ([[user objectForKey:@"avatar"] isEqualToString:@""] ||
            [[user objectForKey:@"avatar"] isEqualToString:@"http://rain-app.com/upload/avatars/"]) {
            
            NSLog(@"[user avatar] is empty string, set placeholder");
            
//            [avatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhoto"]];
            [UIImageView apa_useMaskToImageView:avatarImageView
                                       forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
//            [avatarImageView.layer setMasksToBounds:YES];
        }
        else {
            // set avatar
            avatarString = [user objectForKey:@"avatar_small"];
            NSLog(@"set avatar from %@", avatarString);
            if(!avatarString)
            {
                avatarString  =[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
            }
            
            __weak UIImageView *weakAvatarImageView = avatarImageView;
            NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
            
            [avatarImageView setImageWithURLRequest:photoRequest
                                   placeholderImage:nil
                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//                 [weakAvatarImageView setImage:image];
                [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                           forImage:image];
                 NSLog(@"success for avatar");
                 NSData *imageData = UIImagePNGRepresentation(weakAvatarImageView.image);
                 [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"userPhoto"];
                 
             } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
//                 [weakAvatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhoto"]];
                 [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                            forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                 NSLog(@"fail for avatar");
             }];
//            [self useMaskToImageView:avatarImageView
//                            forImage:avatarImageView.image];
//            [avatarImageView.layer setMasksToBounds:YES];
        }
    }
}

- (void)configureEditButton
{
    [editButton setHidden:!userOwns];
    [followButton setHidden:userOwns];
}

- (void)configureFollowButton
{
    UIImage *selectedImage;
    
    
    if ([followButton isHidden] == NO)
    {
        if ([[user objectForKey:@"followed"] integerValue] == 1)
        {
         
            [followButton setSelected:YES];
            userFollowed = YES;
        }
        else
        {
          
            [followButton setSelected:NO];
            userFollowed = NO;
        }
    }
}

- (void)configureSettingsButton
{
    if (!userOwns) {
        [[self navigationItem] setRightBarButtonItem:nil animated:NO];
    }
}

- (void)configureMessagesButton
{
    [dialogsButton setHidden:!userOwns];
    [sendMessageButton setHidden:userOwns];
}

- (BOOL)isPrivate
{
    BOOL isPrivate = NO;
    
    if (userOwns)
    {
        NSLog(@"user owns - open profile");
        followButton.hidden=YES;
        editButton.hidden=NO;
        lblEdit.hidden=NO;
        isPrivate = NO;
        
    }
    else
    {
        lblEdit.hidden=YES;
        followButton.hidden=NO;
        editButton.hidden=YES;
        
        if ([[user objectForKey:@"confirm_follow"] integerValue] == 0 && [[[user objectForKey:@"settings"] objectForKey:@"hide_profile"] integerValue] == 1) {
            NSLog(@"hide profile");
            isPrivate = YES;
        }
        else
        {
            isPrivate = NO;
            NSLog(@"user is:\n%@", user);
            NSLog(@"user settings is:\n%@", [user objectForKey:@"settings"]);
            NSLog(@"user settings hide profile = %ld", (long)[[[user objectForKey:@"settings"] objectForKey:@"hide_profile"] integerValue]);
        }
    }
    
    
    NSLog(@"return isPrvate = %d", isPrivate);
    return isPrivate;
}

- (BOOL)isBanned
{
    BOOL isBanned = NO;
    
    if ([user objectForKey:@"banned"]) {
        if ([[user objectForKey:@"banned"] integerValue] == 1)
        {
            isBanned = YES;
        } else {
            isBanned = NO;
        }
    }

    NSLog(@"return isBanned = %d", isBanned);
    return isBanned;
}

- (void)setupONLYUIElements {
    [avatarImageView setHidden:NO];
    [averageRateLabel setHidden:NO];
    [followersCountLabel setHidden:NO];
    [postsCountLabel setHidden:NO];
    [followsCountLabel setHidden:NO];
    [badgeLabel setHidden:NO];
    [badgeLabelBackground setHidden:NO];
    [followersLabel setHidden:NO];
    [followsLabel setHidden:NO];
    [postsLabel setHidden:NO];
    [editButton setHidden:NO];
    [followButton setHidden:NO];
    
    [self configureAvatarImageView];
    [self configureEditButton];
    [self configureSettingsButton];
    [self configureMessagesButton];
    [self configureFollowButton];
    
    NSInteger newMessagesCount = [[[user objectForKey:@"stats"] objectForKey:@"new_messages"] integerValue];
    if (newMessagesCount) {
        [badgeLabelBackground setHidden:NO];
        [badgeLabel setHidden:NO];
        [badgeLabel setText:[NSString stringWithFormat:@"%lu", (long)newMessagesCount]];
    } else {
        [badgeLabelBackground setHidden:YES];
        [badgeLabel setHidden:YES];
    }
}

- (void)setupUIElements
{

    
    [self configureNavigationBar];

    NSString *fullName = [NSString stringWithFormat:@"%@ %@", [user objectForKey:@"first_name"], [user objectForKey:@"last_name"]];
    if([fullName isEqualToString:@" "])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
          NSDictionary *userData=[defaults objectForKey:@"userData"];
        fullName = [NSString stringWithFormat:@"%@ %@", [userData objectForKey:@"first_name"], [userData objectForKey:@"last_name"]];
       
    }
    [[self navigationItem] setTitle:fullName];
    [scrollView setContentInset:UIEdgeInsetsMake(0, 0, self.tabBarController.tabBar.frame.size.height, 0)];

    
    [followersLabel setText:NSLocalizedString(@"profileFollowers", nil)];
    [postsLabel setText:NSLocalizedString(@"profilePosts", nil)];
    [followsLabel setText:NSLocalizedString(@"profileFollows", nil)];
    
    [self setupONLYUIElements];
    
    // average rate
    [averageRateLabel setText:[[[user objectForKey:@"stats"] objectForKey:@"rating"] stringValue]];
    
    // followers
    [followersCountLabel setText:[[[user objectForKey:@"stats"] objectForKey:@"followers"] stringValue]];
    
    // posts count
    [postsCountLabel setText:[[[user objectForKey:@"stats"] objectForKey:@"posts"] stringValue]];
    
    // follows
    [followsCountLabel setText:[[[user objectForKey:@"stats"] objectForKey:@"following"] stringValue]];
}

#pragma mark - IBAction Methods

- (IBAction)btnPublickOnClick:(id)sender
{
    Fool=2;
    photosCollectionView.hidden=NO;
    self.friendsTableView.hidden=YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [photosCollectionView reloadData];
    });
    [photosCollectionView layoutIfNeeded];
    dispatch_async(dispatch_get_main_queue(), ^{
        float f = photosCollectionView.contentSize.height;
        collectionViewHeightConstraint.constant = photosCollectionView.contentSize.height;
        [scrollView setContentSize:CGSizeMake(CGRectGetWidth(scrollView.frame), f + 160)];
    });
}


- (IBAction)btnFollowersOnClick:(id)sender
{
    Fool=1;
//    [self.friendsTableView reloadData];
    photosCollectionView.hidden=YES;
    
//    if(followings.count>0)
//    {
//        self.friendsTableView.hidden=NO;
//    }
    
//    [UIView animateWithDuration:0 animations:^{
//        [_friendsTableView reloadData];
//    } completion:^(BOOL finished) {
//        self.tableViewHeightConstraint.constant = self.friendsTableView.contentSize.height;
//    }];

   
    [API getFollowingsForUserWithEmail:userMail];
}

- (IBAction)btnFollowOnClick:(id)sender
{
    Fool=0;
//    [self.friendsTableView reloadData];
    photosCollectionView.hidden=YES;
    
//    if(followers.count>0)
//    {
//        self.friendsTableView.hidden=NO;
//    }
    
//    [UIView animateWithDuration:0 animations:^{
//        [_friendsTableView reloadData];
//    } completion:^(BOOL finished) {
//        self.tableViewHeightConstraint.constant = self.friendsTableView.contentSize.height;
//    }];
    
    [API getFollowersForUserWithEmail:userMail];
}

- (IBAction)follow:(id)sender
{
    Fool=1;
    NSLog(@"follow this user!");
    //[self showFollowActivityIndicator];
    
    if (userFollowed == YES)
    {
        [API unfollowUserWithID:[[user objectForKey:@"id"] stringValue]];
    } else {
        [API followUserWithID:[[user objectForKey:@"id"] stringValue]];
    }
    
    
    
    UIImage *selectedImage;
    
//    if ([self isPrivate])
//    {
//        selectedImage = [UIImage imageNamed:@"checkmark.png"];
//    } else
//    {
//         selectedImage = [UIImage imageNamed:@"ProfileFollowButtonRequest"];
//    }
//    
//    [followButton setBackgroundImage:selectedImage forState:UIControlStateSelected];
    
    if ([followButton isHidden] == NO)
    {
        if ([[user objectForKey:@"followed"] integerValue] == 1)
        {
            [followButton setSelected:NO];
            userFollowed = NO;
        } else
        {
            [followButton setSelected:YES];
            userFollowed = YES;
          
        }
    }
    
    photosCollectionView.hidden = YES;
    
    [API getFollowersForUserWithEmail:userMail];
    [API getFollowingsForUserWithEmail:userMail];
}

- (IBAction)sendMessage:(id)sender
{
       NSLog(@"sendMessage");
    
    
    
    [self performSegueWithIdentifier:@"sendMessage" sender:self];
}

- (IBAction)btnBacgroundOnClick:(id)sender
{
    if (userOwns)
    {

    self.bacgroundActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Bacground", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                     destructiveButtonTitle:NSLocalizedString(@"bacgroundCamera", nil)
                                          otherButtonTitles:NSLocalizedString(@"bacgroundGallery", nil), nil];
    [self.bacgroundActionSheet showFromTabBar:[[self tabBarController] tabBar]];
    }
    
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    if (actionSheet == self.bacgroundActionSheet) {
        switch (buttonIndex)
        {
            case 0:
                [self openCamera];
                break;
                
            case 1:
                 [self openGallery];
               
                break;
                
            default:
                break;
        }
    }
}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info dictionary - \n%@", info);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imageData"];
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID=%@",[[user objectForKey:@"id"]stringValue]];
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"Config" predicate:predicate];
    
    [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error)
     {
         if (!error)
         {
             if(results.count==0)
             {
                 
                 
                 CKRecord *record=[[CKRecord alloc]initWithRecordType:@"Config"];
                 
                 
                 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                 NSString *docs = [paths objectAtIndex:0];
                 NSString* path =  [docs stringByAppendingFormat:@"/imageBacground.jpg"];
                 
                 
                 NSString* pathAvatar =  [docs stringByAppendingFormat:@"/imageAvatar.jpg"];
                 
                 CKAsset *asset = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:path]];
                 
                 CKAsset *assetAvatar = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:pathAvatar]];
                 
                 
                 UIImage *img=[info objectForKey:UIImagePickerControllerOriginalImage];
                 [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(img) forKey:@"imageData"];
                 
                 NSData* data = UIImagePNGRepresentation(img);
                 [data writeToFile:path atomically:YES];
                 
                 record[@"BacgroundImage"] = asset;
                 
                 record[@"ID"]=[[user objectForKey:@"id"]stringValue];
                 
                 [publicDatabase saveRecord:record completionHandler:^(CKRecord *artworkRecord, NSError *error)
                 {
                     if (!error)
                     {
                             CKAsset *asset=[artworkRecord objectForKey:@"BacgroundImage"];
                            CKAsset *assetAvatar=[artworkRecord objectForKey:@"AvatarImage"];
                         
                         if(assetAvatar)
                         {
                             NSData *imageData=[NSData dataWithContentsOfURL:assetAvatar.fileURL];
                             if(imageData)
                             {
//                                 avatarImageView.image = [UIImage imageWithData:imageData];
                                 [UIImageView apa_useMaskToImageView:avatarImageView
                                                            forImage:[UIImage imageWithData:imageData]];
                             }
                             
                         }
                         
                             if(asset)
                             {
                                 NSData *imageData=[NSData dataWithContentsOfURL:asset.fileURL];
                                 if(imageData)
                                 {
//                                     imgBacground.image = [UIImage imageWithData:imageData];
//                                     dispatch_async(dispatch_get_main_queue(), ^{
//                                         UIImage *temp = [UIImage imageWithData:imageData];
//                                         [self saveBackgroundImage:temp
//                                              toCoreDataWithUserId:[[user objectForKey:@"id"] intValue]];
//                                         temp = nil;
//                                     });
                                 }
                             }
                        
                         
                     }
                 }];
                 
             }
             if(results.count>0)
             {
                 CKRecord *artworkRecord=[results lastObject];
                 
                 if (!error)
                 {
                     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                     NSString *docs = [paths objectAtIndex:0];
                     NSString* path =  [docs stringByAppendingFormat:@"/imageBacground.jpg"];
                     
                     
                      NSString* pathAvatar =  [docs stringByAppendingFormat:@"/imageAvatar.jpg"];
                     
                     CKAsset *asset = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:path]];
                     
                    CKAsset *assetAvatar = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:pathAvatar]];
                     
                     
                     UIImage *img=[info objectForKey:UIImagePickerControllerOriginalImage];
//                     [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(img) forKey:@"imageData"];
                     NSData* data = UIImagePNGRepresentation(img);
                     [data writeToFile:path atomically:YES];
                     
                     
                     artworkRecord[@"BacgroundImage"] = asset;

                     [publicDatabase saveRecord:artworkRecord completionHandler:^(CKRecord *savedRecord, NSError *saveError) {
                         if (!error) {
                             
                         }
                     }];
                     
                 }
             }
         }
         
     }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *temp = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self saveBackgroundImage:temp
             toCoreDataWithUserId:[[user objectForKey:@"id"] intValue]];
        temp = nil;
    });
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void) dismissCamera:(id)cameraViewController{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}


- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
   int userID = [[user objectForKey:@"id"] intValue];
    dispatch_async(dispatch_get_main_queue(), ^{
//        imgBacground.image = image;
        [self saveBackgroundImage:image
             toCoreDataWithUserId:userID];
    });
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imageData"];
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID=%@",[[user objectForKey:@"id"]stringValue]];
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"Config" predicate:predicate];
    
    [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error)
     {
         if (!error)
         {
             if(results.count==0)
             {
                 
                 
                 CKRecord *record=[[CKRecord alloc]initWithRecordType:@"Config"];
                 
                 
                 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                 NSString *docs = [paths objectAtIndex:0];
                 NSString* path =  [docs stringByAppendingFormat:@"/imageBacground.jpg"];
                 
                 
                 NSString* pathAvatar =  [docs stringByAppendingFormat:@"/imageAvatar.jpg"];
                 
                 CKAsset *asset = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:path]];
                 
                 CKAsset *assetAvatar = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:pathAvatar]];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"imageData"];
                 
                 NSData* data = UIImagePNGRepresentation(image);
                 [data writeToFile:path atomically:YES];
                 
                 record[@"BacgroundImage"] = asset;
                 
                 record[@"ID"]=[[user objectForKey:@"id"]stringValue];
                 
                 [publicDatabase saveRecord:record completionHandler:^(CKRecord *artworkRecord, NSError *error)
                  {
                      if (!error)
                      {
                          CKAsset *asset=[artworkRecord objectForKey:@"BacgroundImage"];
                          CKAsset *assetAvatar=[artworkRecord objectForKey:@"AvatarImage"];
                          
                          if(assetAvatar)
                          {
                              NSData *imageData=[NSData dataWithContentsOfURL:assetAvatar.fileURL];
                              if(imageData)
                              {
                                  //                                 avatarImageView.image = [UIImage imageWithData:imageData];
                                  [UIImageView apa_useMaskToImageView:avatarImageView
                                                             forImage:[UIImage imageWithData:imageData]];
                              }
                              
                          }
                          
                          if(asset)
                          {
                              NSData *imageData=[NSData dataWithContentsOfURL:asset.fileURL];
                              if(imageData)
                              {
                                  //                                     imgBacground.image = [UIImage imageWithData:imageData];
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      imgBacground.image = [UIImage imageWithData:imageData];
                                      [self saveBackgroundImage:imgBacground.image
                                           toCoreDataWithUserId:[[user objectForKey:@"id"] intValue]];
                                  });
                              }
                          }
                          
                          
                      }
                  }];
                 
             }
             if(results.count>0)
             {
                 CKRecord *artworkRecord=[results lastObject];
                 
                 if (!error)
                 {
                     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                     NSString *docs = [paths objectAtIndex:0];
                     NSString* path =  [docs stringByAppendingFormat:@"/imageBacground.jpg"];
                     
                     
                     NSString* pathAvatar =  [docs stringByAppendingFormat:@"/imageAvatar.jpg"];
                     
                     CKAsset *asset = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:path]];
                     
                     CKAsset *assetAvatar = [[CKAsset alloc]initWithFileURL:[NSURL fileURLWithPath:pathAvatar]];
                     
                     
                     //                     [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(img) forKey:@"imageData"];
                     NSData* data = UIImagePNGRepresentation(image);
                     [data writeToFile:path atomically:YES];
                     
                     
                     artworkRecord[@"BacgroundImage"] = asset;
                     
                     [publicDatabase saveRecord:artworkRecord completionHandler:^(CKRecord *savedRecord, NSError *saveError) {
                         if (!error)
                         {
                             //                             [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imgBacground.image) forKey:@"imageData"];
                             
                         }
                     }];
                     
                 }
             }
         }
         //         else {
         //             [[NSUserDefaults standardUserDefaults] setObject:[info objectForKey:UIImagePickerControllerOriginalImage] forKey:@"imageData"];
         //         }
         
     }];

    
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController pushViewController:createPostVC animated:YES];
//        DetailViewController *detail = [[DetailViewController alloc] init];
//        [detail setDetailImage:image];
//        [self.navigationController pushViewController:detail animated:NO];
//        [cameraViewController restoreFullScreenMode];
//        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}


-(void)openCamera
{
    DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
    cameraController.delegate=self;
    [cameraController setForceQuadCrop:YES];
    
    DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [container setCameraViewController:cameraController];
    [container setFullScreenMode];
    
    self.nav = [[DemoNavigationController alloc] initWithRootViewController:container];
    [self presentViewController:self.nav animated:YES completion:nil];
}

-(void)openGallery
{
    NSLog(@"Photo button tapped");
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        NSArray *mediaTypeArray = [[NSArray alloc] initWithObjects:@"public.image", nil];
        [imagePicker setMediaTypes:mediaTypeArray];
    }
    
    [imagePicker setDelegate:self];
    [imagePicker setAllowsEditing:NO];
    [self presentViewController:imagePicker
                       animated:YES
                     completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    
    if ([[segue identifier] isEqualToString:@"showPostVC"])
    {
        NSIndexPath *selectedPhotoIndexPath = [[photosCollectionView indexPathsForSelectedItems] objectAtIndex:0];
        NSDictionary *post = [userPosts objectAtIndex:[selectedPhotoIndexPath item]];
        
        NSLog(@"selected post:\n\n%@", post);
        
        NSLog(@"author of selected post:\n%@", user);
        
        NSMutableDictionary *author = [[NSMutableDictionary alloc] init];
        
        if ([user objectForKey:@"avatar_small"])
        {
            [author setObject:[user objectForKey:@"avatar_small"] forKey:@"avatar"];
        }
        
        [author setObject:userMail forKey:@"email"];
        [author setObject:[user objectForKey:@"first_name"] forKey:@"first_name"];
        [author setObject:[user objectForKey:@"id"] forKey:@"id"];
        [author setObject:[user objectForKey:@"nick"] forKey:@"nick"];
        
        [[segue destinationViewController] setCurrentPost:post];
        [[segue destinationViewController] setAuthor:author];
    }
    
    else if ([[segue identifier] isEqualToString:@"showEditProfileVC"])
    {
       // [(APEditProfileVC *)[segue destinationViewController] setUser:[user mutableCopy]];
         [[segue destinationViewController] setCurrentUser:user];
    }
    
    else if ([[segue identifier] isEqualToString:@"showFriendsVC"])
    {
        [[segue destinationViewController] setUserMail:userMail];
    }
    
    else if ([[segue identifier] isEqualToString:@"chatViewController"])
    {
        NSString *userID = [[user objectForKey:@"id"] stringValue];
        
        [[segue destinationViewController] setHidesBottomBarWhenPushed:YES];
        
        [[segue destinationViewController] setUserID:userID];
        
        [[segue destinationViewController] setGettingUserImage:avatarImageView.image];
        NSString *selectedUserNickname=[user objectForKey:@"nick"];

        [[segue destinationViewController] setTitle:selectedUserNickname];
    }
    
    else if ([[segue identifier] isEqualToString:@"sendMessage"])
    {
        NSString *userID = [[user objectForKey:@"id"] stringValue];
        
        [[segue destinationViewController] setHidesBottomBarWhenPushed:YES];
        [[segue destinationViewController] setUserID:userID];
        [[segue destinationViewController] setGettingUserImage:avatarImageView.image];
        
        [[segue destinationViewController] setTitle:[user objectForKey:@"nick"]];
    }
    else if ([[segue identifier] isEqualToString:@"showInfo"])
    {
        [[segue destinationViewController] setPhotoImage:avatarImageView.image];
        [[segue destinationViewController] setCurrentUser:user];
    }
}

// Save background image to Core Data's entity.
- (void)saveBackgroundImage:(UIImage *)img toCoreDataWithUserId:(int)userId {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSArray *userInfoArray = [UserBGImage MR_findAllSortedBy:@"imageDate"
                                                       ascending:YES
                                                   withPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userId]];
        UserBGImage* userInfo;
        
        // If entities is exist, so than need to delete all records for it
        if ([userInfoArray count]) {
            
            //Delete all records for current user
            [UserBGImage MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userId]
                                             inContext:localContext];
            [localContext processPendingChanges];
            
//            NSArray *arr = [UserBGImage MR_findAllInContext:localContext];
            userInfo     = [UserBGImage MR_createInContext:localContext];
        } else {
            userInfo     = [UserBGImage MR_createInContext:localContext];
        }
        
        [userInfo setUserImage:img forUserId:userId];
        
    } completion:^(BOOL success, NSError *error) {
        
        if (success) {
            NSLog(@"Background image was save to Core Data");
            UserBGImage *userBG = [UserBGImage MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userId == %d", userId]
                                                                sortedBy:@"imageDate"
                                                               ascending:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // If new image in Core Data is not equal current, installed image - replace
                // current image to Core Data one.
                if (![userBG.userImage isEqualToData:UIImagePNGRepresentation(imgBacground.image)]) {
                    imgBacground.image = [UIImage imageWithData:userBG.userImage];
                }
            });
        }
    }];
}

@end
