//
//  APNewsCell.m
//  Rain
//
//  Created by EgorMac on 20/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APNewsCell.h"

@implementation APNewsCell

@synthesize postID, currentPost;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (void)awakeFromNib {
    // Initialization code
    
    API = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(likeSuccess)
                                                 name:@"LikeSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(likeFailed)
                                                 name:@"LikeFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(repostSuccess)
                                                 name:@"RepostSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(repostFailed)
                                                 name:@"RepostFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(increaseCommentsCount)
                                                 name:@"CommentsCountIncreased"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(decreaseCommentsCount)
                                                 name:@"CommentsCountDecreased"
                                               object:nil];
}

- (void)increaseCommentsCount
{
    NSLog(@"comments count increases...");
    NSInteger commentsCount = [[commentsCountLabel text] integerValue];
    
    [commentsCountLabel setText:[NSString stringWithFormat:@"%d", commentsCount + 1]];
}

- (void)decreaseCommentsCount
{
    NSLog(@"comments count decreases...");
    NSInteger commentsCount = [[commentsCountLabel text] integerValue];
    
    if (commentsCount > 0) {
        [commentsCountLabel setText:[NSString stringWithFormat:@"%d", commentsCount - 1]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCurrentPost:(NSDictionary *)newCurrentPost
{
    if (currentPost != newCurrentPost) {
        
       // [photoImageView setImage:[UIImage imageWithColor:[UIColor whiteColor]]];
        //[photoImageView setBackgroundColor:[UIColor blackColor]];
        
        NSLog(@"set currentPost method");
        
        [progressView setHidden:NO];
        
        currentPost = [newCurrentPost objectForKey:@"post"];
        
        NSLog(@"current post is %@", currentPost);
        
        NSString *photoString;
        if ([[currentPost objectForKey:@"path"] hasPrefix:@"http://rain-app.com/media/video"])
        {
            photoString = [currentPost objectForKey:@"preview_image"];
            [self setVideo];
        } else
        {
            [videoIconView setHidden:YES];
            photoString = [currentPost objectForKey:@"full_image"];
        }
        
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:photoString]
                          placeholderImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                   options:0
                                  progress:^(NSInteger receivedSize, NSInteger expectedSize)
         
        {
                                      NSLog(@"%ld bytes from %ld received", (long)receivedSize, (long)expectedSize);
                                      
                                      [progressView setProgress:(float)receivedSize/expectedSize];
                                  }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                                    {
                                     [photoImageView setImage:image];
                                        photoImageView.frame = CGRectMake(photoImageView.frame.origin.x, 50,
                                                                     image.size.width, image.size.height);
                                     [progressView setHidden:YES];
                                 }];
        
        [self updateStats];
    }
}

- (void)updateStats
{
    if ([[currentPost objectForKey:@"like"] isEqualToNumber:[NSNumber numberWithInt:1]])
    {
        [likesImageView setHighlighted:YES];
    }
    else
    {
        [likesImageView setHighlighted:NO];
    }
    
    NSInteger repost = [[currentPost objectForKey:@"repost"] integerValue];
    NSLog(@"repost = %ld", (long)repost);
    
    if (repost == 1)
    {
        [repostsImageView setHighlighted:YES];
    }
    else
    {
        [repostsImageView setHighlighted:NO];
    }
    
    NSDictionary *stats = [currentPost objectForKey:@"stats"];
    
    [likesCountLabel setText:[[stats
                               objectForKey:@"likes"]
                              stringValue]];
    [commentsCountLabel setText:[[stats
                                  objectForKey:@"comments"]
                                 stringValue]];
    [repostsCountLabel setText:[[stats
                                 objectForKey:@"reposts"]
                                stringValue]];
    [viewsCountLabel setText:[[stats
                               objectForKey:@"views"]
                              stringValue]];
    
}

-(void)updateConstraints{
    [panelLikeLeading setConstant:self.contentView.frame.size.width/16];
    [panelCommentsLeading setConstant:self.contentView.frame.size.width/5];
    [panelViewsLeading setConstant:self.contentView.frame.size.width/5];
    [panelRepostsLeading setConstant:self.contentView.frame.size.width/5];
    [super updateConstraints];
}

#pragma mark - IBAction Methods


- (void) animateLike
{
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        lshowLikeInImage.transform = CGAffineTransformMakeScale(1.3, 1.3);
        lshowLikeInImage.hidden=NO;
        lshowLikeInImage.alpha = 1.0;
    }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                             lshowLikeInImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                                                  lshowLikeInImage.transform = CGAffineTransformMakeScale(1.3, 1.3);
                                                  lshowLikeInImage.alpha = 0.0;
                                              lshowLikeInImage.hidden=YES;
                                              }
                                                               completion:^(BOOL finished) {
                                                                   lshowLikeInImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                                               }];
                                          }];
                     }];
}






- (IBAction)like:(id)sender
{
    
    [self animateLike];
    NSLog(@"like!");
    [likesCountLabel setHidden:YES];
    [likesImageView setHidden:YES];
    [likeButton setEnabled:NO];
    
    [self showLikesActivity];
    
    
    
    
    NSLog(@"like success");
    NSString *liked = @"0";
    [self removeLikesActivity];
    NSInteger currentLikesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"likes"] integerValue];
    
    
    if ([likesImageView isHighlighted])
    {
        
        NSLog(@"likes highlighted!");
        [likesImageView setHighlighted:NO];
        liked = @"0";
    } else {
        NSLog(@"likes not highlighted!");
        [likesImageView setHighlighted:YES];
        liked = @"1";
    }
    
    
    
    [likesCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentLikesCount]];
    [likeButton setEnabled:YES];
    
    NSMutableDictionary *postMutDict = [currentPost mutableCopy];
    [postMutDict setObject:[NSNumber numberWithInt:[liked integerValue]] forKey:@"like"];
    
    NSMutableDictionary *mutStats = [[postMutDict objectForKey:@"stats"] mutableCopy];
    [mutStats setObject:[NSNumber numberWithInt:[likesCountLabel.text integerValue]] forKey:@"likes"];
    [postMutDict setObject:mutStats forKey:@"stats"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLiked"
                                                        object:nil
                                                      userInfo:[postMutDict copy]];
    NSLog(@"postmutdict:\n%@", postMutDict);
    
    //[self likeSuccess];
    
    [API likePostWithID:[[currentPost objectForKey:@"id"] stringValue]];
}

- (IBAction)repost:(id)sender
{
    NSLog(@"repost");
    [repostsCountLabel setHidden:YES];
    [repostsImageView setHidden:YES];
    [upperRepostButton setEnabled:NO];
    
    [self showRepostsActivity];
    
    [API repostPostWithID:[[currentPost objectForKey:@"id"] stringValue]];
}

- (IBAction)comment:(id)sender
{
    NSLog(@"comment");
    
    id <APNewsCellDelegate> newsCellDelegate = [self delegate];
    if (newsCellDelegate) {
        [newsCellDelegate commentPost:currentPost];
    }
}

#pragma mark - Success and Fail methods

- (void)likeSuccess
{
//    NSLog(@"like success");
//    NSString *liked = @"0";
//    [self removeLikesActivity];
    NSInteger currentLikesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"likes"] integerValue];
//    
//    
//    if ([likesImageView isHighlighted])
//    {
//        
//        NSLog(@"likes highlighted!");
//        [likesImageView setHighlighted:NO];
//        liked = @"0";
//    } else {
//        NSLog(@"likes not highlighted!");
//        [likesImageView setHighlighted:YES];
//        liked = @"1";
//    }
//    
//  
//    
    [likesCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentLikesCount]];
    [likeButton setEnabled:YES];
//    
//    NSMutableDictionary *postMutDict = [currentPost mutableCopy];
//    [postMutDict setObject:[NSNumber numberWithInt:[liked integerValue]] forKey:@"like"];
//    
//    NSMutableDictionary *mutStats = [[postMutDict objectForKey:@"stats"] mutableCopy];
//    [mutStats setObject:[NSNumber numberWithInt:[likesCountLabel.text integerValue]] forKey:@"likes"];
//    [postMutDict setObject:mutStats forKey:@"stats"];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLiked"
//                                                        object:nil
//                                                      userInfo:[postMutDict copy]];
//    NSLog(@"postmutdict:\n%@", postMutDict);
}

- (void)likeFailed
{
    [self removeLikesActivity];
    [self showAlertWithIdentifier:serverAlert];
    [likeButton setEnabled:YES];
}

- (void)repostSuccess
{
    NSLog(@"repost success");
    [self removeRepostsActivity];
    [repostsCountLabel setHidden:NO];
    [repostsImageView setHidden:NO];
    
    NSInteger currentRepostsCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"reposts"] integerValue];
    
    if ([repostsImageView isHighlighted]) {
        
        NSLog(@"reposts highlighted!");
        [repostsImageView setHighlighted:NO];
    } else {
        NSLog(@"reposts not highlighted!");
        [repostsImageView setHighlighted:YES];
    }
    
    [repostsCountLabel setText:[NSString stringWithFormat:@"%ld", (long)currentRepostsCount]];
    [upperRepostButton setEnabled:YES];
}

- (void)repostFailed
{
    [self removeRepostsActivity];
    [self showAlertWithIdentifier:serverAlert];
    [upperRepostButton setEnabled:YES];
}

#pragma mark - Alerts

- (void)networkError
{
    [self removeLikesActivity];
    [self removeRepostsActivity];
    
    [self showAlertWithIdentifier:networkAlert];
    [likeButton setEnabled:YES];
    NSLog(@"network error!");
}

- (void)serverError
{
    [self removeLikesActivity];
    [self removeRepostsActivity];
    
    [self showAlertWithIdentifier:serverError];
    [likeButton setEnabled:YES];
    NSLog(@"server error!");
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Activity

- (void)showLikesActivity
{
//    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
//    [activity setFrame:CGRectMake(0,
//                                  0,
//                                  50,
//                                  50)];
//    [activity setCenter:CGPointMake(likesImageView.center.x + 10, likesImageView.center.y)];
//    [activity setColor:[UIColor lightGrayColor]];
//    [activity setTag:11];
//    [statsView addSubview:activity];
//   // [activity startAnimating];
//    [activity setHidden:NO];
}

- (void)removeLikesActivity
{
//    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[statsView viewWithTag:11];
//    [activity stopAnimating];
//    [activity removeFromSuperview];
    [likesCountLabel setHidden:NO];
    [likesImageView setHidden:NO];
}

- (void)showRepostsActivity
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setFrame:CGRectMake(0,
                                  0,
                                  50,
                                  50)];
    [activity setCenter:CGPointMake(repostsImageView.center.x + 10, repostsImageView.center.y)];
    [activity setColor:[UIColor lightGrayColor]];
    [activity setTag:12];
    [statsView addSubview:activity];
    [activity startAnimating];
    [activity setHidden:NO];
}

- (void)removeRepostsActivity
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[statsView viewWithTag:12];
    [activity stopAnimating];
    [activity removeFromSuperview];
}

#pragma mark - Helper Methods

- (UIImage *)placeHolderImage
{
    CGSize imageSize = CGSizeMake(320/3, 320/3);
    UIColor *fillColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [fillColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)setVideo
{
    [videoIconView setHidden:NO];
}

@end
