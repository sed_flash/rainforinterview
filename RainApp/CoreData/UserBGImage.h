//
//  UserBGImage.h
//  Rain
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserBGImage : NSManagedObject

- (void)setUserImage:(UIImage *)img forUserId:(int)userId;

@end

NS_ASSUME_NONNULL_END

#import "UserBGImage+CoreDataProperties.h"
