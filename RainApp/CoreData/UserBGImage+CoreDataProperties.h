//
//  UserBGImage+CoreDataProperties.h
//  Rain
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserBGImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserBGImage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *imageDate;
@property (nullable, nonatomic, retain) NSNumber *userId;
@property (nullable, nonatomic, retain) NSData *userImage;

@end

NS_ASSUME_NONNULL_END
