//
//  UserBGImage.m
//  Rain
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "UserBGImage.h"

@implementation UserBGImage

- (void)setUserImage:(UIImage *)img forUserId:(int)userId {
    NSData *imgData = UIImagePNGRepresentation(img);
    [self setUserImage:imgData];
    [self setImageDate:[NSDate date]];
    [self setUserId:[NSNumber numberWithInt:userId]];
}

@end
