//
//  UserBGImage+CoreDataProperties.m
//  Rain
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserBGImage+CoreDataProperties.h"

@implementation UserBGImage (CoreDataProperties)

@dynamic imageDate;
@dynamic userId;
@dynamic userImage;

@end
