//
//  APFiltersVC.m
//  Rain
//
//  Created by EgorMac on 16/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFiltersVC.h"
#import "APCreatePostVC.h"
#import "GPUImage.h"

#define MINIMUM_SCALE 1.0
#define MAXIMUM_SCALE 4.0

@interface APFiltersVC ()

@end

@implementation APFiltersVC {
    CGRect scollFrame;
    CGRect rightRect;
}

@synthesize photoImage;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUIElements];
    contrast = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureNavigationBar];
    [self setUpUIElements];
    
    [[NSNotificationCenter defaultCenter] addObserver:self forKeyPath:@"scollFrame" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, scrollView.frame.size.height)];
    
    scollFrame = scrollView.frame;
    rightRect = scrollView.frame;
    
    NSLog(@"scroll view frame size: %f %f", scrollView.frame.size.width, scrollView.frame.size.height);
    NSLog(@"scroll view content size: %f %f", scrollView.contentSize.width, scrollView.contentSize.height);
    
    [self performSelector:@selector(showScrollView) withObject:self afterDelay:1.0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [scrollView setHidden:NO];
    [contrastSlider setHidden:YES];
    [filtersButton setSelected:YES];
    [contrastButton setSelected:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:@"scollFrame"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"scollFrame"]) {
        CGRect temp = [[change valueForKey:NSKeyValueChangeNewKey] CGRectValue];
        if (temp.origin.y != rightRect.origin.y) {
            scollFrame = rightRect;
        }
    }
}

- (void)updateViewConstraints
{
    [lightConstraint setConstant:self.view.frame.size.width/7];
    [contrastConstraint setConstant:self.view.frame.size.width/7];
    [widthConstraint setConstant:self.view.frame.size.width];
    [heightConstraint setConstant:self.view.frame.size.width];
    [super updateViewConstraints];
}

- (void)showScrollView
{
    [scrollView setContentSize:CGSizeMake(800, scrollView.frame.size.height)];
//    scollFrame = scrollView.frame;
//    rightRect = scrollView.frame;
}

- (void)setPhotoImage:(UIImage *)newPhotoImage
{
    NSLog(@"set photo");
    
    photoImage = [[UIImage alloc] init];
    
    if (photoImage != newPhotoImage) {
        
        photoImage = newPhotoImage;
        editedImage = newPhotoImage;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper Methods

- (void)setUpUIElements
{
    [self configureNavigationBar];
    [self configurePhotoImageView];
    [self configureContrastSlider];
    [filtersButton setSelected:YES];
    imageScrollView.minimumZoomScale=1.2;
    imageScrollView.maximumZoomScale=12.0;
    imageScrollView.contentSize=CGSizeMake(320, 320);
    
    [scrollView setHidden:NO];
    [contrastSlider setHidden:YES];
    [filtersButton setSelected:YES];
    [contrastButton setSelected:NO];
    
    CGRect scrFrm = imageScrollView.frame;
    scrFrm.origin = CGPointMake(0, 64);
    imageScrollView.frame = scrFrm;
    
    [imageScrollView setZoomScale:1.2];
}

- (void)configurePhotoImageView
{
    NSLog(@"photoimage = %@", photoImage);
    
    [photoImageView setImage:photoImage];
    [photoImageView setContentMode:UIViewContentModeScaleAspectFill];
    [photoImageView setClipsToBounds:YES];
    [photoImageView setUserInteractionEnabled:YES];
    [photoImageView setBounds:CGRectMake(0, 0, 320, 320)];
    NSLog(@"photoImageView = %@", [photoImageView image]);
}

- (void)configureNavigationBar
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)configureContrastSlider
{
    [contrastSlider setMinimumValue:0.0];
    [contrastSlider setMaximumValue:4.0];
    [contrastSlider setValue:0.0];
}

#pragma mark - UIScrollView Delegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return photoImageView;
}

#pragma mark - IBAction Methods

- (IBAction)setOriginalImage:(id)sender
{
    NSLog(@"set original image");
    [photoImageView setImage:photoImage];
    editedImage = photoImage;
}

- (IBAction)activateFilter1:(id)sender
{
    NSLog(@"contrast ");
    
    GPUImageContrastFilter *filter = [[GPUImageContrastFilter alloc] init];
    [filter setContrast:2.03];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [filter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter2:(id)sender
{
    NSLog(@"saturation 1 filter");
    
    
    GPUImageSaturationFilter *stillImageFilter = [[GPUImageSaturationFilter alloc] init];
    [stillImageFilter setSaturation:2.0];
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter3:(id)sender
{
    NSLog(@"saturation 2 filter");
    
    GPUImageSaturationFilter *stillImageFilter = [[GPUImageSaturationFilter alloc] init];
    [stillImageFilter setSaturation:0.0];
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter4:(id)sender
{
    NSLog(@"Amatorka filter");
    
    GPUImageAmatorkaFilter *stillImageFilter = [[GPUImageAmatorkaFilter alloc] init];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter5:(id)sender
{
    
    NSLog(@"Soft elegance filter");
    
    GPUImageSoftEleganceFilter *stillImageFilter = [[GPUImageSoftEleganceFilter alloc] init];
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter6:(id)sender
{
    NSLog(@"Monocrome filter");
    
    GPUImageMonochromeFilter *stillImageFilter = [[GPUImageMonochromeFilter alloc] init];
    [stillImageFilter setIntensity:1.00];
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter7:(id)sender
{
    NSLog(@"Exposure filter");
    
    GPUImageExposureFilter *stillImageFilter = [[GPUImageExposureFilter alloc] init];
    [stillImageFilter setExposure:0.46];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter8:(id)sender
{
    NSLog(@"GPUImageMissEtikateFilter filter");
    
    GPUImageMissEtikateFilter *stillImageFilter = [[GPUImageMissEtikateFilter alloc] init];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter9:(id)sender
{
    NSLog(@"GPUImageGrayscaleFilter filter");
    
    GPUImageGrayscaleFilter *stillImageFilter = [[GPUImageGrayscaleFilter alloc] init];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter10:(id)sender
{
    NSLog(@"GPUImageSepiaFilter filter");
    
    GPUImageSepiaFilter *stillImageFilter = [[GPUImageSepiaFilter alloc] init];
    [stillImageFilter setIntensity:1.00];
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)activateFilter11:(id)sender
{
    NSLog(@"GPUImageUnsharpMaskFilter filter");
    
    GPUImageUnsharpMaskFilter *stillImageFilter = [[GPUImageUnsharpMaskFilter alloc] init];
    [stillImageFilter setBlurRadiusInPixels:-100.00];
    [stillImageFilter setIntensity:3.38];
    
    UIImage *filteredImage = photoImage;
    filteredImage = [stillImageFilter imageByFilteringImage:filteredImage];
    
    [photoImageView setImage:filteredImage];
    editedImage = filteredImage;
    filteredImage = nil;
}

- (IBAction)showEffects:(id)sender
{
    NSLog(@"show filters");
    [filtersButton setSelected:YES];
    [contrastButton setSelected:NO];
    if ([scrollView isHidden]) {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             CGRect frame = contrastSlider.frame;
                             frame.origin.y = frame.origin.y + 600;
                             contrastSlider.frame = frame;
                         }
                         completion:^(BOOL finished)
         {
             [contrastSlider setHidden:YES];
             [scrollView setHidden:NO];
             [UIView animateWithDuration:0.5
                                   delay:0.1
                                 options:UIViewAnimationOptionTransitionNone
                              animations:^{
//                                  [scrollView setHidden:NO];
                                  [scrollView setFrame:scollFrame];
                              }
                              completion:^(BOOL finished){NSLog(@"scrollView: %@", scrollView);}];
         }];
    }
}

- (IBAction)showContrast:(id)sender
{
    NSLog(@"show contrast slider");
    [contrastButton setSelected:YES];
    [filtersButton setSelected:NO];
    
    if ([contrastSlider isHidden]) {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options:UIViewAnimationOptionTransitionNone
                         animations:^{
                             CGRect frame = scrollView.frame;
                             frame.origin.y = frame.origin.y + 600;
                             scrollView.frame = frame;
                             
                         }
                         completion:^(BOOL finished)
         {
             [scrollView setHidden:YES];
//             [contrastSlider setHidden:NO];
             [UIView animateWithDuration:0.5
                                   delay:0.1
                                 options:UIViewAnimationOptionTransitionNone
                              animations:^{
                                  [contrastSlider setHidden:NO];
                                  CGRect frame = contrastSlider.frame;
                                  frame.origin.y = imageScrollView.frame.origin.y + imageScrollView.frame.size.height - contrastSlider.frame.size.height;
                                  contrastSlider.frame = frame;
                              }
                              completion:nil];
         }];
    }
}

- (IBAction)contrastSliderChanged:(id)sender
{
    UISlider *slider = (UISlider *)sender;
    float value = round(slider.value*100)/100;
    NSLog(@"contrast value is %f", value);
    
    UIImage *imageWithContrast = editedImage;
        
    GPUImageHazeFilter *hazeFilter = [[GPUImageHazeFilter alloc] init];
    [hazeFilter setDistance:slider.value/10];
    [hazeFilter setSlope:slider.value/10];
    imageWithContrast = [hazeFilter imageByFilteringImage:imageWithContrast];
    [photoImageView setImage:imageWithContrast];
    photoImage = imageWithContrast;
    imageWithContrast = nil;
}

- (IBAction)goBack:(id)sender
{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - Gesture recognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CreatePostVC"])
    { 
        UIImage *finishedImage =[self cropImageFromScrollView];
        [[segue destinationViewController] setPhotoImage:finishedImage];
    }
}


- (UIImage *)cropImageFromScrollView
{
    UIGraphicsBeginImageContextWithOptions(imageScrollView.bounds.size,
                                           YES,0);
    CGPoint offset=imageScrollView.contentOffset;
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y);
    [imageScrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

@end
