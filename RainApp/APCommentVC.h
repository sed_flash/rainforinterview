//
//  APCommentVC.h
//  Rain
//
//  Created by EgorMac on 02/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@class APPostVC;

@interface APCommentVC : UIViewController <UITextViewDelegate>
{
    IBOutlet UITextView *commentTextView;
    IBOutlet UIBarButtonItem *sendButton;
    
    NSString *postID;
    
    APServerAPI *API;
    APPostVC *parent;
}

@property (nonatomic, strong) APPostVC *parent;
@property (nonatomic, strong) NSString *postID;

- (IBAction)sendComment:(id)sender;

@end
