//
//  APConversationVC.h
//  Rain
//
//  Created by EgorMac on 25/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessagesViewController.h"
#import "APServerAPI.h"
#import "MNMBottomPullToRefreshManager.h"

@interface APConversationVC : SSMessagesViewController <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
    
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UILabel *noMessagesLabel;
    UIActivityIndicatorView *activityView;
    
    APServerAPI *API;
    NSString *selectedUserEmail;
    NSArray *messages;
    NSDictionary *writtenMessage;
    NSIndexPath *deletedMessageIndexPath;
    
    UIActionSheet *failedMessageActionSheet;
    UIActionSheet *attachMediaActionSheet;
    UIActionSheet *viewAttachActionSheet;
  
    
    UIImage *attachmentImage;
    NSURL *attachmentVideoURL;
    
    NSURL *cellAttachmentVideoURL;
    NSURL *cellAttachmentImageURL;
    
    UIImage *currentUserImage;
    UIImage *gettingUserImage;
    
    UIImageView *atImageView;
    
    CGPoint longPressPoint;
}

@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) UIImage *currentUserImage;
@property (strong, nonatomic) UIImage *gettingUserImage;

@end
