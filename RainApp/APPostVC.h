//
//  APPostVC.h
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "NSDate+TimeAgo.h"
#import "APCommentCell.h"
#import "APServerAPI.h"
#import "APCommentVC.h"
#import "APProfileVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/ALAsset.h>

@interface APPostVC : UNViewController <UIActionSheetDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *showLikeInImage;
    IBOutlet UIBarButtonItem *updateButton;
    
    IBOutlet UIImageView *avatarImageView;
    IBOutlet UIImageView *dropImageView;
    IBOutlet UIImageView *photoImageView;
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *timeLabel;
    
    IBOutlet UITextView *descriptionView;
    
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIView *statsView;
    
    IBOutlet UIImageView *likesImageView;
    IBOutlet UIImageView *repostsImageView;
    
    IBOutlet UILabel *repostsCountLabel;
    IBOutlet UILabel *likesCountLabel;
    IBOutlet UILabel *commentsCountLabel;
    IBOutlet UILabel *viewsCountLabel;
    
    IBOutlet UIButton *likeButton;
    IBOutlet UIButton *repostButton;
    IBOutlet UIButton *upperRepostButton;
    
    IBOutlet UIButton *deleteButton;
    
//    IBOutlet UITableView *commentsTableView;
    
    NSArray *userPosts;
    NSArray *commentsIDs;
    NSDictionary *comments;
    
    NSString *selectedUserMail;
    NSString *selectedAuthor;
    
    NSString *postID;
    NSDictionary *user;
    NSDictionary *currentPost;
    NSDictionary *currentContent;
    
    NSString *deletePostLink;
    NSString *deleteCommentLink;
    
    IBOutlet NSLayoutConstraint *tableViewConstraint;
    IBOutlet NSLayoutConstraint *descriptionViewConstraint;
    
    IBOutlet NSLayoutConstraint *commentButtonLeadingConstraint;
    IBOutlet NSLayoutConstraint *repostButtonLeadingConstraint;
    IBOutlet NSLayoutConstraint *likeButtonLeadingConstraint;
    IBOutlet NSLayoutConstraint *moreButtonTrailingConstraint;
    
    IBOutlet NSLayoutConstraint *panelLikeLeading;
    IBOutlet NSLayoutConstraint *panelCommentsLeading;
    IBOutlet NSLayoutConstraint *panelViewsLeading;
    IBOutlet NSLayoutConstraint *panelRepostsLeading;
    
    NSUInteger startingLikesCount;
    NSUInteger startingRepostsCount;
    
    APServerAPI *API;
    
    NSDictionary *author;
    
    UIActionSheet *postActionSheet;
    UIActionSheet *commentActionSheet;
    UIActionSheet *shareActionSheet;
    
    UIAlertView *postAlertView;
    UIAlertView *commentAlertView;
    
    UIActivityIndicatorView *deletingActivityView;
    IBOutlet UIProgressView *progressView;
    
    IBOutlet UITextView *taggetPeopleTextView;
    
    BOOL commentOwner;
}

@property (nonatomic, strong) NSString *postID;
@property (nonatomic, strong) NSDictionary *author;
@property (nonatomic, strong) NSDictionary *currentPost;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

- (IBAction)updateComments:(id)sender;
- (IBAction)like:(id)sender;
- (IBAction)repost:(id)sender;
- (IBAction)share:(id)sender;
- (IBAction)postActionSheet:(id)sender;
- (IBAction)commentActionSheet:(id)sender;

@end
