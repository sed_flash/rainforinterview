//
//  APVideoModel.h
//  Rain
//
//  Created by Vlad on 09.07.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APVideoModel : NSObject

// Builds right path to save video to device based on HTTP URL
// Returns string like /Documents/.../filename.mp4
- (NSString *)getPathToFileWithURL:(NSURL *)url;

// Check if current video is have on device
// <param name="path"> Path to current file in derictory. Exmpl: /Documents/.../filename.mp4 </param>
- (BOOL)checkIfVideoIsHasOnDevice:(NSString *)path;

- (void)saveVideoToDeviceWithURL:(NSURL *)videoURL andPathToSave:(NSString *)path;

@end
