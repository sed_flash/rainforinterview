//
//  APConversationVC.m
//  Rain
//
//  Created by EgorMac on 25/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APConversationVC.h"
#import "APAttachmentVC.h"
#import "SSMessageTableViewCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIColor+CustomColor.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+APColor.h"
#import "JSQLocationMediaItem.h"
#import "JSQMessageMediaData.h"

#import "APSenderTableViewCell.h"
#import "APReceiverTableViewCell.h"
#import "APReceiverMediaTableViewCell.h"
#import "APSenderMediaTableViewCell.h"

#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>

#define kReceiver      @"receiverId"
#define kReceiverMedia @"receiverMediaId"
#define kSender        @"senderId"
#define kSenderMedia   @"senderMediaId"

@interface APConversationVC ()

@end

@implementation APConversationVC {
    UIRefreshControl *refreshControl;
}

@synthesize userID, currentUserImage, gettingUserImage;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static const CGFloat kTableViewMarginBottom = 64.0f;
static const CGFloat kTextViewHeight = 45.0f;

- (void)setUserID:(NSString *)newUserID
{
    if (userID != newUserID) {
        userID = newUserID;
    }
}

- (void)setGettingUserImage:(UIImage *)newGettingUserImage
{
    if (gettingUserImage != newGettingUserImage) {
        gettingUserImage = newGettingUserImage;
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    atImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
//    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f
//                                                                                         tableView:self.tableView
//                                                                                        withClient:self];
    
    [self configureUI];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SenderCellXib"                bundle:nil] forCellReuseIdentifier:kSender];
    [self.tableView registerNib:[UINib nibWithNibName:@"APSenderMediaTableViewCell"   bundle:nil] forCellReuseIdentifier:kSenderMedia];
    [self.tableView registerNib:[UINib nibWithNibName:@"ReceiverCellXib"              bundle:nil] forCellReuseIdentifier:kReceiver];
    [self.tableView registerNib:[UINib nibWithNibName:@"APReceiverMediaTableViewCell" bundle:nil] forCellReuseIdentifier:kReceiverMedia];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 54.0;
    
    refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.0f;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.bottomRefreshControl = refreshControl;
    

}

- (void)refresh {
    [self updateMessages];
}

- (void)stop {
    [refreshControl endRefreshing];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    cellAttachmentVideoURL = nil;
    cellAttachmentImageURL = nil;
    
    API = [[APServerAPI alloc] init];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateMessages)
                                                 name:@"UpdateMessages"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messagesFailed)
                                                 name:@"ConversationFailed"
                                               object:API];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messagesSuccess)
                                                 name:@"ConversationSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messagesFailed)
                                                 name:@"ConversationFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageSentSuccess)
                                                 name:@"MessageSentSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageSentFailed)
                                                 name:@"MessageSentFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(failedMessageButtonPressed)
                                                 name:@"MessageStatusButtonPressed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteWrittenMessage)
                                                 name:@"DeleteWrittenMessage"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteMessageSuccess)
                                                 name:@"DeleteMessageSuccess"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteMessageFailed)
                                                 name:@"DeleteMessageFailed"
                                               object:nil];
//    [self.tableView setHidden:YES];
    [self.tableView setAllowsSelection:YES];
    [self.tableView setAllowsSelectionDuringEditing:YES];
    
    if ([self.tableView numberOfRowsInSection:0] == 0) {
        [self showActivityIndicator];
        
        NSDictionary *currentUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
        NSString *avatarURLString = [currentUser objectForKey:@"avatar_small"];
        
        if(avatarURLString.length==0)
        {
            avatarURLString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
        }
        
        UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
        [avatarImageView setHidden:YES];
        [self.view addSubview:avatarImageView];
        
        [avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatarURLString]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      currentUserImage = image;
                                      [API getConversationWithUserID:userID];
                                  }];
        
        NSLog(@"currentUser is:\n%@", currentUser);
    }
    
    // Long press gesture
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = 1.0;
    longPress.delegate = self;
    [self.tableView addGestureRecognizer:longPress];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    longPressPoint = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:longPressPoint];
    if (indexPath == nil) {
        NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"long press on table view at row %d", indexPath.row);
        
        // selection management code goes here...
        
        // bring up edit menu.
        UIMenuController *theMenu = [UIMenuController sharedMenuController];
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        UIMenuItem *resetMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"delete", nil) action:@selector(menuItemClicked:)];
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
        [theMenu setMenuItems:[NSArray arrayWithObject:resetMenuItem]];
        
        [theMenu setTargetRect:CGRectMake(location.x, location.y, 0.0f, 0.0f) inView:self.tableView];
        [theMenu setMenuVisible:YES animated:YES];
        
    } else {
        NSLog(@"gestureRecognizer.state = %d", gestureRecognizer.state);
    }
}

- (void) copy:(id) sender {
    // called when copy clicked in menu
}

- (void) menuItemClicked:(id)sender{
    // called when Item clicked in menu
    deletedMessageIndexPath = [self.tableView indexPathForRowAtPoint:longPressPoint];
    NSDictionary *selectedMessage = [messages objectAtIndex:deletedMessageIndexPath.row];
    NSString *selectedMessageID = [[[selectedMessage objectForKey:@"message"] objectForKey:@"id"] stringValue];
    NSLog(@"selected message text:\n%@", [[selectedMessage objectForKey:@"message"] objectForKey:@"text"]);
    NSLog(@"selected messageID = %@", selectedMessageID);
    
    [API deleteMessage:selectedMessageID];
}

- (BOOL) canPerformAction:(SEL)selector withSender:(id) sender {
    if (selector == @selector(menuItemClicked:) || selector == @selector(copy:)) {
        return YES;
    }
    return NO;
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
    }
    return YES;
}

#pragma mark - Data Source Method

- (void)setDataSource
{
    if (!currentUserImage) {
        currentUserImage = [UIImage imageNamed:@"ProfileEditPhotoBlue"];
    }
    
    if (!gettingUserImage)
    {
        gettingUserImage = [UIImage imageNamed:@"ProfileEditPhotoBlue"];
    }
    
    NSMutableArray *tempMessagesArray = [[NSMutableArray alloc] init];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    
    messages = [[[NSUserDefaults standardUserDefaults] objectForKey:@"conversation"] copy];
    
    // If conversation has no messages, responce from API has different structure, so we must create empty array
    if (messages.count == 0) {
        messages = [NSArray array];
    }
    NSLog(@"messages (datasource):\n%@", messages);

    NSString *currentUserEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
    NSData *currentUserImageData = UIImagePNGRepresentation(currentUserImage);
    NSData *gettingUserImageData = UIImagePNGRepresentation(gettingUserImage);
    
    for (NSDictionary *currentMessage in messages)
    {
        NSLog(@"current message is:\n%@", currentMessage);
        
        NSMutableDictionary *currentMutableMessage = [currentMessage mutableCopy];
        
        NSMutableDictionary *from = [[currentMutableMessage objectForKey:@"from"] mutableCopy];
        NSMutableDictionary *to = [[currentMutableMessage objectForKey:@"to"] mutableCopy];
        
        if ([[from objectForKey:@"email"] isEqualToString:currentUserEmail])
        {
            [from setObject:currentUserImageData forKey:@"avatar"];
        } else {
            [from setObject:gettingUserImageData forKey:@"avatar"];
        }
        
        if ([[to objectForKey:@"email"] isEqualToString:currentUserEmail]) {
            [to setObject:currentUserImageData forKey:@"avatar"];
        } else {
            [to setObject:gettingUserImageData forKey:@"avatar"];
        }
        
        [currentMutableMessage setObject:from forKey:@"from"];
        [currentMutableMessage setObject:to forKey:@"to"];
        
        [tempMessagesArray addObject:currentMutableMessage];
    }
    
    messages = [tempMessagesArray sortedArrayUsingDescriptors:sortDescriptors];
    
    
    [UIView animateWithDuration:0 animations:^{
        [self.tableView reloadData];
    } completion:^(BOOL finished) {
        if (messages.count) {
            NSIndexPath *lastCellIndexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
            [self.tableView scrollToRowAtIndexPath:lastCellIndexPath
                                  atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
//        [pullToRefreshManager_ relocatePullToRefreshView];
//        [pullToRefreshManager_ tableViewReloadFinished];
    }];
}


// ------
#pragma mark SSMessagesViewController

//- (SSMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    SSMessageStyle messageStyle;
//    
//    NSDictionary *message = [messages objectAtIndex:indexPath.row];
//    NSDictionary *author = [message objectForKey:@"from"];
//    NSString *authorEmail = [author objectForKey:@"email"];
//    NSString *currentProfileEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
//    
//    if ([authorEmail isEqualToString:currentProfileEmail])
//    {
//        messageStyle = SSMessageStyleRight;
////        NSLog(@"authorEmail is equal currentProfileEmail, set style right");
//    } else {
//        messageStyle = SSMessageStyleLeft;
////        NSLog(@"authorEmail is not equal currentProfileEmail, set style left.\nauthorMail is: %@,\ncurrentProfileEmail: %@", authorEmail, currentProfileEmail);
//    }
//    
//    return messageStyle;
//}

// -------

- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *message = [[messages objectAtIndex:indexPath.row] objectForKey:@"message"];
    
    NSData *messageData = [[message objectForKey:@"text"] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *messageText = [[NSString alloc] initWithData:messageData
                                                  encoding:NSNonLossyASCIIStringEncoding];
    
    return messageText;
}

- (NSData *)avatarForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *message = [messages objectAtIndex:indexPath.row];
    NSData *avatarData = [[message objectForKey:@"from"] objectForKey:@"avatar"];
    
    return avatarData;
}

- (NSString *)attachmentForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *message = [messages objectAtIndex:indexPath.row];
    NSString *attachmentURLString;
    
    NSArray *attaches = [[message objectForKey:@"message"] objectForKey:@"attaches"];
    if ([attaches count]) {
        attachmentURLString = [[attaches lastObject] objectForKey:@"path"];
    } else {
        attachmentURLString = nil;
    }
    
    return attachmentURLString;
}

//- (void)attachmentForRowAtIndexPath:(NSIndexPath *)indexPath
//                     withCompletion:(void (^)(UIImage *completionImage))completionHandler
//{
//    completionHandler([UIImage imageWithColor:[UIColor rainLightGrayColor]]);
////    
////    SSMessageTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
////    cell.contentView.hidden = YES;
//    NSDictionary *message = [messages objectAtIndex:indexPath.row];
//    NSString *attachmentURLString;
//    
//    NSArray *attaches = [[message objectForKey:@"message"] objectForKey:@"attaches"];
//    if ([attaches count]) {
//        attachmentURLString = [[attaches lastObject] objectForKey:@"path"];
//    } else {
//        attachmentURLString = nil;
//    }
//    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    
////    [cell.contentView addSubview:imageView];
//    [imageView sd_setImageWithURL:[NSURL URLWithString:attachmentURLString]
//                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                            completionHandler(image);
////                            [imageView removeFromSuperview];
////                            cell.contentView.hidden = NO;
//                        }];
//}

- (BOOL)isHaveAttachesWithIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *message = [messages objectAtIndex:indexPath.row];
    NSArray *attaches = [[message objectForKey:@"message"] objectForKey:@"attaches"];
    if ([attaches count]) {
        return YES;
    }
    
    return NO;
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *cellIdentifier = @"BubbleCellIdentifier";
//    
//    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        cell = [[SSMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        [cell setBackgroundImage:self.leftBackgroundImage forMessageStyle:SSMessageStyleLeft];
//        [cell setBackgroundImage:self.rightBackgroundImage forMessageStyle:SSMessageStyleRight];
//    }
//    
//    cell.messageStyle = [self messageStyleForRowAtIndexPath:indexPath];
//    cell.messageText = [self textForRowAtIndexPath:indexPath];
//    cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
//    cell.attachmentURLString = [self attachmentForRowAtIndexPath:indexPath];
//    [self attachmentForRowAtIndexPath:indexPath
//                       withCompletion:^(UIImage *completionImage) {
//                           cell.attachmentImage = completionImage;
//                       }];
//    
//    return cell;
    id _cell;
    
    NSDictionary *message = [messages objectAtIndex:indexPath.row];
    NSDictionary *author = [message objectForKey:@"from"];
    NSString *authorEmail = [author objectForKey:@"email"];
    NSString *currentProfileEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
    
    if ([authorEmail isEqualToString:currentProfileEmail])
    {
        if ([self isHaveAttachesWithIndexPath:indexPath]) {
            
            APSenderMediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSenderMedia];
            cell.message = [self textForRowAtIndexPath:indexPath];
            cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
            cell.attachmentURLString = [self attachmentForRowAtIndexPath:indexPath];

            cell.contentView.hidden = YES;
            [cell.senderMedia sd_setImageWithURL:[NSURL URLWithString:cell.attachmentURLString]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                        cell.attachmentImage = image;
                                    });
                                    cell.contentView.hidden = NO;
            }];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            _cell = cell;
            
        } else {
        
            APSenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSender];
            
            cell.message = [self textForRowAtIndexPath:indexPath];
            cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            _cell = cell;
        }
        
    } else {
        
        if ([self isHaveAttachesWithIndexPath:indexPath]) {
            
            APReceiverMediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReceiverMedia];
            cell.message = [self textForRowAtIndexPath:indexPath];
            cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
            cell.attachmentURLString = [self attachmentForRowAtIndexPath:indexPath];
            cell.contentView.hidden = YES;
            [cell.receiverMedia sd_setImageWithURL:[NSURL URLWithString:cell.attachmentURLString]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^{
//                                                 cell.attachmentImage = image;
                                             });
                                             cell.contentView.hidden = NO;
            }];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            _cell = cell;
            
        } else {
            
            APReceiverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReceiver];
            cell.message = [self textForRowAtIndexPath:indexPath];
            cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            _cell = cell;
            
        }
    }
    
    return _cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfMessages = messages.count;
    
    if (numberOfMessages == 0 && !activityView.isAnimating) {
        [self.tableView setHidden:YES];
        [noMessagesLabel setText:NSLocalizedString(@"noMessages", nil)];
        [noMessagesLabel setHidden:NO];
    } else {
        [self.tableView setHidden:NO];
        [noMessagesLabel setHidden:YES];
    }
    
    return numberOfMessages;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select cell!");
    [textView resignFirstResponder];
    NSDictionary *message = [messages objectAtIndex:indexPath.row];
    NSArray *attaches = [[message objectForKey:@"message"] objectForKey:@"attaches"];
    if ([attaches count]) {
        NSString *attachmentURLString = [[attaches lastObject] objectForKey:@"path"];
        if ([attachmentURLString hasSuffix:@".mp4"]) {
            cellAttachmentVideoURL = [NSURL URLWithString:attachmentURLString];
            NSLog(@"cell attachment video url: %@", cellAttachmentVideoURL);
        }
        else if ([attachmentURLString hasSuffix:@".jpeg"])
        {
            cellAttachmentImageURL = [NSURL URLWithString:attachmentURLString];
            NSLog(@"cell attachment image url: %@", cellAttachmentImageURL);
        }
        else
        {
            NSLog(@"something wrong with attachmentURLString");
        }
        [self performSegueWithIdentifier:@"viewAttachment" sender:self];
    }
}

#pragma mark - Success and Failed methods

- (void)messagesSuccess
{
    [self stop];
    [self removeActivityIndicator];
    [self.tableView setHidden:NO];
    [self setDataSource];
}

- (void)messagesFailed
{
    [self removeActivityIndicator];
    [self.tableView setHidden:NO];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)deleteMessageSuccess
{
//    [self removeActivityIndicator];
//    [self updateMessages];
    
    NSMutableArray *messagesTempArray = [messages mutableCopy];
    [messagesTempArray removeObject:[messages objectAtIndex:deletedMessageIndexPath.row]];
    
    [self.tableView beginUpdates];
    messages = [messagesTempArray copy];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:deletedMessageIndexPath]
                          withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
//    [pullToRefreshManager_ tableViewReloadFinished];
//    [pullToRefreshManager_ relocatePullToRefreshView];
}

- (void)deleteMessageFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)messageSentSuccess
{
    NSLog(@"message sent success");
    
    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[self.tableView cellForRowAtIndexPath:[self indexPathForLastCell]];
    
    // This method calls [API getConversationWithUserID:userID] method when animation stops
//    [cell stopActivityAnimationWithSuccess:YES andUserID:userID];
    if (attachmentImage) {
        [self deleteAttachment];
    }
//    [pullToRefreshManager_ relocatePullToRefreshView];
//    [pullToRefreshManager_ tableViewReloadFinished];
    
    [self updateMessages];
}

- (void)messageSentFailed
{
    NSLog(@"message failed");
    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[self.tableView cellForRowAtIndexPath:[self indexPathForLastCell]];
    
    if (cell)
    {
        NSLog(@"indexpath is %@", [self indexPathForLastCell]);
        NSLog(@"cell is %@", cell);
        NSLog(@"cell text is:\n%@", [self textForRowAtIndexPath:[self indexPathForLastCell]]);
    
//        [cell stopActivityAnimationWithSuccess:NO andUserID:userID];
    } else {
        NSLog(@"this cell is beyond");
    }
}

- (void)failedMessageButtonPressed
{
    NSLog(@"failed button pressed");
    failedMessageActionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"deleteMessage", nil)
                                                    otherButtonTitles:NSLocalizedString(@"tryAgain", nil), nil];
    [failedMessageActionSheet showInView:self.view];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - UIActionSheet Delegate

static NSString * const kJSQDemoAvatarIdSquires = @"Jesse Squires";

static NSString * const kJSQDemoAvatarDisplayNameCook = @"Tim Cook";



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet == failedMessageActionSheet)
    {
        
        switch (buttonIndex) {
            case 0:
                [self deleteLastMessage];
                break;
                
            case 1:
                [self sendMessageAgain];
                break;
                
            default:
                break;
        }
    } else if (actionSheet == attachMediaActionSheet)
    {
        switch (buttonIndex) {
            case 0:
                [self takePhotoOrVideo];
                break;
                
            case 1:
                [self chooseAttachmentFromLibrary];
                break;
          
            case 2:
                //[self addLocationMediaMessageCompletion];
                break;
            default:
                break;
        }
    }
    else if (actionSheet == viewAttachActionSheet)
    {
        switch (buttonIndex) {
            case 0:
                [self deleteAttachment];
                break;
            case 1:
                [self viewAttachment];
                break;
                
            default:
                break;
        }
    }
    else
    {
        NSLog(@"Something wrong with action sheet");
    }
}


- (void)addLocationMediaMessageCompletion
{
    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:48.4813622 longitude:34.9126651];
    
    JSQLocationMediaItem *locationItem =[[JSQLocationMediaItem alloc] init];
    
    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:nil];
   
    id<JSQMessageMediaData> media=locationItem;
    
    
    
//    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
//    if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera])
//    {
//        [imagePicker setMediaTypes:[NSArray arrayWithObjects:
//                                    (NSString *)kUTTypeMovie,
//                                    (NSString *)kUTTypeImage, nil]];
//    }
//    [imagePicker setDelegate:self];
//    [imagePicker setAllowsEditing:YES];
//    [imagePicker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
//    [imagePicker setVideoMaximumDuration:60.0f];
    
    
    
    [self presentViewController:media
                       animated:YES
                     completion:nil];
    
    
    
}


- (void)sendMessageAgain
{
    NSLog(@"send message again!");
    NSString *messageText = [[writtenMessage objectForKey:@"message"] objectForKey:@"text"];
    
    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[self.tableView cellForRowAtIndexPath:[self indexPathForLastCell]];
//    [cell startActivityAnimation];
    
    [API sendMessage:messageText toUserWithID:[userID intValue]];
}

- (void)deleteLastMessage
{
    NSLog(@"delete last message!");
    NSMutableArray *messagesTempArray = [messages mutableCopy];
    [messagesTempArray removeObject:writtenMessage];
    
    [self.tableView beginUpdates];
    messages = [messagesTempArray copy];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messages.count inSection:0];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                          withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
//    [pullToRefreshManager_ tableViewReloadFinished];
//    [pullToRefreshManager_ relocatePullToRefreshView];
    
    writtenMessage = nil;
}

- (void)takePhotoOrVideo
{
    NSLog(@"take photo or video!");
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera])
        {
            [imagePicker setMediaTypes:[NSArray arrayWithObjects:
                                        (NSString *)kUTTypeMovie,
                                        (NSString *)kUTTypeImage, nil]];
        }
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [imagePicker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        [imagePicker setVideoMaximumDuration:60.0f];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    } else {
        NSLog(@"camera alert");
    }
}

- (void)chooseAttachmentFromLibrary
{
    NSLog(@"choose attachment from library!");
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            [imagePicker setMediaTypes:[NSArray arrayWithObjects:
                                        (NSString *)kUTTypeMovie,
                                        (NSString *)kUTTypeImage, nil]];
        }
        [imagePicker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        [imagePicker setVideoMaximumDuration:60.0f];
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    }
    else
    {
        NSLog(@"photolibrary alert");
    }
}

- (void)deleteAttachment
{
    NSLog(@"delete attachment");
    [textView.attachment removeFromSuperview];
    attachmentImage = nil;
    attachmentVideoURL = nil;
    textView.attachment = nil;
    
    [self growingTextView:textView willChangeHeight:textView.frame.size.height - 12.0f - 48.0f - 6.0f];
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)viewAttachment
{
    [self performSegueWithIdentifier:@"viewAttachment"
                              sender:self];
}

#pragma mark - Helper Methods

- (void)configureUI
{
    [self configureNavigationBar];
    [self configureTextFieldButton];
    [self configureAttachButton];
}

- (void)configureTextFieldButton
{
    [self.doneButton addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureAttachButton
{
    [self.attachButton addTarget:self action:@selector(attachMedia) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
}

- (NSIndexPath *)indexPathForLastCell
{
    NSIndexPath *indexPath;
    indexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
    NSLog(@"indexPath is: \n%@", indexPath);
    NSLog(@"messages count = %lu", (long)messages.count);
    NSLog(@"number of cells = %lu", (long)[self.tableView numberOfRowsInSection:0]);
    return indexPath;
}

- (void)attachMedia
{
    NSLog(@"attach media");
    [textView resignFirstResponder];
//    NSLocalizedString(@"sendLocation", nil)
    attachMediaActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"attachMedia", nil)
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                           destructiveButtonTitle:NSLocalizedString(@"takePhotoOrVideo", nil)
                                                otherButtonTitles:NSLocalizedString(@"chooseFromLibrary", nil),nil];
    [attachMediaActionSheet showInView:self.view];
}

- (void)sendMessage
{
    NSLog(@"send message method");
//    [pullToRefreshManager_ relocatePullToRefreshView];
//    [pullToRefreshManager_ tableViewReloadFinished];
    
    NSString *messageWithoutSpaces = [textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([messageWithoutSpaces isEqualToString:@""] && !attachmentImage) {
        NSLog(@"empty message!");
        
    } else {
        if (textView.text)
        {
            
            NSLog(@"has text:\n%@", textView.text);
            
//            if (writtenMessage) {
//                [self deleteLastMessage];
//            }
            if (writtenMessage)
            {
                writtenMessage = nil;
            }
            
            NSData *messageData = [textView.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            NSString *messageText = [[NSString alloc] initWithData:messageData
                                                          encoding:NSUTF8StringEncoding];
            [textView setText:@""];
            [textView resignFirstResponder];
            [self addNewCellWithText:messageText];
        }
        else
        {
            if (writtenMessage) {
                [self deleteLastMessage];
            }
            
            NSData *messageData = [textView.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            NSString *messageText = [[NSString alloc] initWithData:messageData
                                                          encoding:NSUTF8StringEncoding];
            [textView setText:@""];
            [textView resignFirstResponder];
            [self addNewCellWithText:messageText];
        }
        
        if (textView.attachment) {
            [self deleteAttachment];
        }
        
    }
}

- (void)addNewCellWithText:(NSString *)messageText
{
    writtenMessage = [self createMessageFromText:messageText];
    
    NSMutableArray *messagesTempArray = [messages mutableCopy];
    [messagesTempArray addObject:writtenMessage];
    messages = [messagesTempArray copy];
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[self indexPathForLastCell]]
                          withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:[self indexPathForLastCell]
                          atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    [self.tableView reloadData];
//    [pullToRefreshManager_ relocatePullToRefreshView];
//    [pullToRefreshManager_ tableViewReloadFinished];
    
    
    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[self.tableView cellForRowAtIndexPath:[self indexPathForLastCell]];
//    [cell startActivityAnimation];
    
    if (attachmentImage)
    {
        if (attachmentVideoURL)
        {
            [API sendMessage:messageText
                toUserWithID:[userID intValue]
          andVideoAttachment:attachmentVideoURL];
        }
        else {
            
            NSLog(@"send message with attachment");
            
            NSData *imageData = UIImageJPEGRepresentation(attachmentImage, 0.7);;
            
            [API sendMessage:messageText
                toUserWithID:[userID intValue]
          andPhotoAttachment:imageData];
        }
    }
    else
    {
        [API sendMessage:messageText toUserWithID:[userID intValue]];
    }
//    [pullToRefreshManager_ relocatePullToRefreshView];
//    [pullToRefreshManager_ tableViewReloadFinished];
}


- (NSDictionary *)createMessageFromText:(NSString *)messageText
{
    NSMutableDictionary *message = [[NSMutableDictionary alloc] init];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
    NSString *createdAt = [dateFormatter stringFromDate:date];
    
    NSMutableDictionary *from = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"] mutableCopy];
    [from setObject:[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] forKey:@"email"];
    
    NSData *avatarData = UIImagePNGRepresentation(currentUserImage);
    if(avatarData)
    {
        [from setObject:avatarData forKey:@"avatar"];
    }

    NSDictionary *to = [from copy];
    
    NSArray *attaches = [NSArray array];
    if (attachmentImage) {
        
        if (attachmentVideoURL) {
            NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"videoAttachment.png"], 0.7);
            [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"messageAttachment"];
            NSString *URLString = [NSString stringWithFormat:@"%@", attachmentVideoURL];
            attaches = @[@{@"filename":@"video.mp4", @"path":URLString}];
        } else {
            NSData *imageData = UIImageJPEGRepresentation(attachmentImage, 0.7);
            [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"messageAttachment"];
            attaches = @[@{@"filename":@"image.jpeg", @"path":@"defaults"}];
        }
    }
    
    NSDictionary *messageContent = @{@"attaches":attaches, @"created_at":createdAt, @"id":@3333, @"read":@1, @"text":messageText};
    
    [message setObject:createdAt forKey:@"created_at"];
    [message setObject:from forKey:@"from"];
    [message setObject:to forKey:@"to"];
    [message setObject:messageContent forKey:@"message"];
    
    return [message copy];
}

- (void)deleteWrittenMessage
{
    writtenMessage = nil;
}

#pragma mark - UIImagePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (attachmentImage)
    {
        [self deleteAttachment];
    }
    
    NSLog(@"pickerController info dictionary is:\n%@", info);
    
    // if photo
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
        UIImage *pickerImage = [[info objectForKey:UIImagePickerControllerEditedImage] copy];
        NSLog(@"picker image is:\n%@", pickerImage);
        
        attachmentImage = [pickerImage copy];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } else if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"])
    {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSLog(@"vide captured is:\n%@", videoURL);
        
        attachmentVideoURL = videoURL;
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:attachmentVideoURL options:nil];
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 2);
        CGImageRef oneRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *videoPreviewImage = [[UIImage alloc] initWithCGImage:oneRef];
        attachmentImage = videoPreviewImage;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        NSLog(@"something wrong with UIImagePicker Source Type");
    }
    
    [self addAttachmentToTextView];
}

- (void)addAttachmentToTextView
{
    NSLog(@"add attachment");
    textView.attachment = [[UIImageView alloc] initWithFrame:CGRectMake(52.0f, textView.frame.size.height + 12.0f, 48.0f, 48.0f)];
    [textView.attachment setImage:attachmentImage];
    [textView.attachment setContentMode:UIViewContentModeScaleAspectFill];
    [textView.attachment setClipsToBounds:YES];
    [textView.attachment setUserInteractionEnabled:YES];
    [[textView.attachment layer] setCornerRadius:textView.attachment.frame.size.width/10];
    
    [textView setUserInteractionEnabled:YES];
    
    [containerView addSubview:textView.attachment];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachmentButtonPressed:)];
    [gesture setDelegate:self];
    [textView.attachment addGestureRecognizer:gesture];
    
    [textView setMaxNumberOfLines:4];
    
    [self growingTextView:textView willChangeHeight:textView.frame.size.height + 12.0f + 48.0f + 6.0f];
    self.tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, self.tableView.contentInset.bottom + 12.0f + 48.0f + 6.0f, 0.0f);
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
}

- (void)attachmentButtonPressed:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    NSLog(@"attachment button pressed!");
    viewAttachActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"attachment", nil)
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                          destructiveButtonTitle:NSLocalizedString(@"delete", nil)
                                               otherButtonTitles:NSLocalizedString(@"view", nil), nil];
    [viewAttachActionSheet showInView:self.view];
}

#pragma mark - Activity

- (void)showActivityIndicator
{
    [noMessagesLabel setHidden:YES];
    activityView = [[UIActivityIndicatorView alloc] init];
    [activityView setColor:[UIColor colorWithRed:93.0/255.0
                                           green:124.0/255.0
                                            blue:173.0/255.0
                                           alpha:1.0]];
    
    CGPoint activityViewCenter = self.view.center;
    activityViewCenter.y -= (kTableViewMarginBottom + kTextViewHeight);
    
    [activityView setCenter:activityViewCenter];
    [self.view addSubview:activityView];
    [activityView startAnimating];
}

- (void)removeActivityIndicator
{
    [activityView stopAnimating];
    [activityView removeFromSuperview];
}

#pragma mark  - MNMBottomPullToRefreshManagerClient

- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
    
//    [self performSelector:@selector(updateMessages) withObject:nil afterDelay:1.0f];
}

- (void)updateMessages
{
    [refreshControl beginRefreshing];
    
    [API getConversationWithUserID:userID];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_async(queue, ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:@"ConversationSuccess" object:self];
    });
 
}

#pragma mark - Navigatio

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewAttachment"]) {
        
        if (cellAttachmentImageURL || cellAttachmentVideoURL) {
            [segue.destinationViewController setImageURL:cellAttachmentImageURL];
            [segue.destinationViewController setVideoURL:cellAttachmentVideoURL];
        } else {
            if (attachmentVideoURL) {
                [segue.destinationViewController setVideoURL:attachmentVideoURL];
            } else {
                [segue.destinationViewController setPhoto:attachmentImage];
            }
        }
    }
}


@end
