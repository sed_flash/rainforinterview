//
//  APSettingsVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APSettingsVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface APSettingsVC ()

@end

@implementation APSettingsVC

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    API = [[APServerAPI alloc] init];
    [API getUserSettings];
    [self setupUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logoutSegue)
                                                 name:@"Logout"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(settingsSuccess)
                                                 name:@"SettingsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(settingsFailed)
                                                 name:@"SettingsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteSuccess)
                                                 name:@"DeleteAccountSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteFailed)
                                                 name:@"DeleteAccountFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [self.tabBarController.tabBar setHidden:YES];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settingsSuccess
{
    NSLog(@"settings success");
    [self configureSwitches];
}

- (void)configureSwitches
{
    NSDictionary *settings = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSettings"];
    
    int hideProfile = (int)[[settings objectForKey:@"hide_profile"] integerValue];
    int notifications = (int)[[settings objectForKey:@"notifications"] integerValue];
    
    NSLog(@"hide profile = %d", hideProfile);
    if (hideProfile == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SetAccessOn" object:self];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SetAccessOff" object:self];
    }
    
    NSLog(@"notifications = %d", notifications);
    if (notifications == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SetPushOff" object:self];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SetPushOn" object:self];
    }
}

- (void)deleteSuccess
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Letter sent", nil)
                                                    message:NSLocalizedString(@"Your account will be deleted. Check your email for confirmation link.", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                          otherButtonTitles:nil];
    [alert setTag:2];
    [alert show];
}

- (void)deleteFailed
{
    [self showAlertWithIdentifier:serverError];
}

- (void)settingsFailed
{
    NSLog(@"settings failed");
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    NSLog(@"server error");
    [self showAlertWithIdentifier:serverError];
}

#pragma mark - Alerts

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)deleteAccount:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"areYouSure?", nil)
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"no", nil)
                                          otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
    [alert setTag:1];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 1) {
        if (buttonIndex == 1)
        {
            NSLog(@"delete account!");
            [API deleteAccount];
        } else
        {
            NSLog(@"button index is %d", buttonIndex);
        }
    }
    else if([alertView tag] == 2)
    {
        if (buttonIndex == 0)
        {
            [[self navigationController] popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Helper Methods

- (void)setupUIElements
{
    [self setTitle:NSLocalizedString(@"settings", nil)];
}

- (void)logoutSegue
{
    NSLog(@"logout segue");
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"avatar_small"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
     [login logOut];
    [self performSegueWithIdentifier:@"LogoutSegue"
                              sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LogoutSegue"]) {
        [[[self navigationController] navigationItem] setHidesBackButton:YES animated:NO];
    }
}

@end
