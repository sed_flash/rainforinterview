//
//  APLikeVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "APFollowCell.h"
#import "APLikeCell.h"
#import "APCommentsNotificationCell.h"
#import "APMessageNotificationCell.h"
#import "APFollowingNotificationCell.h"
#import "APFollowingFNotificationCell.h"
#import "APUnconfirmedCell.h"
#import "APProfileVC.h"
#import "APPostVC.h"

@interface APLikeVC : UNViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UISegmentedControl *newsSegmentedControl;
    IBOutlet UITableView *newsTableView;
    IBOutlet UILabel *noNewsLabel;
    
    APServerAPI *API;
    
    NSArray *followingsNews;
    NSArray *userNews;
    
    NSString *selectedUserEmail;
    NSDictionary *selectedPost;
    NSDictionary *selectedUser;
    
    UIActivityIndicatorView *activityView;
    UIRefreshControl *refreshControl;
}

@end
