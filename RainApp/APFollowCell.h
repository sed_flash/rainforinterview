//
//  APFollowCell.h
//  Rain
//
//  Created by EgorMac on 09/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "NSDate+TimeAgo.h"

@interface APFollowCell : UITableViewCell
{
    IBOutlet UIImageView *photoImageView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *timeAgoLabel;
    IBOutlet UIButton *userButton;
    
    NSDictionary *notification;
}

@property (nonatomic, strong) NSDictionary *notification;
@property (nonatomic, strong) IBOutlet UIButton *userButton;

@end
