//
//  FilterCell.h
//  Rain
//
//  Created by EgorMac on 07/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell
{
    NSString *filterID;
    IBOutlet UILabel *filterTitle;
}

@property (nonatomic, strong) NSString *filterID;
@property (nonatomic, strong) IBOutlet UILabel *filterTitle;

@end
