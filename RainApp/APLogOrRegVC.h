//
//  APViewController.h
//  RainApp
//
//  Created by EgorMac on 15/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface APLogOrRegVC : UIViewController
{
     APServerAPI *API;
}

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIButton *loginWithFacebook;

- (IBAction)authWithFB:(id)sender;
@end
