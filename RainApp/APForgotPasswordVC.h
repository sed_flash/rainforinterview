//
//  APForgotPasswordVC.h
//  Rain
//
//  Created by EgorMac on 18/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APForgotPasswordVC : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIBarButtonItem *cancelButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextField *emailTextField;
    IBOutlet UIButton *searchButton;
    IBOutlet UIImageView *backgroundImageView;
    APServerAPI *API;
}

@property (nonatomic, strong) APServerAPI *API;

- (IBAction)search:(id)sender;
- (IBAction)cancel:(id)sender;

@end
