//
//  UIColor+CustomColor.h
//  Rain
//
//  Created by EgorMac on 25/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

+(UIColor *)rainBlueColor;
+(UIColor *)rainGoldColor;
+(UIColor *)rainLightGrayColor;
+(UIColor *)rainPurple;

@end
