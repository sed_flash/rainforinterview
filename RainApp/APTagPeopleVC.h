//
//  APTagPeopleVC.h
//  Rain
//
//  Created by EgorMac on 16/06/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@protocol TagPeopleDelegate <NSObject>
- (void)tagPeopleVCDissmissed:(NSDictionary *)taggedFriend;
@end

@interface APTagPeopleVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    id __unsafe_unretained tagPeopleDelegate;
    
    IBOutlet UIBarButtonItem *cancelButton;
    IBOutlet UITableView *peopleTableView;
    
    UIActivityIndicatorView *activityIndicator;
    APServerAPI *API;
    
    NSArray *people;
    NSArray *searchResults;
}

@property (nonatomic, assign) id<TagPeopleDelegate> tagPeopleDelegate;
@property (nonatomic, strong) APServerAPI *API;

- (IBAction)cancel:(id)sender;

@end
