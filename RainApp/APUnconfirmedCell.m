//
//  APUnconfirmedCell.m
//  Rain
//
//  Created by EgorMac on 25/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APUnconfirmedCell.h"
#import "APServerAPI.h"

@implementation APUnconfirmedCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setNotification:(NSDictionary *)newNotification
{
    [super setNotification:newNotification];
    
    [descriptionLabel setText:NSLocalizedString(@"wantsToFollowYou", nil)];
}

- (IBAction)confirm:(id)sender
{
    NSString *followID = [[notification objectForKey:@"follow_id"] stringValue];
    
    APServerAPI *API = [[APServerAPI alloc] init];
    [API confirmFollowRequestWithID:followID];
}

- (IBAction)decline:(id)sender
{
    NSString *followID = [[notification objectForKey:@"follow_id"] stringValue];
    APServerAPI *API = [[APServerAPI alloc] init];
    [API declineFollowRequestWithID:followID];
}

@end
