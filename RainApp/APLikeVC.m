//
//  APLikeVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APLikeVC.h"

@interface APLikeVC ()

@end

@implementation APLikeVC {
    NSMutableArray *likeArr;
}

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tb=newsTableView;
    
    likeArr = [NSMutableArray new];
    
    // Do any additional setup after loading the view.
    [self configureUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newsSuccess)
                                                 name:@"NewsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newsFailed)
                                                 name:@"NewsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationsSuccess)
                                                 name:@"NotificationsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationsFailed)
                                                 name:@"NotificationsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmationSuccess)
                                                 name:@"confirmFollowSuccess"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmationFailed)
                                                 name:@"confirmFollowFailed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(declineFollowSuccess)
                                                 name:@"declineFollowSuccess"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(declineFollowFailed)
                                                 name:@"declineFollowFailed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteAtIndexPath:)
                                                 name:@"needToDeleteNotification"
                                               object:nil];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor colorWithRed:93.0/255.0
                                               green:124.0/255.0
                                                blue:173.0/255.0
                                               alpha:1.0];
    [refreshControl addTarget:self
                       action:@selector(checkForNews)
             forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = newsTableView;
    controller.refreshControl = refreshControl;
    
    if ([newsTableView numberOfRowsInSection:0] == 0) {
        [self showActivityIndicator];
        [API getNews];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)deleteAtIndexPath:(NSNotification *)notification {
    NSIndexPath *indexPath = [notification object];
    [newsTableView beginUpdates];
    [newsTableView deleteRowsAtIndexPaths:indexPath
                         withRowAnimation:UITableViewRowAnimationNone];
    [newsTableView endUpdates];
}

#pragma mark - Data Source Methods

- (IBAction)changeNewsSegmentedControlIndex:(id)sender
{
    [newsTableView setHidden:YES];
    if ([activityView isAnimating]
        )
    {
        [activityView stopAnimating];
        [activityView removeFromSuperview];
    }
    
    [self showActivityIndicator];
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedLeft"];
        [newsSegmentedControl setBackgroundImage:selectedBackgroundImage
                                        forState:UIControlStateSelected
                                      barMetrics:UIBarMetricsDefault];
        
        [API getNotifications];
    }
    else
    {
        UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedRight"];
        [newsSegmentedControl setBackgroundImage:selectedBackgroundImage
                                        forState:UIControlStateSelected
                                      barMetrics:UIBarMetricsDefault];
        
        [API getNews];
    }
}

- (void)setDataSource
{
    NSMutableArray *tempNewsArray = [[NSMutableArray alloc] init];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    
    if([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        
        followingsNews = [[[NSUserDefaults standardUserDefaults] objectForKey:@"notifications"] copy];
        tempNewsArray = [followingsNews mutableCopy];
        followingsNews = [tempNewsArray sortedArrayUsingDescriptors:sortDescriptors];
    } else {
        
        userNews = [[[NSUserDefaults standardUserDefaults] objectForKey:@"news"] copy];
        tempNewsArray = [userNews mutableCopy];
        userNews = [tempNewsArray sortedArrayUsingDescriptors:sortDescriptors];
    }
    [refreshControl endRefreshing];
    [newsTableView reloadData];
}

- (void)checkForNews
{
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        [API getNotifications];
    }
    else
    {
        [API getNews];
    }
}

#pragma mark - Table View Methods

static const CGFloat PHOTO_HEIGHT = 48.0f;
static const CGFloat PHOTO_MARGIN = 2.0f;
static const CGFloat AUTHOR_HEIGHT = 50.0f;

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 70.0f;
    
    NSDictionary *notification;
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        notification = [followingsNews objectAtIndex:[indexPath row]];
        if ([[notification objectForKey:@"type"] isEqualToString:@"like"]) {
            NSArray *photos = [notification objectForKey:@"posts"];
            NSUInteger photosCount = [photos count];
            
            CGFloat photosHeight = (float)photosCount/5;
            NSUInteger photosRowsCount = ceil(photosHeight);
            
            height = ((PHOTO_HEIGHT + PHOTO_MARGIN) * photosRowsCount) + AUTHOR_HEIGHT;
        }
    } else {
        notification = [userNews objectAtIndex:[indexPath row]];
        
        if ([[notification objectForKey:@"type"] isEqualToString:@"comments"]) {
            height = 84.0f;
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"follow"])
        {
            if ([[notification objectForKey:@"confirm"] integerValue] == 0)
            {
                height = 90.0f;
            } else {
                height = 70.0f;
            }
        }
        else
        {
            height = 70.0f;
        }
    }
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0) {
        count = [followingsNews count];
    } else {
        
//        count = [userNews count];
        count = [self arrayWithinDublicatesWithArray:userNews];
    }
    
    if (count == 0 && ![activityView isAnimating]) {
        [newsTableView setHidden:YES];
        [noNewsLabel setText:NSLocalizedString(@"noNews", nil)];
        [noNewsLabel setHidden:NO];
    } else {
        [newsTableView setHidden:NO];
        [noNewsLabel setHidden:YES];
    }
    
    NSLog(@"notifications count = %ld", (long)count);
    
    return count;
}

- (NSInteger)arrayWithinDublicatesWithArray:(NSArray *)arr {
    if (arr == nil)
        return 0;
    likeArr = [NSMutableArray new];
        for (int i = 0; i < arr.count; i++) {
            BOOL flag = NO;
            for (NSDictionary *d in likeArr) {
                @try {
                    NSString *currentImage = [[d objectForKey:@"item"] objectForKey:@"full_image"];
                    NSString *oldImage = [[arr[i] objectForKey:@"item"] objectForKey:@"full_image"];
                    NSString *currentEmail = [[d objectForKey:@"user"] objectForKey:@"email"];
                    NSString *oldEmail = [[arr[i] objectForKey:@"user"] objectForKey:@"email"];
                    
                    if ([currentImage isEqualToString:oldImage] &&
                        [currentEmail isEqualToString:oldEmail]) {
                        flag = YES;
                        continue;
                    }
                } @catch (NSException *exception) {
                    NSLog(@"Exception is %@", exception);
                }
                
            }
            if (!flag) {
                [likeArr addObject:arr[i]];
            }
        }
    
    userNews = likeArr;
    likeArr = nil;
    return userNews.count;
}

static const NSUInteger USER_BUTTON_TAG_DIFFERENCE = 1;
static const NSUInteger SECOND_USER_BUTTON_TAG_DIFFERENCE = 101;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *notification;
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        @try {
            
        notification = [followingsNews objectAtIndex:[indexPath row]];
        
        if ([[notification objectForKey:@"type"] isEqualToString:@"follow"])
        {
            APFollowingFNotificationCell *cell = (APFollowingFNotificationCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowingFNotificationCell"];
            [cell setNotification:notification];
            
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            [[cell secondUserButton] setTag:[indexPath row] + SECOND_USER_BUTTON_TAG_DIFFERENCE];
            [[cell secondUserButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        
        else if ([[notification objectForKey:@"type"] isEqualToString:@"like"])
        {
            static NSString *followingCellIdentifier = @"FollowingNotificationCell";
            APFollowingNotificationCell *cell = (APFollowingNotificationCell *)[tableView dequeueReusableCellWithIdentifier:followingCellIdentifier];
            [cell setNotification:notification fromTableView:tableView atIndexPath:indexPath];
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else
        {
            UITableViewCell *cell = [[UITableViewCell alloc] init];
            return cell;
        }
        }
        @catch (NSException *exception)
        {
            
        }
      
    }
    else
    {
        @try {
             notification = [userNews objectAtIndex:[indexPath row]];
        
        if ([[notification objectForKey:@"type"] isEqualToString:@"follow"])
        {
            if ([[notification objectForKey:@"confirm"] integerValue] == 0)
            {
                APUnconfirmedCell *cell = (APUnconfirmedCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowUnconfirmedCell"];
                [cell setNotification:notification];
                
                [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
                [[cell userButton] addTarget:self
                                      action:@selector(showUser:)
                            forControlEvents:UIControlEventTouchUpInside];
                
                return cell;
            }
            else
            {
                APFollowCell *cell = (APFollowCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowCell"];
                [cell setNotification:notification];
                
                [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
                [[cell userButton] addTarget:self
                                      action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
                
                return cell;
            }
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"like"])
        {
            APLikeCell *cell = (APLikeCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeCell"];
            [cell setNotification:notification];
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"comments"])
        {
            APCommentsNotificationCell *cell = (APCommentsNotificationCell *)[tableView dequeueReusableCellWithIdentifier:@"CommentsNotificationCell"];
            [cell setNotification:notification];
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"repost"])
        {
            APLikeCell *cell = (APLikeCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeCell"];
            [cell setNotification:notification];
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"message"])
        {
            APMessageNotificationCell *cell = (APMessageNotificationCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageNotificationCell"];
            [cell setNotification:notification];
            
            [[cell userButton] setTag:[indexPath row] + USER_BUTTON_TAG_DIFFERENCE];
            [[cell userButton] addTarget:self
                                  action:@selector(showUser:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else
        {
            NSLog(@"type is - %@", [notification objectForKey:@"type"]);
            NSLog(@"failed notification - \n%@", notification);
            UITableViewCell *cell = [[UITableViewCell alloc] init];
            return cell;
        }
        }
        @catch(NSException *exception)
        {
            
        }

      
        
    }
        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *notification;
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        notification = [followingsNews objectAtIndex:[indexPath row]];
    
        if ([[notification objectForKey:@"type"] isEqualToString:@"like"]) {
            
            APFollowingNotificationCell *cell = (APFollowingNotificationCell *)[tableView cellForRowAtIndexPath:indexPath];
            
            selectedPost = cell.selectedLikedPost;
            NSLog(@"selected post is: \n%@", selectedPost);
            selectedUser = [selectedPost objectForKey:@"user"] ? [selectedPost objectForKey:@"user"] : [self emptyAuthorDictionary];
            
            if (selectedPost) {
                [self performSegueWithIdentifier:@"showPostFromNotifications" sender:self];
            }
        }
    } else {
        notification = [userNews objectAtIndex:[indexPath row]];
        if ([[notification objectForKey:@"type"] isEqualToString:@"comments"] ||
            [[notification objectForKey:@"type"] isEqualToString:@"like"] ||
            [[notification objectForKey:@"type"] isEqualToString:@"repost"])
        {
            selectedPost = [notification objectForKey:@"item"];
//            selectedUser = [notification objectForKey:@"user"];
            selectedUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
            NSLog(@"selected post id is %@", [[selectedPost objectForKey:@"id"] stringValue]);
            [self performSegueWithIdentifier:@"showPostFromNotifications" sender:self];
        }
    }
}

#pragma mark - Success and Failed methods

- (void)newsSuccess
{
    [self removeActivityIndicator];
    [newsTableView setHidden:NO];
    [self setDataSource];
}

- (void)newsFailed
{
    [self removeActivityIndicator];
    [newsTableView setHidden:NO];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)notificationsSuccess
{
    [self removeActivityIndicator];
    [newsTableView setHidden:NO];
    [self setDataSource];
}

- (void)notificationsFailed
{
    [self removeActivityIndicator];
    [newsTableView setHidden:NO];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)confirmationSuccess
{
    NSLog(@"confirmation success");
    [self checkForNews];
}

- (void)confirmationFailed
{
    NSLog(@"confirmationFailed");
    [self showAlertWithIdentifier:serverAlert];
}

- (void)declineFollowSuccess
{
    NSLog(@"declineFollowSuccess");
    [self checkForNews];
}

- (void)declineFollowFailed
{
    NSLog(@"declineFollowFailed");
    [self showAlertWithIdentifier:serverAlert];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)configureUIElements
{
    [self configureNavigationBar];
    [newsSegmentedControl setSelectedSegmentIndex:1];
    [newsSegmentedControl setTitle:NSLocalizedString(@"following", nil)
                    forSegmentAtIndex:0];
    [newsSegmentedControl setTitle:NSLocalizedString(@"news", nil)
                    forSegmentAtIndex:1];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"
                                                                                               size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    
    UIImage *normalBackgroundImage = [UIImage imageNamed:@"sectionBackground"];
    [newsSegmentedControl setBackgroundImage:normalBackgroundImage
                                               forState:UIControlStateNormal
                                             barMetrics:UIBarMetricsDefault];
    UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedRight"];
    [newsSegmentedControl setBackgroundImage:selectedBackgroundImage
                                               forState:UIControlStateSelected
                                             barMetrics:UIBarMetricsDefault];
    [newsSegmentedControl setFrame:CGRectMake(0, 0, 240, 44)];
    [newsSegmentedControl setTintColor:[UIColor clearColor]];
    [newsSegmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:
                                                                  [UIColor colorWithRed:133.0/255.0
                                                                                  green:163.0/255.0
                                                                                   blue:195.0/255.0
                                                                                  alpha:1.0]}
                                                   forState:UIControlStateNormal];
    [newsSegmentedControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                                                   forState:UIControlStateSelected];
    [newsSegmentedControl setWidth:110.0f forSegmentAtIndex:0];
}

- (NSDictionary *)emptyAuthorDictionary
{
    NSDictionary *author = [[NSDictionary alloc] initWithObjects:@[@"", @"", @"", @"", @""]
                                                         forKeys:@[@"avatar", @"avatar_small", @"email", @"id", @"nick"]];
    return author;
}

#pragma mark - Activity

- (void)showActivityIndicator
{
    [newsTableView setHidden:YES];
    [noNewsLabel setHidden:YES];
    activityView = [[UIActivityIndicatorView alloc] init];
    [activityView setColor:[UIColor colorWithRed:93.0/255.0
                                           green:124.0/255.0
                                            blue:173.0/255.0
                                           alpha:1.0]];
    [activityView setCenter:self.view.center];
    [[self view] addSubview:activityView];
    [activityView startAnimating];
}

- (void)removeActivityIndicator
{
    [activityView stopAnimating];
    [activityView removeFromSuperview];
}

- (IBAction)showUser:(id)sender
{
    NSDictionary *notification;
    
    if ([newsSegmentedControl selectedSegmentIndex] == 0)
    {
        if ([sender tag] >= SECOND_USER_BUTTON_TAG_DIFFERENCE) {
            notification = [followingsNews objectAtIndex:[sender tag] - SECOND_USER_BUTTON_TAG_DIFFERENCE];
            selectedUserEmail = [[notification objectForKey:@"to"] objectForKey:@"email"];
        } else {
            notification = [followingsNews objectAtIndex:[sender tag] - USER_BUTTON_TAG_DIFFERENCE];
            selectedUserEmail = [[notification objectForKey:@"user"] objectForKey:@"email"];
        }
    } else {
        notification = [userNews objectAtIndex:[sender tag] - USER_BUTTON_TAG_DIFFERENCE];
        selectedUserEmail = [[notification objectForKey:@"user"] objectForKey:@"email"];
    }

    NSLog(@"User with %@ selected", selectedUserEmail);

    [self performSegueWithIdentifier:@"showUserFromNotifications" sender:self];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showUserFromNotifications"])
    {
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] isEqualToString:selectedUserEmail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserEmail];
    }
    else if ([[segue identifier] isEqualToString:@"showPostFromNotifications"])
    {
        [[segue destinationViewController] setHidesBottomBarWhenPushed:YES];
        [[segue destinationViewController] setCurrentPost:selectedPost];
        [[segue destinationViewController] setAuthor:selectedUser];
    }
}

@end
