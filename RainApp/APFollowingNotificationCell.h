//
//  APFollowingNotificationCell.h
//  Rain
//
//  Created by EgorMac on 28/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "NSDate+TimeAgo.h"

@interface APFollowingNotificationCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>
{
    IBOutlet UIImageView *photoImageView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *timeAgoLabel;
    IBOutlet UIButton *userButton;
    
    IBOutlet UICollectionView *photosCollectionView;
    
    NSDictionary *notification;
    NSDictionary *selectedLikedPost;
    NSArray *photos;
}

@property (nonatomic, weak) UITableView *parentTableView;
@property (nonatomic, strong) NSIndexPath *selfIndexPath;
@property (nonatomic, strong) NSDictionary *notification;
@property (nonatomic, strong) IBOutlet UIButton *userButton;
@property (nonatomic, strong) NSDictionary *selectedLikedPost;

- (void)setNotification:(NSDictionary *)newNotification fromTableView:(UITableView *)aTableView atIndexPath:(NSIndexPath *)indexPath;

@end
