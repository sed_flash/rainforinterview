//
//  APAuthorView.m
//  Rain
//
//  Created by EgorMac on 23/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APAuthorView.h"

@implementation APAuthorView

@synthesize author, timeAgo, button;

- (void)setAuthor:(NSDictionary *)newAuthor
{
    if (author != newAuthor) {
        author = newAuthor;
        
        [nameLabel setText:[author objectForKey:@"first_name"]];
        
        NSString *photoString = [author objectForKey:@"avatar"];
        NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
        
        UIImage *placeholder = [UIImage imageNamed:@"ProfileEditPhotoBlue"];
        
        __weak NSString *weakAvatarPath = photoString;
        __weak UIImageView *weakAvatarImageView = photoView;
        
        [photoView setImageWithURLRequest:photoRequest
                               placeholderImage:placeholder
                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
//             [weakAvatarImageView setImage:image];
             [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                        forImage:image];
         }
         
                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                            NSLog(@"fail for %@", weakAvatarPath);
//                                            [weakAvatarImageView setImage:placeholder];
                                            [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                                                       forImage:placeholder];
                                        }];
    }
}

- (void)setTimeAgo:(NSString *)newTimeAgo
{
    timeAgo = newTimeAgo;
    [timeAgoLabel setText:timeAgo];
}

//- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
//    
//    CGImageRef maskRef = maskImage.CGImage;
//    
//    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
//                                        CGImageGetHeight(maskRef),
//                                        CGImageGetBitsPerComponent(maskRef),
//                                        CGImageGetBitsPerPixel(maskRef),
//                                        CGImageGetBytesPerRow(maskRef),
//                                        CGImageGetDataProvider(maskRef), NULL, false);
//    
//    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
//    return [UIImage imageWithCGImage:masked];
//}

@end
