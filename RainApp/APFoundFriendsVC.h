//
//  APFoundFriendsVC.h
//  Rain
//
//  Created by EgorMac on 13/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APFoundFriendCell.h"
#import "UIImageView+AFNetworking.h"
#import "APProfileVC.h"

@interface APFoundFriendsVC : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIBarButtonItem *filterButton;
    IBOutlet UICollectionView *friendsCollectionView;
    IBOutlet UILabel *noResultsLabel;
    
    NSDictionary *foundUsers;
    NSArray *foundUsersIDs;
    
    NSString *selectedUserEmail;
}

- (IBAction)filtering:(id)sender;

@end
