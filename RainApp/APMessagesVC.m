//
//  APMessagesVC.m
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APMessagesVC.h"

@interface APMessagesVC ()

@property(nonatomic,strong)  UITableViewController *controller;

@end

@implementation APMessagesVC

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    API = [[APServerAPI alloc] init];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadFromPush)
                                                 name:@"loadFromPush"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dialogsSuccess)
                                                 name:@"DialogsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dialogsFailed)
                                                 name:@"DialogsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(conversationDeletedSuccess)
                                                 name:@"DeleteConversationSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(conversationDeletedFailed)
                                                 name:@"DeleteConversationFailed"
                                               object:API];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(checkForNewDialogs)
             forControlEvents:UIControlEventValueChanged];
    
    self.controller = [[UITableViewController alloc] init];
    self.controller.tableView = dialogsTableView;
    self.controller.refreshControl = refreshControl;
    
//    [self beginRefreshingTableView];
    //[self showActivityIndicator];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadFromPush" object:self];
}

-(void)beginRefreshingTableView
{
    
    [refreshControl beginRefreshing];
    
    if (self.controller.tableView.contentOffset.y == 0) {
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){
            
            self.controller.tableView.contentOffset = CGPointMake(0, -refreshControl.frame.size.height);
            
        } completion:^(BOOL finished){
            
        }];
        
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Source Method

- (void)setDataSource
{
    dialogs = [[[NSUserDefaults standardUserDefaults] objectForKey:@"dialogs"] copy];
    [refreshControl endRefreshing];
    [dialogsTableView reloadData];
    if ([dialogs count] == 0)
    {
        [noDialogsLabel setHidden:NO];
        [noDialogsLabel setText:NSLocalizedString(@"noDialogs", nil)];
    } else
    {
        [noDialogsLabel setHidden:YES];
    }
}


- (void)loadFromPush
{
//    [self beginRefreshingTableView];
    [API getDialogs];
}



- (void)checkForNewDialogs
{
    [API getDialogs];
}

#pragma mark - Table View Methods

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSString *deletingDialogEmail = [[[dialogs objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"email"];
        deletedConversationIndexPath = indexPath;
        [API deleteConversation:deletingDialogEmail];
        NSLog(@"deleting dialog with email:\"%@\"", [[[dialogs objectAtIndex:indexPath.row] objectForKey:@"user"] objectForKey:@"email"]);
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [dialogs count];
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APDialogsCell *cell = (APDialogsCell *)[tableView dequeueReusableCellWithIdentifier:@"DialogsCell"];
    
    NSDictionary *dialog = [dialogs objectAtIndex:[indexPath row]];
    
    [cell setDialog:dialog];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dialog = [dialogs objectAtIndex:[indexPath row]];
    NSDictionary *dialogUser = [dialog objectForKey:@"user"];
    
    selectedUserID = [[dialogUser objectForKey:@"id"] stringValue];
    selectedUserNickname = [dialogUser objectForKey:@"nick"];
    
    APDialogsCell *cell = (APDialogsCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    selectedUserAvatarImage = [[cell avatarView] image];
    
    NSLog(@"User with %@ selected", selectedUserID);
    
    [self performSegueWithIdentifier:@"showDialog" sender:self];
}

#pragma mark - Success and Failed methods

- (void)dialogsSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)dialogsFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)conversationDeletedSuccess
{
    NSMutableArray *dialogsMutable = [dialogs mutableCopy];
    [dialogsMutable removeObjectAtIndex:deletedConversationIndexPath.row];
    dialogs = [dialogsMutable copy];
    [dialogsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:deletedConversationIndexPath]
                            withRowAnimation:UITableViewRowAnimationFade];
}

- (void)conversationDeletedFailed
{
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)configureUIElements
{
    [dialogsTableView setAllowsMultipleSelectionDuringEditing:NO];
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[self navigationItem] setTitle:NSLocalizedString(@"dialogs", nil)];
}


#pragma mark - Activity




- (void)showActivityIndicator
{
    [dialogsTableView setHidden:YES];
    activityView = [[UIActivityIndicatorView alloc] init];
    [activityView setColor:[UIColor whiteColor]];
    
    CGPoint center = self.view.center;
    center.y -= (self.tabBarController.tabBar.frame.size.height + self.navigationController.navigationBar.frame.size.height);
    [activityView setCenter:center];
    [[self view] addSubview:activityView];
    [activityView startAnimating];
}

- (void)removeActivityIndicator
{
    [dialogsTableView setHidden:NO];
    [activityView stopAnimating];
    [activityView removeFromSuperview];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDialog"])
    {
        [[segue destinationViewController] setHidesBottomBarWhenPushed:YES];
        [[segue destinationViewController] setUserID:selectedUserID];
        [[segue destinationViewController] setGettingUserImage:selectedUserAvatarImage];
        [[segue destinationViewController] setTitle:selectedUserNickname];
    }
}

@end
