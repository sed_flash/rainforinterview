//
//  APAttachmentVC.m
//  Rain
//
//  Created by EgorMac on 05/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APAttachmentVC.h"
#import "UIImageView+AFNetworking.h"
#import "APVideoModel.h"

@interface APAttachmentVC ()

@end

@implementation APAttachmentVC
@synthesize photo, videoURL, imageURL;
@synthesize moviePlayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"attachmentVC viewDidLoad");
    [self configureDoneButton];
    
    if (videoURL) {
        NSLog(@"show video from URL:\n%@", videoURL);
        
        APVideoModel *videoModel = [[APVideoModel alloc] init];
        
        NSString *path = [videoModel getPathToFileWithURL:videoURL];
        if ([videoModel checkIfVideoIsHasOnDevice:path]) {
            moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
        } else {
            moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
            [videoModel saveVideoToDeviceWithURL:videoURL andPathToSave:path];
        }
        
        [moviePlayer.view setFrame:photoImageView.frame];
        [self.view addSubview:moviePlayer.view];
        
        [moviePlayer setFullscreen:YES animated:YES];
        [moviePlayer setAllowsAirPlay:YES];
        [moviePlayer setShouldAutoplay:NO];
        [moviePlayer setControlStyle:MPMovieControlStyleDefault];
        [moviePlayer setMovieSourceType:MPMovieSourceTypeFile];
        
        [moviePlayer prepareToPlay];
    }
    else if (imageURL)
    {
        __weak UIImageView *weakPhotoImageView = photoImageView;
        
        [photoImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL]
                                   placeholderImage:nil
                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                [weakPhotoImageView setImage:image];
                                            }
                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                NSLog(@"fail attachment image with error:\n%@", [error localizedDescription]);
                                            }];
    }
    else
    {
        NSLog(@"show photo from image:\n%@", photo);
        [photoImageView setImage:photo];
    }
}

- (void)configureDoneButton
{
    [doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveToLibarary:(id)sender
{
    if (videoURL)
    {
        NSLog(@"save video: %@", videoURL);
        NSURLRequest *request = [NSURLRequest requestWithURL:videoURL];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
            NSURL *tempURL = [documentsURL URLByAppendingPathComponent:[videoURL lastPathComponent]];
            [data writeToURL:tempURL atomically:YES];
            UISaveVideoAtPathToSavedPhotosAlbum(tempURL.path, nil, NULL, NULL);
        }];
    }
    else if (imageURL)
    {
        UIImage *image = [photoImageView image];
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    }
    else
    {
        NSLog(@"save photo from image:\n%@", photo);
        UIImageWriteToSavedPhotosAlbum(photo, self, nil, nil);    }
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

@end
