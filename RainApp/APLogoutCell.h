//
//  APLogoutCell.h
//  Rain
//
//  Created by EgorMac on 27/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APLogoutCell : UITableViewCell
{
    IBOutlet UILabel *logoutLabel;
}

- (IBAction)logout:(id)sender;

@end
