//
//  APReceiverTableViewCell.m
//  Rain
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "APReceiverTableViewCell.h"

@interface APReceiverTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *receiverBubble;
@property (weak, nonatomic) IBOutlet UILabel *receiverMessage;


@end

@implementation APReceiverTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *tempImage = [_receiverBubble.image resizableImageWithCapInsets:UIEdgeInsetsMake(7, 27, 20, 8) resizingMode:UIImageResizingModeStretch];
    _receiverBubble.image = tempImage;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setters

- (void)setMessage:(NSString *)message {
    _receiverMessage.text = message;
}

- (void)setAvatarData:(NSData *)avatarData {
    [UIImageView apa_useMaskToImageView:_avatarImage
                               forImage:[UIImage imageWithData:avatarData]];
}

@end
