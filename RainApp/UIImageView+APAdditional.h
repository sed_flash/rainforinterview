//
//  UIImageView+APAdditional.h
//  Rain
//
//  Created by Vlad on 19.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (APAdditional)

+ (void)apa_useMaskToImageView:(UIImageView *)imageView forImage:(UIImage *)image;

@end
