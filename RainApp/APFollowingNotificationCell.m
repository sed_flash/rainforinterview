//
//  APFollowingNotificationCell.m
//  Rain
//
//  Created by EgorMac on 28/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFollowingNotificationCell.h"
#import "APProfilePhotoCell.h"

@implementation APFollowingNotificationCell
@synthesize notification, userButton, selectedLikedPost;

- (void)awakeFromNib {
    // Initialization code
    
    photos = [[NSArray alloc] init];
    selectedLikedPost = [[NSDictionary alloc] init];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNotification:(NSDictionary *)newNotification
{
    if (notification != newNotification) {
        notification = newNotification;
        
//        [photoImageView setImage:[UIImage imageNamed:@"ProfileEditPhoto"]];
        [UIImageView apa_useMaskToImageView:photoImageView
                                   forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [nameLabel setText:[[notification objectForKey:@"user"] objectForKey:@"nick"]];
        
        NSUInteger likedPostsCount = [[notification objectForKey:@"count"] integerValue];
        
        [descriptionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"liked%luposts", nil), likedPostsCount]];
        [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
        
        NSDate *createdAt;
        
        if ([[notification objectForKey:@"created_at"] isKindOfClass:[NSString class]]) {
            NSString *createdAtString = [notification objectForKey:@"created_at"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
            createdAt = [dateFormatter dateFromString:createdAtString];
        } else {
            createdAt = [notification objectForKey:@"created_at"];
        }
        NSString *timeAgo = [createdAt timeAgo];
        [timeAgoLabel setText:timeAgo];
        [self setAvatarImage];
        [self setDataSource];
        
        NSLog(@"notification - \n%@", notification);
    }
}

- (void)setNotification:(NSDictionary *)newNotification fromTableView:(UITableView *)aTableView atIndexPath:(NSIndexPath *)indexPath
{
    self.parentTableView = aTableView;
    self.selfIndexPath = indexPath;
    [self setNotification:newNotification];
}

- (void)setAvatarImage
{
    NSString *photoString = [[notification objectForKey:@"user"] objectForKey:@"avatar_small"];
    
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
    
    __weak NSString *weakPhotoString = photoString;
    __weak UIImageView *weakPhotoImageView = photoImageView;
    
    [photoImageView setImageWithURLRequest:photoRequest
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         NSLog(@"success for %@", weakPhotoString);
//         [weakPhotoImageView setImage:image];
         [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                    forImage:image];
     }
     
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                       NSLog(@"fail for %@, error: %@", weakPhotoString, error);
//                                       [weakPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                       [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                                                  forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                   }];
    
}

- (void)setDataSource
{
    photos = [notification objectForKey:@"posts"];
    [photosCollectionView reloadData];
}

#pragma mark - Collection View Delegate

static const CGFloat ITEM_SIZE = 48.0f;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(ITEM_SIZE, ITEM_SIZE);
    return size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return photos.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedLikedPost = [photos objectAtIndex:[indexPath item]];
    [self.parentTableView.delegate tableView:self.parentTableView didSelectRowAtIndexPath:self.selfIndexPath];
}

#pragma mark - Collection View DataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    APProfilePhotoCell *photoCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"profilePhotoCell" forIndexPath:indexPath];
    
    NSDictionary *currentPost = [photos objectAtIndex:[indexPath row]];
    
    NSString *photoString = [currentPost objectForKey:@"small_image"];
    if ([photoString isEqualToString:@"http://rain-app.com"]) {
        photoString = [currentPost objectForKey:@"path"];
    }
    
    // if content is video
    
    if ([[currentPost objectForKey:@"path"] hasPrefix:@"http://rain-app.com/media/video"]) {
        [[photoCell videoImageView] setHidden:NO];
        [[photoCell videoImageView] setImage:[UIImage imageNamed:@"videoIcon.png"]];
    } else {
        [[photoCell videoImageView] setHidden:YES];
    }
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
    
    __weak UIImageView *weakImageView = [photoCell photoImageView];
    __weak NSString *weakPhotoString = photoString;
    
    [[photoCell photoImageView] setImageWithURLRequest:photoRequest
                                      placeholderImage:[UIImage new]
                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         [weakImageView setAlpha:0.0f];
         [weakImageView setImage:image];
         
         [UIView animateWithDuration:0.5
                               delay:0.1
                             options:UIViewAnimationOptionTransitionNone
                          animations:^
          {
              [weakImageView setAlpha:1.0f];
          }
                          completion:^(BOOL finished)
          {
//              NSLog(@"animation finished");
          }];
     }
     
                                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                   NSLog(@"fail for %@", weakPhotoString);
                                               }];
    return photoCell;
}

@end
