//
//  JPSThumbnailTopViewController.h
//  Rain
//
//  Created by EgorMac on 10.11.15.
//  Copyright © 2015 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "APProfileVC.h"
#import <CoreLocation/CoreLocation.h>


@import MapKit;

@interface JPSThumbnailTopViewController : UNViewController <UITableViewDelegate, UITableViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate>
{
    APServerAPI *API;
    
    
    
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITableView *topTableView;
    UIRefreshControl *refreshControl;
    
    NSMutableArray *users;
    
    NSString *selectedUserMail;
}



@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@property (nonatomic, strong) APServerAPI *API;


@end
