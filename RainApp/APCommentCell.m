//
//  APCommentCell.m
//  Rain
//
//  Created by EgorMac on 03/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APCommentCell.h"
#import "NSDate+TimeAgo.h"

#define kLabelHorizontalInsets      15.0f
#define kLabelVerticalInsets        10.0f

@interface APCommentCell ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation APCommentCell

@synthesize bodyLabel;
@synthesize photoView;
@synthesize timeAgoLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [bodyLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [bodyLabel setNumberOfLines:0];
    [bodyLabel setTextAlignment:NSTextAlignmentLeft];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCreatedAtDate:(NSString *)createdAt
{
    NSDate *createdAtDate;
    
    if ([createdAt isKindOfClass:[NSString class]]) {
        NSString *createdAtString = createdAt;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
        createdAtDate = [dateFormatter dateFromString:createdAtString];
        NSLog(@"created at date is %@", createdAtString);
    }
    
    NSString *timeAgo = [createdAtDate timeAgo];
    [timeAgoLabel setText:timeAgo];
}

@end
