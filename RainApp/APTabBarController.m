//
//  APTabBarController.m
//  Rain
//
//  Created by EgorMac on 29/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APTabBarController.h"
#import "APCreatePostVC.h"
#import "APFiltersVC.h"
#import "APPhotoVC.h"
#import <AVFoundation/AVFoundation.h>
#import "GPUImage.h"


#import "TWPhotoPickerController.h"


static void *AVPlayerDemoPlaybackViewControllerStatusObservationContext = &AVPlayerDemoPlaybackViewControllerStatusObservationContext;

@interface APTabBarController ()

@end

@implementation APTabBarController

@synthesize centerButton;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[self tabBar] setShadowImage:[[UIImage alloc] init]];
        [[self tabBar] setSelectedImageTintColor:[UIColor whiteColor]];
        
         }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITabBarItem *item=[self.tabBar.items objectAtIndex:2];
    
   item.image=nil;
    

    // Do any additional setup after loading the view.
    [[self tabBar] setShadowImage:[[UIImage alloc] init]];
    [[self tabBar] setSelectedImageTintColor:[UIColor whiteColor]];
    
    centerButton = [[UIButton alloc] init];
    [self addCenterButtonWithImage:[UIImage imageNamed:@"photoTab"]
                    highlightImage:[UIImage imageNamed:@"photoTab"]];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSUInteger indexOfTab = [[tabBar items] indexOfObject:item];
    // If a user clicked to "Home" button need to send notification to
    // scroll news table to top of view
    if (indexOfTab == 0) {
        NSLog(@"Clicked to News Tab!");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"newsNeedToScrollToTopNotification" object:@(YES)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBar bringSubviewToFront:centerButton];
}

-(void)addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    centerButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    centerButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [centerButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [centerButton setBackgroundImage:highlightImage forState:UIControlStateHighlighted];

    [centerButton setCenter:CGPointMake(self.tabBar.frame.size.width/2, self.tabBar.frame.size.height/2-4)];
    
    [centerButton addTarget:self
                     action:@selector(centerButtonTouched:)
           forControlEvents:UIControlEventTouchDown];
    [self.tabBar addSubview:centerButton];
    
   // NSLog(@"center Button is %@", centerButton);
    //NSLog(@"like button is %@", [self.tabBar.items objectAtIndex:4]);
}

- (void)centerButtonTouched:(UIButton *)sender
{
    [sender setBackgroundImage:[UIImage imageNamed:@"photoTab"]
                      forState:UIControlStateNormal];
    [self performSelector:@selector(setDefaultImageForCenterButton)
               withObject:self
               afterDelay:1.0f];


    DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
    cameraController.delegate=self;
    [cameraController setForceQuadCrop:YES];
    
//    DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
//    [container setCameraViewController:cameraController];
//    [container setFullScreenMode];
//    
//    self.nav = [[DemoNavigationController alloc] initWithRootViewController:container];
//    [self presentViewController:self.nav animated:YES completion:nil];


    UINavigationController *navController = (UINavigationController *)[[self storyboard] instantiateViewControllerWithIdentifier:@"cameraNavController"];
    [self presentViewController:navController
                       animated:YES
                     completion:nil];

}


- (void) openLibrary
{
//    DBCameraLibraryViewController *vc = [[DBCameraLibraryViewController alloc] init];
//    [vc setDelegate:self]; //DBCameraLibraryViewController must have a DBCameraViewControllerDelegate object
//    //    [vc setForceQuadCrop:YES]; //Optional
//    //    [vc setUseCameraSegue:YES]; //Optional
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    [nav setNavigationBarHidden:YES];
    
    //   UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    //  [self.navigationController presentViewController:photoPicker animated:YES completion:NULL];

    
//    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
//    
//    
//    
//    
//    photoPicker.cropBlock = ^(UIImage *image)
//    {
//        
//        
//        APFiltersVC *filtersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"FiltersVC"];
//        [filtersVC setPhotoImage:image];
//        [self.navigationController pushViewController:filtersVC animated:YES];
//        
//        
//        NSLog(@"choose from library button tapped");
//        
//    };
//    
//       [self presentViewController:photoPicker animated:YES completion:nil];
}

- (void) dismissCamera:(id)cameraViewController{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}


- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    APCreatePostVC *createPostVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"CreatePostVC"];
    [createPostVC setIsVideo:NO];
    [createPostVC setPhotoImage:image];
    
  
    [self.nav pushViewController:createPostVC animated:YES];
  
//    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController pushViewController:createPostVC animated:YES];
    
    
    
    
//    DetailViewController *detail = [[DetailViewController alloc] init];
//    [detail setDetailImage:image];
//    [self.navigationController pushViewController:detail animated:NO];
//    [cameraViewController restoreFullScreenMode];
//    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}



- (void)setDefaultImageForCenterButton
{
    [centerButton setBackgroundImage:[UIImage imageNamed:@"photoTab"] forState:UIControlStateNormal];
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info dictionary - \n%@", info);
    
    UIImage *pickerImage;
    UINavigationController *navController = (UINavigationController *)[[self storyboard] instantiateViewControllerWithIdentifier:@"CreatePostNC"];
    
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
        
        //create an avassetrack with our asset
        AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        //create a video composition and preset some settings
        AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
        videoComposition.frameDuration = CMTimeMake(1, 30);
        //here we are setting its render size to its height x height (Square)
        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height);
        
        //create a video instruction
        AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
        
        AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
        
        //Here we shift the viewing square up to the TOP of the video so we only see the top
        CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, 0 );
        
        //Use this code if you want the viewing square to be in the middle of the video
        //CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height) /2 );
        
        //Make sure the square is portrait
        CGAffineTransform t2 = CGAffineTransformRotate(t1, M_PI_2);
        
        CGAffineTransform finalTransform = t2;
        [transformer setTransform:finalTransform atTime:kCMTimeZero];
        
        //add the transformer layer instructions, then add to video composition
        instruction.layerInstructions = [NSArray arrayWithObject:transformer];
        videoComposition.instructions = [NSArray arrayWithObject: instruction];
        
        //Create an Export Path to store the cropped video
        NSString * documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *exportPath = [documentsPath stringByAppendingFormat:@"/CroppedVideo.mp4"];
        NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
        
        //Remove any prevouis videos at that path
        [[NSFileManager defaultManager]  removeItemAtURL:exportUrl error:nil];
        
        //Export
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        exporter.videoComposition = videoComposition;
        exporter.outputURL = exportUrl;
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        
        [exporter exportAsynchronouslyWithCompletionHandler:^
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Call when finished
//                 [self exportDidFinish:exporter];
                 
                 // thumbnail
                 
                 AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                 generator.appliesPreferredTrackTransform = YES;
                 NSError *err = NULL;
                 CMTime time = CMTimeMake(1, 2);
                 CGImageRef oneRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
                 UIImage *pickerImage = [[UIImage alloc] initWithCGImage:oneRef];
                 
                 APCreatePostVC *createPostVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"CreatePostVC"];
                 [createPostVC setIsVideo:YES];
                 [createPostVC setPhotoImage:pickerImage];
                 [createPostVC setVideoURL: exporter.outputURL];
                 [navController pushViewController:createPostVC animated:NO];
             });
         }];
        
    } else
    {
        pickerImage = [info objectForKey:UIImagePickerControllerEditedImage];
        APFiltersVC *filtersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"FiltersVC"];
        [filtersVC setPhotoImage:pickerImage];
        [navController pushViewController:filtersVC animated:NO];
    }
    
    NSLog(@"picker image is %@", pickerImage);
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:navController
                           animated:YES
                         completion:nil];
    }];
}

- (void)exportDidFinish:(AVAssetExportSession*)session
{
    //Play the New Cropped video
//    NSURL *outputURL = session.outputURL;
//    AVURLAsset* asset = [AVURLAsset URLAssetWithURL:outputURL options:nil];
//    AVPlayerItem * newPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
//    
//    AVPlayer *mPlayer = [AVPlayer playerWithPlayerItem:newPlayerItem];
//    [mPlayer addObserver:self forKeyPath:@"status" options:0 context:AVPlayerDemoPlaybackViewControllerStatusObservationContext];
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([[segue identifier] isEqualToString:@"showCreatePostVC"])
//    {
//        
//    }
}

@end
