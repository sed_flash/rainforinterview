//
//  FilterCell.m
//  Rain
//
//  Created by EgorMac on 07/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

@synthesize filterID;
@synthesize filterTitle;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFilterID:(NSString *)newFilterID
{
    if (filterID != newFilterID) {
        filterID = newFilterID;
        
        [filterTitle setText:[filterID stringByReplacingOccurrencesOfString:@"GPUImage" withString:@""]];
        [filterTitle setAdjustsFontSizeToFitWidth:YES];
    }
}

@end
