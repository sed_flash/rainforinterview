//
//  APLoginVC.h
//  Rain
//
//  Created by EgorMac on 16/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APLoginVC : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UILabel *toLogInLabel;
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UIButton *loginButton;
    IBOutlet UILabel *forgotPasswordLabel;
    
    CGFloat keyboardAnimatedDistance;
    
    APServerAPI *API;
}

- (IBAction)login:(id)sender;

@end
