//
//  APTabBarController.h
//  Rain
//
//  Created by EgorMac on 29/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface APTabBarController : UITabBarController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,DBCameraViewControllerDelegate>
{
    UIButton *centerButton;
}
@property (nonatomic, strong)DemoNavigationController * nav;

@property (nonatomic, strong) UIButton *centerButton;

-(void)addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;

@end
