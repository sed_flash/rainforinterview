//
//  APLogoutCell.m
//  Rain
//
//  Created by EgorMac on 27/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APLogoutCell.h"

@implementation APLogoutCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [logoutLabel setText:NSLocalizedString(@"logout", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)logout:(id)sender
{
    NSLog(@"logout!");
    [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    [self performSelector:@selector(setDefaultBackgroundColor)
               withObject:self
               afterDelay:0.5];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userSettings"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"filledProfileData"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imageData"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userPhoto"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:self];
    }
    else
    {
        NSLog(@"Unknown error!");
    }
}

- (void)setDefaultBackgroundColor
{
    [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
}

@end
