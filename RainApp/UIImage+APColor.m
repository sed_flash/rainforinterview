//
//  UIImage+APColor.m
//  Rain
//
//  Created by EgorMac on 25/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "UIImage+APColor.h"

@implementation UIImage (APColor)

+(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
