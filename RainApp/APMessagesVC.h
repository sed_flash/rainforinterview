//
//  APMessagesVC.h
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "APDialogsCell.h"
#import "APConversationVC.h"

@interface APMessagesVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITableView *dialogsTableView;
    
    APServerAPI *API;
    
    NSArray *dialogs;
    NSString *selectedUserID;
    NSString *selectedUserNickname;
    
    UIImage *selectedUserAvatarImage;
    
    NSIndexPath *deletedConversationIndexPath;
    
    UIActivityIndicatorView *activityView;
    UIRefreshControl *refreshControl;
    
    IBOutlet UILabel *noDialogsLabel;
}

@end
