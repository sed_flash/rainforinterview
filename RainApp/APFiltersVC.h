//
//  APFiltersVC.h
//  Rain
//
//  Created by EgorMac on 16/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APFiltersVC : UIViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *photoImageView;
    __weak IBOutlet UIView *bottomView;
    
    UIImage *photoImage;
    UIImage *editedImage;
    
    __weak IBOutlet UISlider *contrastSlider;
    
    CGRect scrollViewFrame;
    
    __weak IBOutlet UIButton *filtersButton;
    __weak IBOutlet UIButton *contrastButton;
    __weak IBOutlet UIButton *nextButton;
    int contrast;
    
    __weak IBOutlet UIScrollView *imageScrollView;
    IBOutlet UIRotationGestureRecognizer *rotationGesture;
    
    IBOutlet NSLayoutConstraint *lightConstraint;
    IBOutlet NSLayoutConstraint *contrastConstraint;
    IBOutlet NSLayoutConstraint *widthConstraint;
    IBOutlet NSLayoutConstraint *heightConstraint;
}
@property (nonatomic, strong) UIImage *photoImage;

- (IBAction)setOriginalImage:(id)sender;
- (IBAction)activateFilter1:(id)sender;
- (IBAction)activateFilter2:(id)sender;
- (IBAction)activateFilter3:(id)sender;
- (IBAction)activateFilter4:(id)sender;
- (IBAction)activateFilter5:(id)sender;
- (IBAction)activateFilter6:(id)sender;
- (IBAction)activateFilter7:(id)sender;
- (IBAction)activateFilter8:(id)sender;
- (IBAction)activateFilter9:(id)sender;
- (IBAction)activateFilter10:(id)sender;
- (IBAction)activateFilter11:(id)sender;
- (IBAction)showEffects:(id)sender;
- (IBAction)showContrast:(id)sender;
- (IBAction)contrastSliderChanged:(id)sender;
- (IBAction)goBack:(id)sender;

@end
