//
//  APReportProfileVC.h
//  Rain
//
//  Created by EgorMac on 16/01/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import "APReportPostVC.h"

@interface APReportProfileVC : APReportPostVC
{
    NSString *profileEmail;
}

@property (nonatomic, strong) NSString *profileEmail;
@end
