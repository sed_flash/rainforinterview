//
//  APNoFriendsVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APNoFriendsVC.h"

@interface APNoFriendsVC ()

@end

@implementation APNoFriendsVC

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [noFriendsLabel setHidden:YES];
    [searchButton setHidden:YES];
    [self setupUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    API = [[APServerAPI alloc] init];

    [self configureNavigationBar];
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileSuccess)
                                                 name:@"SearchSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileFailed)
                                                 name:@"SearchFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedSuccess)
                                                 name:@"FeedSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedFailed)
                                                 name:@"FeedFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    
    NSDictionary *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *tokenMail = [token objectForKey:@"email"];
    
    [API showUser:tokenMail withOffset:0 andLimit:18];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkFriends
{
    NSDictionary *foundedUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"foundedUser"];
    NSLog(@"founded user - %@", foundedUser);
    NSDictionary *stats = [foundedUser objectForKey:@"stats"];
    NSInteger followingCount = [[stats objectForKey:@"following"] integerValue];
    NSLog(@"following count is %ld", (long)followingCount);
    if (followingCount > 0)
    {
        [self checkFeed];
    }
    else
    {
        [self showNoFriendsLabel];
    }
}

- (void)checkFeed
{
    [API getFeedWithOffset:0 andLimit:10];
}

#pragma mark - alerts

- (void)profileSuccess
{
    NSLog(@"profile success");
    [self checkFriends];
}

- (void)profileFailed
{
    NSLog(@"profile failed");
    [self showAlertWithIdentifier:serverAlert];
}

- (void)feedSuccess
{
    NSDictionary *feed = [[NSUserDefaults standardUserDefaults] objectForKey:@"feed"];
    NSArray *posts = [feed objectForKey:@"posts"];
    if (posts.count) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self showNoFriendsLabel];
    }
}

- (void)showNoFriendsLabel
{
    [noFriendsLabel setHidden:NO];
    [searchButton setHidden:NO];
}

- (void)feedFailed
{
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    NSLog(@"unknown server error");
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        } else {
            NSArray *errorMessageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
            errorMessage = @"";
            for (NSString *currentError in errorMessageArray) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"\n%@", currentError]];
            }
        }
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)setupUIElements
{
    [noFriendsLabel setText:NSLocalizedString(@"youHaveNoFriends", nil)];
    [searchButton setTitle:NSLocalizedString(@"searchFriends", nil) forState:UIControlStateNormal];
    [searchButton setTitle:NSLocalizedString(@"searchFriends", nil) forState:UIControlStateHighlighted];
    [[searchButton layer] setCornerRadius:3.0f];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setHidden:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
