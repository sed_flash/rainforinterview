//
//  APFollowCell.m
//  Rain
//
//  Created by EgorMac on 09/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFollowCell.h"

@implementation APFollowCell
@synthesize notification, userButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNotification:(NSDictionary *)newNotification
{
    if (notification != newNotification) {
        notification = newNotification;
        
        NSDictionary *notificationUser = [notification objectForKey:@"user"];
        
//        [photoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [UIImageView apa_useMaskToImageView:photoImageView
                                   forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [nameLabel setText:[notificationUser objectForKey:@"nick"]];
        [descriptionLabel setText:NSLocalizedString(@"startedFollowingYou", nil)];
        
        NSDate *createdAt;
        
        if ([[notification objectForKey:@"created_at"] isKindOfClass:[NSString class]]) {
            NSString *createdAtString = [notification objectForKey:@"created_at"];
            
            NSLog(@"createdAtString is %@", createdAtString);
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
            createdAt = [dateFormatter dateFromString:createdAtString];
            
            NSLog(@"createdAt is %@", createdAt);
            
        } else {
            createdAt = [notification objectForKey:@"created_at"];
            
            NSLog(@"createdAt (not string) is %@", createdAt);
        }
        NSString *timeAgo = [createdAt timeAgo];
        
        NSLog(@"timeAgo string is %@", timeAgo);
        NSLog(@"timeAgoLabel text is %@", timeAgoLabel.text);
        
        [timeAgoLabel setText:timeAgo];
        
        NSString *photoString = [notificationUser objectForKey:@"avatar_small"];
        if(!photoString)
        {
            photoString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
        }
        
        NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];

        __weak NSString *weakPhotoString = photoString;
        __weak UIImageView *weakPhotoImageView = photoImageView;
        
        UIImage *placeHolderImage = [UIImage imageNamed:@"ProfileEditPhotoBlue"];
        
        [photoImageView setImageWithURLRequest:photoRequest
                              placeholderImage:placeHolderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             NSLog(@"success for %@", weakPhotoString);
//             [weakPhotoImageView setImage:image];
             [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                        forImage:image];
         }
         
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                           NSLog(@"fail for %@, error: %@", weakPhotoString, error);
//                                           [weakPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                           [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                                                      forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                       }];
        
        NSLog(@"notification - \n%@", notification);
    }
}

@end
