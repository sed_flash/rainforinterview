//
//  UNViewController.m
//  RetouchMe
//
//  Created by Nozdrinov Yegor  on 09.12.14.
//  Copyright (c) 2014 ISD. All rights reserved.
//

#import "UNViewController.h"



@interface UNViewController ()

@end



@implementation UNViewController


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
     [self animateVisibleCells];
    
}

-(void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:animated];
    
   
    
   // [self.tb selectRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedcount inSection:0] animated:YES scrollPosition:UITableViewRowAnimationTop];
    
    
     [self prepareVisibleCellsForAnimation];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Private

- (void)prepareVisibleCellsForAnimation
{
   
    NSArray *visiblePaths = [self.tb indexPathsForVisibleRows];
    
    for (int i = 0; i < visiblePaths.count; i++)
    {
        NSIndexPath *index=[visiblePaths objectAtIndex:i];
        UITableViewCell * cell = (UITableViewCell *) [self.tb cellForRowAtIndexPath:index];
        
        cell.frame = CGRectMake(-CGRectGetWidth(cell.bounds), cell.frame.origin.y, CGRectGetWidth(cell.bounds), CGRectGetHeight(cell.bounds));
        cell.alpha = 0.f;
    }
}

- (void)animateVisibleCells
{
     NSArray *visiblePaths = [self.tb indexPathsForVisibleRows];
    for (int i = 0; i < visiblePaths.count; i++)
    {
         NSIndexPath *index=[visiblePaths objectAtIndex:i];
        UITableViewCell * cell = (UITableViewCell *) [self.tb cellForRowAtIndexPath:index];
        
        cell.alpha = 1.f;
        [UIView animateWithDuration:0.25f
                              delay:i * 0.1
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             cell.frame = CGRectMake(0.f, cell.frame.origin.y, CGRectGetWidth(cell.bounds), CGRectGetHeight(cell.bounds));
                         }
                         completion:nil];
    }
}








@end
