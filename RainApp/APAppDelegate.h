//
//  APAppDelegate.h
//  RainApp
//
//  Created by Nozdrinov Yegor on 15/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL allowRotation;

@end
