//
//  APSendPasswordVC.m
//  Rain
//
//  Created by EgorMac on 19/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APSendPasswordVC.h"

@interface APSendPasswordVC ()

@end

@implementation APSendPasswordVC

@synthesize userData;
@synthesize API;

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static NSString *successAlert = @"success";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recoverySuccess)
                                                 name:@"RecoverySuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recoveryFailed)
                                                 name:@"RecoveryFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    NSLog(@"self userdata is %@", userData);
    
    [self setupUIElements];
}

- (void)setUserData:(NSDictionary *)newUserData
{
    if (userData != newUserData)
    {
        userData = newUserData;
    }
    
    NSLog(@"user data: %@", userData);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUIElements
{

    NSString *avatarString = [userData objectForKey:@"avatar"];
    
    NSLog(@"set avatar from %@\n for %@", avatarString, [userData objectForKey:@"first_name"]);
    
    [self showActivityIndicator];
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
    
    __weak typeof(self) weakSelf = self;
    __weak UIImageView *weakPhotoImageView = photoImageView;
    __weak NSDictionary *weakUserData = userData;
    
    [photoImageView setImageWithURLRequest:photoRequest
                                 placeholderImage:nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         [weakSelf removeActivityIndicator];
//         [weakPhotoImageView setImage:image];
         [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                    forImage:image];
         NSLog(@"success for %@", [weakUserData objectForKey:@"first_name"]);
     }
     
                                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                              [weakSelf removeActivityIndicator];
                                              [weakPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                          }];
    
    NSString *nickName;
    if ([userData objectForKey:@"nick"]) {
        nickName = [userData objectForKey:@"nick"];
    } else {
        nickName = @"";
    }
    
    NSLog(@"nickname is %@", nickName);
    
    [nicknameLabel setText:nickName];
    [sendPasswordButton setTitle:NSLocalizedString(@"sendPassword", nil) forState:UIControlStateNormal];
    [sendPasswordButton setTitle:NSLocalizedString(@"sendPassword", nil) forState:UIControlStateHighlighted];
    [[sendPasswordButton titleLabel] setTextAlignment:NSTextAlignmentCenter];
    [[sendPasswordButton layer] setCornerRadius:3.0f];
}

- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    return [UIImage imageWithCGImage:masked];
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor whiteColor]];
    [activity setTag:5];
    [activity setCenter:photoImageView.center];
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
}

- (void)recoverySuccess
{
    [self showAlertWithIdentifier:successAlert];
}

- (void)recoveryFailed
{
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self showAlertWithIdentifier:serverError];
}


- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:successAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"passwordRecovery", nil)
                                                        message:NSLocalizedString(@"newPasswordSentToYourEmail", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - IBAction Methods

- (IBAction)sendPassword:(id)sender
{
    [API recoveryPasswordForEmail:[userData objectForKey:@"email"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
