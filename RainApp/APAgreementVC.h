//
//  APAgreementVC.h
//  Rain
//
//  Created by EgorMac on 18/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APAgreementVC : UIViewController
{
    IBOutlet UIBarButtonItem *closeButton;
}

- (IBAction)goBack:(id)sender;

@end
