//
//  APCommentVC.m
//  Rain
//
//  Created by EgorMac on 02/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APCommentVC.h"
#import "APPostVC.h"

@interface APCommentVC ()

@end

@implementation APCommentVC

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

@synthesize postID;
@synthesize parent;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUIElements];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(commentSuccess)
                                                 name:@"CommentSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(commentFailed)
                                                 name:@"CommentFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [commentTextView becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    API = nil;
}

- (void)commentSuccess
{
    if (parent) {
        APPostVC *postVC = (APPostVC *)parent;
        [postVC performSelector:@selector(updateComments:) withObject:postVC afterDelay:0.5];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentsCountIncreased"
                                                        object:self];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)commentFailed
{
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self showAlertWithIdentifier:serverError];
}

#pragma mark - Alerts

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)setUpUIElements
{
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:18.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"comment", nil)];
    
    [sendButton setTitle:NSLocalizedString(@"send", nil)];
}

#pragma mark - Send Button

- (IBAction)sendComment:(id)sender
{
    NSLog(@"send comment");
    
    NSData *data = [commentTextView.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *commentString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"comment is %@", [commentTextView text]);
    NSLog(@"comment string that will be send is:\n%@", commentString);
    
    
    [API sendComment:commentString forPostID:postID];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
