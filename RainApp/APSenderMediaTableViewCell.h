//
//  APSenderMediaTableViewCell.h
//  Rain
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSenderMediaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *senderMedia;

@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSData *avatarData;
@property (nonatomic, copy) UIImage *attachmentImage;
@property (nonatomic, copy) NSString *attachmentURLString;
@end
