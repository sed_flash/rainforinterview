//
//  APFilterVC.m
//  Rain
//
//  Created by EgorMac on 18/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFilterVC.h"
#import "APTopVC.h"

@interface APFilterVC ()

@end

@implementation APFilterVC

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static NSString *countriesAlert = @"countries";
static NSString *citiesAlert = @"cities";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const int COUNTRIES_PICKER_TAG = 1;
static const int CITIES_PICKER_TAG = 2;
static const int AGE_FROM_PICKER_TAG = 37;
static const int AGE_TO_PICKER_TAG = 38;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    API = [[APServerAPI alloc] init];
    chosenCountryID = 0;
    chosenCityID = 0;
    gender = 1;
    [self createAgeArray];
    
    UIImageView *imgView = maleButton.imageView;
    [imgView setImage:[UIImage imageNamed:@"maleButtonOff"]];
    imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imgView setTintColor:[UIColor colorWithRed:93.0f/255.0f green:124.0f/255.0f blue:173.0f/255.0f alpha:1.0f]];
    [maleButton setImage:imgView.image forState:UIControlStateSelected];
    [maleButton setSelected:NO];
    [femaleButton setSelected:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filteredSuccess)
                                                 name:@"FilterFriendsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filteredFailed)
                                                 name:@"FilterFriendsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesSuccess)
                                                 name:@"CountriesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesFailed)
                                                 name:@"CountriesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesSuccess)
                                                 name:@"CitiesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesFailed)
                                                 name:@"CitiesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    if ([self countriesCount] > 0) {
        countriesDownloaded = YES;
        [self configureCountryPicker];
    }
    else
    {
        countriesDownloaded = NO;
        [API getCountries];
    }
    [self setupUIElements];
}

- (void)createAgeArray
{
    ageFromArray = [NSArray array];
        ageToArray = [NSArray array];
    
    NSMutableArray *ageFromTempArray = [[NSMutableArray alloc] init];
    for (int i = 16; i < 121; i++) {
        [ageFromTempArray addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    ageFromArray = [ageFromTempArray copy];
    ageFromTempArray = nil;
    
    if ([[birthdayStartTextField text] isEqualToString:@""]) {
        ageToArray = [ageFromArray copy];
    } else {
        
        NSMutableArray *ageToTempArray = [[NSMutableArray alloc] init];
        
        NSInteger ageFrom = [[birthdayStartTextField text] integerValue];
        
        for (NSInteger j = ageFrom; j < 121; j++) {
            [ageToTempArray addObject:[NSString stringWithFormat:@"%ld", (long)j]];
        }
        
        ageToArray = [ageToTempArray copy];
        ageToTempArray = nil;
    }
    
    [self configureAgeFromPicker];
    [self configureAgeToPicker];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView *currentView in self.view.subviews) {
        if ([currentView isKindOfClass:[UITextField class]]) {
            [currentView resignFirstResponder];
            if (countryToolbar) {
                [countryToolbar removeFromSuperview];
            }
            if (cityToolbar) {
                [cityToolbar removeFromSuperview];
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat viewHeight, bottomOfTextField, topOfKeyboard;
    
    viewHeight = viewRect.size.height;
    bottomOfTextField = textFieldRect.origin.y + textFieldRect.size.height;
    topOfKeyboard = viewHeight - PORTRAIT_KEYBOARD_HEIGHT - 10; // 10 point padding
    
    if (bottomOfTextField >= topOfKeyboard) keyboardAnimatedDistance = bottomOfTextField - topOfKeyboard;
    else keyboardAnimatedDistance = 0.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == countryTextField) {
        [cityToolbar removeFromSuperview];
        if (!countriesDownloaded) {
            activityIndicator = [[UIActivityIndicatorView alloc] init];
            CGRect frame = CGRectMake(0, 0, 50, 50);
            [activityIndicator setFrame:frame];
            [activityIndicator setCenter:CGPointMake(self.view.center.x, self.view.center.y)];
            UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
            [activityIndicator setColor:customBlueColor];
            [[self view] addSubview:activityIndicator];
            [activityIndicator startAnimating];
        } else {
            countryToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                          self.view.frame.size.height,
                                                                          self.view.frame.size.width,
                                                                          40)];
            [countryToolbar setBarStyle:UIBarStyleDefault];
            [countryToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
            [countryToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCountryPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            countryToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:self->countryToolbar];
                 CGRect frame = self->countryToolbar.frame;
                 frame.origin.y = self->countryToolbar.frame.origin.y - 216.0f - frame.size.height + self->keyboardAnimatedDistance;
                 self->countryToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
    if (textField == cityTextField) {
        [countryToolbar removeFromSuperview];
        if (!citiesDownloaded) {
            
            if ([[countryTextField text] isEqualToString:@""]) {
                [cityTextField resignFirstResponder];
                [self showAlertWithIdentifier:@"noCountryAlert"];
            } else {
                [textField resignFirstResponder];
                activityIndicator = [[UIActivityIndicatorView alloc] init];
                [activityIndicator setFrame:textField.frame];
                [activityIndicator setCenter:textField.center];
                UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
                [activityIndicator setColor:customBlueColor];
                [[self view] addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
            
        } else {
            cityToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                       self.view.frame.size.height,
                                                                       self.view.frame.size.width,
                                                                       40)];
            [cityToolbar setBarStyle:UIBarStyleDefault];
            [cityToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
            [cityToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCityPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            cityToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:self->cityToolbar];
                 CGRect frame = self->cityToolbar.frame;
                 frame.origin.y = self.view.frame.size.height - 216.0f - frame.size.height + self->keyboardAnimatedDistance;
                 self->cityToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
}

- (void)hideCountryPickerAndToolbar:(id)sender
{
    UIPickerView *picker = (UIPickerView *)countryTextField.inputView;
    if ([[countryTextField text] isEqualToString:@""]) {
        
        NSDictionary *allCountriesDictionary = [[NSDictionary alloc] initWithObjects:@[@"all", NSLocalizedString(@"allCountries", nil)]
                                                                             forKeys:@[@"id", @"title"]];
        NSMutableArray *countriesMutableArray = [[NSMutableArray alloc] initWithObjects:allCountriesDictionary, nil];
        NSArray *countriessecArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"];
        
        for (NSDictionary *country in countriessecArray) {
            [countriesMutableArray addObject:country];
        }
        
        NSArray *countriesArray = [countriesMutableArray copy];
        NSDictionary *chosenCountry = [countriesArray objectAtIndex:0];
        
        [countryTextField setText:[chosenCountry objectForKey:@"title"]];
        
        if ([[chosenCountry objectForKey:@"id"] isKindOfClass:[NSString class]]) {
            countryID = @"all";
        } else {
            countryID = [[chosenCountry objectForKey:@"id"] stringValue];
        }
    }
    
    [countryTextField resignFirstResponder];
    [countryToolbar removeFromSuperview];
    countryToolbar = nil;
    
    [self updateCountryTextField:picker];
}

- (void)updateCountryTextField:(id)sender
{
    if ([countryID isEqualToString:@"all"]) {
        [cityTextField setHidden:YES];
        [cityUnderline setHidden:YES];
    }
    else
    {
        [cityTextField setHidden:NO];
        [cityUnderline setHidden:NO];
        [API getCitiesForCountryID:countryID];
        chosenCountryID = (int)[countryID integerValue];
        chosenCityID = 0;
        [cityTextField setText:@""];
        [cityTextField setInputView:nil];
        citiesDownloaded = NO;

    }
}

- (void)hideCityPickerAndToolbar:(id)sender
{
    if ([[cityTextField text] isEqualToString:@""]) {
        
        NSArray *citiesArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"];
        NSDictionary *chosenCity = [citiesArray objectAtIndex:0];
        
        [cityTextField setText:[chosenCity objectForKey:@"title"]];
        cityID = [[chosenCity objectForKey:@"id"] stringValue];
    }
    
    [cityTextField resignFirstResponder];
    [cityToolbar removeFromSuperview];
    cityToolbar = nil;
    
    UIDatePicker *picker = (UIDatePicker *)cityTextField.inputView;
    [self updateCityTextField:picker];
}

- (void)updateCityTextField:(id)sender
{
    [cityTextField resignFirstResponder];
    chosenCityID = (int)[cityID integerValue];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldsAreNotEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] > 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

#pragma mark - UIPickerView Datasource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG) {
        int countriesCount;
        if ([self countriesCount] > 0) {
            countriesCount = [self countriesCount];
        } else {
            countriesCount = 0;
        }
        
        return countriesCount;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        int citiesCount;
        
        if ([self citiesCount] > 0) {
            citiesCount = [self citiesCount];
        } else {
            citiesCount = 0;
        }
        
        return citiesCount;
    }
    else if ([pickerView tag] == AGE_FROM_PICKER_TAG)
    {
        return 105;
    }
    else if ([pickerView tag] == AGE_TO_PICKER_TAG)
    {
        return 105;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in numberOfRows method");
        return 0;
    }
}

#pragma mark - UIPickerView Delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSDictionary *allCountriesDictionary = [[NSDictionary alloc] initWithObjects:@[@"all", NSLocalizedString(@"allCountries", nil)]
                                                                             forKeys:@[@"id", @"title"]];
        NSMutableArray *countriesMutableArray = [[NSMutableArray alloc] initWithObjects:allCountriesDictionary, nil];
        NSArray *countriessecArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"];
        
        for (NSDictionary *country in countriessecArray) {
            [countriesMutableArray addObject:country];
        }
        
        NSArray *countriesArray = [countriesMutableArray copy];
        NSDictionary *chosenCountry = [countriesArray objectAtIndex:row];
        NSString *countryName = [chosenCountry objectForKey:@"title"];
        countriesMutableArray = nil;
        return countryName;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        NSArray *citiesArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"];
        NSDictionary *chosenCity = [citiesArray objectAtIndex:row];
        NSString *cityName = [chosenCity objectForKey:@"title"];
        return cityName;
    }
    else if ([pickerView tag] == AGE_FROM_PICKER_TAG)
    {
        NSString *age = [ageFromArray objectAtIndex:row];
        return age;
    }
    else if ([pickerView tag] == AGE_TO_PICKER_TAG)
    {
        NSString *age = [ageToArray objectAtIndex:row];
        return age;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
        return @"";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"did select");
    
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSLog(@"did select country");
        NSDictionary *allCountriesDictionary = [[NSDictionary alloc] initWithObjects:@[@"all", NSLocalizedString(@"allCountries", nil)]
                                                                             forKeys:@[@"id", @"title"]];
        NSMutableArray *countriesMutableArray = [[NSMutableArray alloc] initWithObjects:allCountriesDictionary, nil];
        NSArray *countriessecArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"];
        
        for (NSDictionary *country in countriessecArray) {
            [countriesMutableArray addObject:country];
        }
        
        NSArray *countriesArray = [countriesMutableArray copy];
        NSDictionary *chosenCountry = [countriesArray objectAtIndex:row];
        [countryTextField setText:[chosenCountry objectForKey:@"title"]];
        if ([[chosenCountry objectForKey:@"id"] isKindOfClass:[NSString class]])
        {
            countryID = @"all";
        } else
        {
            countryID = [[chosenCountry objectForKey:@"id"] stringValue];
        }
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        NSArray *citiesArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"];
        NSDictionary *chosenCity = [citiesArray objectAtIndex:row];
        [cityTextField setText:[chosenCity objectForKey:@"title"]];
        cityID = [[chosenCity objectForKey:@"id"] stringValue];
    }
    else if ([pickerView tag] == AGE_FROM_PICKER_TAG)
    {
        [birthdayStartTextField setText:[ageFromArray objectAtIndex:row]];
        [self createAgeArray];
        [birthdayStartTextField resignFirstResponder];
    }
    else if ([pickerView tag] == AGE_TO_PICKER_TAG)
    {
        [birthdayEndTextField setText:[ageToArray objectAtIndex:row]];
        [birthdayEndTextField resignFirstResponder];
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
    }
}

#pragma mark - Helper Methods

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [[self navigationItem] setTitle:NSLocalizedString(@"searchFriends", nil)];
}

- (void)setupUIElements
{
    [self configureNavigationBar];
    
    [countryTextField setPlaceholder:NSLocalizedString(@"country", nil)];
    [cityTextField setPlaceholder:NSLocalizedString(@"city", nil)];
    [birthdayStartTextField setPlaceholder:NSLocalizedString(@"from", nil)];
    [birthdayEndTextField setPlaceholder:NSLocalizedString(@"to", nil)];
    
    for (UIView *subview in self.view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            UIView *textFieldLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
            [(UITextField *)subview setLeftView:textFieldLeftView];
            [(UITextField *)subview setLeftViewMode:UITextFieldViewModeAlways];
        }
    }
    
    [ageLabel setText:NSLocalizedString(@"age", nil)];
    
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    [searchButton setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateHighlighted];
    [[searchButton layer] setCornerRadius:3.0f];
    
//    [maleButton setSelected:YES];
}

- (void)configureCountryPicker
{
    UIPickerView *countryPicker = [[UIPickerView alloc] init];
    [countryPicker setTag:COUNTRIES_PICKER_TAG];
    [countryPicker setDelegate:self];
    [countryTextField setInputView:countryPicker];
}

- (void)configureCitiesPicker
{
    UIPickerView *citiesPicker = [[UIPickerView alloc] init];
    [citiesPicker setTag:CITIES_PICKER_TAG];
    [citiesPicker setDelegate:self];
    [cityTextField setInputView:citiesPicker];
}

- (void)configureAgeFromPicker
{
    UIPickerView *ageFromPicker = [[UIPickerView alloc] init];
    [ageFromPicker setTag:AGE_FROM_PICKER_TAG];
    [ageFromPicker setDelegate:self];
    [birthdayStartTextField setInputView:ageFromPicker];
}

- (void)configureAgeToPicker
{
    UIPickerView *ageToPicker = [[UIPickerView alloc] init];
    [ageToPicker setTag:AGE_TO_PICKER_TAG];
    [ageToPicker setDelegate:self];
    [birthdayEndTextField setInputView:ageToPicker];
}

- (NSUInteger)countriesCount
{
    NSUInteger count = [(NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count];
    return count;
}

- (NSUInteger)citiesCount
{
    NSUInteger count = [(NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] count];
    return count;
}

#pragma mark - Success and Failed Methods

- (void)filteredSuccess
{
    NSLog(@"success...");
    [self removeActivityIndicator];
//    [self performSegueWithIdentifier:@"filterResultsVC" sender:self];
    [self performSegueWithIdentifier:@"backToTopVC" sender:self];
}

- (void)filteredFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)countriesSuccess
{
    countriesDownloaded = YES;
    if ([[countryTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[countryTextField inputView] stopAnimating];
    }
    [self configureCountryPicker];
}

- (void)citiesSuccess
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    
    citiesDownloaded = YES;
    if ([[cityTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[cityTextField inputView] stopAnimating];
    }
    [self configureCitiesPicker];
}

- (void)countriesFailed
{
    [countryTextField resignFirstResponder];
    [self showAlertWithIdentifier:countriesAlert];
}

- (void)citiesFailed
{
    [self showAlertWithIdentifier:citiesAlert];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError] ||
             [identifier isEqualToString:countriesAlert] ||
             [identifier isEqualToString:citiesAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if ([identifier isEqualToString:@"noCountryAlert"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"chooseTheCountry", nil)
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Activity

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor colorWithRed:149.0/255.0
                                       green:167.0/255.0
                                        blue:196.0/255.0
                                       alpha:1.0f]];
    [activity setTag:5];
    [activity setCenter:searchButton.center];
    searchButton.hidden = YES;
    [[self view] addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activity = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activity stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    searchButton.hidden = NO;
}


#pragma mark - IBAction Methods

- (IBAction)filter:(id)sender
{
    NSLog(@"filter...");
    
    if ([self textFieldsAreNotEmpty]) {
        [self showActivityIndicator];
        
        NSString *birthdayStart = [birthdayStartTextField text];
        NSString *birthdayEnd = [birthdayEndTextField text];
        NSString *genderString = [NSString stringWithFormat:@"%d", gender];
        
        NSMutableDictionary *filterData = [[NSMutableDictionary alloc] init];
        
        if ([[countryTextField text] isEqualToString:@""]) {
            [filterData setObject:@"" forKey:@"countryID"];
        } else {
            [filterData setObject:countryID forKey:@"countryID"];
        }
        
        if ([[cityTextField text] isEqualToString:@""]) {
            [filterData setObject:@"" forKey:@"cityID"];
        } else {
            [filterData setObject:cityID forKey:@"cityID"];
        }
        
        if ([genderString isEqualToString:@"0"]) {
            genderString = @"";
        }
        
        [filterData setObject:birthdayStart forKey:@"birthdayStart"];
        [filterData setObject:birthdayEnd forKey:@"birthdayEnd"];
        [filterData setObject:genderString forKey:@"gender"];
        
        [[NSUserDefaults standardUserDefaults] setObject:filterData forKey:@"filterData"];
        
        NSLog(@"filter data dictionary is %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"filterData"]);
        
        [API filterUsers];
    }
    else
    {
        [self showAlertWithIdentifier:clientAlert];
    }
}

- (IBAction)maleButtonPress:(id)sender
{
    [femaleButton setSelected:NO];
    if (!maleButton.selected)
    {
        [maleButton setSelected:YES];
        gender = 1;
    } else {
        [maleButton setSelected:NO];
        gender = 0;
    }
    
    NSLog(@"gender is %d", gender);
}

- (IBAction)femaleButtonPress:(id)sender
{
    [maleButton setSelected:NO];
    if (!femaleButton.selected) {
        [femaleButton setSelected:YES];
        gender = 2;
    } else {
        [femaleButton setSelected:NO];
        gender = 0;
    }
    NSLog(@"gender is %d", gender);
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"backToTopVC"]) {
        APTopVC *topVC = [segue destinationViewController];
        topVC.isNeedDisplayFilterResults = YES;
    }
}


@end
