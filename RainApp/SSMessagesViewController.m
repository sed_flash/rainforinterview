//
//  SSMessagesViewController.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessagesViewController.h"
#import "SSMessageTableViewCell.h"
#import "SSMessageTableViewCellBubbleView.h"
#import "APTextView.h"
#import "APConversationVC.h"

CGFloat kInputHeight = 45.0f;

@implementation SSMessagesViewController

@synthesize tableView = _tableView;
@synthesize doneButton = _doneButton;
@synthesize attachButton = _attachButton;
@synthesize leftBackgroundImage = _leftBackgroundImage;
@synthesize rightBackgroundImage = _rightBackgroundImage;

#pragma mark NSObject

- (void)dealloc {
	self.leftBackgroundImage = nil;
	self.rightBackgroundImage = nil;
}


#pragma mark UIViewController

static const CGFloat TABLE_VIEW_MARGIN_LEFT = 0.0f;
static const CGFloat TABLE_VIEW_MARGIN_TOP = 64.0f;
static const CGFloat TABLE_VIEW_MARGIN_BOTTOM = 64.0f;



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    api=[[APServerAPI alloc]init];
    
   
    
//    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)]];
	CGSize size = self.view.frame.size;
	
	// Table view
	_tableView = [[UITableView alloc] initWithFrame:CGRectMake(TABLE_VIEW_MARGIN_LEFT,
                                                               TABLE_VIEW_MARGIN_TOP,
                                                               size.width,
                                                               size.height - kInputHeight - TABLE_VIEW_MARGIN_BOTTOM)
                                              style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor whiteColor];
	_tableView.dataSource = self;
	_tableView.delegate = self;
	_tableView.separatorColor = self.view.backgroundColor;
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:_tableView];
    
    // APTextView customization
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 45, self.view.frame.size.width, 45)];
    
    textView = [[APTextView alloc] initWithFrame:CGRectMake(44, 4, self.view.frame.size.width/1.54, 46)];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 8;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    textView.returnKeyType = UIReturnKeyDefault;
    textView.font = [UIFont fontWithName:@"Arial" size:14.0f];
    textView.textColor = [UIColor blackColor];
    textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 5);
    textView.backgroundColor = [UIColor clearColor];
    textView.placeholder = NSLocalizedString(@"typeMessageHere", nil);
    
    // textView.text = @"test\n\ntest";
    // textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
    
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField2.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(44, 0, self.view.frame.size.width/1.52, 46);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground2.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:entryImageView];
    [containerView addSubview:textView];
    
    UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton2.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _doneButton.frame = CGRectMake(containerView.frame.size.width - 69, 6.5, 63, 32.5);
    _doneButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [_doneButton setTitle:NSLocalizedString(@"sendMessage", nil) forState:UIControlStateNormal];
    
    _doneButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:14.0f];
    
    [_doneButton setTitleColor:[UIColor colorWithRed:93.0/255.0
                                               green:124.0/255.0
                                                blue:173.0/255.0
                                               alpha:1.0] forState:UIControlStateNormal];
    [containerView addSubview:_doneButton];
    
    UIImage *attachButtonImage = [UIImage imageNamed:@"attachButton.png"];
    _attachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_attachButton setFrame:CGRectMake(11, 11, 24, 24)];
    _attachButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [_attachButton setTitle:@"" forState:UIControlStateNormal];
    [_attachButton setBackgroundImage:attachButtonImage forState:UIControlStateNormal];
    [containerView addSubview:_attachButton];

    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    self.leftBackgroundImage = [[UIImage imageNamed:@"messageBackgroundLeft.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:14];
    self.rightBackgroundImage = [[UIImage imageNamed:@"messageBackgroundRight.png"] stretchableImageWithLeftCapWidth:14 topCapHeight:8];
    
//    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f
//                                                                                         tableView:self.tableView
//                                                                                        withClient:self];
    
    
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
//    [pullToRefreshManager_ relocatePullToRefreshView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)growingTextView:(APTextView *)growingTextView willChangeHeight:(float)height
{
    
    
    float diff = (growingTextView.frame.size.height - height);
    
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    containerView.frame = r;
}

#pragma mark SSMessagesViewController

// This method is intended to be overridden by subclasses
- (SSMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return SSMessageStyleLeft;
}


// This method is intended to be overridden by subclasses
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath {
	return nil;
}

// This method is intended to be overriden by subclasses
- (NSData *)avatarForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

// This method is intended to be overriden by subclasses
- (NSString *)attachmentForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (NSData *)attachmentDataForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *cellIdentifier = @"BubbleCellIdentifier";
//    
//    SSMessageTableViewCell *cell = (SSMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        cell = [[SSMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//		[cell setBackgroundImage:self.leftBackgroundImage forMessageStyle:SSMessageStyleLeft];
//		[cell setBackgroundImage:self.rightBackgroundImage forMessageStyle:SSMessageStyleRight];
//    }
//
//    cell.messageStyle = [self messageStyleForRowAtIndexPath:indexPath];
//    cell.messageText = [self textForRowAtIndexPath:indexPath];
//    cell.avatarData = [self avatarForRowAtIndexPath:indexPath];
//    cell.attachmentURLString = [self attachmentForRowAtIndexPath:indexPath];
//    cell.attachmentImage = [self attachmentDataForRowAtIndexPath:indexPath];
//    
//    return cell;
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"did select!");
}


#pragma mark UITableViewDelegate

//static const CGFloat CELL_MARGIN_TOP = 22.0f;
//static const CGFloat CELL_MAX_HEIGHT = 235.0f;
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    CGFloat height;
//    
//    if ([self attachmentForRowAtIndexPath:indexPath]) {
//        height = CELL_MAX_HEIGHT + CELL_MARGIN_TOP;
//        NSLog(@"cell height for attach: %f", height);
//    } else {
//        height = [SSMessageTableViewCellBubbleView cellHeightForText:[self textForRowAtIndexPath:indexPath]] + CELL_MARGIN_TOP;
//        NSLog(@"cell height without attach: %f", height);
//    }
//    return height;
//}


#pragma mark UITextFieldDelegate


-(void) keyboardWillShow:(NSNotification *)note{
    
    // get keyboard size and loctaion
    NSLog(@"keyboard will show");
    CGRect keyboardBounds;
    CGRect oldKeyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&oldKeyboardBounds];
    NSLog(@"note.userInfo:\n%@", note.userInfo);
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    oldKeyboardBounds = [self.view convertRect:oldKeyboardBounds toView:nil];
    
    NSLog(@"keyboard bounds: x = %f, y = %f, w = %f, h = %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.width, keyboardBounds.size.height);
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    //    _tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, keyboardBounds.size.height, 0.0f);
    //    _tableView.scrollIndicatorInsets = _tableView.contentInset;
    
    CGFloat tableViewDifference;
    CGFloat keyboardHeightDifference = keyboardBounds.size.height - oldKeyboardBounds.size.height;
    
    NSLog(@"kdDiff = %f", keyboardHeightDifference);
    
    if (keyboardHeightDifference == 0) {
        tableViewDifference = keyboardBounds.size.height;
    }
    else if (keyboardHeightDifference > 0)
    {
        tableViewDifference = keyboardHeightDifference;
    }
    else if (keyboardHeightDifference < 0)
    {
        tableViewDifference = keyboardHeightDifference;
    }
    
    _tableView.frame = CGRectMake(_tableView.frame.origin.x,
                                      _tableView.frame.origin.y,
                                      _tableView.frame.size.width,
                                      _tableView.frame.size.height - tableViewDifference);

    // if tableView has cells
    if ([_tableView numberOfRowsInSection:0]) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_tableView numberOfRowsInSection:0]-1 inSection:0]
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
    }
    
    NSLog(@"commit animations (will show)");
    // commit animations
    [UIView commitAnimations];
    
//    [pullToRefreshManager_ relocatePullToRefreshView];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
//    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.contentInset = UIEdgeInsetsMake(12.0, 0, 0, 0);
//    _tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    _tableView.frame = CGRectMake(_tableView.frame.origin.x,
                                  _tableView.frame.origin.y,
                                  _tableView.frame.size.width,
                                  _tableView.frame.size.height + keyboardBounds.size.height);
    
    NSLog(@"commit animations (will hide)");
    // commit animations
    [UIView commitAnimations];
    
//    [pullToRefreshManager_ relocatePullToRefreshView];
}

#pragma mark  - MNMBottomPullToRefreshManagerClient

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshManagerClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewScrolled]
 *
 * Tells the delegate when the user scrolls the content view within the receiver.
 *
 * @param scrollView: The scroll-view object in which the scrolling occurred.
 */
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//        [pullToRefreshManager_ tableViewScrolled];
//}

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewReleased]
 *
 * Tells the delegate when dragging ended in the scroll view.
 *
 * @param scrollView: The scroll-view object that finished scrolling the content view.
 * @param decelerate: YES if the scrolling movement will continue, but decelerate, after a touch-up gesture during a dragging operation.
 */
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    if (scrollView.contentOffset.y + (self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - 10) >= scrollView.contentSize.height)
//        [pullToRefreshManager_ tableViewReleased];
//}

/**
 * Tells client that refresh has been triggered
 * After reloading is completed must call [MNMBottomPullToRefreshManager tableViewReloadFinished]
 *
 * @param manager PTR manager
 */

@end
