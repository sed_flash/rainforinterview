//
//  APDialogsCell.h
//  Rain
//
//  Created by EgorMac on 17/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface APDialogsCell : UITableViewCell
{
    NSDictionary *dialog;
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UIImageView *avatarView;
    IBOutlet UILabel *unreadMessagesCountLabel;
    IBOutlet UIImageView *unreadMessagesBackground;
}

@property (nonatomic, strong) NSDictionary *dialog;
@property (nonatomic, strong) IBOutlet UIImageView *avatarView;

@end
