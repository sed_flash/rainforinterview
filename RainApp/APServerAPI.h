//
//  APServerAPI.h
//  Rain
//
//  Created by Nozdrinov Yegor  on 03/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APProgressBarDelegate
- (void)updateProgress:(float)progress;
@end

@interface APServerAPI : NSObject
{
    NSMutableArray *connections;
}

- (void)signup;
- (void)getCountries;
- (void)getCitiesForCountryID:(NSString *)countryID;
- (void)getCitiesForCountryID:(NSString *)countryID andUserID:(NSString *)userID;
- (void)fillProfile;
- (void)editProfileWithEmail:(NSString *)email andPassword:(NSString *)password;
- (void)getUserSettings;
- (void)sendProfileAccess:(BOOL)accessIsOn andPushNotifications:(BOOL)pushIsOn;
- (void)showUser:(NSString *)email withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit;
- (void)getFeedWithOffset:(NSUInteger)offset andLimit:(NSUInteger)limit;
- (void)searchUserForRecovery:(NSString *)email;
- (void)searchFriendWithName:(NSString *)name;
- (void)filterUsers;
- (void)followUserWithID:(NSString *)userID;
- (void)unfollowUserWithID:(NSString *)userID;
- (void)getNotifications;
- (void)getNews;
- (void)allUsers;
- (void)getFollowersForUserWithEmail:(NSString *)email;
- (void)getFollowingsForUserWithEmail:(NSString *)email;
- (void)recoveryPasswordForEmail:(NSString *)email;
- (void)changePasswordTo:(NSString *)newPassword andConfirmation:(NSString *)confirmation;
- (void)signinWithEmail:(NSString *)email andPassword:(NSString *)password;
- (void)sendPostWithURL:(NSURL *)videoURL;
- (void)sendPhotoPost;
- (void)deletePost:(NSString *)deleteLink;
- (void)deleteComment:(NSString *)deleteLink;
- (void)deleteMessage:(NSString *)messageID;
- (void)deleteConversation:(NSString *)personEmail;
- (void)deleteAccount;
- (void)sendVideoPost;
- (void)sendComment:(NSString *)comment forPostID:(NSString *)postID;
- (void)getCommentsForPostID:(NSString *)postID;
- (void)likePostWithID:(NSString *)postID;
- (void)repostPostWithID:(NSString *)postID;
- (void)getDialogs;
- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID;
- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID andPhotoAttachment:(NSData *)imageData;
- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID andVideoAttachment:(NSURL *)videoURL;
- (void)getConversationWithUserID:(NSString *)userID;
- (void)confirmFollowRequestWithID:(NSString *)notificationID;
- (void)declineFollowRequestWithID:(NSString *)notificationID;
- (void)sendReportForPostWithID:(NSString *)postID andText:(NSData *)commentTextData;
- (void)sendReportForUserWithEmail:(NSString *)email andText:(NSData *)commentTextData;
- (void)updateBadge;

@property (weak, nonatomic) id <APProgressBarDelegate> delegate;

@end
