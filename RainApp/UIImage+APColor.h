//
//  UIImage+APColor.h
//  Rain
//
//  Created by EgorMac on 25/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (APColor)

+(UIImage *)imageWithColor:(UIColor *)color;

@end
