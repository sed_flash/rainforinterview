//
//  NewFilterVC.h
//  Rain
//
//  Created by EgorMac on 07/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewFilterVC : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    NSString *filterID;
    NSArray *filtersArray;
    NSMutableDictionary *currentFilter;
    
    UIImage *originalImage;
    
    IBOutlet UIImageView *photo;
    IBOutlet UIToolbar *toolbar;
    
    IBOutlet UIScrollView *scrollView;
}

@property (nonatomic, strong) NSString *filterID;

@end
