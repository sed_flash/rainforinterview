//
//  APSenderMediaTableViewCell.m
//  Rain
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "APSenderMediaTableViewCell.h"

@interface APSenderMediaTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *senderBubble;
@property (weak, nonatomic) IBOutlet UILabel *senderMessage;

@end

@implementation APSenderMediaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *tempImage = [_senderBubble.image resizableImageWithCapInsets:UIEdgeInsetsMake(7, 8, 20, 27) resizingMode:UIImageResizingModeStretch];
    _senderBubble.image = tempImage;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Setters

- (void)setMessage:(NSString *)message {
    _senderMessage.text = message;
}

- (void)setAvatarData:(NSData *)avatarData {
    [UIImageView apa_useMaskToImageView:_avatarImage
                               forImage:[UIImage imageWithData:avatarData]];
}

- (void)setAttachmentImage:(UIImage *)attachmentImage {
    dispatch_async(dispatch_get_main_queue(), ^{
        _senderMedia.image = attachmentImage;
    });
}

- (void)setAttachmentURLString:(NSString *)attachmentURLString {
    _attachmentURLString = attachmentURLString;
    
    if ([_attachmentURLString hasSuffix:@".mp4"] || [_attachmentURLString hasSuffix:@".MOV"]) {
        
        // video
        NSURL *videoURL = [NSURL fileURLWithPath:_attachmentURLString];
        
        UIImage *thumbnail = [UIImage imageNamed:@"videoAttachment.png"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_senderMedia setImage:thumbnail];
        });
        
    }
    else if ([_attachmentURLString hasSuffix:@".jpeg"] ||
             [_attachmentURLString hasSuffix:@".png"])
    {
        NSLog(@"**** **** ****");
    }
    else
    {
        if ([_attachmentURLString isEqualToString:@""] ||
            _attachmentURLString == nil)
        {
            NSLog(@"has no attachment (old)");
        } else {
            NSLog(@"attachmentusrl string : %@", _attachmentURLString);
            // For manually created cell with image from NSData (not from URL)
            NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"messageAttachment"];
            UIImage *attachmentImage = [[UIImage alloc] initWithData:imageData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_senderMedia setImage:attachmentImage];
            });
        }
    }
    
}

@end
