//
//  APUserInfoVC.h
//  Rain
//
//  Created by EgorMac on 05/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APUserInfoVC : UIViewController<UIImagePickerControllerDelegate,UIAlertViewDelegate, UIActionSheetDelegate>
{
    NSDictionary *currentUser;
    UIImage *photoImage;
    IBOutlet UIImageView *avatarImageView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *nickLabel;
    IBOutlet UILabel *ageLabel;
    IBOutlet UILabel *locationLabel;
    IBOutlet UIButton *userAgreementButton;
    
    UIActionSheet *chooseMediaActionSheet;
    __weak IBOutlet UIImageView *imgBacground;
}

@property(nonatomic,strong) UIImagePickerController *imagePicker;

@property (nonatomic, strong) NSDictionary *currentUser;
@property (nonatomic, strong) UIImage *photoImage;
- (IBAction)btnChangePhoto:(id)sender;

- (void)reportInappropriate;

@end
