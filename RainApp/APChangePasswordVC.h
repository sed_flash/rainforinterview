//
//  APChangePasswordVC.h
//  Rain
//
//  Created by EgorMac on 29/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APChangePasswordVC : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, UIAlertViewDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *confirmPasswordTextField;
    IBOutlet UIButton *savePasswordButton;
    
    NSString *newPassword;
    
    APServerAPI *API;
}

- (IBAction)savePassword:(id)sender;

@end
