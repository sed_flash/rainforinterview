//
//  APReportProfileVC.m
//  Rain
//
//  Created by EgorMac on 16/01/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import "APReportProfileVC.h"

@interface APReportProfileVC ()

@end

@implementation APReportProfileVC
@synthesize profileEmail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    serverAPI = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportSuccess)
                                                 name:@"ReportProfileSuccess"
                                               object:serverAPI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportFailed)
                                                 name:@"ReportProfileFailed"
                                               object:serverAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setProfileEmail:(NSString *)newProfileEmail
{
    if (profileEmail != newProfileEmail) {
        profileEmail = newProfileEmail;
    }
}

- (void)configureCommentLabel
{
    [commentLabel setText:NSLocalizedString(@"yourComment(require)", nil)];
}

- (IBAction)report:(id)sender
{
    NSString *commentText = [commentTextView text];
    NSData *commentTextData = [commentText dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"comment text: %@", commentText);
    if ([[commentText stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"addYourCommentPlease", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [serverAPI sendReportForUserWithEmail:profileEmail andText:commentTextData];
    }
}

@end
