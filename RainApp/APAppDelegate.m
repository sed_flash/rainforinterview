//
//  APAppDelegate.m
//  RainApp
//
//  Created by Nozdrinov Yegor  on 15/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APAppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
//#import <objc/runtime.h>
//#import <objc/message.h>

#import <MediaPlayer/MediaPlayer.h>
#import "APServerAPI.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation APAppDelegate

@synthesize allowRotation;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
//    if ([self class] == [APAppDelegate class])
//    {
//        [YMMYandexMetrica startWithAPIKey:@"96219"];
//        [YMMYandexMetrica setCustomAppVersion:appBuildString];
//        [YMMYandexMetrica setReportCrashesEnaSDKAppEvents activateApbled:YES];
//        [YMMYandexMetrica setTrackLocationEnabled:YES];
//    }
//    
    
     //[self findMeWithCompletion:nil];
    


//    Class target = NSClassFromString(@"__NSPlaceholderDictionary");
//    class_addMethod(target, @selector(safe_initWithObjects:forKeys:count:), (IMP)&safe_initWithObjects, "@@:**L");
////    
//    Method m1 = class_getInstanceMethod(target, @selector(safe_initWithObjects:forKeys:count:));
//    Method m2 = class_getInstanceMethod(target, @selector(initWithObjects:forKeys:count:));
//    method_exchangeImplementations(m1, m2);
    
    // Override point for customization after application launch.
    // Adding Crashlytics (Fabric) - Crash Reporting Framework
//    [Fabric with:@[CrashlyticsKit]];
    [Fabric with:@[[Crashlytics class]]];
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"RainCoreDataModel"];
    [MagicalRecord setupCoreDataStack];
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        // use registerUserNotificationSettings
        NSLog(@"use registerUserNotificationSettings");
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        
    } else {
        // use registerForRemoteNotifications
        NSLog(@"use registerForRemoteNotifications");
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
   //[self allowBackgroundMusic];
    
    [self configureStatusBarAndTabBar];
    
    if (launchOptions)
    {
        [[NSUserDefaults standardUserDefaults] setObject:launchOptions forKey:@"launchOptions"];
    }
    
    self.allowRotation = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerWillEnterFullscreenNotification:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerWillExitFullscreenNotification:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:nil];
    
    
  // RootViewController *rt=[[RootViewController alloc]init];
   // [rt getUser];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    APServerAPI *API = [[APServerAPI alloc] init];
    [API updateBadge];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
   // [[EVCloudData publicDB]backupAllData];
   

   // EVCloudData.publicDB.backupAllData();
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
   [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Push Notifications Methods


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    
    NSLog(@"My token is: %@", deviceToken);
     NSString *token = deviceToken.description;
    
   // [self saveSubscriptionWithIdent:token options:CKSubscriptionOptionsFiresOnRecordCreation];
    
    NSString *pushTokenString = [[[NSString stringWithFormat:@"%@", deviceToken] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"pushTokenString is:\n %@", pushTokenString);
    
    
    [[NSUserDefaults standardUserDefaults] setObject:pushTokenString forKey:@"pushToken"];
}



- (CKNotificationInfo *)notificationInfo
{
    CKNotificationInfo *note = [[CKNotificationInfo alloc] init];
    note.alertBody=@"Test";
    note.shouldBadge = YES;
    note.shouldSendContentAvailable = YES;
    return note;
}

- (void)saveSubscriptionWithIdent:(NSString*)ident options:(CKSubscriptionOptions)options
{
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *publicDatabase = [container publicCloudDatabase];
    CKSubscription *sub = [[CKSubscription alloc] initWithRecordType:@"Message" predicate:[NSPredicate predicateWithValue:YES] subscriptionID:ident options:options];
    sub.notificationInfo = [self notificationInfo];
    [publicDatabase saveSubscription:sub completionHandler:nil];
}



- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to get token, error: %@", error);
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pushToken"];
    }
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadFromPush" object:self];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"UpdateMessages" object:self];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchSuccess" object:self];
    
//     [EVCloudData.publicDB didReceiveRemoteNotification:userInfo executeIfNonQuery:^{
//         
//        
//     } completed:^{
//         
//         
//     }];
    
    
    
//    [[EVCloudData publicDB]didReceiveRemoteNotification:userInfo executeIfNonQuery:
//        {
//            EVLog("Not a CloudKit Query notification.")
//        }
//        completed: {
//            
//            EVLog("All notifications are processed")
//        }];

}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    NSLog(@"settings : \n%@", notificationSettings);
    [application registerForRemoteNotifications];
}

#pragma mark - Helper Methods

//- (void)allowBackgroundMusic
//{
//    NSError *error;
//    BOOL success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&error];
//    if (!success) {
//        //Handle error
//        NSLog(@"audio error: %@", [error localizedDescription]);
//    }
//    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&error];
//}


- (void)configureStatusBarAndTabBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:0.0/255.0
                                                                     green:122.0/255.0
                                                                      blue:255.0/255.0
                                                                     alpha:0.8]];
    
    for (UITabBarItem *item in [[UITabBar appearance] items])
    {
        [item setImage:[[item image] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        //[item setImage:[[item image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    }
}

- (void) moviePlayerWillEnterFullscreenNotification:(NSNotification*)notification
{
    self.allowRotation = YES;
}

- (void) moviePlayerWillExitFullscreenNotification:(NSNotification*)notification
{
    self.allowRotation = NO;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}


-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.allowRotation) {
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

static id safe_initWithObjects(id self, SEL _cmd, const id objects[], const id <NSCopying> keys[], NSUInteger count)
{
    id orignialResult = nil;
    @try {
//        orignialResult = objc_msgSend(self, @selector(safe_initWithObjects:forKeys:count:), objects, keys, count);
    }
    @catch (NSException *exception) {
        NSLog(@"BUSTED!"); // put breakpoint here
    }
    return orignialResult;
}

@end
