//
//  SSMessageTableViewCell.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

typedef enum {
	SSMessageStyleLeft = 0,
	SSMessageStyleRight = 1
} SSMessageStyle;

@class SSMessageTableViewCellBubbleView;
@class APServerAPI;

@interface SSMessageTableViewCell : UITableViewCell {

@private
	
	SSMessageTableViewCellBubbleView *_bubbleView;
    UIImage *_messageSuccessImage;
    UIImage *_messageFailImage;
    APServerAPI *API;
}

@property (nonatomic, copy) NSString *messageText;
@property (nonatomic, copy) NSData *avatarData;
@property (nonatomic, copy) NSString *attachmentURLString;
@property (nonatomic, copy) UIImage *attachmentImage;
@property (nonatomic, assign) SSMessageStyle messageStyle;
@property (nonatomic, strong) UIActivityIndicatorView *cellActivityView;
@property (nonatomic, strong) UIButton *messageStatusButton;

- (void)setBackgroundImage:(UIImage *)backgroundImage forMessageStyle:(SSMessageStyle)messsageStyle;
- (void)startActivityAnimation;
- (void)stopActivityAnimationWithSuccess:(BOOL)isSuccess andUserID:(NSString *)userID;

@end
