//
//  APSearchFriendsVC.h
//  Rain
//
//  Created by EgorMac on 12/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APSearchFriendsVC : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIImageView *backgroundView;
    IBOutlet UITextField *searchTextField;
    IBOutlet UIButton *searchButton;
    
    APServerAPI *API;
}

- (IBAction)search:(id)sender;

@end
