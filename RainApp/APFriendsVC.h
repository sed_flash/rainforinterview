//
//  APFriendsVC.h
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APFriendCell.h"
#import "APServerAPI.h"
#import "APProfileVC.h"

@interface APFriendsVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UISegmentedControl *friendsSegmentedControl;
    IBOutlet UITableView *friendsTableView;
    
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *activityIndicator;
    
    APServerAPI *API;
    
    NSArray *followers;
    NSArray *followings;
    
    NSString *selectedUserEmail;
    NSString *userMail;
}

@property (nonatomic, strong) NSString *userMail;
@property (nonatomic, strong) APServerAPI *API;

@end
