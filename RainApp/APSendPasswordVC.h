//
//  APSendPasswordVC.h
//  Rain
//
//  Created by EgorMac on 19/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "UIImageView+AFNetworking.h"

@interface APSendPasswordVC : UIViewController <UIAlertViewDelegate>
{
    IBOutlet UIImageView *photoImageView;
    IBOutlet UILabel *nicknameLabel;
    IBOutlet UIButton *sendPasswordButton;
    
    NSDictionary *userData;
    
    APServerAPI *API;
}

@property (nonatomic, strong) NSDictionary *userData;
@property (nonatomic, strong) APServerAPI *API;

- (IBAction)sendPassword:(id)sender;

@end
