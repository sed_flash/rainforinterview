//
//  APTopVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APTopVC.h"
#import "APTopTableCell.h"

#define kCellId @"TopTableCell"

@interface APTopVC ()

@end

@implementation APTopVC {
    NSDictionary *foundUsers;
    NSArray *foundUsersIDs;
    UIBarButtonItem *searchButton;
    NSArray *tempAllUsers;
}

@synthesize API;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tb=topTableView;
    // Do any additional setup after loading the view.
    
    tempAllUsers = [NSArray new];
    
    UIImage *searchButtonImage = [UIImage imageNamed:@"searchIcon.png"];
    searchButton = [[UIBarButtonItem alloc] initWithImage:searchButtonImage
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(searchFriends)];
    [searchButton setImage:[UIImage imageNamed:@"Search Button"]];
    NSMutableArray *buttons = [self.navigationItem.rightBarButtonItems mutableCopy];
    [buttons addObject:searchButton];
    self.navigationItem.rightBarButtonItems = buttons;
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allUsersSuccess)
                                                 name:@"AllUsersSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allUsersFailed)
                                                 name:@"AllUsersFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    
    topTableView.rowHeight = UITableViewAutomaticDimension;
    topTableView.estimatedRowHeight = 78.0;
    
    [topTableView registerNib:[UINib nibWithNibName:@"APTopTableCellXib" bundle:nil] forCellReuseIdentifier:kCellId];

}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.automaticallyAdjustsScrollViewInsets = false;

    //[self prepareVisibleCellsForAnimation];
    
    [self setupUIElements];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(checkForTopUsers)
             forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = topTableView;
    controller.refreshControl = refreshControl;
    
    if (!_isNeedDisplayFilterResults && !_isNeedDisplaySearchResults) {
        if ([topTableView numberOfRowsInSection:0] == 0) {
            [self showActivityIndicator];
            [API allUsers];
        }
    } else {
        if ([topTableView numberOfRowsInSection:0] == 0) {
            [self showActivityIndicator];
            foundUsers = [NSDictionary new];
            foundUsersIDs = [NSArray new];
            [self getFilstersResults];
            [API allUsers];
        }
    }
    
  //  [topTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];

}

- (void)getFilstersResults {
    NSArray *users;
    NSMutableArray *usersIDs;
    NSMutableDictionary *usersDictionary;
    if (_isNeedDisplayFilterResults) {
        users = [[NSUserDefaults standardUserDefaults] objectForKey:@"filteredUsers"];
        usersIDs = [[NSMutableArray alloc] init];
        usersDictionary = [[NSMutableDictionary alloc] init];
        
        for (NSDictionary *user in users) {
            NSString *userID = [[user objectForKey:@"id"] stringValue];
            [usersIDs addObject:userID];
            [usersDictionary setObject:user forKey:userID];
        }
        
        foundUsersIDs = [usersIDs copy];
        foundUsers = [usersDictionary copy];
        
        users = nil;
        usersIDs = nil;
        usersDictionary = nil;
    } else if (_isNeedDisplaySearchResults) {
        foundUsers = [[NSDictionary alloc] init];
        foundUsersIDs = [[NSArray alloc] init];
        
        users = [[NSUserDefaults standardUserDefaults] objectForKey:@"foundFriends"];
        usersIDs = [[NSMutableArray alloc] init];
        usersDictionary = [[NSMutableDictionary alloc] init];
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSDictionary *userData= [defaults objectForKey:@"userData"];
        NSString* Email=[userData  objectForKey:@"email"];
        
        for (NSDictionary *user in users)
            
        {
            NSString *email=[user objectForKey:@"email"];
            if(![Email isEqualToString:email])
            {
                NSString *userID = [[user objectForKey:@"id"] stringValue];
                [usersIDs addObject:userID];
                [usersDictionary setObject:user forKey:userID];
            }
            
        }
        
        foundUsersIDs = [usersIDs copy];
        foundUsers = [usersDictionary copy];
    }
    
    NSMutableArray *tempUsers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allUsers"] mutableCopy];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"rating" ascending:NO];
    NSArray *descriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [tempUsers sortUsingDescriptors:descriptors];
    users = [tempUsers copy];
    
    tempUsers = nil;
    
    users = nil;
    usersIDs = nil;
    usersDictionary = nil;
}

- (void)checkForTopUsers
{
    [API allUsers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor whiteColor]];
    [activity setTag:5];
    [activity setCenter:self.view.center];
    [topTableView setHidden:YES];
    [self.view addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activityIndicator stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    [topTableView setHidden:NO];
}

- (void)allUsersSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)allUsersFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
   if ([identifier isEqualToString:serverAlert])
   {
       NSString *errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isNeedDisplayFilterResults || _isNeedDisplaySearchResults)
        return [foundUsersIDs count];
    return [users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APTopTableCell *cell = (APTopTableCell *)[tableView dequeueReusableCellWithIdentifier:kCellId];
    
    NSDictionary *user;
    if (_isNeedDisplayFilterResults || _isNeedDisplaySearchResults) {
        
        user = [foundUsersIDs objectAtIndex:[indexPath row]];
        NSString *userID = [foundUsersIDs objectAtIndex:indexPath.row];
        NSDictionary *currentUser = [foundUsers objectForKey:userID];
        cell.foundUsersID = userID;
        cell.foundUser = currentUser;
        cell.isCellForFiltersResults = _isNeedDisplayFilterResults | _isNeedDisplaySearchResults;
        
        NSDictionary *locationForCurrenUser = [NSDictionary dictionaryWithDictionary:
                                                    [self dictionaryForEmail:[currentUser objectForKey:@"email"]]
                                               ];
        NSLog(@"current email = %@", [currentUser objectForKey:@"email"]);
        NSLog(@"temp all users = %@\n", locationForCurrenUser);
        [cell setLocation:locationForCurrenUser];
        locationForCurrenUser = nil;
        
    } else {
        
        user = [users objectAtIndex:[indexPath row]];
        [cell setLocation:user];
    }
    
    [cell setUser:user];
    
    return cell;
}

- (NSDictionary *)dictionaryForEmail:(NSString *)email {
    for (NSDictionary *d in tempAllUsers) {
        if ([[d objectForKey:@"email"] isEqualToString:email]) {
            return d;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *user;
    
    if (_isNeedDisplayFilterResults || _isNeedDisplaySearchResults) {
        
        user = [foundUsersIDs objectAtIndex:[indexPath row]];
        NSString *userID = [foundUsersIDs objectAtIndex:indexPath.row];
        NSDictionary *currentUser = [foundUsers objectForKey:userID];
        
        selectedUserMail = [currentUser objectForKey:@"email"];
        
    } else {
        
        user = [users objectAtIndex:[indexPath row]];
        selectedUserMail = [user objectForKey:@"email"];
        
    }

    
    NSLog(@"User with %@ selected", selectedUserMail);
    
    [self performSegueWithIdentifier:@"showUserFromTop" sender:self];
}


#pragma mark - Helper Methods

- (void)setDataSource
{
    NSMutableArray *tempUsers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allUsers"] mutableCopy];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"rating" ascending:NO];
    NSArray *descriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [tempUsers sortUsingDescriptors:descriptors];
    
    if (_isNeedDisplaySearchResults || _isNeedDisplayFilterResults) {
        tempAllUsers = [tempUsers copy];
    } else {
        users = [tempUsers copy];
    }
    
    tempUsers = nil;
    
    [refreshControl endRefreshing];
    [topTableView reloadData];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    if (_isNeedDisplayFilterResults || _isNeedDisplaySearchResults) {
        [self.navigationItem setRightBarButtonItems:nil animated:NO];
        [[self navigationItem] setTitle:NSLocalizedString(@"searchFriends", nil)];
    } else {
        [[self navigationItem] setTitle:NSLocalizedString(@"Top", nil)];
    }
    
}

- (void)searchFriends
{
    [self performSegueWithIdentifier:@"showSearchFriendsVC"
                              sender:self];
}

- (void)setupUIElements
{
    [self configureNavigationBar];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepare for segue");
    
    if ([[segue identifier] isEqualToString:@"showUserFromTop"]) {
        
        NSDictionary *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        NSString *email = [token objectForKey:@"email"];
        
        if ([email isEqualToString:selectedUserMail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserMail];
    }
}
@end
