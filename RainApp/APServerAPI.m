//
//  APServerAPI.m
//  Rain
//
//  Created by Nozdrinov Yegor  on 03/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APServerAPI.h"
#import "JSON.h"
#import "Reachability.h"

@implementation APServerAPI

NSString *apiAddress = @"http://rain-app.com";
static NSString *BOUNDARY_STRING = @"1BEF0A57BE110FD467A";

- (id)init
{
    self = [super init];
    connections = [[NSMutableArray alloc] init];
    return self;
}

- (NSMutableDictionary *)findQueryWithConnection:(NSURLConnection *)connection
{
    for (NSMutableDictionary *query in connections) {
        if ([query objectForKey:@"connection"] == connection) {
            return query;
        }
    }
    return nil;
}

- (void)signup
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
    
    NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
    NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
    [query setObject:@"signup" forKey:@"queryType"];
    NSMutableData *receivedData = [[NSMutableData alloc] init];
    [query setObject:receivedData forKey:@"receivedData"];
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/users", apiAddress]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    request.HTTPMethod = @"POST";
    
    NSDictionary *user = [[NSDictionary alloc] initWithDictionary:
                          [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]];
    
    NSString *email = [user objectForKey:@"email"];
    [params appendFormat:@"email=%@", [email stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *nickname = [user objectForKey:@"nickname"];
    [params appendFormat:@"&nick=%@", [nickname stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *password = [user objectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userPhoto"];
    [params appendFormat:@"&password=%@", [password stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
    
    NSLog(@"http body = \n%@", [request HTTPBody]);
    NSLog(@"message = \n%@", params);
        
        NSMutableDictionary *token = [[NSMutableDictionary alloc] init];
        [token setObject:email forKey:@"email"];
        [token setObject:password forKey:@"password"];
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
    {
        [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
    }
        
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [query setObject:connection forKey:@"connection"];
    [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
    
}

- (void)getCountries
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"countries" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/getCountries", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getCitiesForCountryID:(NSString *)countryID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"cities" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/getCities/%@", apiAddress, countryID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getCitiesForCountryID:(NSString *)countryID andUserID:(NSString *)userID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        NSString *queryName = [NSString stringWithFormat:@"citiesForID%@", userID];
        [query setObject:queryName forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/getCities/%@", apiAddress, countryID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getAllCities
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"]) {
            for (NSDictionary *country in [[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"]) {
                [self getCitiesForCountryID:[[country objectForKey:@"id"] stringValue]];
            }
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)fillProfile
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"fillProfile" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *userEmail;
        NSString *userPassword;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"])
        {
            userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        }
        else
        {
            userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"email"];
            userPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        }
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/edit", apiAddress, userEmail]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSDictionary *user = [[NSDictionary alloc] initWithDictionary:
                              [[NSUserDefaults standardUserDefaults] objectForKey:@"filledProfileData"]];
        
        NSData *firstNameData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"firstName"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"first_name"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:firstNameData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *lastNameData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"lastName"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"last_name"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:lastNameData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *birthdayData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"birthday"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"birthday"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:birthdayData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *genderData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"gender"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"gender"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:genderData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *countryData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"countryID"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"country_id"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:countryData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *cityData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"cityID"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"city_id"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:cityData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)editProfileWithEmail:(NSString *)email andPassword:(NSString *)password
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"editProfile" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = email;
        NSString *userPassword = password;
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/edit", apiAddress, userEmail]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSDictionary *user = [[NSDictionary alloc] initWithDictionary:
                              [[NSUserDefaults standardUserDefaults] objectForKey:@"filledProfileData"]];
        
        NSData *firstNameData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"firstName"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"first_name"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:firstNameData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *lastNameData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"lastName"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"last_name"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:lastNameData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *nicknameData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"nickname"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"nick"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:nicknameData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *birthdayData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"birthday"]] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"birthdayData is %@", [[NSString alloc] initWithData:birthdayData encoding:NSUTF8StringEncoding]);
        
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"birthday"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:birthdayData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *genderData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"gender"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"gender"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:genderData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *countryData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"countryID"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"country_id"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:countryData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *cityData = [[NSString stringWithFormat:@"%@", [user objectForKey:@"cityID"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"city_id"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:cityData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
//        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"userPhoto"] isEqual:[NSNull null]]) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userPhoto"]) {
            // Add the image to the request body
            
            NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userPhoto"];
            
            // If we need to save this photo to camera roll
            // UIImage *image = [UIImage imageWithData:imageData];
            // UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
            
            [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=profile%@.png%@", @"avatar", [user objectForKey:@"firstName"], @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:[[NSString stringWithFormat:@"Content-Type: image/png"] dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:imageData];
            [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else {
            NSLog(@"this user has not profile photo");
        }
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
//        NSLog(@"http body = \n%@", [request HTTPBody]);
//        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)showUser:(NSString *)email withOffset:(NSUInteger)offset andLimit:(NSUInteger)limit
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"user" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
    
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@?offset=%lu&limit=%lu", apiAddress, email, (unsigned long)offset, (unsigned long)limit]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSLog(@"request:\n%@", request);
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getFeedWithOffset:(NSUInteger)offset andLimit:(NSUInteger)limit
{
    
    NSLog(@"get feed");
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"feed" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/feed?offset=%lu&limit=%lu", apiAddress, (unsigned long)offset, (unsigned long)limit]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)searchUserForRecovery:(NSString *)email
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"userForRecovery" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/recovery", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"GET";
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)searchFriendWithName:(NSString *)name
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"searchFriends" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/search/%@", apiAddress, [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)filterUsers
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"filter" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSDictionary *filterData = [[NSUserDefaults standardUserDefaults] objectForKey:@"filterData"];
        NSString *countryID = [filterData objectForKey:@"countryID"];
        NSString *cityID = [filterData objectForKey:@"cityID"];
        NSString *gender = [filterData objectForKey:@"gender"];
        NSString *birthdayStart = [filterData objectForKey:@"birthdayStart"];
        NSString *birthdayEnd = [filterData objectForKey:@"birthdayEnd"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/filter?country=%@&city=%@&gender=%@&birthday_start=%@&birthday_end=%@", apiAddress, countryID, cityID, gender, birthdayStart, birthdayEnd]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite)
         {
             
             NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
             
         }];
        
        [operation start];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)followUserWithID:(NSString *)userID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"follow" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/follow/%@", apiAddress, userID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"POST"];
        
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)unfollowUserWithID:(NSString *)userID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"unfollow" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/follow/%@", apiAddress, userID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"DELETE"];
        
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)allUsers
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"allUsers" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/users", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getNotifications
{
    
    @try
    {
        
        if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
        {
            NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
            [query setObject:@"notifications" forKey:@"queryType"];
            NSMutableData *receivedData = [[NSMutableData alloc] init];
            [query setObject:receivedData forKey:@"receivedData"];
            
            NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/getNotice", apiAddress]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
            [request setHTTPMethod:@"GET"];
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]
                )
            {
                [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
            }
            
            NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            
            [query setObject:connection forKey:@"connection"];
            
            [connections addObject:query];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
        }

        
    }
    @catch (NSException *exception)
    {
        
    }
   
   }

- (void)updateBadge
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"updateBadge" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/update_badge", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"PUT"];
        
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
             NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"];
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getNews
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"news" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/news", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"];
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getFollowersForUserWithEmail:(NSString *)email
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"followers" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/followers", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getFollowingsForUserWithEmail:(NSString *)email
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"followings" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/followed", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        
        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)signinWithEmail:(NSString *)email andPassword:(NSString *)password
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable]) {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"signin" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
        
        NSMutableDictionary *token = [[NSMutableDictionary alloc] init];
        [token setObject:email forKey:@"email"];
        [token setObject:password forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendProfileAccess:(BOOL)accessIsOn andPushNotifications:(BOOL)pushIsOn
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
//        NSMutableData *params = [[NSMutableData alloc] init];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"profileAccess" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        int hide_profile = 0;
        if (!accessIsOn) {
            hide_profile = 1;
        }
        
        int notifications = 0;
        if (pushIsOn) {
            notifications = 1;
        }

        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/settings?notifications=%d&hide_profile=%d", apiAddress, email, notifications, hide_profile]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
//        [params appendData:[[NSString stringWithFormat:@"hide_profile=%d\n", hide_profile] dataUsingEncoding:NSUTF8StringEncoding]];
//        [params appendData:[[NSString stringWithFormat:@"notifications=%d", notifications] dataUsingEncoding:NSUTF8StringEncoding]];
        
//        [request setHTTPBody:params];
        
//        NSLog(@"params is:\n%@", [[NSString alloc] initWithData:params encoding:NSASCIIStringEncoding]);
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
//        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

//- (void)sendProfileAccess:(BOOL)isOn
//{
//    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
//    {
//        NSMutableData *params = [[NSMutableData alloc] init];
//        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
//        [query setObject:@"profileAccess" forKey:@"queryType"];
//        NSMutableData *receivedData = [[NSMutableData alloc] init];
//        [query setObject:receivedData forKey:@"receivedData"];
//        
//        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
//        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
//        
//        int hide_profile = 0;
//        if (!isOn) {
//            hide_profile = 1;
//        }
//        
//        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/settings", apiAddress, email]];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
//        request.HTTPMethod = @"POST";
//        
//        [params appendData:[[NSString stringWithFormat:@"hide_profile=%d", hide_profile] dataUsingEncoding:NSUTF8StringEncoding]];
//        
//        [request setHTTPBody:params];
//        
//        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
//        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
//        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
//                               [authData base64EncodedStringWithOptions:0]];
//        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//        
//        NSLog(@"http body = \n%@", [request HTTPBody]);
//        NSLog(@"message = \n%@", params);
//        
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
//            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
//        }
//        
//        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        [query setObject:connection forKey:@"connection"];
//        [connections addObject:query];
//    }
//    else
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
//    }
//}
//
//- (void)turnPushNotifications:(BOOL)isOn
//{
//    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
//    {
//        NSMutableData *params = [[NSMutableData alloc] init];
//        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
//        [query setObject:@"turnPushNotifications" forKey:@"queryType"];
//        NSMutableData *receivedData = [[NSMutableData alloc] init];
//        [query setObject:receivedData forKey:@"receivedData"];
//        
//        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
//        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
//        
//        int notifications = 0;
//        if (isOn) {
//            notifications = 1;
//        }
//        
//        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/settings", apiAddress, email]];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
//        request.HTTPMethod = @"POST";
//        
//        [params appendData:[[NSString stringWithFormat:@"notifications=%d", notifications] dataUsingEncoding:NSUTF8StringEncoding]];
//        
//        [request setHTTPBody:params];
//        
//        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
//        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
//        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
//                               [authData base64EncodedStringWithOptions:0]];
//        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//        
//        NSLog(@"http body = \n%@", [request HTTPBody]);
//        NSLog(@"message = \n%@", params);
//        
//        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
//            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
//        }
//        
//        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        [query setObject:connection forKey:@"connection"];
//        [connections addObject:query];
//    }
//    else
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
//    }
//}

- (void)getUserSettings
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"getSettings" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/settings", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"GET";
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", email, password];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendPhotoPost
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendPhoto" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSDictionary *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"postData"];
        
        NSData *typeData = [@"photo" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"type"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:typeData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *titleData = [@"title" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"title"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:titleData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *descriptionData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"description"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"description"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:descriptionData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"description in params: \n%@", [postData objectForKey:@"description"]);
        
        NSData *marksData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"marks"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"marks"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:marksData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Add the image to the request body
            
        NSData *imageData = [postData objectForKey:@"imageData"];
            
        // If we need to save this photo to camera roll
        // UIImage *image = [UIImage imageWithData:imageData];
        // UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
            
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=postImage.jpeg%@", @"file", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:imageData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"params are:\n%@", [[NSString alloc] initWithData:params encoding:NSASCIIStringEncoding]);
        
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        //        NSLog(@"http body = \n%@", [request HTTPBody]);
        //        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *responseString = [[NSString alloc] initWithData:responseObject
                                                             encoding:NSUTF8StringEncoding];
            NSLog(@"response string - %@", responseString);
            NSMutableDictionary *response = [[NSMutableDictionary alloc] init];
            response = [responseString JSONValue];
            
            if ([[response valueForKey:@"status"] isEqualToString:@"OK"])
            {
                NSLog(@"post succesfully created!");
                
                NSDictionary *postData = [response objectForKey:@"data"];
                
                [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"createdPostData"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostSuccess"
                                                                    object:self];
            }
            else if ([[response valueForKey:@"status"] isEqualToString:@"ERROR"])
            {
                NSLog(@"Registration failed!");
                NSLog(@"Error is: \n%@", [response objectForKey:@"message"]);
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"message"]
                                                          forKey:@"errorMessage"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostFailed"
                                                                    object:self];
            }
            else
            {
                NSLog(@"unknown status error for edit profile");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                    object:self];
            }
        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"error: %@", error.localizedDescription);
                                         }];
        [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite)
         {
             
             NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
             [self updateProgressForProgressBar:(float)totalBytesWritten/totalBytesExpectedToWrite];
         }];
        
        [operation start];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendVideoPost
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendVideo" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSDictionary *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"postData"];
        
        NSData *typeData = [@"video" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"type"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:typeData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *titleData = [@"title" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"title"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:titleData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *descriptionData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"description"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"description"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:descriptionData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"description in params: \n%@", [postData objectForKey:@"description"]);
        
        NSData *marksData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"marks"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"marks"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:marksData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // Add the video to the request body
        
//        NSURL *videoURL = [postData objectForKey:@"videoURL"];
//        NSString *videoURLString = [NSString stringWithContentsOfURL:videoURL
//                                                            encoding:NSUTF8StringEncoding
//                                                               error:nil];
        
        NSString *videoURLString  = [postData objectForKey:@"videoURL"];
        
        NSData *videoData = [NSData dataWithContentsOfFile:videoURLString];
        
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"videoCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"video"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=postVideo.mp4%@", @"file", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Type: video/quicktime"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:videoData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        //        NSLog(@"http body = \n%@", [request HTTPBody]);
        //        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendPostWithURL:(NSURL *)videoURL
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendPhoto" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSDictionary *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"postData"];
        
        NSData *typeData = [@"video" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"type"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:typeData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *titleData = [@"title" dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"title"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:titleData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *descriptionData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"description"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"description"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:descriptionData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"description in params: \n%@", [postData objectForKey:@"description"]);
        
        NSData *marksData = [[NSString stringWithFormat:@"%@", [postData objectForKey:@"marks"]] dataUsingEncoding:NSUTF8StringEncoding];
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", @"marks"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:marksData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *videoData = [[NSData alloc] initWithContentsOfURL:videoURL];
        
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"videoCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"video"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=postVideo.mp4%@", @"file", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Type: video/quicktime"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:videoData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        //        NSLog(@"http body = \n%@", [request HTTPBody]);
        //        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
//        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//        [query setObject:connection forKey:@"connection"];
//        [connections addObject:query];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *responseString = [[NSString alloc] initWithData:responseObject
                                                             encoding:NSUTF8StringEncoding];
            NSLog(@"response string - %@", responseString);
            NSMutableDictionary *response = [[NSMutableDictionary alloc] init];
            response = [responseString JSONValue];
            
            if ([[response valueForKey:@"status"] isEqualToString:@"OK"])
            {
                NSLog(@"post succesfully created!");
                
                NSDictionary *postData = [response objectForKey:@"data"];
                
                [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"createdPostData"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostSuccess"
                                                                    object:self];
            }
            else if ([[response valueForKey:@"status"] isEqualToString:@"ERROR"])
            {
                NSLog(@"Registration failed!");
                NSLog(@"Error is: \n%@", [response objectForKey:@"message"]);
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"message"]
                                                          forKey:@"errorMessage"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PostFailed"
                                                                    object:self];
            }
            else
            {
                NSLog(@"unknown status error for edit profile");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                    object:self];
            }
        }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"error: %@", error.localizedDescription);
                                         }];
        [operation setUploadProgressBlock:^(NSUInteger bytesWritten,long long totalBytesWritten,long long totalBytesExpectedToWrite)
         {
             
             NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
             [self updateProgressForProgressBar:(float)totalBytesWritten/totalBytesExpectedToWrite];
         }];
        
        [operation start];
        
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
    
    
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
//    NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
//    
//    NSDictionary *postData = [[NSUserDefaults standardUserDefaults] objectForKey:@"postData"];
//    NSLog(@"videoURL is %@", videoURL);
////    NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
////    NSLog(@"videoData is %@", videoData);
//    NSString *description = [postData objectForKey:@"description"];
//    NSDictionary *parameters = @{@"type":@"video", @"title":@"video", @"description":description};
//    
//    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
//    NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
//    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *authValue = [NSString stringWithFormat:@"Basic %@",
//                           [authData base64EncodedStringWithOptions:0]];
//    NSString *boundary = BOUNDARY_STRING;
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
//    [requestSerializer setValue:contentType forHTTPHeaderField:@"Content-Type"];
//    
//    
//    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
//    [responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"application/json"]];
//    
//    [manager setRequestSerializer:requestSerializer];
//    [manager setResponseSerializer:responseSerializer];
//    
//    NSLog(@"HEADERS: %@", manager.requestSerializer.HTTPRequestHeaders);
//    
//    [manager POST:@"http://rain-app.com/api/v1/media"
//       parameters:parameters
//constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//    {
//        [formData appendPartWithFileURL:videoURL
//                                   name:@"video"
//                               fileName:@"croppedVideo.mp4"
//                               mimeType:@"video/quicktime"
//                                  error:nil];
//    }
//          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              NSLog(@"Success: %@", responseObject);
//              if ([[responseObject valueForKey:@"status"] isEqualToString:@"OK"])
//              {
//                  NSLog(@"post succesfully created!");
//                  
//                  NSDictionary *postData = [responseObject objectForKey:@"data"];
//                  
//                  [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"createdPostData"];
//                  [[NSNotificationCenter defaultCenter] postNotificationName:@"PostSuccess"
//                                                                      object:self];
//              }
//              else if ([[responseObject valueForKey:@"status"] isEqualToString:@"ERROR"])
//              {
//                  NSLog(@"Error is: \n%@", [responseObject objectForKey:@"message"]);
//                  [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"message"]
//                                                            forKey:@"error"];
//                  
//                  [[NSNotificationCenter defaultCenter] postNotificationName:@"PostFailed"
//                                                                      object:self];
//              }
//              else
//              {
//                  NSLog(@"unknown status error for post");
//                  [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
//                                                                      object:self];
//              }
//              
//          }
//          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//              NSLog(@"Error: %@", error);
//              NSLog(@"responce object is %@\n%@\n%@", [operation responseString], [operation responseData], [operation response]);
//          }];
}

- (void)sendComment:(NSString *)comment forPostID:(NSString *)postID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"comment" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media/%@/comments", apiAddress, postID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        [params appendFormat:@"text=%@", [comment stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getCommentsForPostID:(NSString *)postID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"get Comments");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"getComments" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media/%@", apiAddress, postID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"GET";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
//        NSLog(@"http body = \n%@", [request HTTPBody]);
//        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)deletePost:(NSString *)deleteLink
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"Delete Post!");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"deletePost" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:deleteLink];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"DELETE";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)deleteComment:(NSString *)deleteLink
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"Delete Comment!");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"deleteComment" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:deleteLink];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"DELETE";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)deleteMessage:(NSString *)messageID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"Delete Message!");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"deleteMessage" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/message/%@", apiAddress, messageID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"DELETE";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)deleteConversation:(NSString *)personEmail
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"Delete Conversation!");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"deleteConversation" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/dialog/%@", apiAddress, personEmail]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"DELETE";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)deleteAccount
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSLog(@"Delete Account!");
        
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        [query setObject:@"deleteAccount" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/users", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"DELETE";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)recoveryPasswordForEmail:(NSString *)email
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"passwordRecovery" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/recovery", apiAddress, email]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        [params appendFormat:@"email=%@", [email stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)changePasswordTo:(NSString *)newPassword andConfirmation:(NSString *)confirmation
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        NSString *queryName = [NSString stringWithFormat:@"changePasswordTo%@", newPassword];
        
        [query setObject:queryName forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user/%@/change_password", apiAddress, userEmail]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        [params appendFormat:@"new_password=%@", [newPassword stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [params appendFormat:@"&confirm_password=%@", [confirmation stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)likePostWithID:(NSString *)postID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        NSString *queryName = @"like";
        
        [query setObject:queryName forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media/%@/like", apiAddress, postID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)repostPostWithID:(NSString *)postID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        
        NSString *queryName = @"repost";
        
        [query setObject:queryName forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/media/%@/repost", apiAddress, postID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSLog(@"http body = \n%@", [request HTTPBody]);
        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)getDialogs
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"dialogs" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/dialogs", apiAddress]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"])
        {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendMessage" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *urlString = [[NSString stringWithFormat:@"%@/api/v1/messages?text=%@&receiver_id=%d",
                                apiAddress,
                                [messageText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                userID]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"urlstring is %@", urlString);
        
        NSURL *requestURL = [NSURL URLWithString:urlString];
        
        NSLog(@"url is %@", requestURL);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"POST"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"])
        {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentFailed" object:self];
    }
}

- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID andPhotoAttachment:(NSData *)imageData
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendMessage" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSString *urlString = [[NSString stringWithFormat:@"%@/api/v1/messages?text=%@&receiver_id=%d",
                                apiAddress,
                                [messageText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                userID]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *requestURL = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        // Add the image to the request body
        
        // If we need to save this photo to camera roll
//        UIImage *image = [UIImage imageWithData:imageData];
//        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=image.jpeg%@", @"attach[]", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:imageData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
                NSLog(@"params are:\n%@", [[NSString alloc] initWithData:params encoding:NSASCIIStringEncoding]);
        
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        //        NSLog(@"http body = \n%@", [request HTTPBody]);
        //        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentFailed" object:self];
    }
}

- (void)sendMessage:(NSString *)messageText toUserWithID:(int)userID andVideoAttachment:(NSURL *)videoURL
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableData *params = [NSMutableData data];
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"sendMessage" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        NSString *urlString = [[NSString stringWithFormat:@"%@/api/v1/messages?text=%@&receiver_id=%d",
                                apiAddress,
                                [messageText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                userID]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *requestURL = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        
        NSString *boundary = BOUNDARY_STRING;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        // Add the image to the request body
        
        NSData *videoData = [[NSData alloc] initWithContentsOfURL:videoURL];
        
        [params appendData:[[NSString stringWithFormat:@"--%@%@", boundary, @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"videoCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@\r\n", @"video"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [params appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=video.mp4%@", @"attach[]", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"Content-Type: video/quicktime"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"%@%@", @"\r\n", @"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:videoData];
        [params appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [params appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"params are:\n%@", [[NSString alloc] initWithData:params encoding:NSASCIIStringEncoding]);
        
        [request setHTTPBody:params];
        
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                               [authData base64EncodedStringWithOptions:0]];
        [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
        [request addValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        //        NSLog(@"http body = \n%@", [request HTTPBody]);
        //        NSLog(@"message = \n%@", params);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentFailed" object:self];
    }
}

- (void)getConversationWithUserID:(NSString *)userID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"conversation" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/message/%@", apiAddress, userID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"GET"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)confirmFollowRequestWithID:(NSString *)notificationID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"confirmFollowRequest" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/follow/%@", apiAddress, notificationID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"PUT"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)declineFollowRequestWithID:(NSString *)notificationID
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"declineFollowRequest" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/follower/%@", apiAddress, notificationID]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"DELETE"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendReportForPostWithID:(NSString *)postID andText:(NSData *)commentTextData
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"reportPost" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *commentText = [[NSString alloc] initWithData:commentTextData encoding:NSUTF8StringEncoding];
        NSString *fullURLString = [NSString stringWithFormat:@"%@/api/v1/media/%@/report/%@", apiAddress, postID, commentText];
        
        NSURL *requestURL = [NSURL URLWithString:[fullURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"POST"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"]) {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)sendReportForUserWithEmail:(NSString *)email andText:(NSData *)commentTextData
{
    if ([[Reachability reachabilityWithHostName:@"rain-app.com"] isReachable])
    {
        NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
        [query setObject:@"reportUser" forKey:@"queryType"];
        NSMutableData *receivedData = [[NSMutableData alloc] init];
        [query setObject:receivedData forKey:@"receivedData"];
        
        NSString *commentText = [[NSString alloc] initWithData:commentTextData encoding:NSUTF8StringEncoding];
        NSString *fullURLString = [NSString stringWithFormat:@"%@/api/v1/user/%@/report/%@", apiAddress, email, commentText];
        
        NSURL *requestURL = [NSURL URLWithString:[fullURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        [request setHTTPMethod:@"POST"];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
            
            NSString *userEmail = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSString *userPassword = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", userEmail, userPassword];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@",
                                   [authData base64EncodedStringWithOptions:0]];
            [request addValue:authValue forHTTPHeaderField:@"Authorization"];
        }
        
        NSLog(@"request:\n%@", request);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"])
        {
            [request addValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"pushToken"] forHTTPHeaderField:@"push"];
        }
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [query setObject:connection forKey:@"connection"];
        [connections addObject:query];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NetworkError" object:self];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    // Close connection
    [connection cancel];
    
    // Remove query
    [connections removeObject:[self findQueryWithConnection:connection]];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    
    if ([urlResponse statusCode] == 200) {
        NSLog(@"StatusCode - OK!");
    }
    
    else if ([urlResponse statusCode] == 201)
    {
        NSLog(@"StatusCode - Created!");
    }
    
    else
    {
        NSLog(@"Something is not ok!\n\nStatus Code = %ld", (long)[urlResponse statusCode]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError" object:self];
    }
    
    
    NSMutableDictionary *query = [self findQueryWithConnection:connection];
    
    // Clear buffer
    [(NSMutableData*)[query objectForKey:@"receivedData"] setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSMutableDictionary *query = [self findQueryWithConnection:connection];
    
    // Save received data to buffer
    NSMutableData *buffer = [query objectForKey:@"receivedData"];
    [buffer appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Remove query
    for (NSDictionary *con in connections) {
        [[con objectForKey:@"connection"] cancel];
        NSLog(@"dictionary is %@", con);
    }
    [connections removeObject:[self findQueryWithConnection:connection]];
    [connections removeAllObjects];
    NSLog(@"error!\n error is %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError" object:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSMutableDictionary *query = [self findQueryWithConnection:connection];
    NSLog(@"query - %@", query);
    
    // Get result
    NSString *responceString = [[NSString alloc] initWithData:[query objectForKey:@"receivedData"] encoding:NSUTF8StringEncoding];
    
    NSLog(@"response string - %@", responceString);
    
    NSMutableDictionary *responce = [[NSMutableDictionary alloc] init];

    responce = [responceString JSONValue];
    
    NSLog(@"response - %@", responce);
    
    if (responce == nil) {
        
        NSLog(@"NULL!!!");
    }
    
    if ([responce isKindOfClass:[NSArray class]]) {
        if ([responce count] == 0) {
            NSLog(@"responce is empty array");
        }
    } else {
        if([[responce valueForKey:@"status"] isEqualToString:@"error"] == YES)
        {
            [connections removeObject:query];
            
            // If all queries are finished - notify
            if([connections count] == 0)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEnd" object:self];
            }
            return;
        }
    }
    
    
    if ([[query objectForKey:@"queryType"] isEqualToString:@"signup"]) {
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"])
        {
            NSLog(@"user successfully created!");
            NSLog(@"this user is %@", [responce valueForKey:@"user"]);
            [[NSUserDefaults standardUserDefaults] setObject:[responce valueForKey:@"user"]
                                                      forKey:@"userData"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SignupSuccess"
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Registration failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"]
                                                      forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SignupFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"countries"]){
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"Countries successfully downloaded");
            NSArray *countriesArray = [[responce objectForKey:@"data"] objectForKey:@"countries"];
            [[NSUserDefaults standardUserDefaults] setObject:countriesArray forKey:@"countriesArray"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CountriesSuccess"
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Countries failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CountriesFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"cities"]){
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSLog(@"Cities successfully downloaded");
            NSArray *citiesArray = [[responce objectForKey:@"data"] objectForKey:@"cities"];
            
            [[NSUserDefaults standardUserDefaults] setObject:citiesArray forKey:@"citiesArray"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CitiesSuccess"
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Cities failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CitiesFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
        }
    }
    
    else if ([[query objectForKey:@"queryType"] hasPrefix:@"citiesForID"]){
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"Cities successfully downloaded");
            NSArray *citiesArray = [[responce objectForKey:@"data"] objectForKey:@"cities"];
            [[NSUserDefaults standardUserDefaults] setObject:citiesArray forKey:@"citiesArray"];
            
            // get userID from queryType
            NSArray *stringsArray = [[query objectForKey:@"queryType"] componentsSeparatedByString:@"ForID"];
            NSString *userID = [stringsArray lastObject];
            NSString *notificationName = [NSString stringWithFormat:@"CitiesSuccess%@", userID];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Cities failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            
            NSArray *stringsArray = [[query objectForKey:@"queryType"] componentsSeparatedByString:@"ForID"];
            NSString *userID = [stringsArray lastObject];
            NSString *notificationName = [NSString stringWithFormat:@"CitiesFailed%@", userID];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
        }
    }
    
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"fillProfile"] ||
             [[query objectForKey:@"queryType"] isEqualToString:@"editProfile"]){
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"])
        {
            NSLog(@"profile successfully filled!");
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"user"] forKey:@"filledProfileData"];
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"user"] forKey:@"currentUser"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileFilledSuccess"
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Registration failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"]
                                                      forKey:@"errorMessage"];
            
            if ([[query objectForKey:@"queryType"] isEqualToString:@"fillProfile"])
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"filledProfileData"];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileFilledFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for edit profile");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
            if ([[query objectForKey:@"queryType"] isEqualToString:@"fillProfile"])
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"filledProfileData"];
            }
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"sendVideo"]){
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"])
        {
            NSLog(@"post succesfully created!");
            
            NSDictionary *postData = [responce objectForKey:@"data"];
            
            [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"createdPostData"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PostSuccess"
                                                                object:self];
        }
        else if ([[responce valueForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"Registration failed!");
            NSLog(@"Error is: \n%@", [responce objectForKey:@"message"]);
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"]
                                                      forKey:@"errorMessage"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PostFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for edit profile");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"signin"])
    {
        if ([[responce valueForKey:@"status"] isEqualToString:@"OK"]) {
            NSDictionary *user = [[responce objectForKey:@"data"] objectForKey:@"user"];
            NSMutableDictionary *currentUser = [[NSMutableDictionary alloc] init];
            BOOL hasNull = NO;
            NSLog(@"all values:\n%@", [user allValues]);
            for (id currentValue in [user allValues]) {
                if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                    hasNull = YES;
                }
            }
            currentUser = [user mutableCopy];
            if (hasNull) {
                
                NSDictionary*UserData= [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
                [currentUser setObject:@"" forKey:@"birthday"];
                
                [currentUser setObject:[UserData objectForKey:@"first_name"] forKey:@"first_name"];
                
                [currentUser setObject:[UserData objectForKey:@"last_name"] forKey:@"last_name"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:currentUser forKey:@"currentUser"];
            NSLog(@"current user: %@", currentUser);
            
            // register Device and get Token
            if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
            {
                // use registerUserNotificationSettings
                
                UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
                
                [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            } else {
                // use registerForRemoteNotifications
                [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
            }

            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SigninSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SigninFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"user"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSDictionary *user = [[responce objectForKey:@"data"] objectForKey:@"user"];
            
            NSMutableDictionary *currentUser = [[NSMutableDictionary alloc] init];
            BOOL hasNull = NO;
            NSLog(@"all values:\n%@", [user allValues]);
            for (id currentValue in [user allValues]) {
                if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                    hasNull = YES;
                }
            }
            currentUser = [user mutableCopy];
            if (hasNull) {
                [currentUser setObject:@"" forKey:@"birthday"];
                [currentUser setObject:@"" forKey:@"first_name"];
                [currentUser setObject:@"" forKey:@"last_name"];
            }
            
            if ([[[responce objectForKey:@"data"] objectForKey:@"posts"] isKindOfClass:[NSArray class]]) {
                NSArray *posts = [[responce objectForKey:@"data"] objectForKey:@"posts"];
                if (posts.count)
                {
                    NSMutableArray *userPosts = [[NSMutableArray alloc] init];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
                    for (NSDictionary *post in posts) {
                        NSMutableDictionary *tempPost = [post mutableCopy];
                        NSDate *createdAt = [dateFormatter dateFromString:[post objectForKey:@"created_at"]];
                        
                        if (createdAt == NULL || createdAt == (id)[NSNull null]) {
                            [tempPost setObject:@"" forKey:@"created_at"];
                        } else {
                            [tempPost setObject:createdAt forKey:@"created_at"];
                        }
                        [userPosts addObject:tempPost];
                    }
                    
                    posts = userPosts;
                    
                    [currentUser setObject:posts forKey:@"posts"];
                }
            }
            
            NSLog(@"current user(founded): %@", currentUser);
            
            [[NSUserDefaults standardUserDefaults] setObject:currentUser forKey:@"foundedUser"];
            NSLog(@"user successfully found: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"foundedUser"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"foundedUser"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"foundedUser"];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"userForRecovery"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSDictionary *user = [responce objectForKey:@"data"];
            
            NSMutableDictionary *currentUser = [[NSMutableDictionary alloc] init];
            BOOL hasNull = NO;
            NSLog(@"all values:\n%@", [user allValues]);
            for (id currentValue in [user allValues]) {
                if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                    hasNull = YES;
                }
            }
            currentUser = [user mutableCopy];
            if (hasNull) {
                [currentUser setObject:@"" forKey:@"birthday"];
                [currentUser setObject:@"" forKey:@"first_name"];
                [currentUser setObject:@"" forKey:@"last_name"];
            }
            
            NSLog(@"current user(founded): %@", currentUser);
            
            [[NSUserDefaults standardUserDefaults] setObject:currentUser forKey:@"userForRecovery"];
            NSLog(@"user successfully found: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"userForRecovery"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSObject *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userForRecovery"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userForRecovery"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"searchFriends"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSArray *usersArray = [[responce objectForKey:@"data"] objectForKey:@"users"];
            NSMutableArray *tempUsersArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *currentUser in usersArray) {
                
                NSMutableDictionary *userTempDict = [[NSMutableDictionary alloc] init];
                BOOL hasNull = NO;
                
                for (id currentValue in [currentUser allValues]) {
                    if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                        hasNull = YES;
                    }
                }
                userTempDict = [currentUser mutableCopy];
                if (hasNull) {
                    [userTempDict setObject:@"" forKey:@"firstname"];
                    [userTempDict setObject:@"" forKey:@"lastname"];
                }
                
                [tempUsersArray addObject:userTempDict];
            }
            
            usersArray = [tempUsersArray copy];

            
            
            [[NSUserDefaults standardUserDefaults] setObject:usersArray forKey:@"foundFriends"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchFriendsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"foundFriends"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchFriendsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"foundFriends"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"filter"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSArray *usersArray = [[responce objectForKey:@"data"] objectForKey:@"users"];
            NSMutableArray *tempUsersArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *currentUser in usersArray) {
                
                NSMutableDictionary *userTempDict = [[NSMutableDictionary alloc] init];
                BOOL hasNull = NO;
                
                for (id currentValue in [currentUser allValues]) {
                    if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                        hasNull = YES;
                    }
                }
                userTempDict = [currentUser mutableCopy];
                if (hasNull) {
                    [userTempDict setObject:@"" forKey:@"firstname"];
                    [userTempDict setObject:@"" forKey:@"lastname"];
                }
                
                [tempUsersArray addObject:userTempDict];
            }
            
            usersArray = [tempUsersArray copy];
            
            [[NSUserDefaults standardUserDefaults] setObject:usersArray forKey:@"filteredUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FilterFriendsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"filteredUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FilterFriendsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"filteredUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"news"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSLog(@"news success");
            [[NSUserDefaults standardUserDefaults] setObject:[[responce objectForKey:@"data"] objectForKey:@"news"] forKey:@"news"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSLog(@"news failed");
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"news"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"news"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"notifications"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"data"] forKey:@"notifications"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"notifications"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"notifications"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"profileAccess"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"ok"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileAccessSuccess"
                                                                object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TurnPushNotificationsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileAccessFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for profile access");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"turnPushNotifications"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"ok"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TurnPushNotificationsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TurnPushNotificationsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for profile access");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"getSettings"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSLog(@"stat set ok");
            
            NSMutableDictionary *settings = [[responce objectForKey:@"data"] mutableCopy];
            if ([settings objectForKey:@"notifications"] == NULL ||
                [settings objectForKey:@"notifications"] == (id)[NSNull null])
            {
                NSLog(@"notif null");
                [settings setObject:[NSNumber numberWithInt:0] forKey:@"notifications"];
            }
            
            if ([settings objectForKey:@"hide_profile"] == NULL ||
                [settings objectForKey:@"hide_profile"] == (id)[NSNull null])
            {
                [settings setObject:[NSNumber numberWithInt:0] forKey:@"hide_profile"];
            }
            
            
            [[NSUserDefaults standardUserDefaults] setObject:settings forKey:@"userSettings"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for getting settings");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"allUsers"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSArray *allUsers = [responce objectForKey:@"users"];
            NSMutableArray *users = [[NSMutableArray alloc] init];
            
            for (NSDictionary *currentUser in allUsers) {

                NSMutableDictionary *userTempDict = [[NSMutableDictionary alloc] init];
                BOOL hasNull = NO;

                for (id currentValue in [currentUser allValues]) {
                    if (currentValue == NULL || currentValue == (id)[NSNull null]) {
                        hasNull = YES;
                    }
                }
                userTempDict = [currentUser mutableCopy];
                if (hasNull) {
                    [userTempDict setObject:@"" forKey:@"birthday"];
                    [userTempDict setObject:@"" forKey:@"first_name"];
                    [userTempDict setObject:@"" forKey:@"last_name"];
                }
                
                [users addObject:userTempDict];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:users forKey:@"allUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AllUsersSuccess"
                                                                object:self];
            NSLog(@"all users (success): %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"allUsers"]);
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"allUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AllUsersFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error for All Users");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"allUsers"];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"followers"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSArray *followers = [[responce objectForKey:@"data"] objectForKey:@"users"];
            [[NSUserDefaults standardUserDefaults] setObject:followers forKey:@"followers"];
            NSLog(@"Followers success");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowersSuccess"
                                                                object:self];
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"followers"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowersFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"followings"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSArray *followings = [[responce objectForKey:@"data"] objectForKey:@"users"];
            [[NSUserDefaults standardUserDefaults] setObject:followings forKey:@"followings"];
            NSLog(@"Followings success");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowingsSuccess"
                                                                object:self];
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"followings"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowingsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"passwordRecovery"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RecoverySuccess"
                                                                object:self];
            NSLog(@"password recovered");
    
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            NSString *message = [responce objectForKey:@"message"];
            [[NSUserDefaults standardUserDefaults] setObject:message forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RecoveryFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] hasPrefix:@"changePasswordTo"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSArray *stringsArray = [[query objectForKey:@"queryType"] componentsSeparatedByString:@"PasswordTo"];
            NSString *newPassword = [stringsArray lastObject];
            NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
            NSMutableDictionary *newToken = [[NSMutableDictionary alloc] init];
            [newToken setObject:email forKey:@"email"];
            [newToken setObject:newPassword forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"token"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangePasswordSuccess"
                                                                object:self];
            NSLog(@"Password successfully changed");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangePasswordFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"comment"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentSuccess"
                                                                object:self];
            NSLog(@"Comment successfully added");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"deletePost"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeletePostSuccess"
                                                                object:self];
            NSLog(@"Post successfully deleted");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeletePostFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"deleteComment"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteCommentSuccess"
                                                                object:self];
            NSLog(@"Comment successfully deleted");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteCommentFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"deleteMessage"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteMessageSuccess"
                                                                object:self];
            NSLog(@"Message successfully deleted");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteMessageFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"deleteConversation"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteConversationSuccess"
                                                                object:self];
            NSLog(@"Conversation successfully deleted");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteConversationFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"deleteAccount"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteAccountSuccess"
                                                                object:self];
            NSLog(@"Account successfully deleted");
            
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteAccountFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"getComments"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSLog(@"Comments found");
            NSLog(@"comments responce = \n%@", responce);
            
            NSArray *comments;
            
            if ([[[responce objectForKey:@"data"] objectForKey:@"comments"] isKindOfClass:[NSArray class]]) {
                comments = [[responce objectForKey:@"data"] objectForKey:@"comments"];
            } else {
                NSLog(@"boolean");
                comments = [NSArray array];
            }
            
            NSString *repost = [[[responce objectForKey:@"data"] objectForKey:@"repost"] stringValue];
            
            NSMutableDictionary *mutContent = [[[responce objectForKey:@"data"] objectForKey:@"content"] mutableCopy];
            [mutContent setObject:repost forKey:@"repost"];
            [mutContent setObject:[[responce objectForKey:@"data"] objectForKey:@"marks"] forKey:@"marks"];
            
            
            NSDictionary *content = [mutContent copy];
            mutContent = nil;
            
            // In original in was commented
            // Раскомметировано для того, чтобы при загрузки страницы с информацией о посте
            // отображалась информация о кол-ве лайков, кол-ве комментов и т.д.
            [[NSUserDefaults standardUserDefaults] setObject:content forKey:@"currentContent"];
            //
            
            [[NSUserDefaults standardUserDefaults] setObject:comments forKey:@"comments"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"comments"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentContent"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CommentsFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"like"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            NSLog(@"likes string from responce: \n%@", [[responce objectForKey:@"likes"] stringValue]);
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[[responce objectForKey:@"likes"] stringValue] forKey:@"likes"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LikeSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LikeFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"repost"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[[[responce objectForKey:@"data"] objectForKey:@"count_reposts"] stringValue] forKey:@"reposts"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RepostSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RepostFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"feed"])
    {
        NSLog(@"feed");
        
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"data"] forKey:@"feed"];
            NSLog(@"%@", [responce objectForKey:@"data"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"follow"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"responce is %@", responce);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
        NSLog(@"responce is %@", responce);
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"unfollow"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"responce is %@", responce);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FollowFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
        NSLog(@"responce is %@", responce);
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"dialogs"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"responce is %@", responce);
            
            NSArray *dialogs = [[responce objectForKey:@"data"] objectForKey:@"dialogs"];
            [[NSUserDefaults standardUserDefaults] setObject:dialogs forKey:@"dialogs"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DialogsSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DialogsFailed"
                                                                object:self];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"dialogs"];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"dialogs"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
        NSLog(@"responce is %@", responce);
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"sendMessage"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"confirmFollowRequest"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"confirmFollowSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"confirmFollowFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"declineFollowRequest"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"declineFollowSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"declineFollowFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"reportPost"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReportPostSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"messages"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReportPostFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"reportUser"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReportProfileSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"messages"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReportProfileFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
    }
    
    else if ([[query objectForKey:@"queryType"] isEqualToString:@"conversation"])
    {
        if ([[responce objectForKey:@"status"] isEqualToString:@"OK"]) {
            NSLog(@"responce is %@", responce);
            
            NSArray *conversation = [[responce objectForKey:@"data"] objectForKey:@"messages"];
            
            NSMutableArray *messagesTempArray = [conversation mutableCopy];
            for (NSMutableDictionary *message in messagesTempArray) {
                NSString *createdAt = [[message objectForKey:@"message"] objectForKey:@"created_at"];
                [message setObject:createdAt forKey:@"created_at"];
            }
            
            conversation = [messagesTempArray copy];
            
            [[NSUserDefaults standardUserDefaults] setObject:conversation forKey:@"conversation"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ConversationSuccess"
                                                                object:self];
        }
        else if ([[responce objectForKey:@"status"] isEqualToString:@"ERROR"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responce objectForKey:@"message"] forKey:@"errorMessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ConversationFailed"
                                                                object:self];
        }
        else
        {
            NSLog(@"unknown status error");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEndWithError"
                                                                object:self];
        }
        NSLog(@"responce is %@", responce);
    }
    
    
    
//    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:@"lastUpdate"];
//    [[Model sharedInstance] saveData];
//    
//    NSLog(@"lastUpdate - %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"]);
    
    // Remove query
    [connections removeObject:query];
    
    // If all queries are finished - notify
    if([connections count] == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerWorkerDidEnd"
                                                            object:self];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Helper Methods

- (void)updateProgressForProgressBar:(float)progress
{
    NSLog(@"method triggered");
    id <APProgressBarDelegate> progressBarDelegate = [self delegate];
    if (progressBarDelegate) {
        NSLog(@"progress is: %f", progress);
        [progressBarDelegate updateProgress:progress];
    } else {
        NSLog(@"no delegate");
    }
}


@end
