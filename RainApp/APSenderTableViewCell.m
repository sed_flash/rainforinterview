//
//  APSenderTableViewCell.m
//  Rain
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "APSenderTableViewCell.h"

@interface APSenderTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *senderBubble;
@property (weak, nonatomic) IBOutlet UILabel *senderMessage;

@end

@implementation APSenderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *tempImage = [_senderBubble.image resizableImageWithCapInsets:UIEdgeInsetsMake(7, 8, 20, 27) resizingMode:UIImageResizingModeStretch];
    _senderBubble.image = tempImage;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setters

- (void)setMessage:(NSString *)message {
    _senderMessage.text = message;
}

- (void)setAvatarData:(NSData *)avatarData {
    [UIImageView apa_useMaskToImageView:_avatarImage
                               forImage:[UIImage imageWithData:avatarData]];
}

@end
