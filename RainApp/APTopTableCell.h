//
//  APTopTableCell.h
//  Rain
//
//  Created by EgorMac on 02/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "UIImageView+AFNetworking.h"

@interface APTopTableCell : UITableViewCell
{
   
    APServerAPI *API;
}

@property (nonatomic, strong) NSArray *foundUsersID;
@property (nonatomic, strong) NSDictionary *foundUser;

@property (nonatomic, assign) BOOL isCellForFiltersResults;

@property (nonatomic, strong) NSDictionary *user;
@property (nonatomic, weak) IBOutlet UIImageView *profilePhotoImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *locationLabel;
@property (nonatomic, weak) IBOutlet UILabel *rateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *rateImageView;

- (void)setLocation:(NSDictionary *)currentUser;

@end
