//
//  APViewController.m
//  RainApp
//
//  Created by EgorMac on 15/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APLogOrRegVC.h"
#import "APTabBarController.h"




@interface APLogOrRegVC ()

@end

@implementation APLogOrRegVC



static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;




- (void)signinSuccess
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:@"userData"]!=nil)
    {
        NSDictionary *currentUser=[defaults objectForKey:@"currentUser"];
        
        NSDictionary *userData=[defaults objectForKey:@"userData"];
        
        NSString *val=[userData objectForKey:@"avatar_small"];
        
        [defaults setObject:val forKey:@"avatar_small"];
        
        [defaults synchronize];

        NSLog(@"userData in Login VC - \n%@", [defaults objectForKey:@"currentUser"]);
        
        BOOL currentUserHasNilFirstname = ([[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] == nil);
        BOOL currentUserHasEmptyFirstname = ([[[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] isEqualToString:@""]);
        
        currentUserHasEmptyFirstname = ([defaults objectForKey:@"nikname"]);
        
        if (currentUserHasEmptyFirstname)
        {
            [self performSegueWithIdentifier:@"signUp2FromLogOrReg" sender:self];
            //[self performSegueWithIdentifier:@"logToStep2" sender:self];
        }
        else
        {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"])
            {
                NSLog(@"User already logged in. Redirecting to news feed");
              
                [[self navigationController] popViewControllerAnimated:YES];
                [self performSegueWithIdentifier:@"showTabBarLogin" sender:self];
            }
        }
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    [[[self navigationController] navigationBar] setTranslucent:YES];
    [[[self navigationController] view] setBackgroundColor:[UIColor clearColor]];
    [[[self navigationController] navigationBar] setTintColor:[UIColor colorWithRed:(251.0/255.0)
                                                                              green:(195.0/255.0)
                                                                               blue:(10.0/255.0)
                                                                              alpha:1.0]];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
}

- (void)signinFailed
{
    [API signup];
   // [self showAlertWithIdentifier:serverAlert];
}



- (void)filledSuccess
{
    [[self navigationController] popViewControllerAnimated:YES];
     [self performSegueWithIdentifier:@"showTabBarLogin" sender:self];
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location , friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (error)
             {
                 NSLog(@"resultis:%@",result);
                 
             }
             else
             {
                 NSDictionary *dict_user=result;
                 
                 NSString *email = [dict_user valueForKey:@"email"];
                 
                 NSString *nickname = [dict_user valueForKey:@"first_name"];
                 NSString *lastName = [dict_user valueForKey:@"last_name"];
                 NSString *password = [dict_user valueForKey:@"id"];
                 
                 NSMutableDictionary *user = [[NSMutableDictionary alloc] init];
                 if(!email)
                 {
                     email=[nickname stringByAppendingString:@"@gmail.com"];
                      [user setObject:email forKey:@"email"];
                 }
                 else
                 {
                     [user setObject:email forKey:@"email"];
                 }
      
                 [user setObject:nickname forKey:@"first_name"];
                 [user setObject:lastName forKey:@"last_name"];
                 [user setObject:nickname forKey:@"nickname"];
                 [user setObject:password forKey:@"password"];
                 [user setObject:[[[dict_user objectForKey:@"picture"]objectForKey:@"data"] valueForKeyPath:@"url"] forKey:@"avatar_small"];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"userData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                [API signinWithEmail:email andPassword:password];
             }
         }];
    }
}

- (void)signupSuccess
{
    
    NSDictionary * dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];

    
    NSString *firstName = [dict valueForKey:@"first_name"];
    NSString *lastName = [dict valueForKey:@"last_first"];
    

    NSString *birthday = [dict valueForKey:@"birthday"];
    
    NSMutableDictionary *filledProfile = [[NSMutableDictionary alloc] init];
    [filledProfile setObject:firstName forKey:@"firstName"];
    [filledProfile setObject:lastName forKey:@"lastName"];
    
    [filledProfile setObject:[NSNumber numberWithInt:dict] forKey:@"gender"];
    if(birthday.length>0)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MM yyyy"];
        NSDate *birthDate = [dateFormatter dateFromString:birthday];
        NSLog(@"birthDate is %@", birthDate);
    
        NSDateFormatter *enDateFormatter = [[NSDateFormatter alloc] init];
        [enDateFormatter setDateFormat:@"dd MMMM yyyy"];
        [enDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
        birthday = [enDateFormatter stringFromDate:birthDate];
        [filledProfile setObject:birthday forKey:@"birthday"];
        
        [[NSUserDefaults standardUserDefaults] setObject:filledProfile forKey:@"filledProfileData"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        [API fillProfile];
    }
    else
    {
        
        [[NSUserDefaults standardUserDefaults] setObject:filledProfile forKey:@"filledProfileData"];
        [[NSUserDefaults standardUserDefaults] synchronize];

         [self performSegueWithIdentifier:@"goToSignUpStep2" sender:self];
    }
    
}

- (void)signupFailed
{
    [self showAlertWithIdentifierFacebook:serverAlert];
    
   // Email must be unique
}

- (void)networkError
{
    [self showAlertWithIdentifier:networkAlert];
}



- (void)showAlertWithIdentifierFacebook:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = @"";
        
        for (NSString *currentMessage in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"])
        {
            errorMessage = [errorMessage stringByAppendingFormat:@"\n%@", currentMessage];
        }
        if([errorMessage isEqualToString:@"\nEmail must be unique"])
        {
            
             errorMessage =@"Такой пользователь уже существует.Авторизируйтесь через email.";
            
        }
        else
        {
            errorMessage =@"Такой пользователь уже существует.Авторизируйтесь через email.";
        }
        
        
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}



- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = @"";
        
        for (NSString *currentMessage in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"])
        {
            errorMessage = [errorMessage stringByAppendingFormat:@"\n%@", currentMessage];
        }
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
     API = [[APServerAPI alloc] init];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signinSuccess)
                                                 name:@"SigninSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signinFailed)
                                                 name:@"SigninFailed"
                                               object:API];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signupSuccess)
                                                 name:@"SignupSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signupFailed)
                                                 name:@"SignupFailed"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledSuccess)
                                                 name:@"ProfileFilledSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledFailed)
                                                 name:@"ProfileFilledFailed"
                                               object:API];

//    _loginWithFacebook.readPermissions =
//    @[@"public_profile", @"email", @"user_friends",@"user_birthday"];
//    _loginWithFacebook.delegate = self;
    
	// Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"defaults userData - %@\n defaults filledProfileData - %@\n defaults currentUser - %@", [defaults objectForKey:@"userData"],
          [defaults objectForKey:@"filledProfileData"], [defaults objectForKey:@"currentUser"]);
    
    // check if user is already logged in
    if([defaults objectForKey:@"userData"]!=nil)
    {
        NSLog(@"userData in LogOrRegVC- \n%@", [defaults objectForKey:@"filledProfileData"]);
        
        BOOL filledProfileHasNilFirstname = ([[defaults objectForKey:@"filledProfileData"] objectForKey:@"first_name"] == nil);
        BOOL currentUserHasNilFirstname = ([[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] == nil);
        BOOL filledProfileHasEmptyFirstname = ([[[defaults objectForKey:@"filledProfileData"] objectForKey:@"first_name"] isEqualToString:@""]);
        BOOL currentUserHasEmptyFirstname = ([[[defaults objectForKey:@"currentUser"] objectForKey:@"first_name"] isEqualToString:@""]);
        
        currentUserHasEmptyFirstname = ([defaults objectForKey:@"nikname"]);
        
        if (filledProfileHasNilFirstname) {
            NSLog(@"filled profile has nil firstname");
        } else {
            NSLog(@"filled profile has not nil firstname");
        }
        
        if (filledProfileHasEmptyFirstname) {
            NSLog(@"filled profile has empty firstname");
        } else {
            NSLog(@"filled profile has not empty firstname");
        }
        
        if (currentUserHasNilFirstname) {
            NSLog(@"current user has nil firstname");
        } else {
            NSLog(@"current user has not nil firstname");
        }
        
        if (currentUserHasEmptyFirstname) {
            NSLog(@"current user has empty firstname");
        } else {
            NSLog(@"current user has not empty firstname");
        }
        
        if (((filledProfileHasNilFirstname || currentUserHasNilFirstname) ||
            (filledProfileHasEmptyFirstname || currentUserHasEmptyFirstname)) &&
            (currentUserHasEmptyFirstname || currentUserHasNilFirstname))
        {
            //[self performSegueWithIdentifier:@"signUp2FromLogOrReg" sender:self];
        }
        else {
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"token"]) {
                NSLog(@"User already logged in. Redirecting to tab bar controller");
                APTabBarController *tabBarController = (APTabBarController *)[[self storyboard] instantiateViewControllerWithIdentifier:@"TabBarViewController"];

                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"launchOptions"])
                {
                    [tabBarController setSelectedViewController:tabBarController.viewControllers[0]];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"launchOptions"];
                }
                else
                {
                    [tabBarController setSelectedViewController:tabBarController.viewControllers[0]];
                }
                
                [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
                [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
                for (UITabBarItem *item in [[UITabBar appearance] items]) {
                    [item setImage:[[item image] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
                }
                
                [[self navigationController] pushViewController:tabBarController animated:NO];
                [[self navigationController] setNavigationBarHidden:YES animated:NO];
            }
        }
    }
    else
    {
        NSLog(@"User need to sign in.");
    }
    [self setupUIElements];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)setupUIElements
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    
    [_loginButton setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
    [_loginButton setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateHighlighted];
    [[_loginButton layer] setCornerRadius:3.0f];
    
    [_signupButton setTitle:NSLocalizedString(@"signupWithEmail", nil) forState:UIControlStateNormal];
    [_signupButton setTitle:NSLocalizedString(@"signupWithEmail", nil) forState:UIControlStateHighlighted];
    [[_signupButton titleLabel] setTextAlignment:NSTextAlignmentCenter];
    [[_signupButton layer] setCornerRadius:3.0f];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark - Facebook

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_emailactions"])
//    if ([result.grantedPermissions containsObject:@"email"])
    {
        NSLog(@"result is:%@",result);
        [self fetchUserInfo];
    }
}

- (IBAction)authWithFB:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends",@"user_birthday"]
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             [self fetchUserInfo];
         }

     }];
//    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//    [login
//     logInWithReadPermissions: _loginWithFacebook.readPermissions
//     fromViewController:self
//     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//         if (error) {
//             NSLog(@"Process error");
//         } else if (result.isCancelled) {
//             NSLog(@"Cancelled");
//         } else {
//             NSLog(@"Logged in");
//         }
//     }];
}

@end
