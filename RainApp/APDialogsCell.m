//
//  APDialogsCell.m
//  Rain
//
//  Created by EgorMac on 17/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APDialogsCell.h"

@implementation APDialogsCell
@synthesize dialog, avatarView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDialog:(NSDictionary *)newDialog
{
    if (newDialog != dialog) {
        dialog = newDialog;
        
        int unreadMessagesCount = (int)[[dialog objectForKey:@"new_messages"] integerValue];
        if (unreadMessagesCount == 0) {
            [unreadMessagesBackground setHidden:YES];
            [unreadMessagesCountLabel setHidden:YES];
        }
        else
        {
            [unreadMessagesBackground setHidden:NO];
            [unreadMessagesCountLabel setHidden:NO];
            [unreadMessagesCountLabel setText:[NSString stringWithFormat:@"%d", unreadMessagesCount]];
        }
        
        [nameLabel setText:[[dialog objectForKey:@"user"] objectForKey:@"nick"]];
        
        if (![[dialog objectForKey:@"user"] objectForKey:@"avatar"]) {
            [avatarView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            NSLog(@"don't set avatar, set placeholder");
        }
        else
        {
            if ([[[dialog objectForKey:@"user"] objectForKey:@"avatar"] isEqualToString:@""] ||
                [[[dialog objectForKey:@"user"] objectForKey:@"avatar"] isEqualToString:@"http://rain-app.com/upload/avatars/IOS_"]) {
                [avatarView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                NSLog(@"don't set avatar");
            }
            else {
                // set avatar
                NSString *avatarString = [[dialog objectForKey:@"user"] objectForKey:@"avatar"];
                
                NSLog(@"set avatar from %@\n for %@", avatarString, [[dialog objectForKey:@"user"] objectForKey:@"nick"]);
                
                NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
                
                __weak UIImageView *weakAvatarImageView = avatarView;
                __weak NSDictionary *weakDialog = dialog;
                
                [avatarView setImageWithURLRequest:photoRequest
                                       placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]
                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                 {
//                     [weakAvatarImageView setImage:image];
                     [UIImageView apa_useMaskToImageView:weakAvatarImageView
                                                forImage:image];
                     NSLog(@"success for %@", [[weakDialog objectForKey:@"user"] objectForKey:@"nick"]);
                 }
                 
                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                    [weakAvatarImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                                }];
                
            }
        }
    }
}

@end
