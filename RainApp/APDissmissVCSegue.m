//
//  APDissmissVCSegue.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APDissmissVCSegue.h"

@implementation APDissmissVCSegue
- (void)perform
{
    if ([[self identifier] isEqualToString:@"goToNoFriendsVC"])
    {
        [[self sourceViewController] dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
