//
//  SSMessageTableViewCell.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessageTableViewCell.h"
#import "SSMessageTableViewCellBubbleView.h"
#import "APServerAPI.h"
#import "UIImage+APColor.h"

@implementation SSMessageTableViewCell

#pragma mark NSObject


#pragma mark UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier])) {
		self.selectionStyle = UITableViewCellSelectionStyleNone;		
		self.textLabel.hidden = YES;
    
        _bubbleView = [[SSMessageTableViewCellBubbleView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        
		_bubbleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
		[self.contentView addSubview:_bubbleView];
		[self.contentView sendSubviewToBack:_bubbleView];
        
        _messageFailImage = [UIImage imageNamed:@"messageFailImage.png"];
        _messageSuccessImage = [UIImage imageNamed:@"messageSuccessImage.png"];
    }
    return self;
}


#pragma mark Getters

- (SSMessageStyle)messageStyle {
	return _bubbleView.messageStyle;
}


- (NSString *)messageText {
	return _bubbleView.messageText;
}

- (NSData *)avatarData {
    return _bubbleView.avatarData;
}

- (NSString *)attachmentURLString
{
    return _bubbleView.attachmentURLString;
}

- (UIImage *)attachmentImage {
    return _bubbleView.attachmentImage;
}

#pragma mark Setters

- (void)setMessageStyle:(SSMessageStyle)aMessageStyle {
	_bubbleView.messageStyle = aMessageStyle;
  [_bubbleView setNeedsDisplay];
}


- (void)setMessageText:(NSString *)text {
	_bubbleView.messageText = text;
  [_bubbleView setNeedsDisplay];
}

- (void)setAvatarData:(NSData *)avatarData{
    _bubbleView.avatarData = avatarData;
    [_bubbleView setNeedsDisplay];
}

- (void)setAttachmentURLString:(NSString *)attachmentURLString
{
    _bubbleView.attachmentURLString = attachmentURLString;
    [_bubbleView setNeedsDisplay];
}

- (void)setAttachmentImage:(UIImage *)attachmentImage
{
    _bubbleView.attachmentImage = attachmentImage;
    [_bubbleView setNeedsDisplay];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage forMessageStyle:(SSMessageStyle)messsageStyle {
	if (messsageStyle == SSMessageStyleLeft) {
		_bubbleView.leftBackgroundImage = backgroundImage;
	} else if (messsageStyle == SSMessageStyleRight) {
		_bubbleView.rightBackgroundImage = backgroundImage;
	}
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)startActivityAnimation
{
    if (_messageStatusButton) {
        [_messageStatusButton removeFromSuperview];
    }
    
    CGFloat actvityX;
    CGFloat activitySize = 24.0f;
    
    if (_bubbleView.messageStyle == SSMessageStyleRight)
    {
        actvityX = 12.0f;
    } else {
        actvityX = self.contentView.frame.size.width - 12.0f;
    }
    
    CGRect activityFrame = CGRectMake(actvityX,
                                      self.contentView.frame.size.height/2 - activitySize/2,
                                      activitySize,
                                      activitySize);
    
    _cellActivityView = [[UIActivityIndicatorView alloc] initWithFrame:activityFrame];
    [_cellActivityView setColor:[UIColor whiteColor]];
    [self.contentView addSubview:_cellActivityView];
    NSLog(@"_cell act = \n%@", _cellActivityView);
    
    [_cellActivityView startAnimating];
}

- (void)stopActivityAnimationWithSuccess:(BOOL)isSuccess andUserID:(NSString *)userID
{
    NSLog(@"stop activity");
    [_cellActivityView stopAnimating];
    _messageStatusButton = [[UIButton alloc] initWithFrame:_cellActivityView.frame];
    
    if (isSuccess) {
        [_messageStatusButton setBackgroundImage:_messageSuccessImage forState:UIControlStateNormal];
        [_messageStatusButton setBackgroundImage:_messageSuccessImage forState:UIControlStateDisabled];
        [_messageStatusButton setEnabled:NO];
        [self.contentView addSubview:_messageStatusButton];
        [UIView animateWithDuration:2.0
                              delay:0.1
                            options:UIViewAnimationOptionTransitionNone
                         animations:^
         {
            [_messageStatusButton setAlpha:1.0f];
            [_messageStatusButton setAlpha:0.0f];
         }
                         completion:^(BOOL finished){
                             API = [[APServerAPI alloc] init];
                             [API getConversationWithUserID:userID];
                             [_messageStatusButton removeFromSuperview];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteWrittenMessage" object:self];
                         }];
        
    } else {
        // message failed
        [_messageStatusButton setBackgroundImage:_messageFailImage forState:UIControlStateNormal];
        [_messageStatusButton setEnabled:YES];
        [_messageStatusButton addTarget:self action:@selector(failedMessageButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_messageStatusButton];
    }
    
    [_cellActivityView removeFromSuperview];
}

- (void)failedMessageButtonPressed
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageStatusButtonPressed"
                                                        object:self];
}

@end
