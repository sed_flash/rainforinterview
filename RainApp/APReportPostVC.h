//
//  APReportPostVC.h
//  Rain
//
//  Created by EgorMac on 09/01/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APReportPostVC : UIViewController <UITextViewDelegate, UIAlertViewDelegate>
{
    APServerAPI *serverAPI;
    
    UIActivityIndicatorView *indicatorView;
    UIBarButtonItem *activityButton;
    
    IBOutlet UILabel *commentLabel;
    IBOutlet UITextView *commentTextView;
    IBOutlet UIImageView *backgroundImageView;
    IBOutlet UIBarButtonItem *reportButton;
    
    NSString *postID;
}

@property (nonatomic, strong) NSString *postID;

- (IBAction)report:(id)sender;

@end
