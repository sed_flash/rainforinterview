//
//  APFriendCell.h
//  Rain
//
//  Created by EgorMac on 14/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "APServerAPI.h"

@interface APFriendCell : UITableViewCell
{
    IBOutlet UILabel *nameLabel;
    IBOutlet UIImageView *avatarImageView;
    IBOutlet UIButton *followButton;
    
    UIActivityIndicatorView *activityView;
    
    NSDictionary *friend;
    
    APServerAPI *API;
}

@property (nonatomic, strong) NSDictionary *friend;
@property (nonatomic) BOOL followed;

- (IBAction)follow:(id)sender;

@end
