//
//  APFriendsVC.m
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFriendsVC.h"

@interface APFriendsVC ()

@end

@implementation APFriendsVC

@synthesize userMail;
@synthesize API;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersSuccess)
                                                 name:@"FollowersSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followersFailed)
                                                 name:@"FollowersFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followingsSuccess)
                                                 name:@"FollowingsSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(followingsFailed)
                                                 name:@"FollowingsFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor colorWithRed:93.0/255.0
                                               green:124.0/255.0
                                                blue:173.0/255.0
                                               alpha:1.0];

    [refreshControl addTarget:self
                       action:@selector(checkForNewFriends)
             forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = friendsTableView;
    controller.refreshControl = refreshControl;
    
    if ([friendsTableView numberOfRowsInSection:0] == 0)
    {
        [self showActivityIndicator];
        [API getFollowersForUserWithEmail:userMail];
    }
}

- (void)checkForNewFriends
{
    if ([friendsSegmentedControl selectedSegmentIndex] == 0)
    {
        [API getFollowersForUserWithEmail:userMail];
    } else {
        [API getFollowingsForUserWithEmail:userMail];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Source Methods

- (IBAction)changeFriendsSegmentedControlIndex:(id)sender
{
    if ([activityIndicator isAnimating]) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }
    
    [self showActivityIndicator];
    
    if ([friendsSegmentedControl selectedSegmentIndex] == 0)
    {
        UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedLeft"];
        [friendsSegmentedControl setBackgroundImage:selectedBackgroundImage
                                                   forState:UIControlStateSelected
                                                 barMetrics:UIBarMetricsDefault];

        [API getFollowersForUserWithEmail:userMail];
    }
    else
    {
        UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedRight"];
        [friendsSegmentedControl setBackgroundImage:selectedBackgroundImage
                                                   forState:UIControlStateSelected
                                                 barMetrics:UIBarMetricsDefault];

        [API getFollowingsForUserWithEmail:userMail];
    }
}

- (void)setDataSource
{
    if([friendsSegmentedControl selectedSegmentIndex] == 0)
    {
        followers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followers"] copy];
    } else {
        followings = [[[NSUserDefaults standardUserDefaults] objectForKey:@"followings"] copy];
    }
    
    [refreshControl endRefreshing];
    [friendsTableView reloadData];
}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    
    if ([friendsSegmentedControl selectedSegmentIndex] == 0) {
        count = [followers count];
    } else {
        count = [followings count];
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APFriendCell *cell = (APFriendCell *)[tableView dequeueReusableCellWithIdentifier:@"FreindsCell"];
    
    NSDictionary *friend;
    
    if ([friendsSegmentedControl selectedSegmentIndex] == 0)
    {
        if ([indexPath row] > followers.count) {
            friend = [followers objectAtIndex:0];
        } else {
            friend = [followers objectAtIndex:[indexPath row]];
        }
    } else {
        if ([indexPath row] > followings.count) {
            friend = [followings objectAtIndex:0];
        } else {
            friend = [followings objectAtIndex:[indexPath row]];
        }
    }
    
    [cell setFriend:friend];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *friend;
    
    if ([friendsSegmentedControl selectedSegmentIndex] == 0)
    {
        friend = [followers objectAtIndex:[indexPath row]];
    } else {
        friend = [followings objectAtIndex:[indexPath row]];
    }
    
    selectedUserEmail = [friend objectForKey:@"email"];
    
    NSLog(@"User with %@ selected", selectedUserEmail);
    
    [self performSegueWithIdentifier:@"showUserFromFriends" sender:self];
}

#pragma mark - Success and Failed methods

- (void)followersSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)followersFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)followingsSuccess
{
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)followingsFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark - Helper Methods

- (void)configureUIElements
{
    [[self navigationItem] setTitle:NSLocalizedString(@"friends", nil)];
    [self configureNavigationBar];
    [friendsSegmentedControl setTitle:NSLocalizedString(@"followers", nil)
                    forSegmentAtIndex:0];
    [friendsSegmentedControl setTitle:NSLocalizedString(@"following", nil)
                    forSegmentAtIndex:1];
    [friendsSegmentedControl setSelectedSegmentIndex:0];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    
    UIImage *normalBackgroundImage = [UIImage imageNamed:@"sectionBackground"];
    [[UISegmentedControl appearance] setBackgroundImage:normalBackgroundImage
                    forState:UIControlStateNormal
                  barMetrics:UIBarMetricsDefault];
    UIImage *selectedBackgroundImage = [UIImage imageNamed:@"sectionBackgroundSelectedLeft"];
    [[UISegmentedControl appearance] setBackgroundImage:selectedBackgroundImage
                    forState:UIControlStateSelected
                  barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setFrame:CGRectMake(0, 0, 240, 44)];
    [[UISegmentedControl appearance] setTintColor:[UIColor clearColor]];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:
                                                                  [UIColor colorWithRed:133.0/255.0
                                                                                  green:163.0/255.0
                                                                                   blue:195.0/255.0
                                                                                  alpha:1.0]}
                                                   forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}
                                                   forState:UIControlStateSelected];
    [[UISegmentedControl appearance] setWidth:110.0f forSegmentAtIndex:0];
}



#pragma mark - Activity

- (void)showActivityIndicator
{
    [friendsTableView setHidden:YES];
    activityIndicator = [[UIActivityIndicatorView alloc] init];
    [activityIndicator setColor:[UIColor colorWithRed:93.0/255.0
                                                green:124.0/255.0
                                                 blue:173.0/255.0
                                                alpha:1.0]];
    CGPoint center = self.view.center;
    center.y -= (self.tabBarController.tabBar.frame.size.height + self.navigationController.navigationBar.frame.size.height);
    [activityIndicator setCenter:center];
    [[self view] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)removeActivityIndicator
{
    [friendsTableView setHidden:NO];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepare for segue");
    
    if ([[segue identifier] isEqualToString:@"showUserFromFriends"]) {
        
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] isEqualToString:selectedUserEmail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserEmail];
    }
}

@end
