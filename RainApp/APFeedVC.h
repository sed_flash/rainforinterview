//
//  APFeedVC.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"
#import "APNewsCell.h"
#import "APPostVC.h"
#import "APAuthorView.h"
#import "APProfileVC.h"
#import "APCommentVC.h"

@interface APFeedVC : UIViewController <UITableViewDelegate, UITableViewDataSource, APNewsCellDelegate>
{
    IBOutlet UIImageView *backgroundImageView;
    
    IBOutlet UITableView *aTableView;
    
    APServerAPI *API;
    NSArray *postsArray;
    NSInteger allPostsCount;
    
    NSTimer *tapTimer;
    
    NSDictionary *selectedPost;
    NSString *selectedUserMail;
    
    BOOL isNewPostsDownloaded;
    BOOL postsDownloading;
    
    UIActivityIndicatorView *paginatorActivityView;
    UIActivityIndicatorView *activityIndicatorView;
    
    UIRefreshControl *refreshControl;
    IBOutlet NSLayoutConstraint *top;
}

@property (nonatomic, strong) NSTimer *tapTimer;

@end
