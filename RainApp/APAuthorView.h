//
//  APAuthorView.h
//  Rain
//
//  Created by EgorMac on 23/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "APProfileVC.h"

@interface APAuthorView : UIView
{
    IBOutlet UIImageView *photoView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *timeAgoLabel;
    IBOutlet UIButton *button;
    
    NSDictionary *author;
    NSString *timeAgo;
}

@property (nonatomic, strong) NSDictionary *author;
@property (nonatomic, strong) NSString *timeAgo;
@property (nonatomic, strong) IBOutlet UIButton *button;;

@end
