//
//  SSMessagesViewController.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessageTableViewCell.h"
#import "APTextView.h"
#import "MNMBottomPullToRefreshManager.h"
#import "APServerAPI.h"

@interface SSMessagesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, APTextViewDelegate, MNMBottomPullToRefreshManagerClient > {

    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    APTextView *textView;
    UIView *containerView;
    APServerAPI *api;
    
@private
	
//	UITableView *_tableView;
    UIButton *_doneButton;
    UIButton *_attachButton;
	
	UIImage *_leftBackgroundImage;
	UIImage *_rightBackgroundImage;
}

@property (nonatomic, retain, readonly) UITableView *tableView;
@property (nonatomic, retain, readonly) UIButton *doneButton;
@property (nonatomic, retain, readonly) UIButton *attachButton;
@property (nonatomic, retain) UIImage *leftBackgroundImage;
@property (nonatomic, retain) UIImage *rightBackgroundImage;

- (SSMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSData *)avatarForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
