//
//  JPSThumbnailTopViewController.m
//  Rain
//
//  Created by EgorMac on 10.11.15.
//  Copyright © 2015 AntsPro. All rights reserved.
//

#import "JPSThumbnailTopViewController.h"
#import "APTopTableCell.h"
#import "JPSThumbnailAnnotation.h"


@implementation JPSThumbnailTopViewController


@synthesize API;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tb=topTableView;
    // Do any additional setup after loading the view.
    
    UIImage *searchButtonImage = [UIImage imageNamed:@"searchIcon.png"];
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithImage:searchButtonImage
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(searchFriends)];
    [searchButton setImage:[UIImage imageNamed:@"Search Button"]];
    NSMutableArray *buttons = [self.navigationItem.rightBarButtonItems mutableCopy];
    [buttons addObject:searchButton];
    self.navigationItem.rightBarButtonItems = buttons;
    
    API = [[APServerAPI alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allUsersSuccess)
                                                 name:@"AllUsersSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allUsersFailed)
                                                 name:@"AllUsersFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    
    
    
    _mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    //_mapView.mapType=MKMapTypeSatellite;
    
    
    _mapView.mapType =MKMapTypeHybridFlyover;
    
    
    //self.questionMapView.camera.altitude=1000;
    _mapView.camera.pitch=50;
    _mapView.camera.heading=180;
    
    
    _mapView.showsBuildings = YES;
    _mapView.showsCompass=YES;
    _mapView.showsTraffic=YES;
    _mapView.showsScale=YES;

    _mapView.showsUserLocation=YES;
    _mapView.delegate = self;
    
    [self.view addSubview:_mapView];
    
    
    
   
    
  
    
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [manager stopUpdatingLocation];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    self.currentLocation=[self.mapView.userLocation location];
    
    if(self.currentLocation!=nil)
    {
        [self.mapView.userLocation  removeObserver:self forKeyPath:@"location"];
    
        NSDictionary *currentUser=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
        
        CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID= %@",[[currentUser objectForKey:@"id"]stringValue]];
        
        CKQuery *query = [[CKQuery alloc] initWithRecordType:@"User_Location" predicate:predicate];
        
        
        [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error)
         {
            
                 CKRecord *artworkRecord;
                 if(results.count>0)
                 {
                     
                     artworkRecord=[results lastObject];
                 }
                 else
                 {
                     artworkRecord = [[CKRecord alloc] initWithRecordType:@"User_Location"];
                     artworkRecord[@"ID"]=[[currentUser objectForKey:@"id"]stringValue];
                 
                 }
                     if (!error)
                     {
                         
                         artworkRecord[@"Location"] =self.currentLocation;
                         
                         [publicDatabase saveRecord:artworkRecord completionHandler:^(CKRecord *savedRecord, NSError *saveError) {
                             if (!error)
                             {
                                 
                                 
                                 NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
                                 NSDictionary *currentUser=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
                                 
                                 CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
                                 
                                 CKQuery *query = [[CKQuery alloc] initWithRecordType:@"User_Location" predicate:predicate];
                                 
                                 [publicDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error)
                                  {
                                      if (!error)
                                      {
                                          
                                          if(results.count>0)
                                          {
                                              [self annotations:users location:results];
                                          }
                                      }
                                  }];

                                 
                             }
                         }];
                         
                     }
                 }];
    }
    
}


- (void)annotations:(NSMutableArray *)users location:(NSMutableArray*)location
{
    __weak UIImageView *weakProfilePhotoImageView;
    //__weak NSDictionary *weakUser = user;

    JPSThumbnail *empire;

    for (int i=0;i<users.count;++i)
    {
        for ( CKRecord *artworkRecord in location)
        {
            
            NSDictionary *dict=[users objectAtIndex:i];
            
            if([[[dict objectForKey:@"id"]stringValue]isEqualToString:artworkRecord[@"ID"]])
            {
    
  
                empire= [[JPSThumbnail alloc] init];
        
                empire.dictTag=i;
                
                
                NSString *fullName = [NSString stringWithFormat:@"%@ %@", [dict objectForKey:@"first_name"], [dict objectForKey:@"last_name"]]
                ;
                if([fullName isEqualToString:@" "])
                {
                   
                   
                    fullName = [NSString stringWithFormat:@"%@", [dict objectForKey:@"nick"]];
                    
                }
                
                empire.title = fullName;
        
                empire.subtitle = [[dict objectForKey:@"rating"] stringValue];
        
        
        
                CLLocation *lc=artworkRecord[@"Location"];

   
        
                if(lc)
                {
        
                    empire.coordinate = lc.coordinate;
        
                if (![dict objectForKey:@"avatar"])
                {
                    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"])
                    {
                        [weakProfilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                    }
                }
                NSString *avatarString = [dict objectForKey:@"avatar_small"];
                if(!avatarString)
                {
                    avatarString  =[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
                }
        
    
                NSLog(@"set avatar from %@\n for %@", avatarString, [dict objectForKey:@"first_name"]);
    
                NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
        
                NSURL *imageURL = [NSURL URLWithString:avatarString];
        
        
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    if(imageData.length>0)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            empire.image = [UIImage imageWithData:imageData];
                            [_mapView addAnnotation: [JPSThumbnailAnnotation annotationWithThumbnail:empire]];
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^
                                       {
                                           empire.image=[UIImage imageNamed:@"ProfileEditPhotoBlue"];
                                           
                                           JPSThumbnailAnnotation *an=[[JPSThumbnailAnnotation alloc]initWithThumbnail:empire];
                                          
                                           
                                           UIButton *disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                                           disclosureButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                                           disclosureButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                                           [disclosureButton addTarget:self action:@selector(didPressPin:) forControlEvents:UIControlEventTouchUpInside];
                                         
                                           
                                           an.view.rightCalloutAccessoryView = disclosureButton;

                                         
                                           [_mapView addAnnotation:an];
                                           
                                           [_mapView showAnnotations:[NSArray arrayWithObjects:[_mapView.annotations firstObject],nil] animated:YES];
                                       });
                    }
                
                
        });
                }
       }
    }
    }
}


#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)])
    {
        JPSThumbnailAnnotation *thumb=(JPSThumbnailAnnotation*)view.annotation;
    
        int count=thumb.thumbnail.dictTag;
        
        NSDictionary *user = [users objectAtIndex: count];
        
        selectedUserMail = [user objectForKey:@"email"];
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];

        [self performSegueWithIdentifier:@"showDetails" sender:self];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)])
    {
        
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}




- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    
    
    //[self prepareVisibleCellsForAnimation];
    
    [self setupUIElements];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(checkForTopUsers)
             forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = topTableView;
    controller.refreshControl = refreshControl;
    
    if ([topTableView numberOfRowsInSection:0] == 0) {
        [self showActivityIndicator];
        [API allUsers];
    }
    
    
    if ([CLLocationManager locationServicesEnabled] )
    {
        if (self.locationManager == nil )
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter =kCLDistanceFilterNone;
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager startUpdatingLocation];
            self.locationManager .desiredAccuracy = kCLLocationAccuracyKilometer;
        }
        else
        {
            [self.locationManager startUpdatingLocation];
        }
    }
    
    
    [self.mapView.userLocation addObserver:self
                                forKeyPath:@"location"
                                   options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld)
                                   context:nil];

    
}

- (void)checkForTopUsers
{
    [API allUsers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setColor:[UIColor whiteColor]];
    [activity setTag:5];
    [activity setCenter:self.view.center];
    [topTableView setHidden:YES];
    [self.view addSubview:activity];
    [activity startAnimating];
}

- (void)removeActivityIndicator
{
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[[self view] viewWithTag:5];
    [activityIndicator stopAnimating];
    [[[self view] viewWithTag:5] removeFromSuperview];
    [topTableView setHidden:NO];
}

- (void)allUsersSuccess
{
   
    [self removeActivityIndicator];
    [self setDataSource];
}

- (void)allUsersFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}



#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellSize;
    
    cellSize = 220 - (indexPath.row + 110);
    
    if (cellSize > 100) {
        cellSize = 96;
    }
    else if (cellSize < 74)
    {
        cellSize = 74;
    }
    
    return cellSize;
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APTopTableCell *cell = (APTopTableCell *)[tableView dequeueReusableCellWithIdentifier:@"TopTableCell"];
    
    
    NSDictionary *user = [users objectAtIndex:[indexPath row]];
    
    [cell setUser:user];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *user = [users objectAtIndex:[indexPath row]];
    selectedUserMail = [user objectForKey:@"email"];
    
    NSLog(@"User with %@ selected", selectedUserMail);
    
    [self performSegueWithIdentifier:@"showUserFromTop" sender:self];
}


#pragma mark - Helper Methods

- (void)setDataSource
{
    NSMutableArray *tempUsers = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allUsers"] mutableCopy];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"rating" ascending:NO];
    NSArray *descriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [tempUsers sortUsingDescriptors:descriptors];
    users = [tempUsers copy];

   
    [_mapView removeAnnotations:_mapView.annotations];

    // Annotations
    
    tempUsers = nil;
    
    [refreshControl endRefreshing];
    [topTableView reloadData];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"Top", nil)];
}

- (void)searchFriends
{
    [self performSegueWithIdentifier:@"showSearchFriendsVC"
                              sender:self];
}

- (void)setupUIElements
{
    [self configureNavigationBar];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepare for segue");
    
    if ([[segue identifier] isEqualToString:@"showDetails"]) {
        
        NSDictionary *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        NSString *email = [token objectForKey:@"email"];
        
        if ([email isEqualToString:selectedUserMail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserMail];
    }
}

@end
