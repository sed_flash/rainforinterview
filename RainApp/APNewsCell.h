//
//  APNewsCell.h
//  Rain
//
//  Created by EgorMac on 20/09/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+APColor.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "NSDate+TimeAgo.h"
#import "APServerAPI.h"

@protocol APNewsCellDelegate
- (void)commentPost:(NSDictionary *)commentingPost;
@end

@interface APNewsCell : UITableViewCell
{
  
    IBOutlet UIImageView *lshowLikeInImage;
    IBOutlet UIImageView *photoImageView;
    IBOutlet UIProgressView *progressView;
    IBOutlet UIImageView *videoIconView;
    IBOutlet UIView *statsView;
    
    IBOutlet UIImageView *likesImageView;
    IBOutlet UIImageView *repostsImageView;
    
    IBOutlet UILabel *repostsCountLabel;
    IBOutlet UILabel *likesCountLabel;
    IBOutlet UILabel *commentsCountLabel;
    IBOutlet UILabel *viewsCountLabel;
    
    IBOutlet UIButton *upperRepostButton;
    IBOutlet UIButton *likeButton;
    
    IBOutlet NSLayoutConstraint *panelLikeLeading;
    IBOutlet NSLayoutConstraint *panelCommentsLeading;
    IBOutlet NSLayoutConstraint *panelViewsLeading;
    IBOutlet NSLayoutConstraint *panelRepostsLeading;
    
    NSString *postID;
    NSDictionary *currentPost;
    
    APServerAPI *API;
}

@property (weak, nonatomic) id <APNewsCellDelegate> delegate;
@property (nonatomic, strong) NSString *postID;
@property (nonatomic, strong) NSDictionary *currentPost;

- (IBAction)like:(id)sender;

@end
