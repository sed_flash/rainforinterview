//
//  SSMessageTableViewCellBubbleView.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "SSMessageTableViewCellBubbleView.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <AVFoundation/AVFoundation.h>

#define kFont [UIFont fontWithName:@"Arial" size:12.0]

static NSLineBreakMode kLineBreakMode = NSLineBreakByWordWrapping;
static CGFloat kMaxWidth = 223.0f;
static CGFloat kPaddingTop = 4.0f;
static CGFloat kPaddingBottom = 8.0f;
static CGFloat kMarginTop = 2.0f;
static CGFloat kMarginBottom = 2.0f;

@implementation SSMessageTableViewCellBubbleView

@synthesize messageText = _messageText;
@synthesize avatarData = _avatarData;
@synthesize attachmentURLString = _attachmentURLString;
@synthesize attachmentImage = _attachmentImage;
@synthesize leftBackgroundImage = _leftBackgroundImage;
@synthesize rightBackgroundImage = _rightBackgroundImage;
@synthesize messageStyle = _messageStyle;

#pragma mark Class Methods

+ (CGSize)textSizeForText:(NSString *)text {
	CGSize maxSize = CGSizeMake(kMaxWidth - 35.0f, 1000.0f);
    CGRect textRect = [text boundingRectWithSize:maxSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:kFont}
                                         context:nil];
    
    return textRect.size;
}


+ (CGSize)bubbleSizeForText:(NSString *)text {
	CGSize textSize = [self textSizeForText:text];
    CGSize bubbleSize = CGSizeMake(textSize.width + 35.0f, textSize.height + kPaddingTop + kPaddingBottom);
	return bubbleSize;
}


+ (CGFloat)cellHeightForText:(NSString *)text {
	return [self bubbleSizeForText:text].height + kMarginTop + kMarginBottom;
}

#pragma mark NSObject


#pragma mark UIView

- (id)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];

	}
	return self;
}


static const CGFloat AVATAR_MARGIN_RIGHT = 4.0f;
static const CGFloat AVATAR_MARGIN_LEFT = AVATAR_MARGIN_RIGHT;
static const CGFloat AVATAR_SIZE = 44.0f;
static const CGFloat ATTACHMENT_WIDTH = 190.0f;
static const CGFloat ATTACHMENT_MARGIN = 10.0f;

- (void)drawRect:(CGRect)frame {
	UIImage *bubbleImage = _messageStyle == SSMessageStyleLeft ? _leftBackgroundImage : _rightBackgroundImage;
    
    CGSize bubbleSize;
    
    if (_attachmentURLString) {
        bubbleSize = CGSizeMake(kMaxWidth, kMaxWidth + kPaddingBottom);
    } else {
        bubbleSize = [[self class] bubbleSizeForText:_messageText];
    }
    
    CGRect avatarImageViewFrame;
    CGRect bubbleFrame;
    
    if (_messageStyle == SSMessageStyleRight) {
        avatarImageViewFrame = CGRectMake(self.frame.size.width - AVATAR_MARGIN_RIGHT - AVATAR_SIZE,
                                          kMarginTop + bubbleSize.height - AVATAR_SIZE/2,
                                          AVATAR_SIZE,
                                          AVATAR_SIZE);

        bubbleFrame = CGRectMake(self.frame.size.width - AVATAR_MARGIN_RIGHT - AVATAR_SIZE - bubbleSize.width, kMarginTop, bubbleSize.width, bubbleSize.height);
    } else
    {
        avatarImageViewFrame = CGRectMake(AVATAR_MARGIN_LEFT,
                                          kMarginTop + bubbleSize.height - AVATAR_SIZE/2,
                                          AVATAR_SIZE,
                                          AVATAR_SIZE);

        bubbleFrame = CGRectMake(AVATAR_MARGIN_LEFT + AVATAR_SIZE,
                                 kMarginTop,
                                 bubbleSize.width,
                                 bubbleSize.height);
    }
    
    UIImage *avatarImage = [UIImage imageWithData:_avatarData];
    [avatarImage drawInRect:avatarImageViewFrame];
    
    if (!_attachmentURLString) {
        [bubbleImage drawInRect:bubbleFrame];
    }
 //   [[UIImage imageNamed:@"DropImage"] drawInRect:avatarImageViewFrame];
	
	CGSize textSize = [[self class] textSizeForText:_messageText];
    CGFloat textX = (CGFloat)bubbleImage.leftCapWidth - 3.0f + ((_messageStyle == SSMessageStyleRight) ? bubbleFrame.origin.x : bubbleFrame.origin.x);
    
	CGRect textFrame = CGRectMake(textX, kPaddingTop + kMarginTop, textSize.width, textSize.height);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = kLineBreakMode;
    paragraphStyle.alignment = (_messageStyle == SSMessageStyleRight) ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    if (!_attachmentURLString) {
        [_messageText drawInRect:textFrame
                  withAttributes:@{NSFontAttributeName: kFont,
                                   NSParagraphStyleAttributeName: paragraphStyle,
                                   NSForegroundColorAttributeName: [UIColor whiteColor]}];
    }
    
    if (_attachmentURLString) {
        
        CGRect attachmentFrame = CGRectMake(textX,
                                            textFrame.origin.y + textFrame.size.height + ATTACHMENT_MARGIN,
                                            ATTACHMENT_WIDTH,
                                            ATTACHMENT_WIDTH);
        attachmentImageView = [[UIImageView alloc] initWithFrame:attachmentFrame];
        
        if ([_attachmentURLString hasSuffix:@".mp4"]) {
            
            // video
            NSURL *videoURL = [NSURL fileURLWithPath:_attachmentURLString];
            
            UIImage *thumbnail = [UIImage imageNamed:@"videoAttachment.png"];
            
            [attachmentImageView setImage:thumbnail];
            
            [bubbleImage drawInRect:CGRectMake(bubbleFrame.origin.x,
                                               bubbleFrame.origin.y,
                                               bubbleFrame.size.width,
                                               textSize.height + attachmentFrame.size.height + kMarginTop + kMarginBottom + kPaddingTop + kPaddingBottom + ATTACHMENT_MARGIN)];
            
            [_messageText drawInRect:textFrame
                         withAttributes:@{NSFontAttributeName: kFont,
                                          NSParagraphStyleAttributeName: paragraphStyle,
                                          NSForegroundColorAttributeName: [UIColor whiteColor]}];
            
            [attachmentImageView.image drawInRect:attachmentImageView.frame];
        }
        else if ([_attachmentURLString hasSuffix:@".jpeg"] ||
                 [_attachmentURLString hasSuffix:@".png"])
        {
            float oldWidth = _attachmentImage.size.width;
            float scaleFactor = ATTACHMENT_WIDTH / oldWidth;
            float newAttachmentHeight = _attachmentImage.size.height * scaleFactor;
            float newAttachmentWidth = oldWidth * scaleFactor;
            
            CGFloat newAttachmentMarginTop = (ATTACHMENT_WIDTH - newAttachmentHeight) / 2;
            
            CGRect attachmentFrame = CGRectMake(attachmentImageView.frame.origin.x,
                                                     attachmentImageView.frame.origin.y + newAttachmentMarginTop,
                                                     newAttachmentWidth,
                                                     newAttachmentHeight);
            
            [bubbleImage drawInRect:CGRectMake(bubbleFrame.origin.x,
                                               bubbleFrame.origin.y,
                                               bubbleFrame.size.width,
                                               textSize.height + attachmentFrame.size.height + kMarginTop + kMarginBottom + kPaddingTop + kPaddingBottom + ATTACHMENT_MARGIN + newAttachmentMarginTop*2)];
            
            NSLog(@"bubble height: %f, \ntextsize.heigth: %f, \nattachheight: %f", textSize.height + attachmentFrame.size.height + kMarginTop + kMarginBottom + kPaddingTop + kPaddingBottom + ATTACHMENT_MARGIN + newAttachmentMarginTop*2, textSize.height, attachmentFrame.size.height);
            
            
            [_messageText drawInRect:textFrame
                      withAttributes:@{NSFontAttributeName: kFont,
                                       NSParagraphStyleAttributeName: paragraphStyle,
                                       NSForegroundColorAttributeName: [UIColor whiteColor]}];
            
            [_attachmentImage drawInRect:attachmentFrame];
        }
        else
        {
            if ([_attachmentURLString isEqualToString:@""] ||
                _attachmentURLString == nil)
            {
                NSLog(@"has no attachment (old)");
            } else {
                NSLog(@"attachmentusrl string : %@", _attachmentURLString);
                // For manually created cell with image from NSData (not from URL)
                NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"messageAttachment"];
                UIImage *attachmentImage = [[UIImage alloc] initWithData:imageData];
                float oldWidth = attachmentImage.size.width;
                float scaleFactor = ATTACHMENT_WIDTH / oldWidth;
                float newAttachmentHeight = attachmentImage.size.height * scaleFactor;
                float newAttachmentWidth = oldWidth * scaleFactor;
                CGFloat newAttachmentMarginTop = (ATTACHMENT_WIDTH - newAttachmentHeight) / 2;
                [attachmentImageView setFrame:CGRectMake(attachmentImageView.frame.origin.x,
                                                         attachmentImageView.frame.origin.y + newAttachmentMarginTop,
                                                         newAttachmentWidth,
                                                         newAttachmentHeight)];
                [attachmentImageView setImage:attachmentImage];
                [bubbleImage drawInRect:CGRectMake(bubbleFrame.origin.x,
                                                   bubbleFrame.origin.y,
                                                   bubbleFrame.size.width,
                                                   textSize.height + attachmentFrame.size.height + kMarginTop + kMarginBottom + kPaddingTop + kPaddingBottom + ATTACHMENT_MARGIN)];
                [_messageText drawInRect:textFrame
                          withAttributes:@{NSFontAttributeName: kFont,
                                           NSParagraphStyleAttributeName: paragraphStyle,
                                           NSForegroundColorAttributeName: [UIColor whiteColor]}];
                
                [attachmentImageView.image drawInRect:attachmentImageView.frame];
            }
        }
    }
}

@end
