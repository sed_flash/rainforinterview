//
//  APReportPostVC.m
//  Rain
//
//  Created by EgorMac on 09/01/15.
//  Copyright (c) 2015 AntsPro. All rights reserved.
//

#import "APReportPostVC.h"
#import "UIColor+CustomColor.h"

@interface APReportPostVC ()
{

}

@end

@implementation APReportPostVC
@synthesize postID;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static NSString *successMessage = @"successMessage";
static NSInteger successAlertTag = 99;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    serverAPI = [[APServerAPI alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportSuccess)
                                                 name:@"ReportPostSuccess"
                                               object:serverAPI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportFailed)
                                                 name:@"ReportPostFailed"
                                               object:serverAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPostID:(NSString *)newPostID
{
    if (postID != newPostID) {
        postID = newPostID;
    }
}

- (IBAction)report:(id)sender
{
    NSString *commentText = [commentTextView text];
    NSData *commentTextData = [commentText dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"send report with comment: %@\nfor postID = %@", commentText, postID);
    [serverAPI sendReportForPostWithID:postID andText:commentTextData];
}

#pragma mark - Success and Failed methods

- (void)reportSuccess
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:successMessage];
}

- (void)reportFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:successMessage])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"thankYou", nil)
                                                        message:NSLocalizedString(@"thankYouForYourHelp", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert setTag:successAlertTag];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == successAlertTag) {
        [[self navigationController] popViewControllerAnimated:YES];
    }
}

- (void)showActivityIndicator
{
    if (activityButton == nil)
    {
        indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [indicatorView setColor:[UIColor rainGoldColor]];
        activityButton = [[UIBarButtonItem alloc] initWithCustomView:indicatorView];
    }
    self.navigationItem.rightBarButtonItem = activityButton;
}

- (void)removeActivityIndicator
{
    [indicatorView stopAnimating];
    self.navigationItem.rightBarButtonItem = reportButton;
}

#pragma mark - Text View Delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView *currentView in self.view.subviews) {
        if ([currentView isKindOfClass:[UITextField class]]) {
            [currentView resignFirstResponder];
        }
        
        else if ([currentView isKindOfClass:[UITextView class]])
        {
            [currentView resignFirstResponder];
        }
    }
}

#pragma mark - Helper Methods

- (void)configureUIElements
{
    [self configureNavigationBar];
    [self configureCommentLabel];
    [commentTextView setText:@""];
    [[commentTextView layer] setCornerRadius:10.0f];
}

- (void)configureNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [reportButton setTitle:NSLocalizedString(@"report", nil)];
    self.navigationItem.rightBarButtonItem = reportButton;
    [self setTitle:@""];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)configureCommentLabel
{
    [commentLabel setText:NSLocalizedString(@"yourComment(optional)", nil)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
