//
//  APTopTableCell.m
//  Rain
//
//  Created by EgorMac on 02/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APTopTableCell.h"

@implementation APTopTableCell {
    NSString *location;
}

@synthesize user;
@synthesize profilePhotoImageView;
@synthesize nameLabel, rateImageView, rateLabel, locationLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUser:(NSDictionary *)newUser
{
//    
        user = newUser;
        
        if (_isCellForFiltersResults) {
            [self cellForFilterResults];
        } else {
            [self cellForTopVC];
        }
//    }
}

- (void)cellForTopVC {
    [profilePhotoImageView setTag:1];
    
    if (![user objectForKey:@"avatar"]) {
        [profilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        NSLog(@"don't set avatar, set placeholder");
    }
    else
    {
        if ([[user objectForKey:@"avatar"] isEqualToString:@""] ||
            [[user objectForKey:@"avatar"] isEqualToString:@"http://rain-app.com/upload/avatars/IOS_"]) {
            [profilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            NSLog(@"don't set avatar");
        }
        else {
            // set avatar
            NSString *avatarString = [user objectForKey:@"avatar_small"];
            if(avatarString.length==0)
            {
                avatarString  =[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
            }
            
            NSLog(@"set avatar from %@\n for %@", avatarString, [user objectForKey:@"first_name"]);
            
            NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:avatarString]];
            
            __weak UIImageView *weakProfilePhotoImageView = profilePhotoImageView;
            __weak NSDictionary *weakUser = user;
            
            [profilePhotoImageView setImageWithURLRequest:photoRequest
                                         placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]
                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [UIImageView apa_useMaskToImageView:weakProfilePhotoImageView forImage:image];
                 NSLog(@"success for %@", [weakUser objectForKey:@"first_name"]);
             }
             
                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                      [weakProfilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                                  }];
            
        }
    }
    
    [rateLabel setText:[[user objectForKey:@"rating"] stringValue]];
    [nameLabel setText:[NSString stringWithFormat:@"%@ %@", [user objectForKey:@"first_name"], [user objectForKey:@"last_name"]]];
    
    
//    NSString *locationString = [NSString stringWithFormat:@"%@, %@", [user objectForKey:@"country_name"], [user objectForKey:@"city_name"]];
    [locationLabel setText:location];
    [self.contentView updateConstraintsIfNeeded];
}

- (void)cellForFilterResults {
    NSString *userID = _foundUsersID;
    NSDictionary *currentUser = _foundUser;
    NSString *userName = [currentUser objectForKey:@"firstname"];
    NSString *rating = [[currentUser objectForKey:@"rating"] stringValue];
    
    NSLog(@"current user is %@", currentUser);
    
    [nameLabel setText:userName];
    [rateLabel setText:rating];
    
    if ([[currentUser objectForKey:@"avatar_small"] isKindOfClass:[NSString class]]) {
        
        if ([[currentUser objectForKey:@"avatar_small"] isEqualToString:@""])
        {
            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"])
            {
                [profilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            }
        }
        else
        {
            NSString *photoString = [currentUser objectForKey:@"avatar_small"];
            if(photoString.length==0)
            {
                photoString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
            }
            
            NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
            
            __weak UIImageView *weakImageView = profilePhotoImageView;
            
            [profilePhotoImageView setImageWithURLRequest:photoRequest
                                         placeholderImage:[UIImage new]
                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 //                 [weakImageView setImage:image];
                 [UIImageView apa_useMaskToImageView:weakImageView
                                            forImage:image];
             }
             
                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                                      NSLog(@"fail for cell photo");
                                                      [weakImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                                  }];
        }
    }
    else
    {
        NSLog(@"hasn't avatar");
        [profilePhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
    }
    
    [locationLabel setText:location];
    
}
- (void)setLocation:(NSDictionary *)currentUser {
    
    NSLog(@"country = %@, city = %@", [currentUser objectForKey:@"country_name"], [currentUser objectForKey:@"city_name"]);
    
    if (nil == [currentUser objectForKey:@"country_name"] &&
        nil == [currentUser objectForKey:@"city_name"] ) {
        location = @"";
    } else if (nil != [currentUser objectForKey:@"country_name"] &&
               nil == [currentUser objectForKey:@"city_name"] ) {
        location = [NSString stringWithFormat:@"%@", [currentUser objectForKey:@"country_name"]];
    } else if (nil == [currentUser objectForKey:@"country_name"] &&
               nil != [currentUser objectForKey:@"city_name"] ) {
        location = [NSString stringWithFormat:@"%@", [currentUser objectForKey:@"city_name"]];
    } else {
        location = [NSString stringWithFormat:@"%@, %@", [currentUser objectForKey:@"country_name"], [currentUser objectForKey:@"city_name"]];
    }
}

@end
