//
//  FilterTableVC.m
//  Rain
//
//  Created by EgorMac on 07/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "FilterTableVC.h"
#import "FilterCell.h"
#import "NewFilterVC.h"


@interface FilterTableVC ()

@end

@implementation FilterTableVC

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setDataSource];
    
}

- (void)setDataSource
{
    filtersArray = [[NSMutableArray alloc] initWithObjects:
                    @"GPUImageBrightnessFilter",
                    @"GPUImageExposureFilter",
                    @"GPUImageContrastFilter",
                    @"GPUImageSaturationFilter",
                    @"GPUImageGammaFilter",
                    @"GPUImageHueFilter",
                    @"GPUImageHighlightShadowFilter",
                    @"GPUImageAmatorkaFilter",
                    @"GPUImageMissEtikateFilter",
                    @"GPUImageSoftEleganceFilter",
                    @"GPUImageColorInvertFilter",
                    @"GPUImageGrayscaleFilter",
                    @"GPUImageMonochromeFilter",
                    @"GPUImageHazeFilter",
                    @"GPUImageSepiaFilter",
                    @"GPUImageOpacityFilter",
                    @"GPUImageLuminanceThresholdFilter",
                    @"GPUImageAdaptiveThresholdFilter",
                    @"GPUImageAverageLuminanceThresholdFilter",
                    @"GPUImageSharpenFilter",
                    @"GPUImageUnsharpMaskFilter",
                    @"GPUImageGaussianBlurFilter",
                    @"GPUImageBoxBlurFilter",
                    @"GPUImageTiltShiftFilter",
                    @"GPUImage3x3ConvolutionFilter",
                    @"GPUImageSobelEdgeDetectionFilter",
                    @"GPUImagePrewittEdgeDetectionFilter",
                    @"GPUImageThresholdEdgeDetectionFilter",
                    @"GPUImageCannyEdgeDetectionFilter",
                    @"GPUImageHarrisCornerDetectionFilter",
                    @"GPUImageNobleCornerDetectionFilter",
                    @"GPUImageShiTomasiCornerDetectionFilter",
                    @"GPUImageNonMaximumSuppressionFilter",
                    @"GPUImageXYDerivativeFilter",
                    @"GPUImageCrosshairGenerator",
                    @"GPUImageDilationFilter",
                    @"GPUImageRGBDilationFilter",
                    @"GPUImageErosionFilter",
                    @"GPUImageRGBErosionFilter",
                    @"GPUImageOpeningFilter",
                    @"GPUImageRGBOpeningFilter",
                    @"GPUImageClosingFilter",
                    @"GPUImageRGBClosingFilter",
                    @"GPUImageLocalBinaryPatternFilter",
                    @"GPUImageLowPassFilter",
                    @"GPUImageHighPassFilter",
                    @"GPUImageMotionDetector",
                    @"GPUImageHoughTransformLineDetector",
                    @"GPUImageLineGenerator",
                    @"GPUImageMotionBlurFilter",
                    @"GPUImageZoomBlurFilter",
                    @"GPUImagePixellateFilter",
                    @"GPUImagePolarPixellateFilter",
                    @"GPUImagePolkaDotFilter",
                    @"GPUImageHalftoneFilter",
                    @"GPUImageCrosshatchFilter",
                    @"GPUImageSketchFilter",
                    @"GPUImageThresholdSketchFilter",
                    @"GPUImageToonFilter",
                    @"GPUImageSmoothToonFilter",
                    @"GPUImageEmbossFilter",
                    @"GPUImagePosterizeFilter",
                    @"GPUImageSwirlFilter",
                    @"GPUImageBulgeDistortionFilter",
                    @"GPUImagePinchDistortionFilter",
                    @"GPUImageStretchDistortionFilter",
                    @"GPUImageSphereRefractionFilter",
                    @"GPUImageGlassSphereFilter",
                    @"GPUImageVignetteFilter",
                    @"GPUImageKuwaharaFilter",
                    @"GPUImageKuwaharaRadius3Filter",
                    @"GPUImagePerlinNoiseFilter",
                    @"GPUImageCGAColorspaceFilter",
                    @"GPUImageMosaicFilter",
                    @"GPUImageJFAVoronoiFilter",
                    @"GPUImageVoronoiConsumerFilter",
                    nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [filtersArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:@"FilterCell"
                                                                      forIndexPath:indexPath];
    NSString *filterID = [filtersArray objectAtIndex:indexPath.row];
    [cell setFilterID:filterID];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"filter"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        FilterCell *cell = (FilterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        [[segue destinationViewController] setFilterID:cell.filterID];
    }
}

@end
