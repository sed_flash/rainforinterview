//
//  APUserInfoVC.m
//  Rain
//
//  Created by EgorMac on 05/11/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APUserInfoVC.h"
#import "APReportProfileVC.h"
#import "UIColor+CustomColor.h"

@interface APUserInfoVC ()

@end

@implementation APUserInfoVC
@synthesize currentUser, photoImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"user info loaded!");
    [self configureUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:18.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
}

- (void)setPhotoImage:(UIImage *)newPhotoImage
{
    if (newPhotoImage != photoImage) {
        photoImage = newPhotoImage;
    }
}

- (void)setCurrentUser:(NSDictionary *)newCurrentUser
{
    if (newCurrentUser != currentUser) {
        currentUser = newCurrentUser;
        NSLog(@"currentUser is:\n%@", currentUser);
    }
}

- (void)configureUI
{
    self.navigationItem.title = NSLocalizedString(@"profileInfo", nil);
    UIBarButtonItem *reportButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"report", nil)
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                        action:@selector(reportInappropriate)];
    [reportButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:17.0],
                                              NSForegroundColorAttributeName: [UIColor whiteColor]}
                                  forState:UIControlStateNormal];
    [self.navigationItem setRightBarButtonItem:reportButtonItem];
    
    nameLabel.text = [NSString stringWithFormat:@"%@ %@",
                      [currentUser objectForKey:@"first_name"],
                      [currentUser objectForKey:@"last_name"]];
    nickLabel.text = [currentUser objectForKey:@"nick"];
    ageLabel.text = [NSString stringWithFormat:@"%@ (%@)",
                     [currentUser objectForKey:@"birthday"],
                     [currentUser objectForKey:@"age"]];
    locationLabel.text = [NSString stringWithFormat:@"%@ %@",
                          [currentUser objectForKey:@"country_name"],
                          [currentUser objectForKey:@"city_name"]];
//    [avatarImageView setImage:photoImage];
    [UIImageView apa_useMaskToImageView:avatarImageView
                               forImage:photoImage];
    NSLog(@"photoImage is:\n%@", photoImage);
    NSLog(@"avatarImageView image is:\n%@", avatarImageView.image);
    [userAgreementButton setTitle:NSLocalizedString(@"userAgreement", nil)
                         forState:UIControlStateNormal];

}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info dictionary - \n%@", info);
    
    
    imgBacground.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imgBacground.image) forKey:@"BacgroundImage"];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)btnChangePhoto:(id)sender
{
    
    NSLog(@"Photo button tapped");
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        NSArray *mediaTypeArray = [[NSArray alloc] initWithObjects:@"public.image", nil];
        [imagePicker setMediaTypes:mediaTypeArray];
    }
    
    [imagePicker setDelegate:self];
    [imagePicker setAllowsEditing:NO];
    [self presentViewController:imagePicker
                       animated:YES
                     completion:nil];
    
}

- (void)reportInappropriate
{
    NSLog(@"report");
    [self performSegueWithIdentifier:@"reportProfile" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"reportProfile"]) {
        
        NSLog(@"report profile");
        [[segue destinationViewController] setProfileEmail:[currentUser objectForKey:@"email"]];
    }
}

@end
