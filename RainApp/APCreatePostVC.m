//
//  APCreatePostVC.m
//  Rain
//
//  Created by EgorMac on 15/07/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APCreatePostVC.h"
#import "UIColor+CustomColor.h"
#import "APTagPeopleVC.h"

@interface APCreatePostVC ()

@end

@implementation APCreatePostVC

@synthesize photoImage;
@synthesize isVideo;
@synthesize videoURL;
@synthesize taggedPeople;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    API = [[APServerAPI alloc] init];
    [API setDelegate:self];
    
    taggedPeople = [[NSMutableArray alloc] init];
    
    [self setUpUIElements];
}

- (void)addTaggedFriendID:(NSDictionary *)taggedFriend
{
    [taggedPeople addObject:taggedFriend];
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:taggedPeople];
    taggedPeople = [[orderedSet array] mutableCopy];
    
    NSLog(@"taggedPeopleIDs is:\n%@", taggedPeople);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postSuccess)
                                                 name:@"PostSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postFailed)
                                                 name:@"PostFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self configureTaggedPeopleTextView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)postSuccess
{
    NSLog(@"post success");
    
    UITabBarController *controller = [[(UINavigationController *)[self presentingViewController] viewControllers] lastObject];
    
    if ([controller isKindOfClass:[UITabBarController class]]) {
        NSLog(@"tabbarcontroller - %@", controller);
        NSLog(@"controllers - %@", controller.viewControllers);
        UINavigationController *feedController = [[controller viewControllers] objectAtIndex:0];
        NSLog(@"feed controller - %@", feedController);
        [feedController popToRootViewControllerAnimated:NO];
        [controller setSelectedIndex:0];
        
//        [self stopAnimatingLoadingIndicator];
        [doneButton setEnabled:YES];
        
//        [self.navigationController popViewControllerAnimated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedChanged" object:self];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedChanged" object:self];
        }];
    }
    
    else
    {
        NSLog(@"controller class is - %@", controller);
        NSLog(@"controllers - %@", controller.viewControllers);
        UINavigationController *navController = (UINavigationController *)controller;
        [navController popToRootViewControllerAnimated:NO];
//        [controller setSelectedIndex:0];
        
        [self stopAnimatingLoadingIndicator];
        [doneButton setEnabled:YES];
        
        
//        [self.navigationController popViewControllerAnimated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedChanged" object:self];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedChanged" object:self];
        }];
    }
}

- (void)postFailed
{
    NSLog(@"post failed");
    [doneButton setEnabled:YES];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    NSLog(@"network error");
    [doneButton setEnabled:YES];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    NSLog(@"server error");
   [doneButton setEnabled:YES];
    [self showAlertWithIdentifier:serverError];
}

#pragma mark - Alerts

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        UIAlertView *alert;
        NSString *errorMessage;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"] isKindOfClass:[NSString class]]) {
            errorMessage = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"];
        }
        else
        {
            errorMessage = @"";
            for (NSString *currentString in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]) {
                errorMessage = [errorMessage stringByAppendingString:[NSString stringWithFormat:@"%@\n", currentString]];
            }
            
        }
        
        // show registration failed alert
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                           message:errorMessage
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                 otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}


- (void)setPhotoImage:(UIImage *)newPhotoImage
{
    photoImage = [[UIImage alloc] init];
    
    if (photoImage != newPhotoImage) {
        photoImage = newPhotoImage;
    }
}

- (void)setVideoURL:(NSURL *)newVideoURL
{
    videoURL = [[NSURL alloc] init];
    
    if (videoURL != newVideoURL) {
        videoURL = newVideoURL;
    }
    
    NSLog(@"Video URL: %@", videoURL);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Helper Methods

- (void)setUpUIElements
{
    [self configureNavigationBar];
    [self configurePhotoImageView];
    [self configureCommentTextView];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [descriptionLabel setText:NSLocalizedString(@"description", nil)];
    [tagPeopleLabel setText:NSLocalizedString(@"tagPeople", nil)];
}

- (void)configureTaggedPeopleTextView
{
    if ([taggedPeople count] > 0) {
        NSString *fullText = @"";
        NSString *localizedTaggedString = NSLocalizedString(@"tagged", nil);
        for (NSDictionary *friend in taggedPeople) {
            NSString *name = [friend objectForKey:@"name"];
            fullText = [fullText stringByAppendingString:[NSString stringWithFormat:@", %@", name]];
        }
        fullText = [fullText substringFromIndex:1];
        
        [taggedPeopleTextView setText:[NSString stringWithFormat:@"%@: %@", localizedTaggedString, fullText]];
        [taggedPeopleTextView setTextColor:[UIColor colorWithRed:93.0/255.0
                                                           green:124.0/255.0
                                                            blue:173.0/255.0
                                                           alpha:1.0]];
    }
}

- (void)configureCommentTextView
{
    [commentTextView setPlaceholder:NSLocalizedString(@"toComment", nil)];
    [commentTextView setIsScrollable:YES];
    [commentTextView setMinNumberOfLines:1];
    [commentTextView setMaxNumberOfLines:8];
    [commentTextView setDelegate:self];
}

- (void)configurePhotoImageView
{
    if (isVideo)
    {
        [videoImageView setHidden:NO];
        
        double x = (photoImage.size.width - 320.0) / 2.0;
        double y = (photoImage.size.height - 320.0) / 2.0;
        
        CGRect cropRect = CGRectMake(x, y, 320.0, 320.0);
        
        CGImageRef imageRef = CGImageCreateWithImageInRect([photoImage CGImage], cropRect);
        
        UIImage *cropped = [UIImage imageWithCGImage:imageRef];
        
        CGImageRelease(imageRef);
        
     
        [photoImageView setImage:cropped];
    } else
    {
        [videoImageView setHidden:YES];
        
//        double x = (photoImage.size.width - 320.0) / 2.0;
//        double y = (photoImage.size.height - 320.0) / 2.0;
//        
//        CGRect cropRect = CGRectMake(x, y, 320.0, 320.0);
//        
//        CGImageRef imageRef = CGImageCreateWithImageInRect([photoImage CGImage], cropRect);
//        
//        UIImage *cropped = [UIImage imageWithCGImage:imageRef];
//        CGImageRelease(imageRef);
        
        [photoImageView setImage:photoImage];
        [photoImageView setContentMode:UIViewContentModeScaleToFill];
        [photoImageView setClipsToBounds:YES];
    }
}

- (void)configureNavigationBar
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button CreatePost"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button CreatePost"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"
                                                                                               size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"newPost", nil)];
    [doneButton setTitle:NSLocalizedString(@"Done", nil)];
}

- (void)updateProgress:(float)progress
{
    NSLog(@"update progress method triggered");
    NSLog(@"progress: %.2f", progress);
    [progressView setProgress:progress];
}

- (IBAction)goBack:(id)sender
{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - Text View Delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView *currentView in self.view.subviews) {
        if ([currentView isKindOfClass:[UITextField class]]) {
            [currentView resignFirstResponder];
        }
        
        else if ([currentView isKindOfClass:[UITextView class]])
        {
            [currentView resignFirstResponder];
        }
    }
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    NSLog(@"growint text view method");
    
    float diff = (growingTextView.frame.size.height - height);
  
    CGRect photoFrame = photoImageView.frame;
    photoFrame.origin.y -= diff;
    photoImageView.frame = photoFrame;
}

#pragma mark - IBAction Methods

- (IBAction)sendPost:(id)sender
{
    [commentTextView resignFirstResponder];
    [doneButton setEnabled:NO];
    
    NSLog(@"send Post");
    
    NSData *data = [commentTextView.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *description = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *taggedPeopleIDs = @"";
    
    if ([taggedPeople count] > 0) {
        for (NSDictionary *friend in taggedPeople) {
            NSString *name = [friend objectForKey:@"id"];
            taggedPeopleIDs = [taggedPeopleIDs stringByAppendingString:[NSString stringWithFormat:@", %@", name]];
        }
        taggedPeopleIDs = [taggedPeopleIDs substringFromIndex:2];
    }
    
    NSData *IDsData = [taggedPeopleIDs dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *IDs = [[NSString alloc] initWithData:IDsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    [postData setObject:description forKey:@"description"];
    [postData setObject:IDs forKey:@"marks"];
    
    if (isVideo) {
        
        NSLog(@"video url is %@", videoURL);
        
        NSString *videoURLString = [NSString stringWithFormat:@"%@", videoURL];
        NSLog(@"videoURL string is %@", videoURLString);
        
        [postData setObject:videoURLString forKey:@"videoURL"];
        [postData setObject:@"" forKey:@"photoURL"];
        
        NSLog(@"description is \n%@", description);
        NSLog(@"description in postData \n%@", [postData objectForKey:@"description"]);
        
        [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"postData"];
        
        NSLog(@"nsuser defaults post data is %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"postData"]);
        [progressView setHidden:NO];
        
        [self animateLoadingIndicator];
        
        [API sendPostWithURL:videoURL];
    }
    else if (isVideo == NO)
    {
        // content is photo

        photoImage = [self squareImageFromImage:photoImage scaledToSize:320];

        // if we need to save photo in gallery
        // UIImageWriteToSavedPhotosAlbum(photoImage, self, nil, nil);

        NSData *imageData = UIImageJPEGRepresentation(photoImage, 0.7);
        
        [postData setObject:imageData forKey:@"imageData"];
        [postData setObject:@"" forKey:@"videoURL"];
        
        NSLog(@"description is \n%@", description);
        NSLog(@"description in postData \n%@", [postData objectForKey:@"description"]);
        
        [[NSUserDefaults standardUserDefaults] setObject:postData forKey:@"postData"];
        [progressView setHidden:NO];
        
        [self animateLoadingIndicator];
        
        [API sendPhotoPost];
    }
    else
    {
        NSLog(@"something wrong with photo/video");
        [postData setObject:@"" forKey:@"imageData"];
        [postData setObject:@"" forKey:@"videoURL"];
    }
}

- (void)animateLoadingIndicator
{
    NSLog(@"animate loading");
    
    if (activityButton == nil)
    {
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [activityIndicator setColor:[UIColor whiteColor]];
        activityButton = [[UIBarButtonItem alloc]initWithCustomView:activityIndicator];
    }
    self.navigationItem.rightBarButtonItem = activityButton;
    [activityIndicator startAnimating];
}

- (void)stopAnimatingLoadingIndicator
{
    NSLog(@"stop animating loading");
    
    [activityIndicator stopAnimating];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)tagPeopleVCDissmissed:(NSDictionary *)taggedFriend
{
    [self addTaggedFriendID:taggedFriend];
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([[segue identifier] isEqualToString:@"showTagPeopleVC"])
     {
         //[(APTagPeopleVC *)[[segue destinationViewController] rootViewController] setTagPeopleDelegate:self];
         [(APTagPeopleVC *)[[[segue destinationViewController] viewControllers] objectAtIndex:0] setTagPeopleDelegate:self];
     }
 }

@end
