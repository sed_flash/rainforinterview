//
//  APFeedVC.m
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APFeedVC.h"
#import "UIViewController+ScrollingNavbar.h"
#import "UIColor+CustomColor.h"

@interface APFeedVC ()
{
    NSInteger tapCount;
    NSUInteger tappedRow;
    float lastContentOffset;
    // Флаг для корректного отображения страницы Поиска Друзей, если необходимо
    BOOL ifGo;
    int countForScrollNews;
}
@end

@implementation APFeedVC

@synthesize tapTimer;

static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static CGFloat activityViewBottomMargin = 84.0f;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ifGo = NO;
    
    [[self navigationItem] setHidesBackButton:YES];
    [self setupUIElements];
    
    postsArray = [[NSArray alloc] init];
    selectedPost = [[NSDictionary alloc] init];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ifGo = NO;
    countForScrollNews = 0;
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:NO];
//    [[[self tabBarController] tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"SelectedTabSmall"]];
    
    NSLog(@"view will appear");
    
    API = [[APServerAPI alloc] init];
    isNewPostsDownloaded = NO;
    
    [self followScrollView:aTableView usingTopConstraint:top];
    [self configureNavigationBar];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedSuccess)
                                                 name:@"FeedSuccess"
                                               object:API];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedFailed)
                                                 name:@"FeedFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePostFromNotification:)
                                                 name:@"postLiked"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedChanged)
                                                 name:@"FeedChanged"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollNewsToTop:)
                                                 name:@"newsNeedToScrollToTopNotification"
                                               object:nil];
    
    [self scrollNewsToTop:nil];
    
    [self checkFriendsCount];
    [self checkForNewPosts ];
    
    [self removeActivityIndicator];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    lastContentOffset = aTableView.contentOffset.y;
    
    [self showNavBarAnimated:NO];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"FeedSuccess"
//                                                  object:API];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"FeedFailed"
//                                                  object:API];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"postLiked"
//                                                  object:API];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NetworkError"
//                                                  object:API];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"ServerWorkerDidEndWithError"
//                                                  object:API];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"FeedChanged"
//                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkFriendsCount
{
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(checkForNewPosts)
             forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *controller = [[UITableViewController alloc] init];
    controller.tableView = aTableView;
    controller.refreshControl = refreshControl;
    
    if (aTableView.numberOfSections == 0)
    {
        [API getFeedWithOffset:0 andLimit:10];
        [self showActivityIndicator];
    }
}

- (void)feedChanged
{
    NSLog(@"feed changed!");
    [self checkForNewPosts];
}

- (void)updatePostFromNotification:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"postLiked"]) {
        
        NSMutableArray *postsMutArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *post in postsArray) {
            
            NSDictionary *currentPost = [post objectForKey:@"post"];
            NSString *currentPostID = [[currentPost objectForKey:@"id"] stringValue];
            NSString *likedPostID = [[[notification userInfo] objectForKey:@"id"] stringValue];
            
            if ([currentPostID isEqualToString:likedPostID])
            {
                NSLog(@"YES");
                NSMutableDictionary *mutPost = [post mutableCopy];
                [mutPost setObject:[notification userInfo] forKey:@"post"];
                [postsMutArray addObject:[mutPost copy]];
            } else {
                [postsMutArray addObject:post];
            }
        }
        
        postsArray = [postsMutArray copy];
    }
}

- (void)setDataSource
{
    NSArray *newPostsArray = [[[NSUserDefaults standardUserDefaults] objectForKey:@"feed"]
                              objectForKey:@"posts"];
    
    if (newPostsArray.count)
    {
        allPostsCount = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"feed"]
                          objectForKey:@"count_posts"] integerValue];
        
        NSMutableArray *tempArray = [postsArray mutableCopy];
        
        if (isNewPostsDownloaded) {
            for (NSDictionary *post in newPostsArray) {
                [tempArray addObject:post];
            }
            postsArray = [tempArray copy];
            
        } else {
            postsArray = newPostsArray;
        }
        
        [refreshControl endRefreshing];
        [aTableView reloadData];
        [aTableView reloadRowsAtIndexPaths:[aTableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    }
    else
    {
      if(!ifGo)
      {
          ifGo = YES;
          [self performSegueWithIdentifier:@"goToNoFriendsVC"
                                    sender:self];
      }
    }
}

#pragma mark - Activity

- (void)showActivityIndicator
{
    [aTableView setHidden:YES];
    activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    [activityIndicatorView setColor:[UIColor whiteColor]];
    [activityIndicatorView setCenter:self.view.center];
    CGRect activityIndicatorFrame = activityIndicatorView.frame;
    activityIndicatorFrame.origin.y -= activityViewBottomMargin;
    activityIndicatorView.frame = activityIndicatorFrame;
    [[self view] addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)showPaginationIndicator
{
    paginatorActivityView = [[UIActivityIndicatorView alloc] init];
    [paginatorActivityView setColor:[UIColor whiteColor]];
    [paginatorActivityView setFrame:CGRectMake(self.view.center.x - 50, aTableView.frame.origin.y + aTableView.frame.size.height, 100, 100)];
    [self.view addSubview:paginatorActivityView];
    [paginatorActivityView startAnimating];
}

- (void)removeActivityIndicator
{
    [activityIndicatorView stopAnimating];
    [activityIndicatorView removeFromSuperview];
    [aTableView setHidden:NO];
}

- (void)removePaginationIndicator
{
    [paginatorActivityView stopAnimating];
    [paginatorActivityView removeFromSuperview];
}

#pragma mark - Success and Failed Methods

- (void)feedSuccess
{
    [self removeActivityIndicator];
    postsDownloading = NO;
    
       [self setDataSource];
    
    
}

- (void)feedFailed
{
    isNewPostsDownloaded = NO;
    postsDownloading = NO;
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:serverAlert])
    {
        // show failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Helper Methods

- (void)setupUIElements
{
    for (UITabBarItem *item in [[[self tabBarController] tabBar] items]) {
        [item setImage:[[item image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

        
        [[[self tabBarController] tabBar] setTintColor:[UIColor colorWithRed:102.0/255.0
                                                                       green:153.0/255.0
                                                                        blue:255.0/255.0
                                                                       alpha:1.0]];
    }
    
    [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setSelectedImage:[UIImage imageNamed:@"home_on_tab_bar"]];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setSelectedImage:[UIImage imageNamed:@"top_on_tab_bar"]];
    
    [[[[[self tabBarController] tabBar] items] objectAtIndex:3] setSelectedImage:[UIImage imageNamed:@"like_on_tab_bar"]];
    
    [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setSelectedImage:[UIImage imageNamed:@"profil_on_tab_bar"]];
    
    [self configureNavigationBar];
}

- (void)configureNavigationBar
{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
 
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:18.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
    [[self navigationItem] setTitle:NSLocalizedString(@"feed", nil)];
    [[[self navigationController] navigationBar] setBarTintColor:[UIColor colorWithRed:93.0/255.0 green:124.0/255.0 blue:173.0/255.0 alpha:1.0]];
    [[[self navigationController] navigationBar] setHidden:NO];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

- (void)scrollViewDidScroll: (UIScrollView*)scroll
{
    @try {
        CGFloat currentOffset = scroll.contentOffset.y;
        CGFloat maximumOffset = scroll.contentSize.height - scroll.frame.size.height;
        
        // Change 10.0 to adjust the distance from bottom
        if (maximumOffset - currentOffset < 0 && postsDownloading == NO) {
            [self getOlderPosts];
        }
    }
    @catch (NSException *exception) {
        
    }
   
    // UITableView only moves in one direction, y axis
   
}

- (void)getOlderPosts
{
    NSLog(@"get new posts");
    NSLog(@"posts count = %ld", (long)[aTableView numberOfSections]);
    NSLog(@"all posts count = %ld", (long)allPostsCount);
    if ([aTableView numberOfSections] == allPostsCount) {
        NSLog(@"All posts downloaded. Nothing to do.");
    } else {
        isNewPostsDownloaded = YES;
        postsDownloading = YES;
        [self showPaginationIndicator];
        [API getFeedWithOffset:[aTableView numberOfSections] andLimit:10];
    }
}

- (void)checkForNewPosts
{
    [API getFeedWithOffset:0 andLimit:10];
}


#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0f;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.view.frame.size.width < 414.0f) {
        if (self.view.frame.size.width == 320.0f) {
            return 360.0f;
        } else {
            return 420.0f;
        }
    } else {
        return 440.0f;
    }
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [postsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APNewsCell *cell = (APNewsCell *)[tableView dequeueReusableCellWithIdentifier:@"NewsCell"];

    NSLog(@"posts array count = %lu", (unsigned long)[postsArray count]);
    NSDictionary *currentPost = [postsArray objectAtIndex:[indexPath section]];
    NSLog(@"index path = %@", indexPath);
    
    NSLog(@"currentPost = %@", currentPost);
    
    [cell setDelegate:self];
    [cell setCurrentPost:currentPost];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //checking for double taps here
    if(tapCount == 1 && tapTimer != nil && tappedRow == indexPath.section){
        //double tap - Put your double tap code here
        
        NSLog(@"double tap!");
        NSLog(@"like this post!");
        [tapTimer invalidate];
        [self setTapTimer:nil];
        APNewsCell *cell = (APNewsCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell like:cell];
        tapCount = 0;
        tappedRow = -1;
    }
    else if(tapCount == 0){
        //This is the first tap. If there is no tap till tapTimer is fired, it is a single tap
        
        tapCount = tapCount + 1;
        tappedRow = indexPath.section;
        [self setTapTimer:[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(tapTimerFired:) userInfo:nil repeats:NO]];
    }
    else if(tappedRow != indexPath.row){
        //tap on new row
        tapCount = 0;
        if(tapTimer != nil){
            [tapTimer invalidate];
            [self setTapTimer:nil];
        }
    }
}

- (void)tapTimerFired:(NSTimer *)aTimer{
    //timer fired, there was a single tap on indexPath.row = tappedRow
    
    NSLog(@"select");
    selectedPost = [postsArray objectAtIndex:tappedRow];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self performSegueWithIdentifier:@"showPostFromFeed" sender:self];
    
    if(tapTimer != nil){
        tapCount = 0;
        tappedRow = -1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"APAuthorView"
                                                             owner:self
                                                           options:nil];
    APAuthorView *headerView = (APAuthorView *) [topLevelObjects objectAtIndex:0];
    
    if ([postsArray count] > 0)
    {
        NSDictionary *post = [postsArray objectAtIndex:section];
        NSDictionary *author = [post objectForKey:@"user"];
        NSDictionary *currentPost = [post objectForKey:@"post"];
        NSDate *createdAt;
        
        if ([[currentPost objectForKey:@"created_at"] isKindOfClass:[NSString class]]) {
            NSString *createdAtString = [currentPost objectForKey:@"created_at"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
            createdAt = [dateFormatter dateFromString:createdAtString];
        } else {
            createdAt = [currentPost objectForKey:@"created_at"];
        }
        NSString *timeAgo = [createdAt timeAgo];
        
        NSLog(@"timeAgo - %@", timeAgo);
        
        [headerView setTimeAgo:timeAgo];
        [headerView setAuthor:author];
        
        [[headerView button] setTag:section];
        [[headerView button] addTarget:self
                                action:@selector(showProfile:)
                      forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return headerView;
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)scrollNewsToTop:(NSNotification *)notification {
    
    if (countForScrollNews == 0 || [notification.object boolValue]) {
        
        countForScrollNews ++;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
        float currentOffsetWithScrollToBottom = aTableView.contentOffset.y - 45;
        float currentOffsetWithScrollToTop    = aTableView.contentOffset.y;
    
        @try {
            if (lastContentOffset == 0 ||
                (lastContentOffset != currentOffsetWithScrollToTop &&
                 lastContentOffset != currentOffsetWithScrollToBottom) )
                    [aTableView scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
        
            lastContentOffset = 0;
        } @catch (NSException *exception) {
        NSLog(@"Can't scroll to 0 index, exception is: %@", exception);
        }
    }
}


#pragma mark - APNewsCell Delegate Method

- (void)commentPost:(NSDictionary *)commentingPost
{
    selectedPost = commentingPost;
    [self performSegueWithIdentifier:@"commentFromFeed" sender:self];
}

- (IBAction)showProfile:(id)sender
{
    NSDictionary *post = [postsArray objectAtIndex:[sender tag]];
    NSDictionary *user = [post objectForKey:@"user"];

    selectedUserMail = [user objectForKey:@"email"];
    [self performSegueWithIdentifier:@"showUserFromFeed" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showPostFromFeed"])
    {
        NSLog(@"segue");
        [[segue destinationViewController] setCurrentPost:[selectedPost objectForKey:@"post"]];
        [[segue destinationViewController] setAuthor:[selectedPost objectForKey:@"user"]];
    }
    else if ([[segue identifier] isEqualToString:@"showUserFromFeed"])
    {
        NSLog(@"segue");
        if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"] isEqualToString:selectedUserMail]) {
            [[segue destinationViewController] setUserOwns:YES];
        } else {
            [[segue destinationViewController] setUserOwns:NO];
        }
        
        [[segue destinationViewController] setUserMail:selectedUserMail];
    }
    else if ([[segue identifier] isEqualToString:@"commentFromFeed"])
    {
        NSLog(@"comment segue");
        [[segue destinationViewController] setPostID:[[selectedPost objectForKey:@"id"] stringValue]];
    }
    else
    {
        NSLog(@"segue is %@", segue.identifier);
    }
}

@end
