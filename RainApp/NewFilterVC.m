//
//  NewFilterVC.m
//  Rain
//
//  Created by EgorMac on 07/08/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "NewFilterVC.h"
#import "GPUImage.h"

@interface NewFilterVC ()

@end

@implementation NewFilterVC

@synthesize filterID;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    originalImage = [UIImage imageNamed:@"photoForFilters.png"];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [self setFiltersArray];
    [self setCurrentFilter];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
//    [scrollViewConstraint setConstant:self.view.bounds.size.height];
    
    CGSize size = CGSizeMake(scrollView.contentSize.width,
                             340 + [[currentFilter objectForKey:@"sliders"] count] * 200);
    [scrollView setContentSize:size];
    NSLog(@"content size height: %f", scrollView.contentSize.height);
    NSLog(@"scroll view frame height: %f", scrollView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self updateViewConstraints];
}

- (void)setCurrentFilter
{
    currentFilter = [[NSMutableDictionary alloc] init];
    NSMutableArray *sliders = [[NSMutableArray alloc] init];
    
    if ([filterID isEqualToString:@"GPUImageBrightnessFilter"]) {
        NSMutableDictionary *brightness = [[NSMutableDictionary alloc] init];
        [brightness setObject:@"brightness" forKey:@"name"];
        [brightness setObject:[NSNumber numberWithFloat:-1.0] forKey:@"min"];
        [brightness setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [brightness setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:brightness];
    }
    else if ([filterID isEqualToString:@"GPUImageExposureFilter"])
    {
        NSMutableDictionary *exposure = [[NSMutableDictionary alloc] init];
        [exposure setObject:@"exposure" forKey:@"name"];
        [exposure setObject:[NSNumber numberWithFloat:-10.0] forKey:@"min"];
        [exposure setObject:[NSNumber numberWithFloat:10.0] forKey:@"max"];
        [exposure setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:exposure];
    }
    else if ([filterID isEqualToString:@"GPUImageContrastFilter"])
    {
        NSMutableDictionary *contrast = [[NSMutableDictionary alloc] init];
        [contrast setObject:@"contrast" forKey:@"name"];
        [contrast setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [contrast setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [contrast setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:contrast];
    }
    else if ([filterID isEqualToString:@"GPUImageSaturationFilter"])
    {
        NSMutableDictionary *saturation = [[NSMutableDictionary alloc] init];
        [saturation setObject:@"saturation" forKey:@"name"];
        [saturation setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [saturation setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [saturation setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:saturation];
    }
    else if ([filterID isEqualToString:@"GPUImageGammaFilter"])
    {
        NSMutableDictionary *gamma = [[NSMutableDictionary alloc] init];
        [gamma setObject:@"gamma" forKey:@"name"];
        [gamma setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [gamma setObject:[NSNumber numberWithFloat:3.0] forKey:@"max"];
        [gamma setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:gamma];
    }
    else if ([filterID isEqualToString:@"GPUImageHueFilter"])
    {
        NSMutableDictionary *hue = [[NSMutableDictionary alloc] init];
        [hue setObject:@"hue" forKey:@"name"];
        [hue setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [hue setObject:[NSNumber numberWithFloat:360.0] forKey:@"max"];
        [hue setObject:[NSNumber numberWithFloat:90.0] forKey:@"default"];
        [sliders addObject:hue];
    }
    else if ([filterID isEqualToString:@"GPUImageHighlightShadowFilter"])
    {
        NSMutableDictionary *shadows = [[NSMutableDictionary alloc] init];
        [shadows setObject:@"shadows" forKey:@"name"];
        [shadows setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [shadows setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [shadows setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:shadows];
        
        NSMutableDictionary *highlights = [[NSMutableDictionary alloc] init];
        [highlights setObject:@"highlights" forKey:@"name"];
        [highlights setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [highlights setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [highlights setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:highlights];
    }
    else if ([filterID isEqualToString:@"GPUImageLookupFilter"])
    {

    }
    else if ([filterID isEqualToString:@"GPUImageAmatorkaFilter"])
    {
        
    }
    else if ([filterID isEqualToString:@"GPUImageMissEtikateFilter"])
    {
        
    }
    else if ([filterID isEqualToString:@"GPUImageSoftEleganceFilter"])
    {
        
    }
    else if ([filterID isEqualToString:@"GPUImageColorInvertFilter"])
    {
        
    }
    else if ([filterID isEqualToString:@"GPUImageGrayscaleFilter"])
    {
        
    }
    else if ([filterID isEqualToString:@"GPUImageMonochromeFilter"])
    {
        NSMutableDictionary *intensity = [[NSMutableDictionary alloc] init];
        [intensity setObject:@"intensity" forKey:@"name"];
        [intensity setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [intensity setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [intensity setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:intensity];
    }
    else if ([filterID isEqualToString:@"GPUImageHazeFilter"])
    {
        NSMutableDictionary *distance = [[NSMutableDictionary alloc] init];
        [distance setObject:@"distance" forKey:@"name"];
        [distance setObject:[NSNumber numberWithFloat:-3.0] forKey:@"min"];
        [distance setObject:[NSNumber numberWithFloat:3.0] forKey:@"max"];
        [distance setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:distance];
        
        NSMutableDictionary *slope = [[NSMutableDictionary alloc] init];
        [slope setObject:@"slope" forKey:@"name"];
        [slope setObject:[NSNumber numberWithFloat:-3.0] forKey:@"min"];
        [slope setObject:[NSNumber numberWithFloat:3.0] forKey:@"max"];
        [slope setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:slope];
    }
    else if ([filterID isEqualToString:@"GPUImageSepiaFilter"])
    {
        NSMutableDictionary *intensity = [[NSMutableDictionary alloc] init];
        [intensity setObject:@"intensity" forKey:@"name"];
        [intensity setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [intensity setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [intensity setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:intensity];
    }
    else if ([filterID isEqualToString:@"GPUImageOpacityFilter"])
    {
        NSMutableDictionary *opacity = [[NSMutableDictionary alloc] init];
        [opacity setObject:@"opacity" forKey:@"name"];
        [opacity setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [opacity setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [opacity setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:opacity];
    }
    else if ([filterID isEqualToString:@"GPUImageLuminanceThresholdFilter"])
    {
        NSMutableDictionary *threshold = [[NSMutableDictionary alloc] init];
        [threshold setObject:@"threshold" forKey:@"name"];
        [threshold setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [threshold setObject:[NSNumber numberWithFloat:1.0] forKey:@"max"];
        [threshold setObject:[NSNumber numberWithFloat:0.5] forKey:@"default"];
        [sliders addObject:threshold];
    }
    
    else if ([filterID isEqualToString:@"GPUImageAdaptiveThresholdFilter"])
    {
        NSMutableDictionary *blurRadiusInPixels = [[NSMutableDictionary alloc] init];
        [blurRadiusInPixels setObject:@"blurRadiusInPixels" forKey:@"name"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:100.0] forKey:@"max"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:4.0] forKey:@"default"];
        [sliders addObject:blurRadiusInPixels];
    }
    
    else if ([filterID isEqualToString:@"GPUImageAverageLuminanceThresholdFilter"])
    {
        NSMutableDictionary *thresholdMultiplier = [[NSMutableDictionary alloc] init];
        [thresholdMultiplier setObject:@"thresholdMultiplier" forKey:@"name"];
        [thresholdMultiplier setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [thresholdMultiplier setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [thresholdMultiplier setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:thresholdMultiplier];
    }
    
    else if ([filterID isEqualToString:@"GPUImageSharpenFilter"])
    {
        NSMutableDictionary *sharpness = [[NSMutableDictionary alloc] init];
        [sharpness setObject:@"sharpness" forKey:@"name"];
        [sharpness setObject:[NSNumber numberWithFloat:-4.0] forKey:@"min"];
        [sharpness setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [sharpness setObject:[NSNumber numberWithFloat:0.0] forKey:@"default"];
        [sliders addObject:sharpness];
    }
    
    else if ([filterID isEqualToString:@"GPUImageUnsharpMaskFilter"])
    {
        NSMutableDictionary *blurRadiusInPixels = [[NSMutableDictionary alloc] init];
        [blurRadiusInPixels setObject:@"blurRadiusInPixels" forKey:@"name"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:100.0] forKey:@"max"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:4.0] forKey:@"default"];
        [sliders addObject:blurRadiusInPixels];
        
        NSMutableDictionary *intensity = [[NSMutableDictionary alloc] init];
        [intensity setObject:@"intensity" forKey:@"name"];
        [intensity setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [intensity setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [intensity setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:intensity];
    }
    
    else if ([filterID isEqualToString:@"GPUImageGaussianBlurFilter"])
    {
        NSMutableDictionary *texelSpacingMultiplier = [[NSMutableDictionary alloc] init];
        [texelSpacingMultiplier setObject:@"texelSpacingMultiplier" forKey:@"name"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:texelSpacingMultiplier];
        
        NSMutableDictionary *blurRadiusInPixels = [[NSMutableDictionary alloc] init];
        [blurRadiusInPixels setObject:@"blurRadiusInPixels" forKey:@"name"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:2.0] forKey:@"default"];
        [sliders addObject:blurRadiusInPixels];
        
        NSMutableDictionary *blurRadiusAsFractionOfImageWidth = [[NSMutableDictionary alloc] init];
        [blurRadiusAsFractionOfImageWidth setObject:@"blurRadiusAsFractionOfImageWidth" forKey:@"name"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:0.1] forKey:@"min"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurRadiusAsFractionOfImageWidth];
        
        NSMutableDictionary *blurRadiusAsFractionOfImageHeight = [[NSMutableDictionary alloc] init];
        [blurRadiusAsFractionOfImageHeight setObject:@"blurRadiusAsFractionOfImageHeight" forKey:@"name"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:0.1] forKey:@"min"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurRadiusAsFractionOfImageHeight];

        NSMutableDictionary *blurPasses = [[NSMutableDictionary alloc] init];
        [blurPasses setObject:@"blurPasses" forKey:@"name"];
        [blurPasses setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurPasses setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [blurPasses setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurPasses];
    }
    
    else if ([filterID isEqualToString:@"GPUImageBoxBlurFilter"])
    {
        NSMutableDictionary *texelSpacingMultiplier = [[NSMutableDictionary alloc] init];
        [texelSpacingMultiplier setObject:@"texelSpacingMultiplier" forKey:@"name"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:0.0] forKey:@"min"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [texelSpacingMultiplier setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:texelSpacingMultiplier];
        
        NSMutableDictionary *blurRadiusInPixels = [[NSMutableDictionary alloc] init];
        [blurRadiusInPixels setObject:@"blurRadiusInPixels" forKey:@"name"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:2.0] forKey:@"default"];
        [sliders addObject:blurRadiusInPixels];
        
        NSMutableDictionary *blurRadiusAsFractionOfImageWidth = [[NSMutableDictionary alloc] init];
        [blurRadiusAsFractionOfImageWidth setObject:@"blurRadiusAsFractionOfImageWidth" forKey:@"name"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:0.1] forKey:@"min"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [blurRadiusAsFractionOfImageWidth setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurRadiusAsFractionOfImageWidth];
        
        NSMutableDictionary *blurRadiusAsFractionOfImageHeight = [[NSMutableDictionary alloc] init];
        [blurRadiusAsFractionOfImageHeight setObject:@"blurRadiusAsFractionOfImageHeight" forKey:@"name"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:0.1] forKey:@"min"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [blurRadiusAsFractionOfImageHeight setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurRadiusAsFractionOfImageHeight];
        
        NSMutableDictionary *blurPasses = [[NSMutableDictionary alloc] init];
        [blurPasses setObject:@"blurPasses" forKey:@"name"];
        [blurPasses setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurPasses setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [blurPasses setObject:[NSNumber numberWithFloat:1.0] forKey:@"default"];
        [sliders addObject:blurPasses];
    }
    
    else if ([filterID isEqualToString:@"GPUImageTiltShiftFilter"])
    {
        NSMutableDictionary *blurRadiusInPixels = [[NSMutableDictionary alloc] init];
        [blurRadiusInPixels setObject:@"blurRadiusInPixels" forKey:@"name"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:1.0] forKey:@"min"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:20.0] forKey:@"max"];
        [blurRadiusInPixels setObject:[NSNumber numberWithFloat:7.0] forKey:@"default"];
        [sliders addObject:blurRadiusInPixels];
        
        NSMutableDictionary *topFocusLevel = [[NSMutableDictionary alloc] init];
        [topFocusLevel setObject:@"topFocusLevel" forKey:@"name"];
        [topFocusLevel setObject:[NSNumber numberWithFloat:0.4] forKey:@"min"];
        [topFocusLevel setObject:[NSNumber numberWithFloat:2.0] forKey:@"max"];
        [topFocusLevel setObject:[NSNumber numberWithFloat:0.4] forKey:@"default"];
        [sliders addObject:topFocusLevel];
        
        NSMutableDictionary *bottomFocusLevel = [[NSMutableDictionary alloc] init];
        [bottomFocusLevel setObject:@"bottomFocusLevel" forKey:@"name"];
        [bottomFocusLevel setObject:[NSNumber numberWithFloat:2.1] forKey:@"min"];
        [bottomFocusLevel setObject:[NSNumber numberWithFloat:10.0] forKey:@"max"];
        [bottomFocusLevel setObject:[NSNumber numberWithFloat:2.1] forKey:@"default"];
        [sliders addObject:bottomFocusLevel];
        
        NSMutableDictionary *focusFallOffRate = [[NSMutableDictionary alloc] init];
        [focusFallOffRate setObject:@"focusFallOffRate" forKey:@"name"];
        [focusFallOffRate setObject:[NSNumber numberWithFloat:0.1] forKey:@"min"];
        [focusFallOffRate setObject:[NSNumber numberWithFloat:4.0] forKey:@"max"];
        [focusFallOffRate setObject:[NSNumber numberWithFloat:0.2] forKey:@"default"];
        [sliders addObject:focusFallOffRate];
    }
    
    
    
    [currentFilter setObject:filterID forKey:@"name"];
    [currentFilter setObject:sliders forKey:@"sliders"];
    
    NSLog(@"currentFilter: \n%@", currentFilter);
    
    [self configureUIFromDictionary:currentFilter];
}

- (void)configureUIFromDictionary:(NSMutableDictionary *)filter
{
    for (int i = 0; i < [[filter objectForKey:@"sliders"] count]; i++) {
        
        int sliderTag = i + 1;
        int minLabelTag = sliderTag + 100;
        int maxLabelTag = sliderTag + 101;
        int currentLabelTag = sliderTag + 102;
        
        NSDictionary *currentSlider = [[filter objectForKey:@"sliders"] objectAtIndex:i];
        
        float min = [[currentSlider objectForKey:@"min"] floatValue];
        float max = [[currentSlider objectForKey:@"max"] floatValue];
        float defaultValue = [[currentSlider objectForKey:@"default"] floatValue];
        
        CGFloat nameLabelX = 40.0f;
        CGFloat nameLabelHeight = 24.0f;
        CGFloat nameLabelY = 332 + (i * (nameLabelHeight + 16 + 50));
        CGFloat nameLabelWidth = 320 - 80;
        
        CGRect labelFrame = CGRectMake(nameLabelX,
                                       nameLabelY,
                                       nameLabelWidth,
                                       nameLabelHeight);
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:labelFrame];
        [nameLabel setText:[currentSlider objectForKey:@"name"]];
        [nameLabel setTextColor:[UIColor blackColor]];
        [nameLabel setTag:currentLabelTag];
        [scrollView addSubview:nameLabel];
        
        UISlider *slider = [[UISlider alloc] init];
        
        CGRect sliderFrame = nameLabel.frame;
        sliderFrame.origin.y = sliderFrame.origin.y + nameLabelHeight + 10;
        slider.frame = sliderFrame;
        [slider setTag:sliderTag];
        
        [scrollView addSubview:slider];
        
        [slider setMinimumValue:min];
        [slider setMaximumValue:max];
        [slider setValue:defaultValue];
        
        UILabel *minLabel = [[UILabel alloc] init];
        CGRect minLabelFrame = slider.frame;
        minLabelFrame.origin.x = 4;
        minLabelFrame.size.width = 50;
        [minLabel setFrame:minLabelFrame];
        [minLabel setTag:minLabelTag];
        [scrollView addSubview:minLabel];
        
        [minLabel setText:[NSString stringWithFormat:@"%.2f", min]];
        [minLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
        
        UILabel *maxLabel = [[UILabel alloc] init];
        CGRect maxLabelFrame = slider.frame;
        maxLabelFrame.origin.x = maxLabelFrame.origin.x + slider.frame.size.width + 4;
        maxLabelFrame.size.width = 50;
        [maxLabel setFrame:maxLabelFrame];
        [maxLabel setTag:maxLabelTag];
        [scrollView addSubview:maxLabel];
        
        [maxLabel setText:[NSString stringWithFormat:@"%.2f", max]];
        [maxLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
        
        [slider addTarget:self action:@selector(updateValues) forControlEvents:UIControlEventValueChanged];
    }
    
    [self performSelector:@selector(updateViewConstraints) withObject:self afterDelay:1.0];
}

- (void)updateValues
{
    for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
        
        NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
        NSString *filterName = [currentSliderDictionary objectForKey:@"name"];
        
        int sliderTag = i + 1;
        int currentLabelTag = sliderTag + 102;
        
        UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
        UILabel *nameLabel = (UILabel *)[scrollView viewWithTag:currentLabelTag];
        
        float value = currentSlider.value;
        [nameLabel setText:[NSString stringWithFormat:@"%@ = %.2f", filterName, value]];
    }
}

- (void)setFiltersArray
{
    filtersArray = [[NSMutableArray alloc] initWithObjects:
                    @"GPUImageBrightnessFilter",
                    @"GPUImageExposureFilter",
                    @"GPUImageContrastFilter",
                    @"GPUImageSaturationFilter",
                    @"GPUImageGammaFilter",
                    @"GPUImageHueFilter",
                    @"GPUImageHighlightShadowFilter",
                    @"GPUImageAmatorkaFilter",
                    @"GPUImageMissEtikateFilter",
                    @"GPUImageSoftEleganceFilter",
                    @"GPUImageColorInvertFilter",
                    @"GPUImageGrayscaleFilter",
                    @"GPUImageMonochromeFilter",
                    @"GPUImageHazeFilter",
                    @"GPUImageSepiaFilter",
                    @"GPUImageOpacityFilter",
                    @"GPUImageLuminanceThresholdFilter",
                    @"GPUImageAdaptiveThresholdFilter",
                    @"GPUImageAverageLuminanceThresholdFilter",
                    @"GPUImageTransformFilter",
                    @"GPUImageCropFilter",
                    @"GPUImageLanczosResamplingFilter",
                    @"GPUImageSharpenFilter",
                    @"GPUImageUnsharpMaskFilter",
                    @"GPUImageGaussianBlurFilter",
                    @"GPUImageBoxBlurFilter",
                    @"GPUImageSingleComponentGaussianBlurFilter",
                    @"GPUImageGaussianSelectiveBlurFilter",
                    @"GPUImageGaussianBlurPositionFilter",
                    @"GPUImageiOSBlurFilter",
                    @"GPUImageMedianFilter",
                    @"GPUImageBilateralFilter",
                    @"GPUImageTiltShiftFilter",
                    @"GPUImage3x3ConvolutionFilter",
                    @"GPUImageSobelEdgeDetectionFilter",
                    @"GPUImagePrewittEdgeDetectionFilter",
                    @"GPUImageThresholdEdgeDetectionFilter",
                    @"GPUImageCannyEdgeDetectionFilter",
                    @"GPUImageHarrisCornerDetectionFilter",
                    @"GPUImageNobleCornerDetectionFilter",
                    @"GPUImageShiTomasiCornerDetectionFilter",
                    @"GPUImageNonMaximumSuppressionFilter",
                    @"GPUImageXYDerivativeFilter",
                    @"GPUImageCrosshairGenerator",
                    @"GPUImageDilationFilter",
                    @"GPUImageRGBDilationFilter",
                    @"GPUImageErosionFilter",
                    @"GPUImageRGBErosionFilter",
                    @"GPUImageOpeningFilter",
                    @"GPUImageRGBOpeningFilter",
                    @"GPUImageClosingFilter",
                    @"GPUImageRGBClosingFilter",
                    @"GPUImageLocalBinaryPatternFilter",
                    @"GPUImageLowPassFilter",
                    @"GPUImageHighPassFilter",
                    @"GPUImageMotionDetector",
                    @"GPUImageHoughTransformLineDetector",
                    @"GPUImageLineGenerator",
                    @"GPUImageMotionBlurFilter",
                    @"GPUImageZoomBlurFilter",
                    @"GPUImagePixellateFilter",
                    @"GPUImagePolarPixellateFilter",
                    @"GPUImagePolkaDotFilter",
                    @"GPUImageHalftoneFilter",
                    @"GPUImageCrosshatchFilter",
                    @"GPUImageSketchFilter",
                    @"GPUImageThresholdSketchFilter",
                    @"GPUImageToonFilter",
                    @"GPUImageSmoothToonFilter",
                    @"GPUImageEmbossFilter",
                    @"GPUImagePosterizeFilter",
                    @"GPUImageSwirlFilter",
                    @"GPUImageBulgeDistortionFilter",
                    @"GPUImagePinchDistortionFilter",
                    @"GPUImageStretchDistortionFilter",
                    @"GPUImageSphereRefractionFilter",
                    @"GPUImageGlassSphereFilter",
                    @"GPUImageVignetteFilter",
                    @"GPUImageKuwaharaFilter",
                    @"GPUImageKuwaharaRadius3Filter",
                    @"GPUImagePerlinNoiseFilter",
                    @"GPUImageCGAColorspaceFilter",
                    @"GPUImageMosaicFilter",
                    @"GPUImageJFAVoronoiFilter",
                    @"GPUImageVoronoiConsumerFilter",
                    nil];

}

- (void)setFilterID:(NSString *)newFilterID
{
    filterID = newFilterID;
    [[self navigationItem] setTitle:filterID];
}

- (IBAction)originalImage:(id)sender
{
    [photo setImage:originalImage];
}

- (IBAction)confirm:(id)sender
{
    if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageBrightnessFilter"]) {
        GPUImageBrightnessFilter *filter = [[GPUImageBrightnessFilter alloc] init];
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"brightness"]) {
                [filter setBrightness:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageHazeFilter"])
    {
        GPUImageHazeFilter *filter = [[GPUImageHazeFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"distance"]) {
                [filter setDistance:value];
            }
            else if ([sliderName isEqualToString:@"slope"])
            {
                [filter setSlope:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageExposureFilter"])
    {
        GPUImageExposureFilter *filter = [[GPUImageExposureFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"exposure"]) {
                [filter setExposure:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageContrastFilter"])
    {
        GPUImageContrastFilter *filter = [[GPUImageContrastFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"contrast"]) {
                [filter setContrast:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageSaturationFilter"])
    {
        GPUImageSaturationFilter *filter = [[GPUImageSaturationFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"saturation"]) {
                [filter setSaturation:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageGammaFilter"])
    {
        GPUImageGammaFilter *filter = [[GPUImageGammaFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"gamma"]) {
                [filter setGamma:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageHueFilter"])
    {
        GPUImageHueFilter *filter = [[GPUImageHueFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"hue"]) {
                [filter setHue:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageHighlightShadowFilter"])
    {
        GPUImageHighlightShadowFilter *filter = [[GPUImageHighlightShadowFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"shadows"]) {
                [filter setShadows:value];
            } else if ([sliderName isEqualToString:@"highlights"])
            {
                [filter setHighlights:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageLookupFilter"])
    {
        GPUImageLookupFilter *filter = [[GPUImageLookupFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageAmatorkaFilter"])
    {
        GPUImageAmatorkaFilter *filter = [[GPUImageAmatorkaFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageMissEtikateFilter"])
    {
        GPUImageMissEtikateFilter *filter = [[GPUImageMissEtikateFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageSoftEleganceFilter"])
    {
        GPUImageSoftEleganceFilter *filter = [[GPUImageSoftEleganceFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageColorInvertFilter"])
    {
        GPUImageColorInvertFilter *filter = [[GPUImageColorInvertFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageGrayscaleFilter"])
    {
        GPUImageGrayscaleFilter *filter = [[GPUImageGrayscaleFilter alloc] init];
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageMonochromeFilter"])
    {
        GPUImageMonochromeFilter *filter = [[GPUImageMonochromeFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"intensity"]) {
                [filter setIntensity:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageSepiaFilter"])
    {
        GPUImageSepiaFilter *filter = [[GPUImageSepiaFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"intensity"]) {
                [filter setIntensity:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageOpacityFilter"])
    {
        GPUImageOpacityFilter *filter = [[GPUImageOpacityFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"opacity"]) {
                [filter setOpacity:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageLuminanceThresholdFilter"])
    {
        GPUImageLuminanceThresholdFilter *filter = [[GPUImageLuminanceThresholdFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"threshold"]) {
                [filter setThreshold:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageAdaptiveThresholdFilter"])
    {
        GPUImageAdaptiveThresholdFilter *filter = [[GPUImageAdaptiveThresholdFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"blurRadiusInPixels"]) {
                [filter setBlurRadiusInPixels:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageAverageLuminanceThresholdFilter"])
    {
        GPUImageAverageLuminanceThresholdFilter *filter = [[GPUImageAverageLuminanceThresholdFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"thresholdMultiplier"]) {
                [filter setThresholdMultiplier:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageSharpenFilter"])
    {
        GPUImageSharpenFilter *filter = [[GPUImageSharpenFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"sharpness"]) {
                [filter setSharpness:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageUnsharpMaskFilter"])
    {
        GPUImageUnsharpMaskFilter *filter = [[GPUImageUnsharpMaskFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"blurRadiusInPixels"]) {
                [filter setBlurRadiusInPixels:value];
            }
            else if ([sliderName isEqualToString:@"intensity"])
            {
                [filter setIntensity:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageGaussianBlurFilter"])
    {
        GPUImageGaussianBlurFilter *filter = [[GPUImageGaussianBlurFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"texelSpacingMultiplier"]) {
                [filter setTexelSpacingMultiplier:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusInPixels"])
            {
                [filter setBlurRadiusInPixels:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusAsFractionOfImageWidth"])
            {
                [filter setBlurRadiusAsFractionOfImageWidth:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusAsFractionOfImageHeight"])
            {
                [filter setBlurRadiusAsFractionOfImageHeight:value];
            }
            else if ([sliderName isEqualToString:@"blurPasses"])
            {
                [filter setBlurPasses:(int)value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageBoxBlurFilter"])
    {
        GPUImageBoxBlurFilter *filter = [[GPUImageBoxBlurFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"texelSpacingMultiplier"]) {
                [filter setTexelSpacingMultiplier:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusInPixels"])
            {
                [filter setBlurRadiusInPixels:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusAsFractionOfImageWidth"])
            {
                [filter setBlurRadiusAsFractionOfImageWidth:value];
            }
            else if ([sliderName isEqualToString:@"blurRadiusAsFractionOfImageHeight"])
            {
                [filter setBlurRadiusAsFractionOfImageHeight:value];
            }
            else if ([sliderName isEqualToString:@"blurPasses"])
            {
                [filter setBlurPasses:(int)value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
    else if ([[currentFilter objectForKey:@"name"] isEqualToString:@"GPUImageTiltShiftFilter"])
    {
        GPUImageTiltShiftFilter *filter = [[GPUImageTiltShiftFilter alloc] init];
        
        for (int i = 0; i < [[currentFilter objectForKey:@"sliders"] count]; i++) {
            
            NSDictionary *currentSliderDictionary = [[currentFilter objectForKey:@"sliders"] objectAtIndex:i];
            NSString *sliderName = [currentSliderDictionary objectForKey:@"name"];
            
            int sliderTag = i + 1;
            
            UISlider *currentSlider = (UISlider *)[scrollView viewWithTag:sliderTag];
            
            float value = currentSlider.value;
            
            if ([sliderName isEqualToString:@"blurRadiusInPixels"]) {
                [filter setBlurRadiusInPixels:value];
            }
            else if ([sliderName isEqualToString:@"topFocusLevel"])
            {
                [filter setTopFocusLevel:value];
            }
            else if ([sliderName isEqualToString:@"bottomFocusLevel"])
            {
                [filter setBottomFocusLevel:value];
            }
            else if ([sliderName isEqualToString:@"focusFallOffRate"])
            {
                [filter setFocusFallOffRate:value];
            }
        }
        
        UIImage *filteredImage = originalImage;
        filteredImage = [filter imageByFilteringImage:filteredImage];
        [photo setImage:filteredImage];
    }
    
}

- (void)takePhoto
{
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    }

}

- (void)chooseFromLibrary
{
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    }
}

- (IBAction)changeImage:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Photo"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose from Library",
                                  nil];
    [actionSheet showFromToolbar:toolbar];
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
            
        case 1:
            [self chooseFromLibrary];
            break;

        default:
            break;
    }
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *pickerImage = [[info objectForKey:UIImagePickerControllerEditedImage] copy];
    originalImage = pickerImage;
    [photo setImage:pickerImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
