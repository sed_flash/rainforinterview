//
//  APEditProfileVC.m
//  Rain
//
//  Created by EgorMac on 28/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APEditProfileVC.h"

@interface APEditProfileVC ()

@end

@implementation APEditProfileVC {
    BOOL isBirthdayPicker;
    BOOL isCountryPicker;
    BOOL isCityPeaker;
}

@synthesize user;

static NSString *clientAlert = @"client";
static NSString *serverAlert = @"server";
static NSString *networkAlert = @"network";
static NSString *serverError = @"serverError";
static NSString *countriesAlert = @"countries";
static NSString *citiesAlert = @"cities";
static const NSString *birthdayPicker = @"birthdayPicker";
static const NSString *countryPicker = @"countryPicker";
static const NSString *cityPicker = @"cityPicker";
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.2;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const int COUNTRIES_PICKER_TAG = 1;
static const int CITIES_PICKER_TAG = 2;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setHidesBottomBarWhenPushed:YES];
    }
    return self;
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    if ([[UIScreen mainScreen] bounds].size.height <= 480.0f) {
        [firstNameYConstraint setConstant:7.0f];
        [firstNameHeightConstraint setConstant:30.0f];
        [lastNameTopSpaceContraint setConstant:7.0f];
        [nickNameConstraint setConstant:7.0f];
        [dateTopSpaceConstraint setConstant:7.0f];
        [countryTopSpaceConstraint setConstant:7.0f];
        [cityTopSpaceConstraint setConstant:7.0f];
        [sexButtonsTopSpaceConstraint setConstant:10.0f];
        [editPasswordButtonTopSpaceConstraint setConstant:10.0f];
    }
}

- (void)setCurrentUser:(NSDictionary *)newCurrentUser
{
    if (newCurrentUser != user) {
        user = newCurrentUser;
        NSLog(@"currentUser is:\n%@", user);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    API = [[APServerAPI alloc] init];
    chosenCountryID = 0;
    chosenCityID = 0;
    gender = 1;
    avatarChanged = NO;
    
    UIImageView *imgView = maleButton.imageView;
    [imgView setImage:[UIImage imageNamed:@"maleButtonOff"]];
    imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imgView setTintColor:[UIColor colorWithRed:93.0f/255.0f green:124.0f/255.0f blue:173.0f/255.0f alpha:1.0f]];
    [maleButton setImage:imgView.image forState:UIControlStateSelected];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledSuccess)
                                                 name:@"ProfileFilledSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filledFailed)
                                                 name:@"ProfileFilledFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesSuccess)
                                                 name:@"CountriesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(countriesFailed)
                                                 name:@"CountriesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesSuccess)
                                                 name:@"CitiesSuccess"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(citiesFailed)
                                                 name:@"CitiesFailed"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkError)
                                                 name:@"NetworkError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverError)
                                                 name:@"ServerWorkerDidEndWithError"
                                               object:API];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count] > 0) {
        countriesDownloaded = YES;
        [self setCurrentUserCountry];
        [self configureCountryPicker];
    }
    else
    {
        countriesDownloaded = NO;
        [API getCountries];
    }

    [self setupUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureNavigationController];
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)setCurrentUserCountry
{
    NSArray *countriesArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"];
    NSLog(@"countries array = \n%@", countriesArray);
    
    for (NSDictionary *country in countriesArray) {
        if ([[[user objectForKey:@"country_id"] stringValue] isEqualToString:[[country objectForKey:@"id"] stringValue]])
        {
            [API getCitiesForCountryID:[[user objectForKey:@"country_id"] stringValue]];
            NSString *countryTitle = [country objectForKey:@"title"];
            [countryTextField setText:countryTitle];
            break;
        }
    }
}

- (void)setCurrentUserCity
{
    NSArray *citiesArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"];
    NSLog(@"cities array = \n%@", citiesArray);
    
    for (NSDictionary *city in citiesArray) {
        if ([[[user objectForKey:@"city_id"] stringValue] isEqualToString:[[city objectForKey:@"id"] stringValue]])
        {
            NSString *cityTitle = [city objectForKey:@"title"];
            [cityTextField setText:cityTitle];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard notification

- (void)willShowKeyboard:(NSNotification *)notification {
    if (!dateToolbar.isHidden && ([firstNameTextField isFirstResponder] || [lastNameTextField isFirstResponder] || [nicknameTextField isFirstResponder])) {
        if (isBirthdayPicker) {
            [self hideDatePickerAndToolbar:nil];
        } else if (isCountryPicker) {
            [self hideCountryPickerAndToolbar:nil];
        } else if (isCityPeaker) {
            [self hideCityPickerAndToolbar:nil];
        }
    }
}

- (void)setActivePicker:(NSString *)pickerName {
    if ([birthdayPicker isEqualToString:pickerName]) {
        isBirthdayPicker = YES;
        isCountryPicker = NO;
        isCityPeaker = NO;
    } else if ([countryPicker isEqualToString:pickerName]) {
        isBirthdayPicker = NO;
        isCountryPicker = YES;
        isCityPeaker = NO;
    } else if ([cityPicker isEqualToString:pickerName]){
        isBirthdayPicker = NO;
        isCountryPicker = NO;
        isCityPeaker = YES;
    } else {
        isBirthdayPicker = NO;
        isCountryPicker = NO;
        isCityPeaker = NO;
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat viewHeight, bottomOfTextField, topOfKeyboard;
    
    viewHeight = viewRect.size.height;
    bottomOfTextField = textFieldRect.origin.y + textFieldRect.size.height;
    topOfKeyboard = viewHeight - PORTRAIT_KEYBOARD_HEIGHT - 10; // 10 point padding
    
    if (bottomOfTextField >= topOfKeyboard) keyboardAnimatedDistance = bottomOfTextField - topOfKeyboard;
    else keyboardAnimatedDistance = 0.0f;
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == dayTextField || textField == monthTextField || textField == yearTextField)
    {
        //add toolbar
        [self setActivePicker:birthdayPicker];
        [cityToolbar removeFromSuperview];
        [countryToolbar removeFromSuperview];
        
        dateToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                   self.view.frame.size.height,
                                                                   self.view.frame.size.width,
                                                                   40)];
        [dateToolbar setBarStyle:UIBarStyleDefault];
        [dateToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
        [dateToolbar setTranslucent:NO];
        
        UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                           style:UIBarButtonItemStyleDone target:nil
                                                                          action:@selector(hideDatePickerAndToolbar:)];
        [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                  NSForegroundColorAttributeName: [UIColor blackColor]}
                                      forState:UIControlStateNormal];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
        dateToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options:UIViewAnimationOptionTransitionNone
                         animations:^
         {
             [self.view addSubview:dateToolbar];
             CGRect frame = dateToolbar.frame;
             frame.origin.y = dateToolbar.frame.origin.y - 216.0f - frame.size.height + keyboardAnimatedDistance;
             dateToolbar.frame = frame;
         }
                         completion:^(BOOL finished)
         {
             
         }];
    }
    
    if (textField == countryTextField) {
        [self setActivePicker:countryPicker];
        [dateToolbar removeFromSuperview];
        [cityToolbar removeFromSuperview];
        if (!countriesDownloaded) {
            activityIndicator = [[UIActivityIndicatorView alloc] init];
            CGRect frame = CGRectMake(0, 0, 50, 50);
            [activityIndicator setFrame:frame];
            [activityIndicator setCenter:CGPointMake(self.view.center.x, self.view.center.y)];
            UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
            [activityIndicator setColor:customBlueColor];
            [[self view] addSubview:activityIndicator];
            [activityIndicator startAnimating];
            
        } else {
            countryToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                          self.view.frame.size.height,
                                                                          self.view.frame.size.width,
                                                                          40)];
            [countryToolbar setBarStyle:UIBarStyleDefault];
            [countryToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0
                                                            green:216.0/255.0
                                                             blue:219.0/255.0
                                                            alpha:1.0]];
            [countryToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCountryPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            countryToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:countryToolbar];
                 CGRect frame = countryToolbar.frame;
                 frame.origin.y = countryToolbar.frame.origin.y - 216.0f - frame.size.height + keyboardAnimatedDistance;
                 countryToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
    if (textField == cityTextField) {
        [self setActivePicker:cityPicker];
        [countryToolbar removeFromSuperview];
        [dateToolbar removeFromSuperview];
        if (!citiesDownloaded) {
            
            if ([[countryTextField text] isEqualToString:@""]) {
                [cityTextField resignFirstResponder];
                [self showAlertWithIdentifier:@"noCountryAlert"];
            } else {
                [textField resignFirstResponder];
                activityIndicator = [[UIActivityIndicatorView alloc] init];
                [activityIndicator setFrame:textField.frame];
                [activityIndicator setCenter:textField.center];
                UIColor *customBlueColor = [UIColor colorWithRed:(50.0/255.0) green:(93.0/255.0) blue:(120.0/255.0) alpha:1.0];
                [activityIndicator setColor:customBlueColor];
                [[self view] addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
            
        } else {
            cityToolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0,
                                                                       self.view.frame.size.height,
                                                                       self.view.frame.size.width,
                                                                       40)];
            [cityToolbar setBarStyle:UIBarStyleDefault];
            [cityToolbar setBarTintColor:[UIColor colorWithRed:206.0/255.0 green:216.0/255.0 blue:219.0/255.0 alpha:1.0]];
            [cityToolbar setTranslucent:NO];
            
            UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                                                               style:UIBarButtonItemStyleDone target:nil
                                                                              action:@selector(hideCityPickerAndToolbar:)];
            [doneButtonItem setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Arial" size:18.0],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}
                                          forState:UIControlStateNormal];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];
            cityToolbar.items = [NSArray arrayWithObjects:flex, doneButtonItem, nil];
            
            [UIView animateWithDuration:0.5
                                  delay:0.1
                                options:UIViewAnimationOptionTransitionNone
                             animations:^
             {
                 [self.view addSubview:cityToolbar];
                 CGRect frame = cityToolbar.frame;
                 frame.origin.y = self.view.frame.size.height - 216.0f - frame.size.height + keyboardAnimatedDistance;
                 cityToolbar.frame = frame;
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
    }
}

- (void)hideCountryPickerAndToolbar:(id)sender
{
    UIPickerView *picker = (UIPickerView *)countryTextField.inputView;
    if ([[countryTextField text] isEqualToString:@""] ||
        (chosenCountryID == 0)) {
        
        [countryTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:0] objectForKey:@"title"]];
        countryID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:0] objectForKey:@"id"] stringValue];
    }
    
    [countryTextField resignFirstResponder];
    [countryToolbar removeFromSuperview];
    countryToolbar = nil;
    
    [self updateCountryTextField:picker];
    [self setActivePicker:nil];
}

- (void)updateCountryTextField:(id)sender
{
    [API getCitiesForCountryID:countryID];
    chosenCountryID = [countryID integerValue];
    chosenCityID = 0;
    [cityTextField setText:@""];
    [cityTextField setInputView:nil];
    citiesDownloaded = NO;
}

- (void)hideCityPickerAndToolbar:(id)sender
{
    if ([[cityTextField text] isEqualToString:@""]) {
        [cityTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:0] objectForKey:@"title"]];
        cityID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:0] objectForKey:@"id"] stringValue];
    }
    
    [cityTextField resignFirstResponder];
    [cityToolbar removeFromSuperview];
    cityToolbar = nil;
    
    UIDatePicker *picker = (UIDatePicker *)cityTextField.inputView;
    [self updateCityTextField:picker];
    [self setActivePicker:nil];
}

- (void)updateCityTextField:(id)sender
{
    [cityTextField resignFirstResponder];
    chosenCityID = [cityID integerValue];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += keyboardAnimatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldIsEmpty
{
    BOOL result = NO;
    
    for (UIView *subview in [[self view] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *currentTextField = (UITextField *)subview;
            NSString *currentString = [currentTextField text];
            currentString = [currentString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            if ([currentString length] == 0) {
                result = YES;
            }
        }
    }
    
    return result;
}

#pragma mark - UIPickerView Datasource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG) {
        int countriesCount;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count] > 0) {
            countriesCount = (int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] count];
        } else {
            countriesCount = 0;
        }
        
        return countriesCount;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        int citiesCount;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] count] > 0) {
            citiesCount = (int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] count];
        } else {
            citiesCount = 0;
        }
        
        return citiesCount;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in numberOfRows method");
        return 0;
    }
}

#pragma mark - UIPickerView Delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSString *countryName = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"title"];
        return countryName;
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        NSString *cityName = [[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"title"];
        return cityName;
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
        return @"";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView tag] == COUNTRIES_PICKER_TAG)
    {
        NSLog(@"did select country");
        [countryTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"title"]];
        countryID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"countriesArray"] objectAtIndex:row] objectForKey:@"id"] stringValue];
    }
    else if ([pickerView tag] == CITIES_PICKER_TAG)
    {
        [cityTextField setText:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"title"]];
        cityID = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"citiesArray"] objectAtIndex:row] objectForKey:@"id"] stringValue];
    }
    else
    {
        NSLog(@"something wrong with pickerView tag in titleForRow method");
    }
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"The %@ button was tapped.", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    switch (buttonIndex) {
        case 0:
            [self removeCurrentPhoto];
            break;
            
        case 1:
            [self takePhoto];
            break;
            
        case 2:
            [self choosePhotoFromLibrary];
            break;
            
        default:
            break;
    }
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *pickerImage = [[info objectForKey:UIImagePickerControllerEditedImage] copy];
    
    originalAvatarImage = pickerImage;
    
//    [editPhotoImageView setImage:pickerImage];
//    UIImage *maskImage = [UIImage imageNamed:@"dropMask.png"];
//    UIImage *newPhotoImage = [self maskImage:editPhotoImageView.image withMask:maskImage];
//    [editPhotoImageView setImage:newPhotoImage];
//    [editPhotoImageView.layer setMasksToBounds:YES];
    
    [UIImageView apa_useMaskToImageView:editPhotoImageView
                               forImage:pickerImage];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    avatarChanged = YES;
}

#pragma mark - Helper Methods

- (void)configureNavigationController
{
    [[self navigationItem] setTitle:NSLocalizedString(@"editProfile", nil)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain target:nil
                                                                            action:nil];
    [[[self navigationController] navigationBar] setBackIndicatorImage:[UIImage imageNamed:@"Back Button Yellow"]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"Back Button Yellow"]];
    
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[[self navigationController] navigationBar] setTitleTextAttributes:@{ NSFontAttributeName:
                                                                               [UIFont fontWithName:@"Arial"size:16.0],
                                                                           NSForegroundColorAttributeName:
                                                                               [UIColor whiteColor]}];
}

- (void)configureTextFields
{
    [firstNameTextField setPlaceholder:NSLocalizedString(@"firstname", nil)];
    [firstNameTextField setText:[user objectForKey:@"first_name"]];
    [lastNameTextField setPlaceholder:NSLocalizedString(@"lastname", nil)];
    [lastNameTextField setText:[user objectForKey:@"last_name"]];
    [nicknameTextField setPlaceholder:NSLocalizedString(@"nickname", nil)];
    NSLog(@"user is is is %@", user);
    [nicknameTextField setText:[user objectForKey:@"nick"]];
    [dayTextField setPlaceholder:NSLocalizedString(@"day", nil)];
    [monthTextField setPlaceholder:NSLocalizedString(@"month", nil)];
    [yearTextField setPlaceholder:NSLocalizedString(@"year", nil)];
    [countryTextField setPlaceholder:NSLocalizedString(@"country", nil)];
    [cityTextField setPlaceholder:NSLocalizedString(@"city", nil)];
    
    for (UIView *subview in self.view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]] &&
            subview != dayTextField &&
            subview != monthTextField &&
            subview != yearTextField)
        {
            UIView *textFieldLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
            [(UITextField *)subview setLeftView:textFieldLeftView];
            [(UITextField *)subview setLeftViewMode:UITextFieldViewModeAlways];
        }
    }
}

- (void)configureAvatarImageView
{
    NSString *avatarString;
        if (![user objectForKey:@"avatar_small"])
        {
            
          if(![[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"])
          {
            
              [editPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
          }
          else
          {
              avatarString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
              [editPhotoImageView setImageWithURL:[NSURL URLWithString:avatarString]
                                 placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
              [UIImageView apa_useMaskToImageView:editPhotoImageView
                                         forImage:editPhotoImageView.image];
          }
            
        }
        else
        {
            if ([[user objectForKey:@"avatar_small"] isEqualToString:@""] ||
                [[user objectForKey:@"avatar_small"] isEqualToString:@"http://rain-app.com/upload/avatars/IOS_"])
            {
                NSLog(@"set placeholder avatar");
//                [editPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhoto"]];
                [UIImageView apa_useMaskToImageView:editPhotoImageView
                                           forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
            }
            else {
                // set avatar
                avatarString = [user objectForKey:@"avatar_small"];
                
                if(!avatarString)
                {
                    avatarString=[[NSUserDefaults standardUserDefaults]objectForKey:@"avatar_small"];
                }
                NSLog(@"set avatar from %@", avatarString);
                [editPhotoImageView setImageWithURL:[NSURL URLWithString:avatarString]
                                   placeholderImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                [UIImageView apa_useMaskToImageView:editPhotoImageView
                                           forImage:editPhotoImageView.image];
            }
        }
}

- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    
    UIImage *maskedImage = [UIImage imageWithCGImage:masked];
    CGImageRelease(mask);
    CGImageRelease(masked);
    
    return maskedImage;
}

- (void)setupUIElements
{
    [self configureNavigationController];
    [self configureDatePicker];
    [self configureCountryPicker];
    [self configureTextFields];
    [self configureAvatarImageView];
    
    [editPasswordButton setTitle:NSLocalizedString(@"changePassword", nil) forState:UIControlStateNormal];
    [editPasswordButton setTitle:NSLocalizedString(@"changePassword", nil) forState:UIControlStateHighlighted];
    [[editPasswordButton layer] setCornerRadius:3.0f];
    
    // get user's gender and set it
    if ([[[user objectForKey:@"gender"] stringValue] isEqualToString:@"1"]) {
        [maleButton setSelected:YES];
        gender = 1;
    } else {
        gender = 2;
        [maleButton setSelected:NO];
        [femaleButton setSelected:YES];
    }
    
}

- (void)configureDatePicker
{
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self
                   action:@selector(updateDateTextFields:)
         forControlEvents:UIControlEventValueChanged];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    NSLocale *ukLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    [currentCalendar setLocale:ukLocale];
    [datePicker setCalendar:currentCalendar];
    [datePicker setMaximumDate:[NSDate date]];
    [dayTextField setInputView:datePicker];
    [monthTextField setInputView:datePicker];
    [yearTextField setInputView:datePicker];
    
    NSString *birthdayString = [user objectForKey:@"birthday"];
    
    if (![birthdayString isEqualToString:@""])
    {
        NSLog(@"user - %@", user);
        NSLog(@"birthdaystring - %@", birthdayString);
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        //    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
        [dateFormat setDateFormat:@"dd.MM.yyyy"];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
        
        NSDate *currentDate = [dateFormat dateFromString:birthdayString];
        if(currentDate)
        {
            [datePicker setDate:currentDate];
            NSLog(@"current date = %@", currentDate);
        }
        [self updateDateTextFields:self];
    }
}

- (void)configureCountryPicker
{
    UIPickerView *countryPicker = [[UIPickerView alloc] init];
    [countryPicker setTag:COUNTRIES_PICKER_TAG];
    [countryPicker setDelegate:self];
    [countryTextField setInputView:countryPicker];
}

- (void)configureCitiesPicker
{
    NSLog(@"configureCitiesPicker");
    UIPickerView *citiesPicker = [[UIPickerView alloc] init];
    [citiesPicker setTag:CITIES_PICKER_TAG];
    [citiesPicker setDelegate:self];
    [cityTextField setInputView:citiesPicker];
}

- (void)updateDateTextFields:(id)sender
{
    UIDatePicker *picker = (UIDatePicker *)dayTextField.inputView;
    
    NSDateFormatter *dayDateFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *yearDateFormatter = [[NSDateFormatter alloc] init];
    [dayDateFormatter setDateFormat:@"dd"];
    [monthDateFormatter setDateFormat:@"MMMM"];
    [yearDateFormatter setDateFormat:@"yyyy"];
    NSDate *date = [picker date];
    [dayTextField setText:[NSString stringWithFormat:@"%@", [dayDateFormatter stringFromDate:date]]];
    [monthTextField setText:[NSString stringWithFormat:@"%@", [monthDateFormatter stringFromDate:date]]];
    [yearTextField setText:[NSString stringWithFormat:@"%@", [yearDateFormatter stringFromDate:date]]];
}

- (void)hideDatePickerAndToolbar:(id)sender
{
    [dayTextField resignFirstResponder];
    [monthTextField resignFirstResponder];
    [yearTextField resignFirstResponder];
    [dateToolbar removeFromSuperview];
    dateToolbar = nil;
    
    UIDatePicker *picker = (UIDatePicker *)dayTextField.inputView;
    [self updateDateTextFields:picker];
    [self setActivePicker:nil];
}

#pragma mark - Change Photo Methods

- (void)showCameraAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cameraIsUnavailable", nil)
                                                    message:NSLocalizedString(@"cameraIsUnavailablePleaseTryToPickPhotoFromYourPhotoLibraryInsteed", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (void)showPhotoLibraryAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"photoLibraryIsUnavailable", nil)
                                                    message:NSLocalizedString(@"photoLibraryIsUnavailablePleaseTryToTakeAPictureWithYourCameraInstead", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (void)configureCircleLayoutForImagePicker:(UIImagePickerController *)controller
{
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    UIView *plCropOverlay = [[[controller.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
    
    plCropOverlay.hidden = YES;
    CGFloat position = screenHeight/9.16;
    
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    
    UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:
                           CGRectMake(1.0f, position+4, self.view.frame.size.width - self.view.frame.size.width/106.67, self.view.frame.size.width)];

    [path2 setUsesEvenOddFillRule:YES];
    
    [circleLayer setPath:[path2 CGPath]];
    
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, position, self.view.frame.size.width, screenHeight-(screenHeight/7.89)-position-(screenHeight/284)) cornerRadius:0];
    
    [path appendPath:path2];
    [path setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor colorWithWhite:0.0 alpha:0.37].CGColor;
    fillLayer.opacity = 0.8;
    [controller.view.layer addSublayer:fillLayer];
}

- (void)removeCurrentPhoto
{
//    [editPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhoto"]];
    [UIImageView apa_useMaskToImageView:editPhotoImageView
                               forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
    
//    UIImage *maskImage = [UIImage imageNamed:@"dropMask.png"];
//    UIImage *newPhotoImage = [self maskImage:editPhotoImageView.image withMask:maskImage];
//    [editPhotoImageView setImage:newPhotoImage];
}

- (void)takePhoto
{
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [self configureCircleLayoutForImagePicker:imagePicker];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    } else {
        [self showCameraAlert];
    }
    
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([navigationController.viewControllers count] == 3)
    {
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        UIView *plCropOverlay = [[[viewController.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
        
        plCropOverlay.hidden = YES;
        CGFloat position = screenHeight/4.58;
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        
        UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:
                               CGRectMake(0.0f, position, self.view.frame.size.width, self.view.frame.size.width)];
        [path2 setUsesEvenOddFillRule:YES];
        
        [circleLayer setPath:[path2 CGPath]];
        
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.view.frame.size.width, screenHeight-72) cornerRadius:0];
        
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor blackColor].CGColor;
        fillLayer.opacity = 0.8;
        [viewController.view.layer addSublayer:fillLayer];
        
        UILabel *moveLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 50)];
        [moveLabel setText:@"Move and Scale"];
        [moveLabel setTextAlignment:NSTextAlignmentCenter];
        [moveLabel setTextColor:[UIColor whiteColor]];
        
        [viewController.view addSubview:moveLabel];
    }
}

- (void)choosePhotoFromLibrary
{
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [imagePicker setDelegate:self];
        [imagePicker setAllowsEditing:YES];
        [imagePicker allowsEditing];
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    }
    else
    {
        [self showPhotoLibraryAlert];
    }
}

- (void)filledSuccess
{
    [self removeActivityIndicator];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)filledFailed
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverAlert];
}

- (void)countriesSuccess
{
    countriesDownloaded = YES;
    if ([[countryTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[countryTextField inputView] stopAnimating];
        [self configureCountryPicker];
    }
    if ([user objectForKey:@"country_id"]) {
        [self setCurrentUserCountry];
    }
}

- (void)citiesSuccess
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    
    NSLog(@"citiesSuccess method");
    citiesDownloaded = YES;
    if ([[cityTextField inputView] isKindOfClass:[UIActivityIndicatorView class]]) {
        [(UIActivityIndicatorView *)[cityTextField inputView] stopAnimating];
    }
    [self configureCitiesPicker];
    [self setCurrentUserCity];
}

- (void)countriesFailed
{
    [countryTextField resignFirstResponder];
    [self showAlertWithIdentifier:countriesAlert];
}

- (void)citiesFailed
{
    [self showAlertWithIdentifier:citiesAlert];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}

- (void)networkError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:networkAlert];
}

- (void)serverError
{
    [self removeActivityIndicator];
    [self showAlertWithIdentifier:serverError];
}

- (void)showAlertWithIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:clientAlert]) {
        // show wrong filled textfields alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:NSLocalizedString(@"someOfFieldsAreFilledWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverAlert])
    {
        NSString *errorMessage = @"";
        
        for (NSString *currentMessage in [[NSUserDefaults standardUserDefaults] objectForKey:@"errorMessage"])
        {
            errorMessage = [errorMessage stringByAppendingFormat:@"\n%@", currentMessage];
        }
        
        // show registration failed alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"wrongFilledForm", nil)
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:networkAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"networkconnectionlost", nil)
                                                        message:NSLocalizedString(@"connectionWithInternetIsLost", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([identifier isEqualToString:serverError] ||
             [identifier isEqualToString:countriesAlert] ||
             [identifier isEqualToString:citiesAlert])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"serverError", nil)
                                                        message:NSLocalizedString(@"somethingWentWrong", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else if ([identifier isEqualToString:@"noCountryAlert"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"chooseTheCountry", nil)
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil, nil];
        [alert show];
    }

}

#pragma mark - IBAction Methods

- (IBAction)editPhoto:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"changeProfilePicture", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"removeCurrentPhoto", nil)
                                                    otherButtonTitles:NSLocalizedString(@"takePhoto", nil),
                                                                      NSLocalizedString(@"chooseFromLibrary", nil), nil];
    [actionSheet showFromTabBar:[[self tabBarController] tabBar]];
}

- (IBAction)maleButtonPress:(id)sender
{
    if (maleButton.selected) {
        [maleButton setSelected:NO];
        gender = 2;
    } else {
        [maleButton setSelected:YES];
        [femaleButton setSelected:NO];
        gender = 1;
    }
}

- (IBAction)femaleButtonPress:(id)sender
{
    if (femaleButton.selected) {
        [femaleButton setSelected:NO];
        gender = 1;
    } else {
        [femaleButton setSelected:YES];
        [maleButton setSelected:NO];
        gender = 2;
    }
}

- (IBAction)saveChanges:(id)sender
{
    if ([self textFieldIsEmpty]) {
        [self showAlertWithIdentifier:clientAlert];
    } else {
        [self showActivityIndicator];
        NSString *firstName = [firstNameTextField text];
        NSString *lastName = [lastNameTextField text];
        NSString *nickname = [nicknameTextField text];
        
        NSString *birthday = [NSString stringWithFormat:@"%@ %@ %@", [dayTextField text], [monthTextField text], [yearTextField text]];
        
        NSLog(@"birthday in EditProfileVC - \n%@", birthday);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MM yyyy"];
        NSDate *birthDate = [dateFormatter dateFromString:birthday];
        NSLog(@"birthDate is %@", birthDate);
        
        NSLog(@"%@", [NSLocale availableLocaleIdentifiers]);
        
        NSDateFormatter *enDateFormatter = [[NSDateFormatter alloc] init];
        [enDateFormatter setDateFormat:@"dd MMMM yyyy"];
        [enDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
        birthday = [enDateFormatter stringFromDate:birthDate];
        
        NSLog(@"birthday new string is %@", birthday);
        
        NSMutableDictionary *filledProfile = [[NSMutableDictionary alloc] init];
        [filledProfile setObject:firstName forKey:@"firstName"];
        [filledProfile setObject:lastName forKey:@"lastName"];
        [filledProfile setObject:nickname forKey:@"nickname"];
        [filledProfile setObject:birthday forKey:@"birthday"];
        [filledProfile setObject:[NSNumber numberWithInt:(int)gender] forKey:@"gender"];
        
        if (chosenCountryID == 0) {
            if ([user objectForKey:@"country_id"]) {
                chosenCountryID = [[user objectForKey:@"country_id"] integerValue];
            }
        }
        
        if (chosenCityID == 0) {
            if ([user objectForKey:@"city_id"]) {
                chosenCityID = [[user objectForKey:@"city_id"] integerValue];
            }
        }
        
        [filledProfile setObject:[NSNumber numberWithInt:(int)chosenCountryID] forKey:@"countryID"];
        [filledProfile setObject:[NSNumber numberWithInt:(int)chosenCityID] forKey:@"cityID"];
        
        if (avatarChanged == YES) {
            if (![[user valueForKey:@"avatar"] isEqualToString:@""]) {
                
                if([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
                    
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.view.frame.size.width, self.view.frame.size.width), NO, [UIScreen mainScreen].scale);
                else
                    UIGraphicsBeginImageContext(self.view.frame.size);
                
                if (originalAvatarImage == nil) {
                    originalAvatarImage = editPhotoImageView.image;
                }
                
//                UIImage *maskImage = [UIImage imageNamed:@"dropMask.png"];
//                UIImage *newPhotoImage = [self maskImage:originalAvatarImage withMask:maskImage];
//                UIImage *newPhotoImage = maskImage;
//                [hiddenImageView setImage:newPhotoImage];
                [UIImageView apa_useMaskToImageView:hiddenImageView
                                           forImage:originalAvatarImage];
//                [hiddenImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
                
//                UIImage *imageForExport = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                //    If we need to save avatar to photo library
                //    UIImageWriteToSavedPhotosAlbum(imageForExport, self, nil, nil);
                
                // Previous developers using "imageForExport" for initWithData parameter
                NSData *imageData = [[NSData alloc] initWithData:UIImagePNGRepresentation(hiddenImageView.image)];
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"userPhoto"];
            }
        } else {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userPhoto"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userPhoto"];
        }
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:filledProfile forKey:@"filledProfileData"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"filled profile in EditProfileVC: \n%@", filledProfile);
        
        NSString *email = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"email"];
        NSString *password = [[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] objectForKey:@"password"];
        
        [API editProfileWithEmail:email andPassword:password];
    }
}

- (void)showActivityIndicator
{
    if (activityButton == nil)
    {
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [activityIndicator setColor:[UIColor whiteColor]];
        activityButton = [[UIBarButtonItem alloc]initWithCustomView:activityIndicator];
    }
    self.navigationItem.rightBarButtonItem = activityButton;
    [activityIndicator startAnimating];
}

- (void)removeActivityIndicator
{
    [activityIndicator stopAnimating];
    self.navigationItem.rightBarButtonItem = saveButton;
}

//- (void)useMaskToImageView:(UIImageView *)imageView forImage:(UIImage *)image {
//    CALayer *mask = [CALayer layer];
//    mask.contents = (id)[[UIImage imageNamed:@"mask_for_avatar"] CGImage];
//    mask.frame = CGRectMake(0, 0, CGRectGetWidth(imageView.frame), CGRectGetHeight(imageView.frame));
//    
//    imageView.layer.mask = mask;
//    imageView.layer.masksToBounds = YES;
//    imageView.image = image;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
