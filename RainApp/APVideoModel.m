//
//  APVideoModel.m
//  Rain
//
//  Created by Vlad on 09.07.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "APVideoModel.h"

#define TIME_TO_REMOVE_VIDEO 604800

@implementation APVideoModel

- (instancetype)init {
    self = [super init];
    if (self) {}
    
    return self;
}

- (BOOL)checkIfVideoIsHasOnDevice:(NSString *)path {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    [self deleteOldVideosForPath:paths[0]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:path]) {
        return YES;
    }
    
    return NO;
}

- (NSString *)getPathToFileWithURL:(NSURL *)url {
    NSString *str = [url absoluteString];
    NSArray *separator = [str componentsSeparatedByString:@"/"];
    
    if (separator.count > 0) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:separator[separator.count - 1]];
        
        return path;
    }
    
    return @"";
}

- (void)saveVideoToDeviceWithURL:(NSURL *)videoURL andPathToSave:(NSString *)path {
    NSURLRequest *request = [NSURLRequest requestWithURL:videoURL];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file to %@", path);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
}

// Deleting video older than 7 days
// <param name="path"> Path to the root directory </param>
- (void)deleteOldVideosForPath:(NSString *)path {
    int count;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:path error:NULL];
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        NSString *pathToFile = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", [directoryContent objectAtIndex:count]]];
        NSDictionary* attrs = [fileManager attributesOfItemAtPath:path error:nil];
        if (attrs != nil) {
            NSDate *date = (NSDate*)[attrs objectForKey: NSFileCreationDate];
            NSDate *currentDate = [NSDate date];
            
            NSLog(@"File %d: %@", (count + 1), [directoryContent objectAtIndex:count]);
            NSLog(@"Date Created: %@", [date description]);
            
            NSTimeInterval timestamp = [currentDate timeIntervalSinceDate:date];
            if (fabsf(timestamp) > TIME_TO_REMOVE_VIDEO) {
                NSError *error = nil;
                [fileManager removeItemAtPath:pathToFile error:&error];
                
                if (!error) {
                    NSLog(@"Deleting file to path: %@\n succedded", pathToFile);
                } else {
                    NSLog(@"File was not deleted");
                }
            }
            
        }
        else {
            NSLog(@"Files not found for current directory: %@", path);
        }
    }
}

@end
