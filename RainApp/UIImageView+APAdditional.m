//
//  UIImageView+APAdditional.m
//  Rain
//
//  Created by Vlad on 19.06.16.
//  Copyright © 2016 AntsPro. All rights reserved.
//

#import "UIImageView+APAdditional.h"

@implementation UIImageView (APAdditional)

+ (void)apa_useMaskToImageView:(UIImageView *)imageView forImage:(UIImage *)image {
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[[UIImage imageNamed:@"mask_for_avatar"] CGImage];
    mask.frame = CGRectMake(0, 0, CGRectGetWidth(imageView.frame), CGRectGetHeight(imageView.frame));
    
    imageView.layer.mask = mask;
    imageView.layer.masksToBounds = YES;
    imageView.image = image;
}

@end
