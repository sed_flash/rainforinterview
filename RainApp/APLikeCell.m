//
//  APLikeCell.m
//  Rain
//
//  Created by EgorMac on 10/10/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import "APLikeCell.h"

@implementation APLikeCell
@synthesize notification, userButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNotification:(NSDictionary *)newNotification
{
    if (notification != newNotification) {
        notification = newNotification;
        
//        [photoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [UIImageView apa_useMaskToImageView:photoImageView
                                   forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
        [nameLabel setText:[[notification objectForKey:@"user"] objectForKey:@"nick"]];
        
        if ([[notification objectForKey:@"type"] isEqualToString:@"like"]) {
            [descriptionLabel setText:NSLocalizedString(@"likedYourPost", nil)];
        }
        else if ([[notification objectForKey:@"type"] isEqualToString:@"repost"])
        {
            [descriptionLabel setText:NSLocalizedString(@"repostedYourPost", nil)];
        }
        
        [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
        
        NSDate *createdAt;
        
        if ([[notification objectForKey:@"created_at"] isKindOfClass:[NSString class]]) {
            NSString *createdAtString = [notification objectForKey:@"created_at"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Almaty"]];
            createdAt = [dateFormatter dateFromString:createdAtString];
        } else {
            createdAt = [notification objectForKey:@"created_at"];
        }
        NSString *timeAgo = [createdAt timeAgo];
        
        [timeAgoLabel setText:timeAgo];
        
        NSString *photoString = [[notification objectForKey:@"item"] objectForKey:@"preview_image"];
        NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
        
        __weak __typeof(self)weakSelf = self;
        __weak NSString *weakPhotoString = photoString;
        __weak UIImageView *weakPhotoImageView = likedPhotoView;
        
        [likedPhotoView setImageWithURLRequest:photoRequest
                              placeholderImage:nil
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             NSLog(@"success for %@", weakPhotoString);
//             [weakPhotoImageView setImage:image];
             [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                        forImage:image];
             [weakSelf setAvatarImage];
         }
         
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                           NSLog(@"fail for %@, error: %@", weakPhotoString, error);
                                           [weakSelf setAvatarImage];
                                       }];

        
        NSLog(@"notification - \n%@", notification);
    }
}

- (void)setAvatarImage
{
    NSString *photoString = [[notification objectForKey:@"user"] objectForKey:@"avatar_small"];
    
    NSURLRequest *photoRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photoString]];
    
    __weak NSString *weakPhotoString = photoString;
    __weak UIImageView *weakPhotoImageView = photoImageView;
    
    [photoImageView setImageWithURLRequest:photoRequest
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         NSLog(@"success for %@", weakPhotoString);
//         [weakPhotoImageView setImage:image];
         [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                    forImage:image];
     }
     
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                       NSLog(@"fail for %@, error: %@", weakPhotoString, error);
//                                       [weakPhotoImageView setImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                       [UIImageView apa_useMaskToImageView:weakPhotoImageView
                                                                  forImage:[UIImage imageNamed:@"ProfileEditPhotoBlue"]];
                                   }];

}

@end
