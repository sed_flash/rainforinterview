//
//  APAccesCell.h
//  Rain
//
//  Created by EgorMac on 21/06/14.
//  Copyright (c) 2014 AntsPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APServerAPI.h"

@interface APAccesCell : UITableViewCell
{
    IBOutlet UILabel *accessLabel;
    IBOutlet UISwitch *accessSwitch;
    APServerAPI *API;
}

- (IBAction)switchAccess:(id)sender;

@end
